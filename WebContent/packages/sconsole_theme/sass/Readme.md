# sconsole_theme/sass

This folder contains SASS files of various kinds, organized in sub-folders:

    sconsole_theme/sass/etc
    sconsole_theme/sass/src
    sconsole_theme/sass/var
