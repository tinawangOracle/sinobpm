Ext.define('MyApp.MyPanel', {
     extend      : 'Ext.Panel',
     width       : 200,
     height      : 150,
     bodyPadding : 5
 });





 Ext.application({
     name   : 'ItemSelector',
     
     requires : ['Ext.form.Panel',
                 'Ext.ux.form.MultiSelect',
                 'Ext.ux.form.ItemSelector',
                 'Ext.tip.QuickTipManager',
                 'Ext.util.Point'],
                 
     launch : function() {
    	 
    	 Ext.tip.QuickTipManager.init();
    	 function createDockedItems(fieldId) {
    	        return [{
    	            xtype: 'toolbar',
    	            dock: 'bottom',
    	            ui: 'footer',
    	            defaults: {
    	                minWidth: 75
    	            },
    	            items: ['->', {
    	                text: 'Clear',
    	                handler: function(){
    	                    var field = Ext.getCmp(fieldId);
    	                    if (!field.disabled) {
    	                        field.clearValue();
    	                    }
    	                }
    	            }, {
    	                text: 'Reset',
    	                handler: function() {
    	                	ds.each(function(record){
    	               		 select.push(record.get('id'));
    	               	 });
    	               	 itsl.queryById('itemselector-field').setValue(select);
    	                }
    	            }, {
    	                text: 'Save',
    	                handler: function(){
    	                    var form = Ext.getCmp(fieldId).up('form').getForm();
    	                    form.getValues(true);
    	                    if (form.isValid()){
    	                        Ext.Msg.alert('Submitted Values', 'The following will be sent to the server: <br />'+
    	                            form.getValues(true));
    	                    }
    	                }
    	            }]
    	        }];
    	 }
    	 
    	 
    	 

    	 var allGroup = Ext.create('Ext.data.ArrayStore', {
 			autoLoad : false,
			proxy : {
				type : 'ajax',
				api : {
					read : "admin/getGroups.action"
				},
				timeout : 0,
				reader : {
					type : 'json',
					totalProperty : "totalCount",
					root : "records",
					idProperty : 'id',
					messageProperty: 'userId'
				}
			},
			fields : [ 'id', 'name', 'type'],
			sortInfo: {
	            field: 'name',
	            direction: 'ASC'
	        }
		});
    	 
    	 var userGroup = Ext.create('Ext.data.ArrayStore', {
    			autoLoad : false,
    			proxy : {
    				type : 'ajax',
    				api : {
    					read : "admin/getGroupsByUserId.action"
    				},
    				timeout : 0,
    				reader : {
    					type : 'json',
    					totalProperty : "totalCount",
    					root : "records",
    					idProperty : 'id',
    					messageProperty: 'userId'
    				}
    			},
    			fields : [ 'id', 'name', 'type'],
    			sortInfo: {
    	            field: 'name',
    	            direction: 'ASC'
    	        }
    		});
    	
    	allGroup.load();
    	userGroup.getProxy().setExtraParam('userId', 'admin');
    	userGroup.load();
    	//alert(userGroup.getTotalCount());
    	 
//    	 var ds = Ext.create('Ext.data.ArrayStore', {
//    	        fields: ['id','name'],
//    	        data: [['admin','adminname'],['student','studentname']],
//    	        sortInfo: {
//    	            field: 'value',
//    	            direction: 'ASC'
//    	        }
//    	    });
    	
    	var ds = Ext.create('Ext.data.ArrayStore', {
	        fields: ['id'],
	        data: [['admin'],['student']],
	    });
    	 
    	 //alert(ds.getTotalCount());
    	 
    	 var itsl = Ext.create('Ext.form.Panel', {
    	        title: 'Add or Remove Groups for User',
    	        width: 700,
    	        bodyPadding: 10,
    	        height: 300,
    	        renderTo: Ext.getBody(),
    	        layout: 'fit',
    	        items:[{
    	            xtype: 'itemselector',
    	            name: 'itemselector',
    	            id: 'itemselector-field',
    	            anchor: '100%',
    	            fieldLabel: 'Group',
    	            //imagePath: 'ux/images/',
    	            store: allGroup,
    	            displayField: 'name',
    	            valueField: 'id',
    	            allowBlank: false,
    	            msgTarget: 'side',
    	            fromTitle: 'Available Group',
    	            toTitle: 'Selected Group'
    	        }],
    	        dockedItems: createDockedItems('itemselector-field'),
    	    });
    	 
    	 var select = [];
    	 //itsl.queryById('itemselector-field').setValue(['admin','student']);
    	 ds.each(function(record){
    		 select.push(record.get('id'));
    	 });
    	 
    	 
    	 itsl.queryById('itemselector-field').setValue(select);
     }
 });