/*
    This file is generated and updated by Sencha Cmd. You can edit this file as
    needed for your application, but these edits will have to be merged by
    Sencha Cmd when upgrading.
 */

Ext
		.application({
			name : 'sconsole',
			controllers : [ 'Login', 'Menu', 'TranslationManager', 'mail.Mail',
					'user', 'workflow', 'admin', 'tasks' ],
			requires : [ 'Ext.util.Grouper', 'Ext.window.MessageBox',
					'Ext.FocusManager', 'Ext.menu.Menu', 'Ext.form.Panel',
					'Ext.layout.container.Accordion', 'Ext.form.Label',
					'Ext.form.FieldSet', 'Ext.form.field.Hidden',
					'Ext.data.proxy.Ajax', 'Ext.form.field.ComboBox',
					'Ext.form.field.File', 'Ext.grid.plugin.CellEditing',
					'Ext.chart.series.Pie', 'Ext.chart.series.Column',
					'Ext.chart.axis.Numeric', 'Ext.chart.axis.Category',
					'Ext.ux.grid.FiltersFeature', 'Ext.grid.column.Date',
					'Ext.grid.column.Action', 'Ext.grid.column.CheckColumn',
					'Ext.layout.container.Column', 'Ext.form.CheckboxGroup',
					'Ext.grid.column.RowNumberer', 'Ext.selection.RowModel',
					'Ext.grid.feature.Grouping', 'Ext.container.Viewport',
					'Ext.layout.container.Border', 'Ext.ux.layout.Center',
					'Ext.util.Cookies', 'Ext.form.Panel',
					'Ext.ux.TabCloseMenu', 'sconsole.util.CustomDateTimeField',
					'sconsole.util.DateTimeFieldCustomBtn',
					'Ext.ux.form.MultiSelect', 'Ext.ux.form.ItemSelector',
					'Ext.tip.QuickTipManager', 'Ext.util.Point' ],
			splashscreen : {},

			// autoCreateViewport: true,

			init : function() {

				// Start the mask on the body and get a reference to the mask
				splashscreen = Ext.getBody().mask(translations.loadingApp,
						'splashscreen');

				// Add a new class to this mask as we want it to look different
				// from the default.
				splashscreen.addCls('splashscreen');

				// Insert a new div before the loading icon where we can place
				// our logo.
				Ext.DomHelper.insertFirst(Ext.query('.x-mask-msg')[0], {
					cls : 'x-splash-icon'
				});

				// console.log('init');
			},
			launch : function() {

				Ext.tip.QuickTipManager.init();

				Ext
						.apply(
								Ext.form.field.VTypes,
								{
									daterange : function(val, field) {
										var date = field.parseDate(val);

										if (!date) {
											return false;
										}
										if (field.startDateField
												&& (!this.dateRangeMax || (date
														.getTime() != this.dateRangeMax
														.getTime()))) {
											var start = field.up('form').down(
													'#' + field.startDateField);
											start.setMaxValue(date);
											start.validate();
											this.dateRangeMax = date;
										} else if (field.endDateField
												&& (!this.dateRangeMin || (date
														.getTime() != this.dateRangeMin
														.getTime()))) {
											var end = field.up('form').down(
													'#' + field.endDateField);
											end.setMinValue(date);
											end.validate();
											this.dateRangeMin = date;
										}
										/*
										 * Always return true since we're only
										 * using this vtype to set the min/max
										 * allowed values (these are tested for
										 * after the vtype test)
										 */
										return true;
									},

									daterangeText : 'Start date must be less than end date',

									password : function(val, field) {
										if (field.initialPassField) {
											var pwd = field
													.up('form')
													.down(
															'#'
																	+ field.initialPassField);
											return (val == pwd.getValue());
										}
										return true;
									},

									passwordText : 'Passwords do not match'
								});
				var task = new Ext.util.DelayedTask(
						function() {

							// Fade out the body mask
							splashscreen.fadeOut({
								duration : 1000,
								remove : true
							});

							// Fade out the icon and message
							splashscreen
									.next()
									.fadeOut(
											{
												duration : 1000,
												remove : true,
												listeners : {
													afteranimate : function(el,
															startTime, eOpts) {
														this.viewport = Ext
																.create(
																		"Ext.container.Viewport",
																		{
																			renderTo : Ext
																					.getBody(),
																			layout : "card",
																			height : 500,
																			autoScroll : true,
																			id : 'appViewport',
																			items : [],
																			listeners : {
																				beforerender : function() {

																					var appViewport = Ext.ComponentQuery
																							.query("#appViewport")[0];
																					Ext.Ajax
																							.request({
																								url : 'login/inSession.action',
																								success : function(
																										conn,
																										response,
																										options,
																										eOpts) {
																									var result = sconsole.util.Util
																											.decodeJSON(conn.responseText);
																									var _card = appViewport
																											.getLayout();
																									if (result.success == "1") {
																										var _child = appViewport
																												.getComponent("homescreen");
																										if (Ext
																												.isEmpty(_child)) {
																											appViewport
																													.add({
																														xtype : "mainviewport",
																														id : "homescreen"
																													});
																											_child = appViewport
																													.getComponent("homescreen");
																										}
																										_card
																												.setActiveItem(_child);

																										sconsole.util.Util
																												.changeMenu(
																														'officeGloBtn',
																														2);
																										sconsole.util.SessionMonitor
																												.start();

																									} else {
																										var _child = appViewport
																												.getComponent("loginscreen");
																										if (Ext
																												.isEmpty(_child)) {
																											appViewport
																													.add({
																														xtype : "login",
																														id : "loginscreen"
																													});
																											_child = appViewport
																													.getComponent("loginscreen");
																										}
																										_card
																												.setActiveItem(_child);

																									}

																								},
																								failure : function(
																										conn,
																										response,
																										options,
																										eOpts) {
																									appViewport
																											.add({
																												xtype : "login",
																												id : "loginscreen"
																											});
																								}
																							});
																				},
																				afterrender : function() {

																				}
																			}

																		});
													}
												}
											});

							// Ext.widget('mainviewport');
							// Ext.widget('login');

							// console.log('launch');
						});

				task.delay(2000);

			}

		});
