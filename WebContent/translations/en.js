translations = {
	consoleTitle : '<h1>BPM Console</h1>',
	logoTitle : '<span class="university-label"> | <b>BPM System</b></span>',
	homePage:'Home',
	login : "Please Login",
	user : "User",
	password : "Password",

	cancel : "Cancel",
	submit : "Submit",
	logout : 'Logout',
	signIn : 'Sign In',
	sessionStatus : 'Session Status',

	capsLockTitle : 'Caps Lock is On',
	capsLockMsg1 : 'Having Caps Lock on may cause you to enter your password',
	capsLockMsg2 : 'incorrectly.',
	capsLockMsg3 : 'You should press Caps Lock to turn it off before entering',
	capsLockMsg4 : 'your password.',

	// Menus
	menu1 : 'Security',
	menu11 : 'Groups and Permissions',
	menu12 : 'Users',
	menu13 : 'Profile',

	staticData : 'Static Data',
	actors : 'Actors',
	categories : 'Categories',
	languages : 'Languages',
	cities : 'Cities',
	countries : 'Countries',
	cms : 'Content Management',
	films : 'Films',
	reports : 'Reports',
	salesfilmcategory : 'Sales by Film Category',
	mailclient : 'MailClient',
	mail : 'Mail',

	loadingApp : 'Loading application',
	copyright : '<p>© 2014 SinaEarth. All rights reserved.</p>',
	loginMsg1 : '<h3>No Account Yet?</h3>',
	loginMsg2 : 'Have an Account?',
	loginMsg3 : 'If you already have a password, please ',
	registerTitle : 'Register',
	today : 'Today ',
	userEmail : 'Login Email',
	phonenumber : 'Cell Phone',
	idcard : 'ID Card',
	chineseName : 'Chinese Name',
	confirmPwd : 'Confirm Password',
	userpwd : 'Password',
	registerBtn : 'Register',
	resetBtn : 'Reset',

	globalmenu_admin : 'System Administration',
	globalmenu_office : 'Personal Office',
	gloalmenu_workflow : 'Workflow Management',
	globalmenu_user : 'Personel Management',

	wfModelMenu : 'Workflow Models',
	wfProcsMenu : 'Deployed Workflows',
	wfMyInstsMenu : 'My Instances',
	adminUserMenu : 'User Administration',
	adminGroupMenu : 'Group Administration',

	todoJobsMenu : 'todo',
	doneJobsMenu : 'done',
	
	
	JOB_HEADER_EXECUTION :'Job execution',
	ADMIN_DEFINITIONS : 'Process Definitions',
	PROCESS_INSTANCES: 'Process instances'
};