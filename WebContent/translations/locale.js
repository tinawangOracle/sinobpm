var lang = localStorage ? (localStorage.getItem('user-lang') || 'zh_CN') : 'zh_CN';
var file = 'translations/' + lang + '.js';
var extjsFile = 'ext/locale/ext-lang-' + lang + '.js';

document.write('<script type="text/javascript" src="' + file + '"></script>');
document.write('<script type="text/javascript" src="' + extjsFile + '"></script>');