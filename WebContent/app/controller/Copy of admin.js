Ext
		.define(
				'sconsole.controller.admin',
				{
					extend : 'Ext.app.Controller',
					requires : [ 'sconsole.util.MD5' ],
					refs : [ {
						ref : 'adminUserGrid',
						selector : 'adminUsers #adminUserGrid'
					}, {
						ref : 'adminGroupGrid',
						selector : 'adminGroups #adminGroupGrid'
					}, {
						ref : 'adminUserGroupGrid',
						selector : 'userGroups #adminUserGroupGrid'
					}, {
						ref : 'adminGroupUserGrid',
						selector : 'groupUsers #adminGroupUserGrid'
					}, {
						ref : 'adminDeployGrid',
						selector : 'adminDeployments #adminDeployGrid'
					}, {
						ref : "deployResourcesGrid",
						selector : "adminDeployments #deployResourcesGrid"
					}, {
						ref : "deploymentProcsGrid",
						selector : "adminDeployments #deploymentProcsGrid"
					}, {
						ref : 'deploymentForm',
						selector : 'adminDeployments #deploymentForm'
					} ],
					views : [ 'admin.newUser', 'admin.modifyUser',
							'admin.newGroup', 'admin.modifyGroup',
							'admin.groupUsers', 'admin.addGroups',
							'admin.userGroups', 'admin.addUsers',
							'admin.deployments' ],
					stores : [ 'admin.users', 'admin.groups',
							'admin.groupUsers', 'admin.userGroups',
							'admin.userNoGroups', 'admin.groupNoUsers',
							'admin.deployments', 'admin.deploymentResources',
							'admin.deploymentProcs' ],
					init : function(application) {
						this
								.control({
									// "groupUsers #addBtn" : {
									// click : function(){
									// //win =
									// Ext.create('sconsole.view.admin.addGroups');
									// //win.show();
									// }
									// },
									"userGroups #addBtn" : {
										click : function(thisobj) {
											_win = thisobj.up("grid");
											_win = _win.up("userGroups");
											// alert(_win.userId);

											// _g =
											// Ext.ComponentQuery.query('userGroups')[0];
											win = Ext
													.create('sconsole.view.admin.addGroups');
											// alert(win);
											win.userId = _win.userId;
											win.queryById('addGroupGrid')
													.getStore().getProxy()
													.setExtraParam('userId',
															win.userId);
											win.queryById('addGroupGrid')
													.getStore().load();
											win.show();

										}
									},
									"groupUsers #addBtn" : {
										click : function(thisobj) {
											_win = thisobj.up("grid");
											_win = _win.up("groupUsers");
											// alert(_win.userId);

											// _g =
											// Ext.ComponentQuery.query('userGroups')[0];
											win = Ext
													.create('sconsole.view.admin.addUsers');
											// alert(win);
											win.groupId = _win.groupId;
											win.queryById('addUserGrid')
													.getStore().getProxy()
													.setExtraParam('groupId',
															win.groupId);
											win.queryById('addUserGrid')
													.getStore().load();
											win.show();

										}
									},
									"userGroups #removeBtn" : {
										click : function(thisobj) {
											_g = this.getAdminUserGroupGrid();
											var _chosens = _g
													.getSelectionModel()
													.getSelection();
											if (Ext.isEmpty(_chosens)) {
												Ext.Msg
														.alert('Waring',
																'Please select a row at first.');
												return;
											} else {

												var groupIds = new Array();
												var names = new Array();
												_chosens.forEach(function(
														element, index, array) {
													groupIds.push(element
															.get('id'));
													names.push(element
															.get('name'));
												});

												Ext.MessageBox
														.confirm(
																'Remove Group',
																'Are you sure to remove Group ?',
																function(bId,
																		text,
																		opt) {
																	if (bId == 'yes') {
																		Ext.Ajax
																				.request({
																					url : 'admin/removeGroup.action',
																					params : {
																						groupIds : groupIds,
																						userId : _g
																								.up("window").userId,
																						names : names
																					},
																					success : function(
																							response,
																							opts) {
																						try {
																							var c = Ext.JSON
																									.decode(response.responseText);
																						} catch (err) {
																							_g
																									.getStore()
																									.load();
																							return;
																						}
																						if (c.success == 'false') {
																							sconsole.util.Util
																									.showErrorMsg(c.msg);
																							return;
																						} else {
																							Ext.Msg
																									.alert(
																											'Status',
																											c.msg);
																							_g
																									.getStore()
																									.load();
																						}
																					},
																					failure : function(
																							response,
																							opts) {
																						var c = Ext.JSON
																								.decode(response.responseText);
																						sconsole.util.Util
																								.showErrorMsg(c.msg);
																					}
																				});
																	}
																});
											}
										}
									},
									"groupUsers #removeBtn" : {
										click : function(thisobj) {
											_g = this.getAdminGroupUserGrid();
											var _chosens = _g
													.getSelectionModel()
													.getSelection();
											if (Ext.isEmpty(_chosens)) {
												Ext.Msg
														.alert('Waring',
																'Please select a row at first.');
												return;
											} else {

												var userIds = new Array();
												var emails = new Array();
												_chosens.forEach(function(
														element, index, array) {
													userIds.push(element
															.get('id'));
													emails.push(element
															.get('email'));
												});

												Ext.MessageBox
														.confirm(
																'Remove Users',
																'Are you sure to remove chosen User(s) from the group ?',
																function(bId,
																		text,
																		opt) {
																	if (bId == 'yes') {
																		Ext.Ajax
																				.request({
																					url : 'admin/removeUser.action',
																					params : {
																						groupId : _g
																								.up("window").groupId,
																						userIds : userIds,
																						emails : emails
																					},
																					success : function(
																							response,
																							opts) {
																						try {
																							var c = Ext.JSON
																									.decode(response.responseText);
																						} catch (err) {
																							_g
																									.getStore()
																									.load();
																							return;
																						}
																						if (c.success == 'false') {
																							sconsole.util.Util
																									.showErrorMsg(c.msg);
																							return;
																						} else {
																							Ext.Msg
																									.alert(
																											'Status',
																											c.msg);
																							_g
																									.getStore()
																									.load();
																						}
																					},
																					failure : function(
																							response,
																							opts) {
																						var c = Ext.JSON
																								.decode(response.responseText);
																						sconsole.util.Util
																								.showErrorMsg(c.msg);
																					}
																				});
																	}
																});
											}
										}
									},
									"addGroups #submitBtn" : {
										click : function(thisobj) {
											_g = thisobj.up("grid");
											_g2 = _g.up("addGroups");
											// alert(_g);
											// _g2 =
											// Ext.ComponentQuery.query('userGroups
											// #adminUserGroupGrid')[0].getStore();
											// _g2 =
											// Ext.ComponentQuery.query('adminUsers
											// #adminUserGrid')[0];
											// alert(_g2.getSelectionModel().getSelection()[0].get('id'));
											// alert(_g.up("addGroups").userId);

											// return;

											var _chosens = _g
													.getSelectionModel()
													.getSelection();

											if (Ext.isEmpty(_chosens)) {
												Ext.Msg
														.alert('Waring',
																'Please select a row at first.');
												return;
											}

											var groupIds = new Array();
											_chosens
													.forEach(function(element,
															index, array) {
														groupIds.push(element
																.get('id'));
													});

											// alert(groupIds.toString());

											Ext.Ajax
													.request({
														url : 'admin/addGroupsToUser.action',
														params : {
															userId : _g2.userId,
															groupIds : groupIds
														},
														success : function(
																response, opts) {
															try {
																var c = Ext.JSON
																		.decode(response.responseText);
															} catch (err) {
																Ext.ComponentQuery
																		.query('userGroups #adminUserGroupGrid')[0]
																		.getStore()
																		.load();
																return;
															}
															if (c.success == 'false') {
																sconsole.util.Util
																		.showErrorMsg(c.msg);
																Ext.ComponentQuery
																		.query('userGroups #adminUserGroupGrid')[0]
																		.getStore()
																		.load();
																return;
															} else {
																Ext.Msg
																		.alert(
																				'Status',
																				c.msg);
																Ext.ComponentQuery
																		.query('userGroups #adminUserGroupGrid')[0]
																		.getStore()
																		.load();
															}
														},
														failure : function(
																response, opts) {
															var c = Ext.JSON
																	.decode(response.responseText);
															sconsole.util.Util
																	.showErrorMsg(c.msg);
														}
													});
											_g.up("window").close();
										}
									},
									"addGroups #cancelBtn" : {
										click : function(thisobj) {
											thisobj.up("grid").up("window")
													.close();
										}
									},
									"addUsers #submitBtn" : {
										click : function(thisobj) {
											_g = thisobj.up("grid");
											_g2 = _g.up("addUsers");

											var _chosens = _g
													.getSelectionModel()
													.getSelection();

											if (Ext.isEmpty(_chosens)) {
												Ext.Msg
														.alert('Waring',
																'Please select a row at first.');
												return;
											}

											var userIds = new Array();
											_chosens
													.forEach(function(element,
															index, array) {
														userIds.push(element
																.get('id'));
													});

											// alert(groupIds.toString());

											Ext.Ajax
													.request({
														url : 'admin/addUsersToGroup.action',
														params : {
															userIds : userIds,
															groupId : _g2.groupId,
														},
														success : function(
																response, opts) {
															try {
																var c = Ext.JSON
																		.decode(response.responseText);
															} catch (err) {
																Ext.ComponentQuery
																		.query('groupUsers #adminGroupUserGrid')[0]
																		.getStore()
																		.load();
																return;
															}
															if (c.success == 'false') {
																sconsole.util.Util
																		.showErrorMsg(c.msg);
																Ext.ComponentQuery
																		.query('groupUsers #adminGroupUserGrid')[0]
																		.getStore()
																		.load();
																return;
															} else {
																Ext.Msg
																		.alert(
																				'Status',
																				c.msg);
																Ext.ComponentQuery
																		.query('groupUsers #adminGroupUserGrid')[0]
																		.getStore()
																		.load();
															}
														},
														failure : function(
																response, opts) {
															var c = Ext.JSON
																	.decode(response.responseText);
															sconsole.util.Util
																	.showErrorMsg(c.msg);
														}
													});
											_g.up("window").close();
										}
									},
									"addUsers #cancelBtn" : {
										click : function(thisobj) {
											thisobj.up("grid").up("window")
													.close();
										}
									},
									"adminUsers #newBtn" : {
										click : function() {
											win = Ext
													.create('sconsole.view.admin.newUser');
											win.show();
										}
									},
									"newUserWin #submitBtn" : {
										click : function(thisobj) {
											formPanel = thisobj.up("form");
											_win = formPanel.up("newUserWin");
											_form = formPanel.getForm();
											if (_form.isValid()) {
												password_temp = formPanel.down(
														'#password').getValue();
												confirmPassword_temp = formPanel
														.down(
																'#confirmPassword')
														.getValue();
												if (password_temp != confirmPassword_temp) {
													sconsole.util.Util
															.showErrorMsg('两次密码输入不一致');
													return;
												}

												_form
														.submit({
															clientValidation : true,
															url : 'admin/newUser.action',
															scope : this,
															success : function(
																	form,
																	action) {
																try {
																	var c = action.result;
																} catch (err) {
																	Ext.Msg
																			.alert(
																					'Status',
																					'Fail to get response message');
																	return;
																}
																if (c.success == 'false') {
																	sconsole.util.Util
																			.showErrorMsg(c.msg);
																	return;
																} else {
																	Ext.Msg
																			.show({
																				title : 'Info',
																				msg : c.msg,
																				icon : Ext.Msg.INFO,
																				buttons : Ext.Msg.OK,
																				fn : function() {
																					_win = Ext.ComponentQuery
																							.query('newUserWin')[0];
																					_win
																							.close();
																					_g = Ext.ComponentQuery
																							.query('adminUsers #adminUserGrid')[0];
																					_g
																							.getStore()
																							.load();
																				}
																			});
																}
															},
															failure : sconsole.util.Util.ajaxFailure
														});
											}
										}
									},
									"newUserWin #cancelBtn" : {
										click : function() {
											_win = Ext.ComponentQuery
													.query('newUserWin')[0];
											if (!Ext.isEmpty(_win))
												_win.close();
										}
									},
									"adminUsers #deleteBtn" : {
										click : function() {
											_g = this.getAdminUserGrid();
											var _chosens = _g
													.getSelectionModel()
													.getSelection();
											if (Ext.isEmpty(_chosens)) {
												Ext.Msg
														.alert('Waring',
																'Please select a row at first.');
												return;
											} else {

												// alert(_chosens.length);

												var userIds = new Array();
												var emails = new Array();
												_chosens.forEach(function(
														element, index, array) {
													userIds.push(element
															.get('id'));
													emails.push(element
															.get('email'));
												});

												// alert(userIds.length);
												Ext.MessageBox
														.confirm(
																'Delete User',
																'Are you sure to delete User ?',
																function(bId,
																		text,
																		opt) {
																	if (bId == 'yes') {
																		Ext.Ajax
																				.request({
																					url : 'admin/deleteUser.action',
																					params : {
																						userIDs : userIds,
																						emails : emails
																					},
																					success : function(
																							response,
																							opts) {
																						try {
																							var c = Ext.JSON
																									.decode(response.responseText);
																						} catch (err) {
																							_g
																									.getStore()
																									.load();
																							return;
																						}
																						if (c.success == 'false') {
																							sconsole.util.Util
																									.showErrorMsg(c.msgErr);
																							return;
																						} else {
																							Ext.Msg
																									.alert(
																											'Status',
																											c.msg);
																							_g
																									.getStore()
																									.load();
																						}
																					},
																					failure : function(
																							response,
																							opts) {
																						var c = Ext.JSON
																								.decode(response.responseText);
																						sconsole.util.Util
																								.showErrorMsg(c.msg);
																					}
																				});
																	}
																});
											}
										}
									},
									"adminUsers #modifyBtn" : {
										click : function() {
											_g = this.getAdminUserGrid();
											var _chosens = _g
													.getSelectionModel()
													.getSelection();
											if (Ext.isEmpty(_chosens)) {
												Ext.Msg
														.alert('Waring',
																'Please select a row at first.');
												return;
											} else {
												win = Ext
														.create('sconsole.view.admin.modifyUser');
												win.down('#userID').setValue(
														_chosens[0].get('id'));
												win
														.down('#firstName')
														.setValue(
																_chosens[0]
																		.get('firstName'));
												win
														.down('#lastName')
														.setValue(
																_chosens[0]
																		.get('lastName'));
												win.down('#email').setValue(
														_chosens[0]
																.get('email'));
												win.show();
											}

										}
									},
									"modifyUserWin #submitBtn" : {
										click : function(thisobj) {
											formPanel = thisobj.up("form");
											_win = formPanel
													.up("modifyUserWin");
											_form = formPanel.getForm();
											if (_form.isValid()) {
												password_temp = formPanel.down(
														'#password').getValue();
												confirmPassword_temp = formPanel
														.down(
																'#confirmPassword')
														.getValue();
												if (password_temp != confirmPassword_temp) {
													sconsole.util.Util
															.showErrorMsg('两次密码输入不一致');
													return;
												}
												_form
														.submit({
															clientValidation : true,
															url : 'admin/updateUser.action',
															scope : this,
															success : function(
																	form,
																	action) {
																try {
																	var c = action.result;
																} catch (err) {
																	Ext.Msg
																			.alert(
																					'Status',
																					'Fail to get response message');
																	return;
																}
																if (c.success == 'false') {
																	sconsole.util.Util
																			.showErrorMsg(c.msg);
																	return;
																} else {
																	Ext.Msg
																			.show({
																				title : 'Info',
																				msg : c.msg,
																				icon : Ext.Msg.INFO,
																				buttons : Ext.Msg.OK,
																				fn : function() {
																					_win = Ext.ComponentQuery
																							.query('modifyUserWin')[0];
																					_win
																							.close();
																					_g = Ext.ComponentQuery
																							.query('adminUsers #adminUserGrid')[0];
																					_g
																							.getStore()
																							.load();
																				}
																			});
																}
															},
															failure : sconsole.util.Util.ajaxFailure
														});

											}
										}
									},
									"modifyUserWin #cancelBtn" : {
										click : function() {
											_win = Ext.ComponentQuery
													.query('modifyUserWin')[0];
											if (!Ext.isEmpty(_win))
												_win.close();
										}
									},
									"adminGroups #newBtn" : {
										click : function() {
											win = Ext
													.create('sconsole.view.admin.newGroup');
											win.show();
										}
									},
									"newGroupWin #submitBtn" : {
										click : function(thisobj) {
											formPanel = thisobj.up("form");
											_win = formPanel.up("newGroupWin");
											_form = formPanel.getForm();
											if (_form.isValid()) {
												_form
														.submit({
															clientValidation : true,
															url : 'admin/newGroup.action',
															scope : this,
															success : function(
																	form,
																	action) {
																try {
																	var c = action.result;
																} catch (err) {
																	Ext.Msg
																			.alert(
																					'Status',
																					'Fail to get response message');
																	return;
																}
																if (c.success == 'false') {
																	sconsole.util.Util
																			.showErrorMsg(c.msg);
																	return;
																} else {
																	Ext.Msg
																			.show({
																				title : 'Info',
																				msg : c.msg,
																				icon : Ext.Msg.INFO,
																				buttons : Ext.Msg.OK,
																				fn : function() {
																					_win = Ext.ComponentQuery
																							.query('newGroupWin')[0];
																					_win
																							.close();
																					_g = Ext.ComponentQuery
																							.query('adminGroups #adminGroupGrid')[0];
																					_g
																							.getStore()
																							.load();
																				}
																			});
																}
															},
															failure : sconsole.util.Util.ajaxFailure
														});
											}
										}
									},
									"newGroupWin #cancelBtn" : {
										click : function() {
											_win = Ext.ComponentQuery
													.query('newGroupWin')[0];
											if (!Ext.isEmpty(_win))
												_win.close();
										}
									},
									"adminGroups #deleteBtn" : {
										click : function() {
											_g = this.getAdminGroupGrid();
											var _chosens = _g
													.getSelectionModel()
													.getSelection();
											if (Ext.isEmpty(_chosens)) {
												Ext.Msg
														.alert('Waring',
																'Please select a row at first.');
												return;
											} else {

												var ids = new Array();
												var names = new Array();
												_chosens
														.forEach(function(
																element, index,
																array) {
															ids.push(element
																	.get('id'));
															names
																	.push(element
																			.get('name'));
														});

												Ext.MessageBox
														.confirm(
																'Delete Group',
																'Are you sure to delete Group ?',
																function(bId,
																		text,
																		opt) {
																	if (bId == 'yes') {
																		Ext.Ajax
																				.request({
																					url : 'admin/deleteGroup.action',
																					params : {
																						ids : ids,
																						names : names
																					},
																					success : function(
																							response,
																							opts) {
																						try {
																							var c = Ext.JSON
																									.decode(response.responseText);
																						} catch (err) {
																							_g
																									.getStore()
																									.load();
																							return;
																						}
																						if (c.success == 'false') {
																							sconsole.util.Util
																									.showErrorMsg(c.msg);
																							return;
																						} else {
																							Ext.Msg
																									.alert(
																											'Status',
																											c.msg);
																							_g
																									.getStore()
																									.load();
																						}
																					},
																					failure : function(
																							response,
																							opts) {
																						var c = Ext.JSON
																								.decode(response.responseText);
																						sconsole.util.Util
																								.showErrorMsg(c.msg);
																					}
																				});
																	}
																});
											}
										}
									},
									"adminGroups #modifyBtn" : {
										click : function() {
											_g = this.getAdminGroupGrid();
											var _chosens = _g
													.getSelectionModel()
													.getSelection();
											if (Ext.isEmpty(_chosens)) {
												Ext.Msg
														.alert('Waring',
																'Please select a row at first.');
												return;
											} else {
												win = Ext
														.create('sconsole.view.admin.modifyGroup');
												win.down('#id').setValue(
														_chosens[0].get('id'));
												win
														.down('#name')
														.setValue(
																_chosens[0]
																		.get('name'));
												win
														.down('#type')
														.setValue(
																_chosens[0]
																		.get('type'));
												win.show();
											}

										}
									},
									"modifyGroupWin #submitBtn" : {
										click : function(thisobj) {
											formPanel = thisobj.up("form");
											_win = formPanel
													.up("modifyGroupWin");
											_form = formPanel.getForm();
											if (_form.isValid()) {
												_form
														.submit({
															clientValidation : true,
															url : 'admin/updateGroup.action',
															scope : this,
															success : function(
																	form,
																	action) {
																try {
																	var c = action.result;
																} catch (err) {
																	Ext.Msg
																			.alert(
																					'Status',
																					'Fail to get response message');
																	return;
																}
																if (c.success == 'false') {
																	sconsole.util.Util
																			.showErrorMsg(c.msg);
																	return;
																} else {
																	Ext.Msg
																			.show({
																				title : 'Info',
																				msg : c.msg,
																				icon : Ext.Msg.INFO,
																				buttons : Ext.Msg.OK,
																				fn : function() {
																					_win = Ext.ComponentQuery
																							.query('modifyGroupWin')[0];
																					_win
																							.close();
																					_g = Ext.ComponentQuery
																							.query('adminGroups #adminGroupGrid')[0];
																					_g
																							.getStore()
																							.load();
																				}
																			});
																}
															},
															failure : sconsole.util.Util.ajaxFailure
														});
											}
										}
									},
									"modifyGroupWin #cancelBtn" : {
										click : function() {
											_win = Ext.ComponentQuery
													.query('modifyGroupWin')[0];
											if (!Ext.isEmpty(_win))
												_win.close();
										}
									},
									"adminDeployments #adminDeployGrid" : {
										beforerender : function(thisobj) {
											thisobj.getStore().load();
										},
										selectionchange : function(thisobj,
												_selected, eOpts) {
											record = _selected[0];
											if (!Ext.isEmpty(record)) {
												Ext.Ajax
														.request({
															url : "admin/getDeployment.action",
															scope : this,
															success : function(
																	a) {
																// _w.getEl().unmask();
																var c = sconsole.util.Util
																		.decodeJSON(a.responseText);

																_f = this
																		.getDeploymentForm();
																_f
																		.loadRecord(record);

																_g = this
																		.getDeployResourcesGrid();

																_s = _g
																		.getStore();
																_s.removeAll();
																_s
																		.loadData(c.resources);
																_g1 = this
																		.getDeploymentProcsGrid();
																_s1 = _g1
																		.getStore();
																_s1.removeAll();
																_s1
																		.loadData(c.procs);
															},
															failure : function(
																	a) {
																console
																		.log(a.responseText);
																sconsole.util.Util
																		.sendErrorMsg("Error");
															},
															params : {
																id : record
																		.get('id')
															},
															waitMsg : "waiting...."
														});
											} else {
												sconsole.util.Util
														.resetForm('adminDeployments #deploymentForm');
												_g = this
														.getDeployResourcesGrid();
												_s = _g.getStore();
												_s.removeAll();
												_g1 = this
														.getDeploymentProcsGrid();
												_s1 = _g1.getStore();
												_s1.removeAll();

											}
										}
									},
									"adminUsers #adminUserGrid" : {
										beforerender : function(thisobj) {
											thisobj.getStore().load();
										}
									},
									"adminGroups #adminGroupGrid" : {
										beforerender : function(thisobj) {
											thisobj.getStore().load();
										}
									},
									"adminDeployments #deleteBtn" : {
										click : function(thisobj) {
											_g = this.getAdminDeployGrid();
											_chosens = _g.getSelectionModel()
													.getSelection();
											record = _chosens[0];
											Ext.Ajax
													.request({
														url : "admin/prepareDeleteDeployment.action",
														scope : this,
														success : function(a) {
															var c = sconsole.util.Util
																	.decodeJSON(a.responseText);

															if (c.icnt == 0) {
																msg = 'No runtime process instances found for this deployment,Continue to delete?';
															} else {
																msg = ' Found <b>'
																		+ c.icnt
																		+ ' running process instances</b> for this deployment.<br/>Are you sure you want to go ahead?'
															}

															Ext.MessageBox
																	.confirm(
																			'Delete Deployment',
																			msg,
																			function(
																					bId,
																					text,
																					opt) {
																				if (bId == 'yes') {
																					Ext.Ajax
																							.request({
																								url : 'admin/deleteDeployment.action',
																								success : function(
																										response,
																										opts) {

																									var c = sconsole.util.Util
																											.decodeJSON(response.responseText);
																									if (c.success == 'false') {
																										sconsole.util.Util
																												.showErrorMsg(c.msg);
																										return;
																									} else {
																										sconsole.util.Util
																												.showInfoMsg(c.msg);
																										_g
																												.getStore()
																												.load();
																									}
																								},
																								failure : function(
																										response,
																										opts) {
																									sconsole.util.Util
																											.showErrorMsg(response.responseText);
																								},
																								params : {
																									id : record
																											.get('id')
																								}
																							});
																				}
																			},
																			{
																				scope : this
																			});
														},
														failure : function(a) {
															console
																	.log(a.responseText);
															sconsole.util.Util
																	.sendErrorMsg("Error");
														},
														params : {
															id : record
																	.get('id')
														},
														waitMsg : "waiting...."
													});
										}
									}
								});
					}
				});