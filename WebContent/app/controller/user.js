Ext
		.define(
				'sconsole.controller.user',
				{
					extend : 'Ext.app.Controller',
					refs : [ {
						ref : 'mainPanel',
						selector : 'mainpanel'
					}, {
						ref : 'editProfile',
						selector : 'editProfile'
					} ],
					init : function(application) {
						this
								.control({
									"changePwd #okBtn" : {
										click : function() {
											_w = Ext.ComponentQuery
													.query('changePwd');
											if (Ext.isEmpty(_w)) {
												return;
											}
											_w = _w[0];
											_c = Ext.ComponentQuery
													.query('changePwd #currentPwd')[0];
											_n = Ext.ComponentQuery
													.query('changePwd #pass')[0];
											if (Ext.isEmpty(_c)
													|| Ext.isEmpty(_n)) {
												console
														.log('cannot find currentPwd field');
												return;
											}
											if (_n.getValue() == _c.getValue()) {
												sconsole.util.Util
														.showErrorMsg('New Password cannot be same as Old Password!');
												return;
											}
											cpass = sconsole.util.MD5.encode(_c
													.getValue());
											npass = sconsole.util.MD5.encode(_n
													.getValue());

											_w.getEl().mask('Saving...',
													'x-mask-loading');
											Ext.Ajax
													.request({
														url : "user/changePwd.action",
														success : function(a) {
															_w.getEl().unmask();
															var c = Ext
																	.decode(a.responseText);
															if (c.success == "true") {
																Ext.Msg
																		.show({
																			title : 'Info',
																			msg : c.msg,
																			icon : Ext.Msg.INFO,
																			buttons : Ext.Msg.OK,
																			fn : function() {
																				_w
																						.close();
																			}
																		});
															} else {
																sconsole.util.Util
																		.showErrorMsg(c.msg);
															}
														},
														failure : function(a) {
															_w.getEl().unmask();
															// var c =
															// Ext.decode(a.responseText);
															console
																	.log(a.responseText);
															sconsole.util.Util
																	.sendErrorMsg();
														},
														params : {
															cpass : cpass,
															npass : npass
														},
														waitMsg : "waiting...."
													});
										}
									},
									"changePwd #cancelBtn" : {
										click : function() {
											_c = Ext.ComponentQuery
													.query('changePwd');
											if (!Ext.isEmpty(_c)) {
												_c[0].close();
											}
										}
									},
									"editProfile" : {
										beforerender : function() {
											var store = Ext
													.create('sconsole.store.user.user');
											store
													.load({
														callback : function(r,
																options,
																success) {
															//alert(success);
															if (success) {
																//alert(r.length);
																if (r.length > 0) {
																	formPanel1 = Ext.ComponentQuery.query('editProfile #last')[0];
																	formPanel1.setValue(r[0].getData().fullname);
																	//alert(r[0].getId);
																	formPanel2 = Ext.ComponentQuery.query('editProfile #phone')[0];
																	formPanel2.setValue(r[0].getData().phone);
																	formPanel3 = Ext.ComponentQuery.query('editProfile #idcard')[0];
																	formPanel3.setValue(r[0].getData().idcard);
																	formPanel4 = Ext.ComponentQuery.query('editProfile #email')[0];
																	formPanel4.setValue(r[0].getData().email);
																	
																}
															}
														},
														scope : this
													});
										}
									},
									"editProfile #okBtn" : {
										click : function(button) {
											var formPanel = button.up('form');
											_form = formPanel.getForm();
											if (_form.isValid()) {
												Ext
														.get(formPanel.getEl())
														.mask(
																"Saving...",
																'loading');
												_form
														.submit({
															url : 'user/saveUser.action',
															success : function(
																	form,
																	action) {
																me = this;
																Ext
																		.get(
																				formPanel
																						.getEl())
																		.unmask();
																if (action.result.success == 'true') {
																	;
																} else {
																	sconsole.util.Util
																			.showErrorMsg(action.result.msg);
																}
															},
															failure : function(
																	conn,
																	response,
																	options,
																	eOpts) {
																Ext
																		.get(
																				formPanel
																						.getEl())
																		.unmask();
																sconsole.util.Util
																		.showErrorMsg(conn.responseText);
															},
															scope : this
														});
											}
										}
									},
									"editProfile #cancelBtn" : {
										click : function() {
											var mainPanel = this.getMainPanel();

											var newTab = mainPanel.items
													.findBy(function(tab) {
														return tab.title == 'Edit Profile';
													});
											if (newTab) {
												newTab.close();
											}
										}
									}
								});
					}
				});