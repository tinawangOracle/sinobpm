Ext
		.define(
				'sconsole.controller.admin',
				{
					extend : 'Ext.app.Controller',
					requires : [ 'sconsole.util.MD5',
							'sconsole.util.CustomDateTimeField',
							'sconsole.util.CustomDateTimePicker' ],
					refs : [ {
						ref : 'adminUserGrid',
						selector : 'adminUsers #adminUserGrid'
					}, {
						ref : 'adminGroupGrid',
						selector : 'adminGroups #adminGroupGrid'
					}, {
						ref : 'adminUserGroupGrid',
						selector : 'userGroups #adminUserGroupGrid'
					}, {
						ref : 'adminGroupUserGrid',
						selector : 'groupUsers #adminGroupUserGrid'
					}, {
						ref : 'adminDeployGrid',
						selector : 'adminDeployments #adminDeployGrid'
					}, {
						ref : "deployResourcesGrid",
						selector : "adminDeployments #deployResourcesGrid"
					}, {
						ref : "deploymentProcsGrid",
						selector : "adminDeployments #deploymentProcsGrid"
					}, {
						ref : 'deploymentForm',
						selector : 'adminDeployments #deploymentForm'
					}, {
						ref : 'suspendProcsGrid',
						selector : 'suspendedProcess #suspendProcsGrid'
					}, {
						ref : 'activeProcsGrid',
						selector : 'activeProcess #activeProcsGrid'
					}, {
						ref : 'jobsGrid',
						selector : 'jobsPanel #jobsGrid'
					}, {
						ref : 'jobsGrid',
						selector : 'jobsPanel #jobsGrid'
					}, {
						ref : 'doneInstancesGrid',
						selector : 'doneInstancesPanel #doneInstancesGrid'
					}, {
						ref : 'activeInstancesGrid',
						selector : 'activeInstancesPanel #activeInstancesGrid'
					} ],
					requires : [ 'sconsole.view.admin.instDetails' ],
					views : [ 'admin.newUser', 'admin.modifyUser',
							'admin.newGroup', 'admin.modifyGroup',
							'admin.groupUsers', 'admin.addGroups',
							'admin.userGroups', 'admin.addUsers',
							'admin.deployments', 'admin.suspendedProcs',
							'admin.activeProcs', 'admin.activateSuspendProc',
							'admin.suspendActiveProc', 'admin.jobs',
							'admin.doneInstances', 'admin.activeInstances',
							'admin.instancePanel', 'admin.instDetails',
							'admin.newDeployment' ],
					stores : [ 'admin.users', 'admin.groups',
							'admin.groupUsers', 'admin.userGroups',
							'admin.userNoGroups', 'admin.groupNoUsers',
							'admin.deployments', 'admin.deploymentResources',
							'admin.deploymentProcs', 'admin.suspendedProcs',
							'admin.activeProcs', 'admin.jobs',
							'admin.doneDefinitions', 'admin.activeDefinitions',
							'admin.doneInstances', 'admin.activeInstances',

							'workflow.instTasks', 'workflow.instVars' ],
					init : function(application) {
						this
								.control({
									// "groupUsers #addBtn" : {
									// click : function(){
									// //win =
									// Ext.create('sconsole.view.admin.addGroups');
									// //win.show();
									// }
									// },
									"userGroups #clrBtn" : {
										click : function(thisobj) {
											// _win = thisobj.up("grid");
											_win = thisobj.up("userGroups");
											_win = _win
													.queryById('adminUserGroupItemselector');
											if (!_win.disabled) {
												_win.clearValue();
											}
										}
									},
									"groupUsers #clrBtn" : {
										click : function(thisobj) {
											//_win = thisobj.up("grid");
											_win = thisobj.up("groupUsers");
											_win = _win.queryById('adminGroupUserItemselector');
											if (!_win.disabled) {
				    	                    	_win.clearValue();
				    	                    }
										}
									},
									"userGroups #reBtn" : {
										click : function(thisobj) {
											// _win = thisobj.up("grid");
											_win = thisobj.up("userGroups");

											var userGroup = Ext
													.create(
															'Ext.data.ArrayStore',
															{
																autoLoad : false,
																proxy : {
																	type : 'ajax',
																	api : {
																		read : "admin/getGroupsByUserId.action"
																	},
																	timeout : 0,
																	reader : {
																		type : 'json',
																		totalProperty : "totalCount",
																		root : "records",
																		idProperty : 'id',
																		messageProperty : 'userId'
																	}
																},
																fields : [
																		'id',
																		'name',
																		'type' ],
																sortInfo : {
																	field : 'name',
																	direction : 'ASC'
																}
															});
											userGroup.getProxy().setExtraParam(
													'userId', _win.userId);
											userGroup
													.load(function(records,
															operation, success) {
														var select = [];
														userGroup
																.each(function(
																		record) {
																	// alert(record.get('id'));
																	select
																			.push(record
																					.get('id'));
																});
														_win
																.queryById(
																		'adminUserGroupItemselector')
																.setValue(
																		select);
													});
										}
									},
									"groupUsers #reBtn" : {
										click : function(thisobj) {
											//_win = thisobj.up("grid");
											_win = thisobj.up("groupUsers");
											
				    	                    
				    	                    var groupUser = Ext.create('Ext.data.ArrayStore', {
								    			autoLoad : false,
								    			proxy : {
								    				type : 'ajax',
								    				api : {
								    					read : "admin/getUsersByGroupId.action"
								    				},
								    				timeout : 0,
								    				reader : {
								    					type : 'json',
								    					totalProperty : "totalCount",
								    					root : "records",
								    					idProperty : 'id',
								    					messageProperty: 'groupId'
								    				}
								    			},
								    			fields : [ 'id', 'email'],
								    			sortInfo: {
								    	            field: 'email',
								    	            direction: 'ASC'
								    	        }
								    		});
											groupUser.getProxy().setExtraParam('groupId',_win.groupId);
											groupUser.load(function(records, operation, success){
												var select = [];
												groupUser.each(function(record){
													//alert(record.get('id'));
										    		select.push(record.get('id'));
										    	});
												_win.queryById('adminGroupUserItemselector').setValue(select);
											});
										}
									},
									"userGroups #subBtn" : {
										click : function(thisobj) {
											// _win = thisobj.up("grid");
											_win = thisobj.up("userGroups");
											var userId = _win.userId;
											_win = _win
													.queryById('adminUserGroupItemselector');
											var select = _win.up('form')
													.getForm().getValues(true);
											var groupIds = select.split('=')[1]
													.split('%2C');

											Ext.Ajax
													.request({
														url : 'admin/addGroupsToUser.action',
														params : {
															userId : userId,
															groupIds : groupIds
														},
														success : function(
																response, opts) {
															try {
																var c = Ext.JSON
																		.decode(response.responseText);
															} catch (err) {
																return;
															}
															if (c.success == 'false') {
																sconsole.util.Util
																		.showErrorMsg(c.msg);
																return;
															} else {
																Ext.Msg
																		.alert(
																				'Status',
																				c.msg);
															}
														},
														failure : function(
																response, opts) {
															var c = Ext.JSON
																	.decode(response.responseText);
															sconsole.util.Util
																	.showErrorMsg(c.msg);
														}
													});
										}
									},
									"groupUsers #subBtn" : {
										click : function(thisobj) {
											//_win = thisobj.up("grid");
											_win = thisobj.up("groupUsers");
											var groupId = _win.groupId;
											_win = _win.queryById('adminGroupUserItemselector');
											var select = _win.up('form').getForm().getValues(true);
											var userIds = select.split('=')[1].split('%2C');
											
											Ext.Ajax.request({
												url : 'admin/addUsersToGroup.action',
												params : {
													userIds : userIds,
													groupId : groupId
												},
												success : function(response, opts) {
													try {
														var c = Ext.JSON.decode(response.responseText);
													} catch (err) {
														return;
													}
													if (c.success == 'false') {
														sconsole.util.Util.showErrorMsg(c.msg);
														return;
													} else {
														Ext.Msg.alert('Status',c.msg);
													}
												},
												failure : function(response, opts) {
													var c = Ext.JSON.decode(response.responseText);
													sconsole.util.Util.showErrorMsg(c.msg);
												}
											});
										}
									},
									"userGroups #addBtn" : {
										click : function(thisobj) {
											_win = thisobj.up("grid");
											_win = _win.up("userGroups");
											// alert(_win.userId);

											// _g =
											// Ext.ComponentQuery.query('userGroups')[0];
											win = Ext
													.create('sconsole.view.admin.addGroups');
											// alert(win);
											win.userId = _win.userId;
											win.queryById('addGroupGrid')
													.getStore().getProxy()
													.setExtraParam('userId',
															win.userId);
											win.queryById('addGroupGrid')
													.getStore().load();
											win.show();

										}
									},
									"groupUsers #addBtn" : {
										click : function(thisobj) {
											_win = thisobj.up("grid");
											_win = _win.up("groupUsers");
											// alert(_win.userId);

											// _g =
											// Ext.ComponentQuery.query('userGroups')[0];
											win = Ext
													.create('sconsole.view.admin.addUsers');
											// alert(win);
											win.groupId = _win.groupId;
											win.queryById('addUserGrid')
													.getStore().getProxy()
													.setExtraParam('groupId',
															win.groupId);
											win.queryById('addUserGrid')
													.getStore().load();
											win.show();

										}
									},
									"userGroups #removeBtn" : {
										click : function(thisobj) {
											_g = this.getAdminUserGroupGrid();
											var _chosens = _g
													.getSelectionModel()
													.getSelection();
											if (Ext.isEmpty(_chosens)) {
												Ext.Msg
														.alert('Waring',
																'Please select a row at first.');
												return;
											} else {

												var groupIds = new Array();
												var names = new Array();
												_chosens.forEach(function(
														element, index, array) {
													groupIds.push(element
															.get('id'));
													names.push(element
															.get('name'));
												});

												Ext.MessageBox
														.confirm(
																'Remove Group',
																'Are you sure to remove Group(s) ?',
																function(bId,
																		text,
																		opt) {
																	if (bId == 'yes') {
																		Ext.Ajax
																				.request({
																					url : 'admin/removeGroup.action',
																					params : {
																						groupIds : groupIds,
																						userId : _g
																								.up("window").userId,
																						names : names
																					},
																					success : function(
																							response,
																							opts) {
																						try {
																							var c = Ext.JSON
																									.decode(response.responseText);
																						} catch (err) {
																							_g
																									.getStore()
																									.load();
																							return;
																						}
																						if (c.success == 'false') {
																							sconsole.util.Util
																									.showErrorMsg(c.msg);
																							return;
																						} else {
																							Ext.Msg
																									.alert(
																											'Status',
																											c.msg);
																							_g
																									.getStore()
																									.load();
																						}
																					},
																					failure : function(
																							response,
																							opts) {
																						var c = Ext.JSON
																								.decode(response.responseText);
																						sconsole.util.Util
																								.showErrorMsg(c.msg);
																					}
																				});
																	}
																});
											}
										}
									},
									"groupUsers #removeBtn" : {
										click : function(thisobj) {
											_g = this.getAdminGroupUserGrid();
											var _chosens = _g
													.getSelectionModel()
													.getSelection();
											if (Ext.isEmpty(_chosens)) {
												Ext.Msg
														.alert('Waring',
																'Please select a row at first.');
												return;
											} else {

												var userIds = new Array();
												var emails = new Array();
												_chosens.forEach(function(
														element, index, array) {
													userIds.push(element
															.get('id'));
													emails.push(element
															.get('email'));
												});

												Ext.MessageBox
														.confirm(
																'Remove User',
																'Are you sure to remove these user(s) from the group?',
																function(bId,
																		text,
																		opt) {
																	if (bId == 'yes') {
																		Ext.Ajax
																				.request({
																					url : 'admin/removeUser.action',
																					params : {
																						groupId : _g
																								.up("window").groupId,
																						userIds : userIds,
																						emails : emails
																					},
																					success : function(
																							response,
																							opts) {
																						try {
																							var c = Ext.JSON
																									.decode(response.responseText);
																						} catch (err) {
																							_g
																									.getStore()
																									.load();
																							return;
																						}
																						if (c.success == 'false') {
																							sconsole.util.Util
																									.showErrorMsg(c.msg);
																							return;
																						} else {
																							Ext.Msg
																									.alert(
																											'Status',
																											c.msg);
																							_g
																									.getStore()
																									.load();
																						}
																					},
																					failure : function(
																							response,
																							opts) {
																						var c = Ext.JSON
																								.decode(response.responseText);
																						sconsole.util.Util
																								.showErrorMsg(c.msg);
																					}
																				});
																	}
																});
											}
										}
									},
									"addGroups #submitBtn" : {
										click : function(thisobj) {
											_g = thisobj.up("grid");
											_g2 = _g.up("addGroups");
											// alert(_g);
											// _g2 =
											// Ext.ComponentQuery.query('userGroups
											// #adminUserGroupGrid')[0].getStore();
											// _g2 =
											// Ext.ComponentQuery.query('adminUsers
											// #adminUserGrid')[0];
											// alert(_g2.getSelectionModel().getSelection()[0].get('id'));
											// alert(_g.up("addGroups").userId);

											// return;

											var _chosens = _g
													.getSelectionModel()
													.getSelection();

											if (Ext.isEmpty(_chosens)) {
												Ext.Msg
														.alert('Waring',
																'Please select a row at first.');
												return;
											}

											var groupIds = new Array();
											_chosens
													.forEach(function(element,
															index, array) {
														groupIds.push(element
																.get('id'));
													});

											// alert(groupIds.toString());

											Ext.Ajax
													.request({
														url : 'admin/addGroupsToUser.action',
														params : {
															userId : _g2.userId,
															groupIds : groupIds
														},
														success : function(
																response, opts) {
															try {
																var c = Ext.JSON
																		.decode(response.responseText);
															} catch (err) {
																Ext.ComponentQuery
																		.query('userGroups #adminUserGroupGrid')[0]
																		.getStore()
																		.load();
																return;
															}
															if (c.success == 'false') {
																sconsole.util.Util
																		.showErrorMsg(c.msg);
																Ext.ComponentQuery
																		.query('userGroups #adminUserGroupGrid')[0]
																		.getStore()
																		.load();
																return;
															} else {
																Ext.Msg
																		.alert(
																				'Status',
																				c.msg);
																Ext.ComponentQuery
																		.query('userGroups #adminUserGroupGrid')[0]
																		.getStore()
																		.load();
															}
														},
														failure : function(
																response, opts) {
															var c = Ext.JSON
																	.decode(response.responseText);
															sconsole.util.Util
																	.showErrorMsg(c.msg);
														}
													});
											_g.up("window").close();
										}
									},
									"addGroups #cancelBtn" : {
										click : function(thisobj) {
											thisobj.up("grid").up("window")
													.close();
										}
									},
									"addUsers #submitBtn" : {
										click : function(thisobj) {
											_g = thisobj.up("grid");
											_g2 = _g.up("addUsers");

											var _chosens = _g
													.getSelectionModel()
													.getSelection();

											if (Ext.isEmpty(_chosens)) {
												Ext.Msg
														.alert('Waring',
																'Please select a row at first.');
												return;
											}

											var userIds = new Array();
											_chosens
													.forEach(function(element,
															index, array) {
														userIds.push(element
																.get('id'));
													});

											// alert(groupIds.toString());

											Ext.Ajax
													.request({
														url : 'admin/addUsersToGroup.action',
														params : {
															userIds : userIds,
															groupId : _g2.groupId,
														},
														success : function(
																response, opts) {
															try {
																var c = Ext.JSON
																		.decode(response.responseText);
															} catch (err) {
																Ext.ComponentQuery
																		.query('groupUsers #adminGroupUserGrid')[0]
																		.getStore()
																		.load();
																return;
															}
															if (c.success == 'false') {
																sconsole.util.Util
																		.showErrorMsg(c.msg);
																Ext.ComponentQuery
																		.query('groupUsers #adminGroupUserGrid')[0]
																		.getStore()
																		.load();
																return;
															} else {
																Ext.Msg
																		.alert(
																				'Status',
																				c.msg);
																Ext.ComponentQuery
																		.query('groupUsers #adminGroupUserGrid')[0]
																		.getStore()
																		.load();
															}
														},
														failure : function(
																response, opts) {
															var c = Ext.JSON
																	.decode(response.responseText);
															sconsole.util.Util
																	.showErrorMsg(c.msg);
														}
													});
											_g.up("window").close();
										}
									},
									"addUsers #cancelBtn" : {
										click : function(thisobj) {
											thisobj.up("grid").up("window")
													.close();
										}
									},
									"adminUsers #newBtn" : {
										click : function() {
											win = Ext
													.create('sconsole.view.admin.newUser');
											win.show();
										}
									},
									"newUserWin #submitBtn" : {
										click : function(thisobj) {
											formPanel = thisobj.up("form");
											_win = formPanel.up("newUserWin");
											_form = formPanel.getForm();
											if (_form.isValid()) {
												password_temp = formPanel.down(
														'#password').getValue();
												confirmPassword_temp = formPanel
														.down(
																'#confirmPassword')
														.getValue();

												if (password_temp != confirmPassword_temp) {
													sconsole.util.Util
															.showErrorMsg('两次密码输入不一致');
													return;
												}

												Ext.Ajax
														.request({
															url : 'admin/newUser.action',
															params : {
																userID : formPanel
																		.down(
																				'#userID')
																		.getValue(),
																firstName : formPanel
																		.down(
																				'#firstName')
																		.getValue(),
																lastName : formPanel
																		.down(
																				'#lastName')
																		.getValue(),
																email : formPanel
																		.down(
																				'#email')
																		.getValue(),
																password : sconsole.util.MD5
																		.encode(password_temp),
																confirmPassword : sconsole.util.MD5
																		.encode(confirmPassword_temp),
																groupID : formPanel
																		.down(
																				'#groupID')
																		.getValue(),
															},
															success : function(
																	response,
																	opts) {
																try {
																	var c = Ext.JSON
																			.decode(response.responseText);
																} catch (err) {
																	Ext.Msg
																			.alert(
																					'Status',
																					'Fail to get response message');
																	return;
																}
																if (c.success == 'false') {
																	sconsole.util.Util
																			.showErrorMsg(c.msg);
																	return;
																} else {
																	Ext.Msg
																			.show({
																				title : 'Info',
																				msg : c.msg,
																				icon : Ext.Msg.INFO,
																				buttons : Ext.Msg.OK,
																				fn : function() {
																					_win = Ext.ComponentQuery
																							.query('newUserWin')[0];
																					_win
																							.close();
																					_g = Ext.ComponentQuery
																							.query('adminUsers #adminUserGrid')[0];
																					_g
																							.getStore()
																							.load();
																				}
																			});
																}
															},
															failure : sconsole.util.Util.ajaxFailure
														});
											}
										}
									},
									"newUserWin #cancelBtn" : {
										click : function() {
											_win = Ext.ComponentQuery
													.query('newUserWin')[0];
											if (!Ext.isEmpty(_win))
												_win.close();
										}
									},
									"adminUsers #deleteBtn" : {
										click : function() {
											_g = this.getAdminUserGrid();
											var _chosens = _g
													.getSelectionModel()
													.getSelection();
											if (Ext.isEmpty(_chosens)) {
												Ext.Msg
														.alert('Waring',
																'Please select a row at first.');
												return;
											} else {

												// alert(_chosens.length);

												var userIds = new Array();
												var emails = new Array();
												_chosens.forEach(function(
														element, index, array) {
													userIds.push(element
															.get('id'));
													emails.push(element
															.get('email'));
												});

												// alert(userIds.length);
												Ext.MessageBox
														.confirm(
																'Delete User',
																'Are you sure to delete User ?',
																function(bId,
																		text,
																		opt) {
																	if (bId == 'yes') {
																		Ext.Ajax
																				.request({
																					url : 'admin/deleteUser.action',
																					params : {
																						userIDs : userIds,
																						emails : emails
																					},
																					success : function(
																							response,
																							opts) {
																						try {
																							var c = Ext.JSON
																									.decode(response.responseText);
																						} catch (err) {
																							_g
																									.getStore()
																									.load();
																							return;
																						}
																						if (c.success == 'false') {
																							sconsole.util.Util
																									.showErrorMsg(c.msgErr);
																							return;
																						} else {
																							Ext.Msg
																									.alert(
																											'Status',
																											c.msg);
																							_g
																									.getStore()
																									.load();
																						}
																					},
																					failure : function(
																							response,
																							opts) {
																						var c = Ext.JSON
																								.decode(response.responseText);
																						sconsole.util.Util
																								.showErrorMsg(c.msg);
																					}
																				});
																	}
																});
											}
										}
									},
									"adminUsers #modifyBtn" : {
										click : function() {
											_g = this.getAdminUserGrid();
											var _chosens = _g
													.getSelectionModel()
													.getSelection();
											if (Ext.isEmpty(_chosens)) {
												Ext.Msg
														.alert('Waring',
																'Please select a row at first.');
												return;
											} else {
												win = Ext
														.create('sconsole.view.admin.modifyUser');
												win.down('#userID').setValue(
														_chosens[0].get('id'));
												win
														.down('#firstName')
														.setValue(
																_chosens[0]
																		.get('firstName'));
												win
														.down('#lastName')
														.setValue(
																_chosens[0]
																		.get('lastName'));
												win.down('#email').setValue(
														_chosens[0]
																.get('email'));
												win.show();
											}

										}
									},
									"modifyUserWin #submitBtn" : {
										click : function(thisobj) {
											formPanel = thisobj.up("form");
											_win = formPanel
													.up("modifyUserWin");
											_form = formPanel.getForm();
											if (_form.isValid()) {
												password_temp = formPanel.down(
														'#password').getValue();
												confirmPassword_temp = formPanel
														.down(
																'#confirmPassword')
														.getValue();

												// alert(sconsole.util.MD5.encode(""));

												// alert(password_temp);
												//							
												if (password_temp != confirmPassword_temp) {
													sconsole.util.Util
															.showErrorMsg('两次密码输入不一致');
													return;
												}

												Ext.Ajax
														.request({
															url : 'admin/updateUser.action',
															params : {
																userID : formPanel
																		.down(
																				'#userID')
																		.getValue(),
																firstName : formPanel
																		.down(
																				'#firstName')
																		.getValue(),
																lastName : formPanel
																		.down(
																				'#lastName')
																		.getValue(),
																email : formPanel
																		.down(
																				'#email')
																		.getValue(),
																password : sconsole.util.MD5
																		.encode(password_temp),
																confirmPassword : sconsole.util.MD5
																		.encode(confirmPassword_temp),
															},
															success : function(
																	response,
																	opts) {
																try {
																	var c = Ext.JSON
																			.decode(response.responseText);
																} catch (err) {
																	Ext.Msg
																			.alert(
																					'Status',
																					'Fail to get response message');
																	return;
																}
																if (c.success == 'false') {
																	sconsole.util.Util
																			.showErrorMsg(c.msg);
																	return;
																} else {
																	Ext.Msg
																			.show({
																				title : 'Info',
																				msg : c.msg,
																				icon : Ext.Msg.INFO,
																				buttons : Ext.Msg.OK,
																				fn : function() {
																					_win = Ext.ComponentQuery
																							.query('modifyUserWin')[0];
																					_win
																							.close();
																					_g = Ext.ComponentQuery
																							.query('adminUsers #adminUserGrid')[0];
																					_g
																							.getStore()
																							.load();
																				}
																			});
																}
															},
															failure : sconsole.util.Util.ajaxFailure
														});
											}
										}
									},
									"modifyUserWin #cancelBtn" : {
										click : function() {
											_win = Ext.ComponentQuery
													.query('modifyUserWin')[0];
											if (!Ext.isEmpty(_win))
												_win.close();
										}
									},
									"adminGroups #newBtn" : {
										click : function() {
											win = Ext
													.create('sconsole.view.admin.newGroup');
											win.show();
										}
									},
									"newGroupWin #submitBtn" : {
										click : function(thisobj) {
											formPanel = thisobj.up("form");
											_win = formPanel.up("newGroupWin");
											_form = formPanel.getForm();
											if (_form.isValid()) {
												_form
														.submit({
															clientValidation : true,
															url : 'admin/newGroup.action',
															scope : this,
															success : function(
																	form,
																	action) {
																try {
																	var c = action.result;
																} catch (err) {
																	Ext.Msg
																			.alert(
																					'Status',
																					'Fail to get response message');
																	return;
																}
																if (c.success == 'false') {
																	sconsole.util.Util
																			.showErrorMsg(c.msg);
																	return;
																} else {
																	Ext.Msg
																			.show({
																				title : 'Info',
																				msg : c.msg,
																				icon : Ext.Msg.INFO,
																				buttons : Ext.Msg.OK,
																				fn : function() {
																					_win = Ext.ComponentQuery
																							.query('newGroupWin')[0];
																					_win
																							.close();
																					_g = Ext.ComponentQuery
																							.query('adminGroups #adminGroupGrid')[0];
																					_g
																							.getStore()
																							.load();
																				}
																			});
																}
															},
															failure : sconsole.util.Util.ajaxFailure
														});

											}
										}
									},
									"newGroupWin #cancelBtn" : {
										click : function() {
											_win = Ext.ComponentQuery
													.query('newGroupWin')[0];
											if (!Ext.isEmpty(_win))
												_win.close();
										}
									},
									"adminGroups #deleteBtn" : {
										click : function() {
											_g = this.getAdminGroupGrid();
											var _chosens = _g
													.getSelectionModel()
													.getSelection();
											if (Ext.isEmpty(_chosens)) {
												Ext.Msg
														.alert('Waring',
																'Please select a row at first.');
												return;
											} else {

												var ids = new Array();
												var names = new Array();
												_chosens
														.forEach(function(
																element, index,
																array) {
															ids.push(element
																	.get('id'));
															names
																	.push(element
																			.get('name'));
														});

												Ext.MessageBox
														.confirm(
																'Delete Group',
																'Are you sure to delete Group ?',
																function(bId,
																		text,
																		opt) {
																	if (bId == 'yes') {
																		Ext.Ajax
																				.request({
																					url : 'admin/deleteGroup.action',
																					params : {
																						ids : ids,
																						names : names
																					},
																					success : function(
																							response,
																							opts) {
																						try {
																							var c = Ext.JSON
																									.decode(response.responseText);
																						} catch (err) {
																							_g
																									.getStore()
																									.load();
																							return;
																						}
																						if (c.success == 'false') {
																							sconsole.util.Util
																									.showErrorMsg(c.msg);
																							return;
																						} else {
																							Ext.Msg
																									.alert(
																											'Status',
																											c.msg);
																							_g
																									.getStore()
																									.load();
																						}
																					},
																					failure : function(
																							response,
																							opts) {
																						var c = Ext.JSON
																								.decode(response.responseText);
																						sconsole.util.Util
																								.showErrorMsg(c.msg);
																					}
																				});
																	}
																});
											}
										}
									},
									"adminGroups #modifyBtn" : {
										click : function() {
											_g = this.getAdminGroupGrid();
											var _chosens = _g
													.getSelectionModel()
													.getSelection();
											if (Ext.isEmpty(_chosens)) {
												Ext.Msg
														.alert('Waring',
																'Please select a row at first.');
												return;
											} else {
												win = Ext
														.create('sconsole.view.admin.modifyGroup');
												win.down('#id').setValue(
														_chosens[0].get('id'));
												win
														.down('#name')
														.setValue(
																_chosens[0]
																		.get('name'));
												win
														.down('#type')
														.setValue(
																_chosens[0]
																		.get('type'));
												win.show();
											}

										}
									},
									"modifyGroupWin #submitBtn" : {
										click : function(thisobj) {
											formPanel = thisobj.up("form");
											_win = formPanel
													.up("modifyGroupWin");
											_form = formPanel.getForm();
											if (_form.isValid()) {
												_form
														.submit({
															clientValidation : true,
															url : 'admin/updateGroup.action',
															scope : this,
															success : function(
																	form,
																	action) {
																try {
																	var c = action.result;
																} catch (err) {
																	Ext.Msg
																			.alert(
																					'Status',
																					'Fail to get response message');
																	return;
																}
																if (c.success == 'false') {
																	sconsole.util.Util
																			.showErrorMsg(c.msg);
																	return;
																} else {
																	Ext.Msg
																			.show({
																				title : 'Info',
																				msg : c.msg,
																				icon : Ext.Msg.INFO,
																				buttons : Ext.Msg.OK,
																				fn : function() {
																					_win = Ext.ComponentQuery
																							.query('modifyGroupWin')[0];
																					_win
																							.close();
																					_g = Ext.ComponentQuery
																							.query('adminGroups #adminGroupGrid')[0];
																					_g
																							.getStore()
																							.load();
																				}
																			});
																}
															},
															failure : sconsole.util.Util.ajaxFailure
														});
											}
										}
									},
									"modifyGroupWin #cancelBtn" : {
										click : function() {
											_win = Ext.ComponentQuery
													.query('modifyGroupWin')[0];
											if (!Ext.isEmpty(_win))
												_win.close();
										}
									},
									"adminDeployments #adminDeployGrid" : {
										beforerender : function(thisobj) {
											thisobj.getStore().load();
										},
										selectionchange : function(thisobj,
												_selected, eOpts) {
											record = _selected[0];
											if (!Ext.isEmpty(record)) {
												Ext.Ajax
														.request({
															url : "admin/getDeployment.action",
															scope : this,
															success : function(
																	a) {
																// _w.getEl().unmask();
																var c = sconsole.util.Util
																		.decodeJSON(a.responseText);

																_f = this
																		.getDeploymentForm();
																_f
																		.loadRecord(record);

																_g = this
																		.getDeployResourcesGrid();

																_s = _g
																		.getStore();
																_s.removeAll();
																_s
																		.loadData(c.resources);
																_g1 = this
																		.getDeploymentProcsGrid();
																_s1 = _g1
																		.getStore();
																_s1.removeAll();
																_s1
																		.loadData(c.procs);
															},
															failure : function(
																	a) {
																console
																		.log(a.responseText);
																sconsole.util.Util
																		.showErrorMsg("Error");
															},
															params : {
																id : record
																		.get('id')
															},
															waitMsg : "waiting...."
														});
											} else {
												sconsole.util.Util
														.resetForm('adminDeployments #deploymentForm');
												_g = this
														.getDeployResourcesGrid();
												_s = _g.getStore();
												_s.removeAll();
												_g1 = this
														.getDeploymentProcsGrid();
												_s1 = _g1.getStore();
												_s1.removeAll();

											}
										}
									},
									"adminUsers #adminUserGrid" : {
										beforerender : function(thisobj) {
											thisobj.getStore().load();
										}
									},
									"adminGroups #adminGroupGrid" : {
										beforerender : function(thisobj) {
											thisobj.getStore().load();
										}
									},
									"adminDeployments #newBtn" : {
										click : function(thisobj) {
											_win = Ext
													.create('sconsole.view.admin.newDeployment');
											_win.show();
										}
									},
									"adminDeployments #deleteBtn" : {
										click : function(thisobj) {
											_g = this.getAdminDeployGrid();
											_chosens = _g.getSelectionModel()
													.getSelection();
											if (Ext.isEmpty(_chosens)) {
												Ext.Msg
														.alert('Waring',
																'Please select a row at first.');
												return;
											}
											record = _chosens[0];
											Ext.Ajax
													.request({
														url : "admin/prepareDeleteDeployment.action",
														scope : this,
														success : function(a) {
															var c = sconsole.util.Util
																	.decodeJSON(a.responseText);

															if (c.icnt == 0) {
																msg = 'No runtime process instances found for this deployment,Continue to delete?';
															} else {
																msg = ' Found <b>'
																		+ c.icnt
																		+ ' running process instances</b> for this deployment.<br/>Are you sure you want to go ahead?'
															}

															Ext.MessageBox
																	.confirm(
																			'Delete Deployment',
																			msg,
																			function(
																					bId,
																					text,
																					opt) {
																				if (bId == 'yes') {
																					Ext.Ajax
																							.request({
																								url : 'admin/deleteDeployment.action',
																								success : function(
																										response,
																										opts) {

																									var c = sconsole.util.Util
																											.decodeJSON(response.responseText);
																									if (c.success == 'false') {
																										sconsole.util.Util
																												.showErrorMsg(c.msg);
																										return;
																									} else {
																										sconsole.util.Util
																												.showInfoMsg(c.msg);
																										_g
																												.getStore()
																												.load();
																									}
																								},
																								failure : function(
																										response,
																										opts) {
																									sconsole.util.Util
																											.showErrorMsg(response.responseText);
																								},
																								params : {
																									id : record
																											.get('id')
																								}
																							});
																				}
																			},
																			{
																				scope : this
																			});
														},
														failure : function(a) {
															console
																	.log(a.responseText);
															sconsole.util.Util
																	.showErrorMsg("Error");
														},
														params : {
															id : record
																	.get('id')
														},
														waitMsg : "waiting...."
													});
										}
									},
									"activeProcess #activeProcsGrid" : {
										beforerender : function(thisobj) {
											thisobj.getStore().load();
										}
									},
									"suspendedProcess #suspendProcsGrid" : {
										beforerender : function(thisobj) {
											thisobj.getStore().load();
										}
									},
									"suspendedProcess #activateBtn" : {
										click : function(thisobj) {
											_g = this.getSuspendProcsGrid();
											var _chosens = _g
													.getSelectionModel()
													.getSelection();
											if (Ext.isEmpty(_chosens)) {
												Ext.Msg
														.alert('Waring',
																'Please select a row at first.');
												return;
											}

											_win = Ext
													.create('sconsole.view.admin.activateSuspendProc');
											_win.show();
										}
									},
									"activeProcess #suspendBtn" : {
										click : function(thisobj) {
											_g = this.getActiveProcsGrid();
											var _chosens = _g
													.getSelectionModel()
													.getSelection();
											if (Ext.isEmpty(_chosens)) {
												Ext.Msg
														.alert('Waring',
																'Please select a row at first.');
												return;
											}
											Ext.Ajax
													.request({
														url : 'admin/hasSuspendJobs.action',
														success : function(a) {
															// _w.getEl().unmask();
															var c = sconsole.util.Util
																	.decodeJSON(a.responseText);
															if (c.success == 'true') {
																if (c.jobs == 'false') {
																	_win = Ext
																			.create(
																					'sconsole.view.admin.suspendActiveProc',
																					{
																						pid : _chosens[0]
																								.get('id')
																					});
																	_win.show();
																} else {
																	sconsole.util.Util
																			.showErrorMsg('已经有人提交了挂起该流程的作业了！');
																}
															} else {
																sconsole.util.Util
																		.showErrorMsg(c.msg);
															}
														},
														failure : function(a) {
															console
																	.log(a.responseText);
															sconsole.util.Util
																	.showErrorMsg("Error");
														},
														params : {
															id : _chosens[0]
																	.get('id')
														},

													});

										}
									},
									"suspendedProcess #activateBtn" : {
										click : function(thisobj) {
											_g = this.getSuspendProcsGrid();
											var _chosens = _g
													.getSelectionModel()
													.getSelection();
											if (Ext.isEmpty(_chosens)) {
												Ext.Msg
														.alert('Waring',
																'Please select a row at first.');
												return;
											}
											Ext.Ajax
													.request({
														url : 'admin/hasActiveJobs.action',
														success : function(a) {
															// _w.getEl().unmask();
															var c = sconsole.util.Util
																	.decodeJSON(a.responseText);
															if (c.success == 'true') {
																if (c.jobs == 'false') {
																	_win = Ext
																			.create(
																					'sconsole.view.admin.activateSuspendProc',
																					{
																						pid : _chosens[0]
																								.get('id')
																					});
																	_win.show();
																} else {
																	sconsole.util.Util
																			.showErrorMsg('已经有人提交了激活该流程的作业了！');
																}
															} else {
																sconsole.util.Util
																		.showErrorMsg(c.msg);
															}
														},
														failure : function(a) {
															console
																	.log(a.responseText);
															sconsole.util.Util
																	.showErrorMsg("Error");
														},
														params : {
															id : _chosens[0]
																	.get('id')
														},

													});

										}
									},

									"datetimepicker #clearBtn" : {
										click : function(thisobj) {
											console.log('ddd');
										}
									},
									"suspendActiveProcWin #submitBtn" : {
										click : function(thisobj) {
											_win = thisobj.up('window');
											_form = thisobj.up('form');

											_radio = _form
													.down('#suspendTimeFld');
											_v = _radio.getValue().suspendTime;

											if (Ext.isEmpty(_v)) {
												sconsole.util.Util
														.showErrorMsg('请选择一种时间类型！');
												return;
											}

											if (Ext.String.startsWith(_v, '1')) {
												_t = _form
														.down('#suspendCustomTimeFld');
												if (Ext.isEmpty(_t.getValue())) {
													sconsole.util.Util
															.showErrorMsg('未给出具体时间！');
													return;
												}
											}

											if (_form.getForm().isValid()) {

												_form
														.submit({
															clientValidation : true,
															url : 'admin/suspendActiveProc.action',
															scope : this,
															params : {
																id : _win.pid
															},
															success : function(
																	form,
																	action) {
																var c = action.result;

																if (c.success == 'false') {
																	sconsole.util.Util
																			.showErrorMsg(c.msg);
																	_win
																			.close();
																	return;
																}
																sconsole.util.Util
																		.showInfoMsg(c.msg);
																_win.close();
																_grid = this
																		.getActiveProcsGrid();
																_grid
																		.getStore()
																		.load();
															},
															failure : sconsole.util.Util.ajaxFailure
														});

											}

										}
									},
									"activateSuspendProcWin #submitBtn" : {
										click : function(thisobj) {
											_win = thisobj.up('window');
											_form = thisobj.up('form');
											if (_form.getForm().isValid()) {

												_t01 = _form.down(
														'#activeTimeFld1')
														.getValue();
												_t02 = _form.down(
														'#activeTimeFld2')
														.getValue();

												if (!(_t02 || _t01)) {
													sconsole.util.Util
															.showErrorMsg('必须选择一个启动时间类型！');
													return;
												}
											//	console.log(_t01);
											//	console.log(_t02);
												if (_t01) {
													_t = 0;
												} else {
													_t = 1;
													_time = _form
															.down('#customeDTimeFld');
													if (Ext.isEmpty(_time
															.getValue())) {
														sconsole.util.Util
																.showErrorMsg('未给出具体时间！');
														return;
													}
												}

												_form
														.submit({
															clientValidation : true,
															url : 'admin/activeSuspendProc.action',
															scope : this,
															params : {
																id : _win.pid
															},
															success : function(
																	form,
																	action) {
																var c = action.result;

																if (c.success == 'false') {
																	sconsole.util.Util
																			.showErrorMsg(c.msg);
																	_win
																			.close();
																	return;
																}
																sconsole.util.Util
																		.showInfoMsg(c.msg);
																_win.close();
																_grid = this
																		.getSuspendProcsGrid();
																_grid
																		.getStore()
																		.load();
															},
															failure : sconsole.util.Util.ajaxFailure
														});

											}
										}
									},
									"jobsPanel #deleteBtn" : {
										click : function(thisobj) {
											_g = this.getJobsGrid();
											var _chosens = _g
													.getSelectionModel()
													.getSelection();
											if (Ext.isEmpty(_chosens)) {
												Ext.Msg
														.alert('Waring',
																'Please select a row at first.');
												return;
											}
											Ext.Ajax
													.request({
														url : 'admin/deleteJob.action',
														success : function(a) {
															// _w.getEl().unmask();
															var c = sconsole.util.Util
																	.decodeJSON(a.responseText);
															if (c.success == 'true') {
																sconsole.util.Util
																		.showInfoMsg(c.msg);
																_g.getStore()
																		.load();
															} else {
																sconsole.util.Util
																		.showErrorMsg(c.msg);
															}
														},
														failure : function(a) {
															console
																	.log(a.responseText);
															sconsole.util.Util
																	.showErrorMsg("Error");
														},
														params : {
															id : _chosens[0]
																	.get('id')
														},

													});

										}
									},
									"jobsPanel #executeBtn" : {
										click : function(thisobj) {
											_g = this.getJobsGrid();
											var _chosens = _g
													.getSelectionModel()
													.getSelection();
											if (Ext.isEmpty(_chosens)) {
												Ext.Msg
														.alert('Waring',
																'Please select a row at first.');
												return;
											}
											Ext.Ajax
													.request({
														url : 'admin/executeJob.action',
														success : function(a) {
															// _w.getEl().unmask();
															var c = sconsole.util.Util
																	.decodeJSON(a.responseText);
															if (c.success == 'true') {
																sconsole.util.Util
																		.showInfoMsg(c.msg);
																_g.getStore()
																		.load();
															} else {
																sconsole.util.Util
																		.showErrorMsg(c.msg);
															}
														},
														failure : function(a) {
															console
																	.log(a.responseText);
															sconsole.util.Util
																	.showErrorMsg("Error");
														},
														params : {
															id : _chosens[0]
																	.get('id')
														},

													});

										}
									},
									"activeInstancesPanel #activeDefinitionsGrid" : {
										beforerender : function(thisobj) {
											thisobj.getStore().load();
										}
									},
									"doneInstancesPanel #doneDefinitionsGrid" : {
										beforerender : function(thisobj) {
											thisobj.getStore().load();
										}
									},
									// "instanceDetailWIn #instTasksGrid" : {
									// beforerender : function(thisobj) {
									// _win = thisobj.up('window');
									// thisobj.getStore().load({
									// id : _win.instId
									// });
									// }
									// },
									// "instanceDetailWIn #instVarsGrid" : {
									// beforerender : function(thisobj) {
									// _win = thisobj.up('window');
									// thisobj.getStore().load({
									// id : _win.instId
									// });
									// }
									// },
									"instDetailPanel #tasksGrid" : {
										beforerender : function(thisobj) {
											_win = thisobj.up('window');
											thisobj.getStore().load({
												id : _win.instId
											});
										}
									},
									"instDetailPanel #varsGrid" : {
										beforerender : function(thisobj) {
											_win = thisobj.up('window');
											thisobj.getStore().load({
												params : {
													id : _win.instId,
													ty : _win.instType
												}
											});
										}
									},
									"instPanelWin" : {
										beforerender : function(thisobj) {

											_b = thisobj;
											_p = _b.down('#instPngPanel');
											_p.remove('procInstDetail');
											_p
													.add({
														xtype : 'box',
														itemId : 'procInstDetail',
														height : 500,
														autoEl : {
															tag : 'img',
															src : 'workflow/showProcInstPng.action?sid='
																	+ thisobj.instId
																	+ '&type='
																	+ thisobj.instType,
														}
													});
											// Ext.Ajax
											// .request({
											// url :
											// "workflow/getProcInst.action",
											// success : function(a) {
											//
											// var c = sconsole.util.Util
											// .decodeJSON(a.responseText);
											//
											// _b = thisobj;
											// _p = _b
											// .down('#instPngPanel');
											//
											// if (c.success == "0") {
											// _p
											// .remove('procInstDetail');
											// _p.height = Ext.Number
											// .from(
											// c.maxY,
											// 600);
											// _p
											// .insert(
											// 0,
											// {
											// xtype : 'box',
											// itemId : 'procInstDetail',
											// autoEl : {
											// tag : 'iframe',
											// name : 'instDetail',
											// src : c.url,
											// }
											// });
											//
											// } else if (c.success == "2") {
											// _p
											// .remove('procInstDetail');
											// _p.height = 200;
											// _p
											// .add([ {
											// xtype : 'box',
											// id : 'procInstDetail',
											// flex : 4,
											// autoEl : {
											// tag : 'iframe',
											// name : 'procdetail',
											// src : 'nodiagram.html',
											//
											// }
											// } ]);
											//
											// } else if (c.success == "1") {
											// _p
											// .remove('procInstDetail');
											// _p
											// .add({
											// xtype : 'box',
											// itemId : 'procInstDetail',
											// height : 500,
											// autoEl : {
											// tag : 'img',
											// src :
											// 'workflow/showProcPng.action?id='
											// + record
											// .get('sid'),
											// }
											// });
											// }
											// },
											// failure : function(a) {
											// console
											// .log(a.responseText);
											// sconsole.util.Util
											// .sendErrorMsg("Error");
											// },
											// params : {
											// id : thisobj.instId,
											// type: thisobj.instType
											// },
											// waitMsg : "waiting...."
											// });
										}
									}
								});
					}
				});