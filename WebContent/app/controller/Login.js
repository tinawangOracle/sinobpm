Ext
		.define(
				'sconsole.controller.Login',
				{
					extend : 'Ext.app.Controller',

					requires : [ 'sconsole.util.MD5', 'sconsole.util.Alert',
							'sconsole.view.MyViewport', 'sconsole.util.Util',
							'sconsole.view.comp.LinkButton',
							'sconsole.util.SessionMonitor',
							'sconsole.view.Translation', 'sconsole.util.Util',
							'sconsole.view.toolbar.chgNotifyPagingbar',
							'sconsole.store.baseStore',
							'sconsole.proxy.baseProxy' ],
					views : [ 'Login', 'LoginHeader',
							'authentication.CapsLockTooltip',
							'comp.LinkButton', 'regist' ],

					stores : [ 'LoginStates' ],
					refs : [ {
						ref : 'capslockTooltip',
						selector : 'capslocktooltip'
					}, {
						ref : 'mainviewport',
						selector : 'mainviewport'
					}, {
						ref : 'appViewport',
						selector : '#appViewport'
					} ],

					init : function(application) {
						this
								.control({
									// "login #loginBtn" : {
									// click : this.onButtonClickSubmit
									// },
									"login form button#submit" : {
										click : this.onButtonClickSubmit
									},
									"login form button#cancel" : {
										click : this.onButtonClickCancel
									},
									"login form textfield" : {
										specialkey : this.onTextfielSpecialKey
									},
									"login form textfield[name=password]" : {
										keypress : this.onTextfielKeyPress
									},
									"MyHeader button#logout" : {
										click : this.onButtonClickLogout
									},
									"login #registerBtn" : {
										click : function() {
											// _win = Ext
											// .create('sconsole.view.register');
											// _win.show();
											appViewport = this.getAppViewport();
											var _c = Ext.ComponentQuery
													.query('regist')[0];
											if (Ext.isEmpty(_c)) {
												appViewport.add({
													xtype : "regist",
													id : "register"
												});
												_c = Ext.ComponentQuery
														.query('regist')[0];
											}
											appViewport.getLayout()
													.setActiveItem(_c);
										}
									},
									"regist #loginBtn" : {
										click : this.gotoLoginPage
									},
									"regist #registerBtn" : {
										click : this.onRegisterSubmit
									}
								});

					},
					gotoLoginPage : function() {
						appViewport = this.getAppViewport();
						var _c = Ext.ComponentQuery.query('login')[0];
						if (Ext.isEmpty(_c)) {
							appViewport.add({
								xtype : "login",
								id : "loginscreen"
							});
							_c = Ext.ComponentQuery.query('login')[0];
						}
						appViewport.getLayout().setActiveItem(_c);
					},
					onRegisterSubmit : function(button, e, options) {
						var formPanel = button.up('form'), login = button
								.up('regist'), appViewport = button
								.up("#appViewport"), user = formPanel.down(
								'textfield[name=userEmail]').getValue(), pass = formPanel
								.down('textfield[name=userpwd]').getValue();

						_form = formPanel.getForm();
						if (_form.isValid()) {

							pass = sconsole.util.MD5.encode(pass);

							Ext.get(login.getEl()).mask(
									"Registering... Please wait...", 'loading');
							_form
									.submit({
										url : 'login/register.action',
										params : {
											newPass : pass,
										},
										success : function(form, action) {
											me = this;
											Ext.get(login.getEl()).unmask();

											// var result = sconsole.util.Util
											// .decodeJSON(conn.responseText);

											if (action.result.success == 'true') {
												me.gotoLoginPage();
											} else {
												sconsole.util.Util
														.showErrorMsg(action.result.msg);
											}
										},
										failure : function(conn, response,
												options, eOpts) {
											Ext.get(login.getEl()).unmask();
											sconsole.util.Util
													.showErrorMsg(conn.responseText);
										},
										scope : this
									});
						}
					},
					onButtonClickSubmit : function(button, e, options) {
						var formPanel = button.up('form'), login = button
								.up('login'), appViewport = button
								.up("#appViewport"), user = formPanel.down(
								'textfield[name=user]').getValue(), pass = formPanel
								.down('textfield[name=password]').getValue(), logins = formPanel
								.down('combobox[name=logins]').getValue();

						if (formPanel.getForm().isValid()) {

							// alert(pass);
							pass = sconsole.util.MD5.encode(pass);
							// alert(pass);
							Ext.get(login.getEl()).mask(
									"Authenticating... Please wait...",
									'loading');
							Ext.Ajax.request({
								url : 'login/login.action',
								params : {
									email : user,
									passwd : pass,
									logins : logins
								},
								success : function(conn, response, options,
										eOpts) {

									Ext.get(login.getEl()).unmask();

									var result = sconsole.util.Util
											.decodeJSON(conn.responseText);

									// alert(result.success);

									if (result.success == 'true') {
										// alert('true');
										var _c = Ext.ComponentQuery
												.query('mainviewport')[0];
										if (Ext.isEmpty(_c)) {
											appViewport.add({
												xtype : "mainviewport",
												id : "homescreen"
											});
											_c = Ext.ComponentQuery
													.query('mainviewport')[0];
										}
										appViewport.getLayout().setActiveItem(
												_c);
										sconsole.util.Util.changeMenu(
												'officeGloBtn', 2);
										sconsole.util.SessionMonitor.start();
										Ext.util.Cookies.set("email",
												result.email);
										var emailFld = Ext
												.getDom("welcomeContainer");
										emailFld.innerHTML = '<span></span>欢迎:'
												+ result.email;
									} else {
										// alert('false');
										sconsole.util.Util
												.showErrorMsg(result.msg);
									}
								},
								failure : function(conn, response, options,
										eOpts) {

									Ext.get(login.getEl()).unmask();

									sconsole.util.Util
											.showErrorMsg(conn.responseText);
								}
							});
						}
					},

					onButtonClickCancel : function(button, e, options) {
						button.up('form').getForm().reset();
					},

					onTextfielSpecialKey : function(field, e, options) {
						if (e.getKey() == e.ENTER) {
							var submitBtn = field.up('form').down(
									'button#submit');
							submitBtn.fireEvent('click', submitBtn, e, options);
						}
					},

					onTextfielKeyPress : function(field, e, options) {
						var charCode = e.getCharCode();

						if ((e.shiftKey && charCode >= 97 && charCode <= 122)
								|| (!e.shiftKey && charCode >= 65 && charCode <= 90)) {

							if (this.getCapslockTooltip() === undefined) {
								Ext.widget('capslocktooltip');
							}

							this.getCapslockTooltip().show();

						} else {

							if (this.getCapslockTooltip() !== undefined) {
								this.getCapslockTooltip().hide();
							}
						}
					},

					onButtonClickLogout : function(options) {
						fromWhere = 0;
						if (!Ext.isEmpty(options)) {
							// Ext.MessageBox.alert(options);
							fromWhere = options;
						}
						Ext.Ajax.request({
							// url :
							// 'http://localhost/masterextjs/php/logout.php',
							url : 'login/logout.action',
							success : function(conn, response, options, eOpts) {
								var result = sconsole.util.Util
										.decodeJSON(conn.responseText);
								if (result.success) {
									// Ext.ComponentQuery
									// .query('mainviewport')[0]
									// .destroy();
									if (fromWhere == 1) {
										Ext.MessageBox.alert("Info",
												"Your session has expired!",
												function(btn) {
													window.location.reload();
												});
									} else {
										window.location.reload();
										// var appViewport = Ext.ComponentQuery
										// .query("#appViewport")[0];
										// var _c = Ext.ComponentQuery
										// .query('login')[0];
										// appViewport.getLayout().setActiveItem(
										// _c);
										// _c.down('form').getForm().reset();
									}
								} else {
									sconsole.util.Util
											.showErrorMsg(conn.responseText);
								}
							},
							failure : function(conn, response, options, eOpts) {
								sconsole.util.Util
										.showErrorMsg(conn.responseText);
							}
						});
					}
				});