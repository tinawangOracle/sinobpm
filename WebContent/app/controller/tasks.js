Ext.define('sconsole.controller.tasks', {
	extend : 'Ext.app.Controller',
	refs : [ {
		ref : 'mainPanel',
		selector : 'mainpanel'
	}, {
		ref : 'todoJobsPanel',
		selector : 'todoJobsPanel'
	} ],
	// requires : [ 'sconsole.util.LinkButton' ],
	stores : [ 'tasks.todo', 'tasks.taskAttachments', 'tasks.subTasks',
			'tasks.done' ],
	views : [ 'tasks.todo', 'tasks.done' ],
	init : function(application) {
		this.control({
			"todoJobsPanel #taskAttachs #addBtn" : {
				click : function(thisobj) {
					_g = thisobj.up('todoJobsPanel').down('#todoJobsList');

					_chosens = _g.getSelectionModel().getSelection();
					console.log(_chosens[0].get('processInstanceId'));
					win = Ext.create('sconsole.view.tasks.uploadContent', {
						tid : _chosens[0].get('id'),
						sid : _chosens[0].get('processInstanceId')
					});
					win.show();
				}
			},
			"todoJobsPanel #todoJobDetail #resetBtn" : {
				click : function(thisobj) {
					_parent = thisobj.up('todoJobsPanel');
					_g = _parent.down('#todoJobsList');
					_chosens = _g.getSelectionModel().getSelection();

					if (_chosens.length == 0) {
						sconsole.util.Util.showInfoMsg("请先选择一个待办事宜！");
						return;
					}

					Ext.Ajax
							.request({
								url : "workflow/getTaskReset.action",
								success : function(a) {
									var c = sconsole.util.Util
											.decodeJSON(a.responseText);

									if (c.success == 'true') {
										sconsole.util.Util.showInfoMsg(c.msg);
										_parent = Ext.ComponentQuery
												.query('todoJobsPanel')[0];
										mcomp = _parent.down('#taskGeneral');
										// mcomp.setVisible(true);
										_v = mcomp.down('#taskDesc');
										_v.setValue(c.desc);
										_v = mcomp.down('#taskDue');
										_v.setValue(c.dueto);

										// form
										mcomp = _parent.down('#taskForm');
										mcomp.setVisible(true);
										sconsole.util.Util.openStartForm(mcomp,
												c.form);
									} else {
										sconsole.util.Util.showErrorMsg(c.msg);
									}

								},
								failure : function(response, opts) {
									console.log(response.status + ": "
											+ response.responseText);
								},
								params : {
									tid : _chosens[0].get('id')
								},
								waitMsg : "waiting...."
							});
				}

			},
			"todoJobsPanel #todoJobDetail #submitBtn" : {
				click : function(thisobj) {
					_parent = thisobj.up('todoJobsPanel');
					_g = _parent.down('#todoJobsList');
					_chosens = _g.getSelectionModel().getSelection();

					if (_chosens.length == 0) {
						sconsole.util.Util.showInfoMsg("请先选择一个待办事宜！");
						return;
					}
					_desc=_parent.down('#taskDesc');
					_f = _parent.down('#taskForm');
					_form = _f.getForm();
					if (_form.isValid()) {
						_form.submit({
							url : 'workflow/submitTask.action',
							timeout: 1800,
							success : function(form, action) {
								_ret = action.result;
								if (_ret.success == "true") {
									sconsole.util.Util.showInfoMsg(_ret.msg);
									_g.getStore().load();
									_g.getSelectionModel().deselectAll();

									// clear
									_parent = Ext.ComponentQuery
											.query('todoJobsPanel')[0];
									_mm = _parent.down('#todoJobDetail');
									_items = _mm.items;
									_items.each(function(item, idx, len) {
										item.setVisible(false);
									});

								} else {
									sconsole.util.Util.showErrorMsg(_ret.msg);
								}

							},
							failure : function(conn, response, options, eOpts) {
								sconsole.util.Util
										.showErrorMsg(conn.responseText);
							},
							params : {
								tid : _chosens[0].get('id'),
								sid : _chosens[0].get('processInstanceId'),
								desc: _desc.getValue()
							},
							scope : this
						});

					} else {
						sconsole.util.Util.showErrorMs('表单里有错误，修复错误后才能继续提交！');
					}

					// Ext.Ajax.request({
					// url : 'workflow/submitTask.action',
					// success : function(response, opts) {
					// _ret = sconsole.util.Util
					// .decodeJSON(response.responseText);
					// _g.getStore().load();
					//
					// },
					// failure : function(conn, response, options, eOpts) {
					// sconsole.util.Util.showErrorMsg(conn.responseText);
					// },
					// params : {
					// tid : _chosens[0].get('id'),
					// sid : _chosens[0].get('processInstanceId')
					// }
					// });
				}
			},
			"todoJobsPanel #taskAttachs #refreshBtn" : {
				click : function(thisobj) {
					_g = thisobj.up('todoJobsPanel').down('#todoJobsList');

					_chosens = _g.getSelectionModel().getSelection();
					Ext.Ajax.request({
						url : 'workflow/getTaskAttachments.action',
						success : function(response, opts) {
							_ret = sconsole.util.Util
									.decodeJSON(response.responseText);
							_g = Ext.ComponentQuery
									.query('todoJobsPanel #taskAttachs')[0];
							if (_ret.success == 'true') {
								_s = _g.getStore();
								_s.removeAll();
								_s.loadData(_ret.attachments);
							}

						},
						failure : function(conn, response, options, eOpts) {
							sconsole.util.Util.showErrorMsg(conn.responseText);
						},
						params : {
							tid : _chosens[0].get('id'),
							sid : _chosens[0].get('processInstanceId')
						}
					});
				}
			}
		});
	}
});