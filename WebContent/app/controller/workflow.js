Ext
		.define(
				'sconsole.controller.workflow',
				{
					extend : 'Ext.app.Controller',
					refs : [ {
						ref : 'modelGrid',
						selector : 'workflowModels #wfModelsGrid'
					}, {
						ref : 'instsGrid',
						selector : 'myInstancesPanel #instsList'
					}, {
						ref : 'myInstancesPanel',
						selector : 'myInstancesPanel'
					} ],
					views : [ 'workflow.copyModel', 'workflow.newModel',
							'workflow.startForm', 'workflow.myInsts',
							'tasks.uploadContent', 'workflow.importModel' ],
					stores : [ 'workflow.procs', 'workflow.myInsts',
							'workflow.tasks', 'workflow.vars' ],
					init : function(application) {
						this
								.control({
									"workflowModels #copyBtn" : {
										click : function() {

											_g = this.getModelGrid();
											var _chosens = _g
													.getSelectionModel()
													.getSelection();
											if (Ext.isEmpty(_chosens)) {
												Ext.Msg
														.alert('Waring',
																'Please select a row at first.');
												return;
											}
											win = Ext
													.create(
															'sconsole.view.workflow.copyModel',
															{
																mid : _chosens[0]
																		.get('id'),
																mname : _chosens[0]
																		.get('name')
															});
											_name = win.down('#modelName');
											_name.setValue(_chosens[0]
													.get('name')
													+ " 1");
											win.show();
										}
									},
									"workflowModels #importBtn" : {
										click : function() {
											win = Ext
													.create('sconsole.view.workflow.importModel');
											win.show();
										}
									},
									"workflowModels #exportBtn" : {
										click : function() {
											_g = this.getModelGrid();
											var _chosens = _g
													.getSelectionModel()
													.getSelection();
											if (Ext.isEmpty(_chosens)) {
												Ext.Msg
														.alert('Waring',
																'Please select a row at first.');
												return;
											}
											// console.log(_chosens[0].get('id'));
											var win = window
													.open(
															encodeURI("workflow/exportModel.action?id="
																	+ _chosens[0]
																			.get('id')),
															'newwindow',
															"height=100, width=400, toolbar=no, menubar=no, scrollbars=no, resizable=no, location=no, status=no");
											win.focus();

											// d =
											// Ext.DomHelper.append(document.body,
											// {
											// tag : "iframe",
											// frameBorder : 0,
											// width : 0,
											// height : 0,
											// css :
											// "display:none;visibility:hidden;height:0px;",
											// src :
											// encodeURI("workflow/exportModel.action?id="
											// + _chosens[0].get('id'))
											// }, true);
											// console.log(d);
											//					
										}
									},
									"workflowModels #deleteBtn" : {
										click : function() {
											_g = this.getModelGrid();
											var _chosens = _g
													.getSelectionModel()
													.getSelection();
											if (Ext.isEmpty(_chosens)) {
												Ext.Msg
														.alert('Waring',
																'Please select a row at first.');
												return;
											}
											Ext.MessageBox
													.confirm(
															'Delete Model',
															'Are you sure to delete this model?',
															function(bId, text,
																	opt) {
																if (bId == 'yes') {
																	Ext.Ajax
																			.request({
																				url : 'workflow/deleteModel.action',
																				success : function(
																						response,
																						opts) {

																					var c = sconsole.util.Util
																							.decodeJSON(response.responseText);
																					if (c.success == 'false') {
																						sconsole.util.Util
																								.showErrorMsg(c.msg);
																						return;
																					} else {
																						sconsole.util.Util
																								.showInfoMsg(c.msg);
																						_g
																								.getStore()
																								.load();
																					}
																				},
																				failure : function(
																						response,
																						opts) {
																					sconsole.util.Util
																							.showErrorMsg(response.responseText);
																				},
																				params : {
																					id : _chosens[0]
																							.get('id')
																				}
																			});
																}
															}, {
																obj : this
															});

										}
									},
									"workflowModels #newBtn" : {
										click : function() {
											win = Ext
													.create('sconsole.view.workflow.newModel');
											win.show();
										}
									},
									"copyModelWin #submitBtn" : {
										click : function(thisobj) {
											formPanel = thisobj.up("form");
											_win = formPanel.up('copyModelWin');
											_form = formPanel.getForm();
											if (_form.isValid()) {

												Ext.Ajax
														.request({
															url : 'workflow/copyModel.action',
															success : function(
																	response,
																	opts) {
																try {
																	var c = Ext.JSON
																			.decode(response.responseText);
																} catch (err) {
																	Ext.Msg
																			.alert(
																					'Status',
																					'Copy succeed.');
																	return;
																}
																if (c.success == 'false') {
																	sconsole.util.Util
																			.showErrorMsg(c.msg);
																	return;
																} else {
																	Ext.Msg
																			.show({
																				title : 'Info',
																				msg : c.msg,
																				icon : Ext.Msg.INFO,
																				buttons : Ext.Msg.OK,
																				fn : function() {
																					_win = Ext.ComponentQuery
																							.query('copyModelWin')[0];
																					_win
																							.close();
																					_g = Ext.ComponentQuery
																							.query('workflowModels #wfModelsGrid')[0];
																					_g
																							.getStore()
																							.load();
																				}
																			});
																}
															},
															failure : sconsole.util.Util.ajaxFailure,
															params : {
																id : _win.mid,
																name : formPanel
																		.down(
																				'#modelName')
																		.getValue(),
																desc : formPanel
																		.down(
																				'#modelDesc')
																		.getValue(),
															}
														});

											} else {
												sconsole.util.Util
														.showErrorMsg("表单有错误！");
											}
										}
									},
									"copyModelWin #cancelBtn" : {
										click : function() {
											win = Ext.ComponentQuery
													.query('copyModelWin');
											if (!Ext.isEmpty(win)) {
												win[0].close();
											}
										}
									},
									"newModelWin #submitBtn" : {
										click : function(thisobj) {
											formPanel = thisobj.up("form");
											_win = formPanel.up('newModelWin');
											_form = formPanel.getForm();
											if (_form.isValid()) {

												Ext.Ajax
														.request({
															url : 'workflow/newModel.action',
															success : function(
																	response,
																	opts) {
																try {
																	var c = Ext.JSON
																			.decode(response.responseText);
																} catch (err) {
																	Ext.Msg
																			.alert(
																					'Status',
																					'Copy succeed.');
																	return;
																}
																if (c.success == 'false') {
																	sconsole.util.Util
																			.showErrorMsg(c.msg);
																	return;
																} else {
																	Ext.Msg
																			.show({
																				title : 'Info',
																				msg : c.msg,
																				icon : Ext.Msg.INFO,
																				buttons : Ext.Msg.OK,
																				fn : function() {
																					_win = Ext.ComponentQuery
																							.query('newModelWin')[0];
																					_win
																							.close();
																					_g = Ext.ComponentQuery
																							.query('workflowModels #wfModelsGrid')[0];
																					_g
																							.getStore()
																							.load();
																				}
																			});
																}
															},
															failure : sconsole.util.Util.ajaxFailure,
															params : {
																name : formPanel
																		.down(
																				'#modelName')
																		.getValue(),
																desc : formPanel
																		.down(
																				'#modelDesc')
																		.getValue(),
															}
														});

											} else {
												sconsole.util.Util
														.showErrorMsg("表单有错误！");
											}
										}
									},
									"newModelWin #cancelBtn" : {
										click : function() {
											alert('test');
											win = Ext.ComponentQuery
													.query('newModelWin');
											if (!Ext.isEmpty(win)) {
												win[0].close();
											}
										}
									},
									"workflowProcdefs #startBtn" : {
										click : function() {
											_g = Ext.ComponentQuery
													.query("workflowProcdefs #procsList")[0];
											_m = _g.getSelectionModel();
											_rec = _m.getSelection()[0];
											if (Ext.isEmpty(_rec)) {
												sconsole.util.Util
														.showInfoMsg('Please select a row at first.');
												return;
											}
											Ext.Ajax
													.request({
														url : "workflow/startProc.action",
														success : function(a) {
															try {
																var c = Ext
																		.decode(a.responseText);
																if (c.success == "true") {
																	console
																			.log(c.msg);
																	if (c.mode == "0")
																		alert(c.msg);
																	else {
																		_win = Ext
																				.create('sconsole.view.workflow.startForm');
																		_win.pid = _rec
																				.get('id');
																		_win
																				.show();
																		mcomp = Ext.ComponentQuery
																				.query('procStartForm #startPropForm')[0];
																		sconsole.util.Util
																				.openStartForm(
																						mcomp,
																						c.records);
																	}
																}

															} catch (err) {
																console
																		.log(err);
															}
														},
														failure : function(a) {
															console
																	.log(a.responseText);
															sconsole.util.Util
																	.sendErrorMsg();
														},
														params : {
															pid : _rec
																	.get('id')
														},
														waitMsg : "waiting...."
													});
										}
									},
									"myInstancesPanel #delBtn" : {
										click : function(thisobj) {
											_g = this.getInstsGrid();
											var _chosens = _g
													.getSelectionModel()
													.getSelection();
											if (Ext.isEmpty(_chosens)) {
												Ext.Msg
														.alert('Waring',
																'Please select a row at first.');
												return;
											}
											Ext.MessageBox
													.confirm(
															'Delete Instance',
															'您确定要删除流程实例吗？\n此操作会删除和流程实例相关的任务和数据。',
															function(bId, text,
																	opt) {
																if (bId == 'yes') {
																	Ext
																			.get(
																					_g
																							.getEl())
																			.mask(
																					'Deleting...',
																					'Waiting');
																	Ext.Ajax
																			.request({
																				url : 'workflow/deleteProcInst.action',
																				success : function(
																						response,
																						opts) {
																					Ext
																							.get(
																									_g
																											.getEl())
																							.unmask();
																					var c = sconsole.util.Util
																							.decodeJSON(response.responseText);

																					if (c.success == 'false') {
																						sconsole.util.Util
																								.showErrorMsg(c.msg);
																						return;
																					} else {
																						Ext.Msg
																								.alert(
																										'Status',
																										c.msg);
																						_g
																								.getStore()
																								.load();

																						_b = Ext.ComponentQuery
																								.query('myInstancesPanel #myInstance')[0];
																						_p = _b
																								.down('#instPngPanel');
																						_p
																								.removeAll();
																						_g = _b
																								.down('#tasksGrid');
																						_g
																								.getStore()
																								.removeAll();
																						_g1 = _b
																								.down('#varsGrid');
																						_s1 = _g1
																								.getStore();
																						_s1
																								.removeAll();
																					}
																				},
																				failure : function(
																						response,
																						opts) {
																					Ext
																							.get(
																									_g
																											.getEl())
																							.unmask();
																					sconsole.util.Util
																							.showErrorMsg(response.responseText);
																				},
																				params : {
																					id : _chosens[0]
																							.get('id')
																				}
																			});
																}
															}, {
																obj : this
															});
										}
									},
									"myInstancesPanel #truncateBtn" : {
										click : function(thisobj) {
											_g = this.getInstsGrid();
											_f = this.getMyInstancesPanel();
											Ext.MessageBox
													.confirm(
															'Delete Instance',
															'您确定要删除所有流程实例吗？\n此操作会删除和流程实例相关的任务和数据。',
															function(bId, text,
																	opt) {
																if (bId == 'yes') {
																	Ext
																			.get(
																					_f
																							.getEl())
																			.mask(
																					'Truncating',
																					'Waiting');
																	Ext.Ajax
																			.request({
																				url : 'workflow/truncateProcInsts.action',
																				timeout : 0,
																				success : function(
																						response,
																						opts) {
																					Ext
																							.get(
																									_f
																											.getEl())
																							.unmask();
																					var c = sconsole.util.Util
																							.decodeJSON(response.responseText);

																					if (c.success == 'false') {
																						sconsole.util.Util
																								.showErrorMsg(c.msg);
																						return;
																					} else {
																						Ext.Msg
																								.alert(
																										'Status',
																										c.msg);
																						_g
																								.getStore()
																								.load();
																					}
																				},
																				failure : function(
																						response,
																						opts) {
																					Ext
																							.get(
																									_f
																											.getEl())
																							.unmask();
																					sconsole.util.Util
																							.showErrorMsg(response.responseText);
																				}
																			});
																}
															}, {
																obj : this
															});
										}
									},
									"workflowProcdefs #procsList" : {
										beforerender : function(thisobj) {
											thisobj.getStore().load();
										}
									},
									"myInstancesPanel #instsList" : {
										beforerender : function(thisobj) {
											thisobj.getStore().load();
										}
									},
									"workflowModels #wfModelsGrid" : {
										beforerender : function(thisobj) {
											thisobj.getStore().load();
										}
									}
								});
					}
				});