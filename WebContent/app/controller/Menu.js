Ext.define('sconsole.controller.Menu', {
	extend : 'Ext.app.Controller',
	models : [ 'menu.Root', 'menu.Item' ], // , 'menu.node'
	views : [ 'menu.Accordion', 'menu.Item', 'workflow.models',
			'workflow.procdef', 'mail.MailContainer', 'user.changePwd',
			'user.editProfile', 'admin.users', 'admin.groups' ],
	stores : [ 'MenuUser', 'User', 'MenuWorkflow', 'MenuAdmin', 'MenuOffice',
			'workflow.models', 'MenuDesktop', 'mail.MailMessages',
			'mail.MailMenu', 'admin.users', 'admin.groups', 'admin.groupUsers',
			'admin.userGroups' ],
	refs : [ {
		ref : 'mainPanel',
		selector : 'mainpanel'
	} ],
	requires : [ 'sconsole.util.Util' ],

	menuAction : function(thisobj, record, item, index, e, eOpts) {

		var mainPanel = this.getMainPanel();

		var newTab = mainPanel.items.findBy(function(tab) {
			return tab.title === record.get('text');
		});
		//console.log(record.raw.className);
		if (!newTab) {
			newTab = mainPanel.add({
				xtype : record.raw.className,
				closable : true,
				// iconCls: record.get('iconCls'),
				title : record.get('text')
			});
		}

		mainPanel.setActiveTab(newTab);

	},
	init : function(application) {
		this.control({
			"mainmenuitem" : {
				itemclick : this.menuAction
			}
		});
	}
});
