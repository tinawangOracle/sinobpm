Ext.define('sconsole.proxy.baseProxy', {
	extend : 'Ext.data.proxy.Ajax',
	alias : 'proxy.baseProxy',
	//type : 'ajax',
//    constructor: function(config) { 
//        this.callParent(arguments); 
//        this.setExtraParam('tcnt', 0);
//    } ,
	listeners : {
		exception : function(proxy, response, operation) {
			Ext.MessageBox.show({
				title : 'REMOTE EXCEPTION',
				msg : response.responseText,// operation.getError(),
				icon : Ext.MessageBox.ERROR,
				buttons : Ext.Msg.OK
			});
		}
	}
});