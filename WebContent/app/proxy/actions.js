Ext.define('sconsole.proxy.actions', {
	extend : 'Ext.data.proxy.Ajax',
	alias : 'proxy.actions',
	api : {
		read : 'common/actionView.action'
	},
	type : 'ajax',
	timeout : 0,
	reader : {
		type : 'json',
		totalProperty : 'totalCount',
		idProperty : 'dummy',
		root : 'records',
	},
//	autoLoad:false
});