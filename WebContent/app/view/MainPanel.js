Ext.define('sconsole.view.MainPanel', {
	extend : 'Ext.tab.Panel',
	alias : 'widget.mainpanel',
	activeTab : 0,
	plugins : [Ext.create('Ext.ux.TabCloseMenu')],
	items : [ {
		xtype : 'panel',
		closable : false,
		title : translations.homePage,
		layout : 'fit',
		
		items : [ {
			xtype : 'treepanel',
			store : 'MenuDesktop',
			itemId : 'mytree',
			useArrows : true,
			rootVisible : false,
			listeners : {
				afterrender : function(thisobj) {

				}
			}
		} ]
	} ]
});