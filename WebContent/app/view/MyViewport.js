Ext
		.define(
				'sconsole.view.MyViewport',
				{
					// extend: 'Ext.container.Viewport',
					
					
					extend : 'Ext.panel.Panel',
					alias : 'widget.mainviewport',

					requires : [ 'sconsole.view.MyHeader',
							'sconsole.view.MainPanel',
							'sconsole.view.menu.Accordion' ],

					layout : {
						type : 'border'
					},

					items : [
							{
								xtype : 'MyHeader',
								region : 'north',
								split: true,
							},
							{
								xtype:'mainmenu',
								region : 'west',
								split: true,
							},
							{
								xtype : 'mainpanel',
								region : 'center'
							},
							{
								xtype : 'container',
								region : 'south',
								split: true,
								height : 30,
								style : 'border-top: 1px solid #4c72a4;',
								html : '<div id="titleHeader"><center><span style="font-size:10px;">Copyright 2010-2014 © Oracle GE. All rights reserved.</span></center></div>'
							} ]
				});