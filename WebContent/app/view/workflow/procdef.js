Ext
		.define(
				'sconsole.view.workflow.procdef',
				{
					extend : 'Ext.panel.Panel',
					alias : 'widget.workflowProcdefs',
					title : '已部署的流程定义',
					layout : {
						type : 'border'
					},
					items : [
							{
								region : 'west',
								split : true,
								xtype : 'grid',
								itemId : 'procsList',
								store : 'workflow.procs',
								flex : 2,
								dockedItems : [ {
									xtype : 'chgNotifyPagingbar',
									dock : 'top',
									displayInfo : false,
									store : 'workflow.procs',
								} ],
								columns : [ {
									header : "name",
									dataIndex : "name",
									resizable : true,
									flex : 1,
									renderer : function(val, meta, rec, ridx) {
										if (Ext.isEmpty(val))
											return rec.get('key');
										else
											return val;
									}

								} ],
								listeners : { // step 1
									beforerender : function(thisobj) {
										thisobj.getStore().load();
									},
									selectionchange : function(thisobj,
											selected, eOpts) {
										// console.log(record.get('id'));
										record = selected[0];
										_p = Ext.ComponentQuery
												.query('workflowProcdefs #myProcess')[0];
										if (Ext.isEmpty(record)) {
											_p.setTitle('');
											_p.removeAll();
											return;
										}

										Ext.Ajax
												.request({
													url : "workflow/showProc.action",
													success : function(a) {
														// _w.getEl().unmask();
														var c = Ext
																.decode(a.responseText);

														_p
																.setTitle(c.pname
																		+ ' [版本'
																		+ c.pver
																		+ ', 部署于'
																		+ c.ptime
																		+ ']');
														_b = Ext.ComponentQuery
																.query('workflowProcdefs #myProcess #editBtn')[0];
														if (c.editable == 'true')
															_b
																	.setDisabled(false);
														else
															_b
																	.setDisabled(true);
														if (c.success == "0") {
															_p.removeAll();
															_p
																	.add({
																		xtype : 'box',
																		itemId : 'procdetail',
																		autoEl : {
																			tag : 'iframe',
																			name : 'procdetail',
																			src : c.url
																		// 'diagram-viewer/index.html?processDefinitionId='
																		// +
																		// record
																		// .get('id'),
																		}
																	});
															// window.frames["procdetail"].location.href
															// =
															// 'diagram-viewer/index.html?processDefinitionId='
															// + record
															// .get('id');
														} else if (c.success == "2") {
															_p.removeAll();
															_p
																	.add([ {
																		xtype : 'box',
																		itemId : 'procdetail',
																		flex : 4,
																		autoEl : {
																			tag : 'iframe',
																			name : 'procdetail',
																			src : 'nodiagram.html',

																		}
																	} ]);

															// window.frames["procdetail"].location.href
															// =
															// 'nodiagram.html';
														} else if (c.success == "1") {
															_p.removeAll();
															_p
																	.add({
																		xtype : 'box',
																		itemId : 'procdetail',
																		autoEl : {
																			tag : 'img',
																			src : 'workflow/showProcPng.action?id='
																					+ record
																							.get('id'),
																		}
																	});
														}
													},
													failure : function(a) {
														console
																.log(a.responseText);
														sconsole.util.Util
																.sendErrorMsg("Error");
													},
													params : {
														id : record.get('id')
													},
													waitMsg : "waiting...."
												});

									}
								},
							// itemSelector : 'div.selector',
							// tpl : [ // step five
							// '<tpl for=".">',
							// '<div class="selector">{name}</div>',
							// '</tpl>' ].join(''),
							//								
							// listeners : {
							// itemclick : function(view, rec, item,
							// index, event, opts) {
							// Ext.Msg
							// .alert('Ext JS', rec
							// .get('name'));
							// }
							// }
							}, {
								xtype : 'panel',
								region : 'center',
								dockedItems : [ {
									xtype : 'toolbar',
									dock : 'top',
									items : [ {
										text : 'start',
										itemId : 'startBtn'
									}, '-', {
										text : 'Change to Editable Model',
										itemId : 'editBtn',
										disabled : true
									}, '-', {
										text : 'Open In New Window'
									}, '-' ]
								} ],
								itemId : 'myProcess',
								icon : 'resources/icons/process-50.png',
								layout : 'fit',
								items : [],
								flex : 5
							} ],

				});