Ext.define('sconsole.view.workflow.copyModel', {
	extend : 'Ext.window.Window',
	alias : 'widget.copyModelWin',
	title : 'Copy Model',
	layout : {
		type : 'vbox',
		align : 'stretch'
	},
	mid : -1,
	mname : '',
	width : 400,
	height : 200,
	items : [ {
		xtype : 'form',
		defaults : {
			xtype : 'textfield',
			margin : 10,
		},
		flex : 1,
		items : [ {
			fieldLabel : 'Name',
			itemId : 'modelName',
			name : 'newname',
			allowBlank : false
		}, {
			fieldLabel : 'Description',
			itemId : 'modelDesc',
			name : 'desc'
		} ],
		dockedItems : [ {
			xtype : 'toolbar',
			dock : 'bottom',
			items : [ {
				text : 'Submit',
				itemId : 'submitBtn'
			}, '-', {
				text : 'Cancel',
				itemId : 'cancelBtn',

			} ]
		} ],

	} ],
	listeners : {
		beforerender : function(thisobj) {
			console.log(thisobj.mid);
			// _name = Ext.ComponentQuery.query('copyModelWin #modelName')[0];
			// _name.setValue(this.mname);
		}
	}
});