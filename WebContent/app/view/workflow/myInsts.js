Ext
		.define(
				'sconsole.view.workflow.myInsts',
				{
					extend : 'Ext.panel.Panel',
					alias : 'widget.myInstancesPanel',
					title : '我的流程',
					layout : {
						type : 'border'
					},
					items : [
							{
								region : 'west',
								split : true,
								xtype : 'grid',
								itemId : 'instsList',
								store : 'workflow.myInsts',
								flex : 2,
								dockedItems : [ {
									xtype : 'chgNotifyPagingbar',
									dock : 'top',
									displayInfo : false,
									store : 'workflow.myInsts',
								} ],
								columns : [ {
									header : "name",
									dataIndex : "name",
									resizable : true,
									flex : 1,
									renderer : function(val, meta, rec, ridx) {
										if (Ext.isEmpty(val))
											return rec.get('key');
										else
											return val;
									}

								} ],
								listeners : { // step 1
									selectionchange : function(thisobj,
											_selected, eOpts) {
										record = _selected[0];

										_b = Ext.ComponentQuery
												.query('myInstancesPanel #myInstance')[0];
										_g = _b.down('#tasksGrid');
										_s = _g.getStore();
										_s.removeAll();
										_g1 = _b.down('#varsGrid');
										_s1 = _g1.getStore();
										_s1.removeAll();
										_p = _b.down('#instPngPanel');
										_p.remove('procInstDetail');
										
										if (Ext.isEmpty(record)) {
											return;
										}
										// console.log(record.get('id'));

										Ext.Ajax
												.request({
													url : "workflow/getProcInst.action",
													success : function(a) {
														// _w.getEl().unmask();
														var c = sconsole.util.Util
																.decodeJSON(a.responseText);
														_b = Ext.ComponentQuery
																.query('myInstancesPanel #myInstance')[0];
														_p = _b
																.down('#instPngPanel');
														_g = _b
																.down('#tasksGrid');
														_s = _g.getStore();
														_s.removeAll();
														_s.loadData(c.tasks);
														_g1 = _b
																.down('#varsGrid');
														_s1 = _g1.getStore();
														_s1.removeAll();
														_s1.loadData(c.vars);
														// _p
														// .setTitle(c.pname
														// + ' [版本'
														// + c.pver
														// + ', 部署于'
														// + c.ptime
														// + ']');

														if (c.success == "0") {
															// alert(c.maxY);
															_p
																	.remove('procInstDetail');
															_p.height = Ext.Number
																	.from(
																			c.maxY,
																			600);
															_p
																	.insert(
																			0,
																			{
																				xtype : 'box',
																				itemId : 'procInstDetail',
																				// height
																				// :
																				// c.maxY,
																				autoEl : {
																					tag : 'iframe',
																					name : 'instDetail',
																					src : c.url,
																				}
																			});

														} else if (c.success == "2") {
															_p
																	.remove('procInstDetail');
															_p.height = 200;
															_p
																	.add([ {
																		xtype : 'box',
																		id : 'procInstDetail',
																		flex : 4,
																		autoEl : {
																			tag : 'iframe',
																			name : 'procdetail',
																			src : 'nodiagram.html',

																		}
																	} ]);

														} else if (c.success == "1") {
															_p
																	.remove('procInstDetail');
															_p
																	.add({
																		xtype : 'box',
																		itemId : 'procInstDetail',
																		height : 500,
																		autoEl : {
																			tag : 'img',
																			src : 'workflow/showProcPng.action?id='
																					+ record
																							.get('sid'),
																		}
																	});
														}
													},
													failure : function(a) {
														console
																.log(a.responseText);
														sconsole.util.Util
																.sendErrorMsg("Error");
													},
													params : {
														id : record.get('id')
													},
													waitMsg : "waiting...."
												});
									}
								},
							}, {
								xtype : 'panel',
								region : 'center',
								dockedItems : [ {
									xtype : 'toolbar',
									dock : 'top',
									items : [ {
										text : 'Delete',
										itemId : 'delBtn',
									// disabled : true
									}, '-', {
										text : 'Delete All',
										itemId : 'truncateBtn'
									}, '-' ]
								} ],
								flex : 5,
								itemId : 'myInstance',
								icon : 'resources/icons/process-50.png',
								layout : 'fit',
								autoScroll : true,
								items : [ {
									xtype : 'panel',
									layout : {
										xtype : 'vbox',
										align : 'stretch'
									},
									defaults : {
										margin : 10
									},
									autoScroll : true,
									items : [ {
										xtype : 'grid',
										store : 'workflow.tasks',
										itemId : 'tasksGrid',
										emptyText : 'No tasks',
										title : 'Tasks',
										columns : [ {
											header : "name",
											dataIndex : "name",
											flex : 1
										}, {
											header : "Priority",
											dataIndex : "priority",
											flex : 1
										}, {
											header : "Assignee",
											dataIndex : "assignee",
											flex : 1,
										}, {
											header : "Due Date",
											dataIndex : "dueDate",
											flex : 1
										}, {
											header : "Start Date",
											dataIndex : "startTime",
											flex : 1
										}, {
											header : "End Date",
											dataIndex : "endTime",
											flex : 1
										} ],
										flex : 1
									}, {
										xtype : 'grid',
										store : 'workflow.vars',
										itemId : 'varsGrid',
										title : 'Variables',
										emptyText : 'No Vairables',
										columns : [ {
											header : "name",
											dataIndex : "name",
											flex : 1
										}, {
											header : "Value",
											dataIndex : "value",
											emptyText : 'No variables',
											flex : 2
										} ],
										flex : 1
									}, {
										xtype : 'panel',
										layout : 'fit',
										itemId : 'instPngPanel',
										height : 500
									} ],
								} ]
							} ],

				});