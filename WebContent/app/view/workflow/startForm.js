Ext
		.define(
				'sconsole.view.workflow.startForm',
				{
					extend : 'Ext.window.Window',
					alias : 'widget.procStartForm',
					title : "Start Process",
					layout : {
						type : 'vbox',
						align : 'stretch'
					},
					width : 640,
					height : 350,
					pid : -1,
					items : [ {
						xtype : 'form',
						itemId : 'startPropForm',
						autoScroll : true,
						defaults : {
							labelWidth : 100,
							labelAlign : 'right',
							margin : '10 20 3 10' // (top, right, bottom,
						// left).
						},
						flex : 1,
						items : [],
						dockedItems : [ {
							xtype : 'toolbar',
							dock : 'top',
							// ui : 'footer',
							items : [
									{
										text : 'Start',
										itemId : 'startBtn',
										handler : function(thisobj) {
											_win = thisobj.up("procStartForm");
											_form = thisobj
													.up("#startPropForm")
													.getForm();
											if (_form.isValid()) {
												_form
														.submit({
															url : 'workflow/startProcWithForm.action',
															params : {
																pid : _win.pid
															},
															success : function(
																	form,
																	action) {
																result = action.result;
																if (!result.success
																		|| result.success == "false") {
																	sconsole.util.Util
																			.showErrorMsg(result.msg);
																} else {
																	sconsole.util.Util
																			.showInfoMsg(result.msg);
																	_win
																			.close();
																}
															},
															failure : function(
																	form,
																	action) {
																sconsole.util.Util
																		.showErrorMsg(action.result.msg);
															}
														});
											}

										}
									}, '-', {
										text : 'Cancel',
										itemId : 'cancelBtn',
										handler : function(thisobj) {
											_win = thisobj.up("procStartForm");
											_win.close();
										}
									} ]
						} ]
					} ]
				});