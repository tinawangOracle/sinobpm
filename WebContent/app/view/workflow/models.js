Ext.define('sconsole.view.workflow.models', {
	extend : 'Ext.panel.Panel',
	alias : 'widget.workflowModels',
	title : 'Workflow Models',
	layout : {
		type : 'hbox',
		align : 'stretch'
	},
	items : [ {
		xtype : 'grid',
		store : 'workflow.models',
		flex : 1,
		itemId : 'wfModelsGrid',
		dockedItems : [ {
			xtype : 'chgNotifyPagingbar',
			dock : 'top',
			displayInfo : true,
			store : 'workflow.models',
			items : [ '-', {
				xtype : 'button',
				text : 'New',
				itemId : 'newBtn'
			}, '-', {
				text : 'Copy',
				itemId : 'copyBtn'
			}, '-', {
				text : 'Delete',
				itemId : 'deleteBtn'
			}, '-', {
				text : 'Export',
				itemId : 'exportBtn'
			}, '-', {
				text : 'Import',
				itemId : 'importBtn'
			} ]
		} ],
		columns : [
				{
					xtype : 'actioncolumn',
					items : [ {
						icon : 'resources/icons/advanced.png',
						tooltip : 'edit',
						handler : function(_g, rowIdx, colIdx) {
							_rec = _g.getStore().getAt(rowIdx);
							window.open('service/editor?id=' + _rec.get('id'),
									'_blank');
						}
					} ]
				},
				{
					xtype : 'actioncolumn',
					items : [ {
						icon : 'resources/icons/advanced.png',
						tooltip : 'Deploy',
						handler : function(_g, rowIdx, colIdx) {
							_rec = _g.getStore().getAt(rowIdx);
							_w = Ext.ComponentQuery.query('workflowModels')[0];
							if (!Ext.isEmpty(_w))
								_w.getEl().mask('Deploying...',
										'x-mask-loading');
							Ext.Ajax.request({
								url : "workflow/deployModel.action",
								success : function(a) {
									if (!Ext.isEmpty(_w))
										_w.getEl().unmask();
									var c = Ext.decode(a.responseText);
									if (c.success = "true") {
										Ext.Msg.show({
											title : 'Info',
											msg : c.msg,
											icon : Ext.Msg.INFO,
											buttons : Ext.Msg.OK,
											fn : function() {
												sconsole.util.Util.openTab(
														'workflowProcdefs',
														'已部署的流程定义');
											}
										});
									} else {
										sconsole.util.Util.showErrorMsg(c.msg);
									}
								},
								failure : function(a) {
									if (!Ext.isEmpty(_w))
										_w.getEl().unmask();
									console.log(a.responseText);
									sconsole.util.Util.sendErrorMsg();
								},
								params : {
									id : _rec.get('id'),
									name : _rec.get('name')
								},
								waitMsg : "waiting...."
							});
						}
					} ]
				},
				{
					header : "ID",
					dataIndex : "id",
					resizable : true,
					flex : 1,
				},
				{
					header : "name",
					dataIndex : "name",
					resizable : true,
					flex : 1,
				},
				{
					header : "Description",
					dataIndex : "info",
					resizable : true,
					flex : 1,
					renderer : function(data, metadata, record, rowIndex,
							columnIndex, store) {
						_d = Ext.JSON.decode(data);
						return _d.description;
					}
				}, {
					header : "Version",
					dataIndex : "version",
					resizable : true,
					flex : 1,
				}, {
					header : "Created Date",
					dataIndex : "cT",
					resizable : true,
					flex : 1,
				}, {
					header : "Last Updated Time",
					dataIndex : "lUT",
					resizable : true,
					flex : 1,
				} ]
	} ]
});