Ext
		.define(
				'sconsole.view.workflow.importModel',
				{
					extend : 'Ext.window.Window',
					alias : 'widget.importModelForm',
					title : "Import Model",
					layout : {
						type : 'vbox',
						align : 'stretch',
						margin : 10
					},
					width : 600,
					height : 150,
					items : [ {
						xtype : 'form',
						flex : 1,
						defaults : {
							labelWidth : 100,
							labelAlign : 'right',
							margin : '10 20 3 10' // (top, right, bottom,
						// left).
						},
						dockedItems : [ {
							xtype : 'toolbar',
							dock : 'top',
							items : [
									{
										text : 'OK',
										handler : function(thisobj) {
											var form = thisobj.up('form')
													.getForm();
											var _p = thisobj
													.up('importModelForm');

											if (form.isValid()) {
												form
														.submit({
															url : 'workflow/deployUploadedFile.action',
															waitMsg : 'Importing model...',
															success : function(
																	fp, o) {
																_ret = o.result;
																if (_ret.success == 'true') {
																	sconsole.util.Util
																			.showInfoMsg(_ret.msg);
																	_g = Ext.ComponentQuery
																			.query('workflowModels #wfModelsGrid')[0];
																	if (Ext
																			.isEmpty(_g))
																		return;
																	_g
																			.getStore()
																			.load();
																} else {
																	sconsole.util.Util
																			.showErrorMsg(o.result.msg);
																}
																_p.close();
															},
															failure : function(
																	fp, o) {
																sconsole.util.Util
																		.showErrorMsg(o.result.msg);
																_p.close();
															}
														});
											}
										}
									},
									'-',
									{
										text : 'Cancel',
										handler : function(thisobj) {
											_win = thisobj
													.up("importModelForm");
											_win.close();
										}
									} ]
						} ],
						items : [ {
							xtype : 'filefield',
							name : 'bpmnFilePath',
							fieldLabel : 'File',
							labelWidth : 50,
							msgTarget : 'side',
							allowBlank : false,
							anchor : '100%',
							buttonText : 'Select bpmn file...'
						} ]
					} ]
				});