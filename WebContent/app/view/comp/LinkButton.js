Ext.define('sconsole.view.comp.LinkButton', {
	extend : 'Ext.Component',
	alias : 'widget.linkbutton',

	childEls : [ 'btnEl' ],
	renderTpl : [ '<button id="{id}-btnEl" type="button" class="linkButton"',
			'<tpl if="fontSize"> style="font-size:{fontSize}"</tpl>',
			'<tpl if="tabIndex"> tabIndex="{tabIndex}"</tpl>',
			'>{text}</button>' ],

	/**
	 * @cfg {String} text The text to be used as innerHTML.
	 */

	/**
	 * @cfg {Number} tabIndex Set a DOM tabIndex for this button.
	 */

	/**
	 * @cfg {String} fontSize Sets the font-size on the button. Must be in a
	 *      valid format like "11px". Really only needed if supporting IE7.
	 */

	config : {
		handler : function() {
		}
	},
	initComponent : function() {
		var me = this;
		me.callParent(arguments);
	},
	beforeRender : function() {
		var me = this;
		Ext.applyIf(me.renderData, {
			text : me.text || '',
			tabIndex : me.tabIndex,
			fontSize : me.fontSize
		});
	},
	onRender : function(ct, position) {
		var me = this, btn;
		me.callParent(arguments);

		btn = me.btnEl;
		me.mon(btn, 'click', me.onClick, me);
	},
	onClick : function(e) {
		var me = this;
		if (me.preventDefault || (me.disabled && me.getHref()) && e) {
			e.preventDefault();
		}
		if (e.button !== 0) {
			return;
		}
		if (!me.disabled) {
			me.fireHandler(e);
		}
	},
	fireHandler : function(e) {
		var me = this, handler = me.handler;

		me.fireEvent('click', me, e);
		if (handler) {
			handler.call(me.scope || me, me, e);
		}
	},

	setText : function(text) {
		var me = this;
		me.text = text;
		if (me.rendered) {
			me.btnEl.update(text || '');
			me.updateLayout();
		}
	}
});