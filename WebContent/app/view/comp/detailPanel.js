/**
 * author: tina.wang
 */
Ext.define('sconsole.view.comp.detailPanel', {
	extend : 'Ext.panel.Panel',
	alias : 'widget.detailPanel',
	margin : 10,
	header : false,
	layout : {
		xtype : 'vbox',
		align : 'stretch'
	},
	items : [ {
		xtype : 'panel',
		margin : 10,
		border : false,
		componentCls : 'bottom-border',
		flex : 1,
		layout : {
			type : 'table',
			columns : 3
		},
		items : [ {
			xtype : 'image',
			src : 'resources/icons/job-50.png',
			itemId : 'detailIconFld',
			width : 58,
			height : 50,
			rowspan : 2
		} ]
	} ]
});
