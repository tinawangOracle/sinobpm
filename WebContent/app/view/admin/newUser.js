Ext.define('sconsole.view.admin.newUser',{
	extend : 'Ext.window.Window',
	alias : 'widget.newUserWin',
	title : 'New User',
	layout : {
		type : 'vbox',
		align : 'stretch'
	},
	mid : -1,
	mname : '',
	width : 400,
	height : 500,
	items : [ {
		xtype : 'form',
		itemId : 'newUserForm',
		defaults : {
			xtype : 'textfield',
			margin : 10,
		},
		flex : 1,
		items : [ {
			fieldLabel : 'ID',
			itemId : 'userID',
			name : 'userID',
			allowBlank : false
		}, {
			fieldLabel : 'First Name',
			itemId : 'firstName',
			name : 'firstName',
			allowBlank : false
		}, {
			fieldLabel : 'Last Name',
			itemId : 'lastName',
			name : 'lastName',
			allowBlank : false
		}, {
			fieldLabel : 'Email',
			itemId : 'email',
			name : 'email',
			allowBlank : false
		}, {
			fieldLabel : 'Password',
			itemId : 'password',
			name : 'password',
			inputType : 'password',
			allowBlank : false
		}, {
			fieldLabel : 'Confirm Password',
			itemId : 'confirmPassword',
			name : 'confirmPassword',
			inputType : 'password',
			allowBlank : false
		}, {
			xtype : 'combo',
			store : 'admin.groups',
			multiSelect : 'true',
			fieldLabel : 'Group',
			itemId : 'groupID',
			name : 'groupID',
			queryMode: 'remote',
			displayField: 'name',
		    valueField: 'id',
		    renderTo: Ext.getBody(),
			allowBlank : false
		} ],
		dockedItems : [ {
			xtype : 'toolbar',
			dock : 'bottom',
			items : [ {
				text : 'Submit',
				itemId : 'submitBtn'
			}, '-', {
				text : 'Cancel',
				itemId : 'cancelBtn',

			} ]
		} ],

	} ],
	listeners : {
		beforerender : function(thisobj) {
			console.log(thisobj.mid);
		}
	}
})