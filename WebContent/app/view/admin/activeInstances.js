Ext
		.define(
				'sconsole.view.admin.activeInstances',
				{
					extend : 'Ext.panel.Panel',
					alias : 'widget.activeInstancesPanel',
					layout : {
						type : 'vbox',
						align : 'stretch'
					},
					defaults : {
						margin : 5
					},
					items : [
							{
								xtype : 'grid',
								store : 'admin.activeDefinitions',
								itemId : 'activeDefinitionsGrid',
								flex : 1,
								dockedItems : [ {
									xtype : 'pagingtoolbar',
									dock : 'top',
									displayInfo : false,
									store : 'admin.activeDefinitions',
								} ],
								columns : [ {
									header : "ID",
									dataIndex : "id",
									resizable : true,
									flex : 1,
								}, {
									header : "name",
									dataIndex : "name",
									resizable : true,
									flex : 1,
								}, {
									header : "Count",
									dataIndex : "cnt",
									resizable : true,
									flex : 1,
								} ],
								listeners : {
									selectionchange : function(thisobj,
											selected, eOpts) {
										record = selected[0];
										_p = Ext.ComponentQuery
												.query('activeInstancesPanel #activeInstancesGrid')[0];
										_s = _p.getStore();
										if (Ext.isEmpty(record)) {
											_s.clear();
										}
										_s.load({
											params : {
												pid : record.get('id'),
												type : 0
											},
											callback : function(records,
													operation, success) {
											}
										});
									}
								}
							},
							{
								xtype : 'grid',
								store : 'admin.activeInstances',
								itemId : 'activeInstancesGrid',
								flex : 1,
								title : translations.PROCESS_INSTANCES,
								dockedItems : [ {
									xtype : 'pagingtoolbar',
									dock : 'top',
									displayInfo : false,
									store : 'admin.activeInstances',
								} ],
								columns : [ {
									header : "ID",
									dataIndex : "id",
									resizable : true,
									flex : 1,
								}, {
									header : "Business key",
									dataIndex : "bkey",
									resizable : true,
									flex : 1,
								}, {
									header : "Start User Id",
									dataIndex : "sUid",
									resizable : true,
									flex : 1,
								}, {
									header : "Start Activity Id",
									dataIndex : "sAid",
									resizable : true,
									flex : 1,
								}, {
									header : "Start Time",
									dataIndex : "st",
									resizable : true,
									flex : 1,
								}, {
									header : "End Time",
									dataIndex : "et",
									resizable : true,
									flex : 1,
								}, {
									header : "During",
									dataIndex : "dur",
									resizable : true,
									flex : 1,
								} ],
								listeners : {
									itemdblclick : function(thisobj, record,
											item, index, e, eOpts) {
										_win = Ext.ComponentQuery
												.query('instPanelWin')[0];
										if (!Ext.isEmpty(_win)) {
											sconsole.util.Util
													.showErrorMsg('请先关闭上一个实例详细信息对话框！');
											return;
										}
										_win = Ext
												.create(
														'sconsole.view.admin.instancePanel',
														{
															instId : record
																	.get('id'),
															instType : 0
														// active
														});
										_win.show();
									}
								}
							} ],

				});