Ext.define('sconsole.view.admin.modifyGroup',{
	extend : 'Ext.window.Window',
	alias : 'widget.modifyGroupWin',
	title : 'Modify Group',
	layout : {
		type : 'vbox',
		align : 'stretch'
	},
	mid : -1,
	mname : '',
	width : 400,
	height : 200,
	items : [ {
		xtype : 'form',
		defaults : {
			xtype : 'textfield',
			margin : 10,
		},
		flex : 1,
		items : [ {
			fieldLabel : 'ID',
			itemId : 'id',
			name : 'id',
			allowBlank : false,
			readOnly : true
		}, {
			fieldLabel : 'Name',
			itemId : 'name',
			name : 'name',
			allowBlank : false
		}, {
			xtype : 'combo',
			store : Ext.create('Ext.data.Store', {
			    fields: ['name'],
			    data : [
			        {"name":"security-role"},
			        {"name":"assignment"}
			    ]
			}),
			fieldLabel : 'Type',
			itemId : 'type',
			name : 'type',
			queryMode: 'local',
			displayField: 'name',
		    valueField: 'name',
		    renderTo: Ext.getBody(),
			allowBlank : false
		}],
		dockedItems : [ {
			xtype : 'toolbar',
			dock : 'bottom',
			items : [ {
				text : 'Submit',
				itemId : 'submitBtn'
			}, '-', {
				text : 'Cancel',
				itemId : 'cancelBtn',

			} ]
		} ],

	} ],
	listeners : {
		beforerender : function(thisobj) {
			console.log(thisobj.mid);
		}
	}
})