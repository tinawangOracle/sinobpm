Ext
		.define(
				'sconsole.view.admin.deployments',
				{
					extend : 'Ext.panel.Panel',
					alias : 'widget.adminDeployments',
					title : 'Admin Users',
					layout : {
						type : 'hbox',
						align : 'stretch'
					},
					items : [
							{
								xtype : 'grid',
								store : 'admin.deployments',
								flex : 1,
								itemId : 'adminDeployGrid',
								dockedItems : [ {
									xtype : 'pagingtoolbar',
									dock : 'top',
									displayInfo : true,
									store : 'admin.deployments',
									items : [ '-', {
										text : 'New',
										itemId : 'newBtn'
									}, '-', {
										text : 'Delete',
										itemId : 'deleteBtn'
									} ]
								} ],
								columns : [ {
									header : "ID",
									dataIndex : "id",
									resizable : true,
									flex : 1,
									hidden : true
								}, {
									header : "Name",
									dataIndex : "name",
									resizable : true,
									flex : 1,
								} ]
							},
							{
								xtype : 'panel',
								flex : 2,
								layout : 'fit',
								autoScroll : true,
								items : [ {
									xtype : 'panel',
									flex : 2,
									layout : {
										xtype : 'vbox',
										align : 'stretch'
									},
									defaults : {
										margin : 10
									},
									items : [
											{
												xtype : 'form',
												flex : 1,
												title : 'Deployment Details',
												itemId : 'deploymentForm',
												defaults : {
													xtype : 'displayfield',
													margin : 10,
													anchor : '70%'
												},
												layout : 'anchor',
												items : [
														{
															name : 'id',
															itemId : 'deployIdFld',
															fieldLabel : 'ID'
														},
														{
															name : 'name',
															itemId : 'deployNameFld',
															fieldLabel : 'Deployment Name'
														},
														{
															name : 'deploymentTime',
															itemId : 'deployTimeFld',
															fieldLabel : 'Deployment Time'
														},
														{
															name : 'new',
															itemId : 'deployNewFld',
															fieldLabel : 'is New?'
														} ]
											},
											{
												xtype : 'grid',
												flex : 1,
												title : 'Processes',
												store : 'admin.deploymentProcs',
												itemId : 'deploymentProcsGrid',
												columns : [
														{
															header : 'name',
															dataIndex : 'name',
															flex : 1,
															renderer : function(
																	data,
																	metadata,
																	record,
																	rowIndex,
																	columnIndex,
																	store) {
																return '<a href="javascript:sconsole.util.Util.openProcess(\''
																		+ record
																				.get("id")

																		+ "')\">"
																		+ data
																		+ "</a>";
															}
														}, ]
											},
											{
												xtype : 'grid',
												flex : 1,
												title : ' Related Resources',
												store : 'admin.deploymentResources',
												itemId : 'deployResourcesGrid',
												columns : [
														{
															header : 'name',
															dataIndex : 'name',
															flex : 3

														},
														{
															xtype : 'actioncolumn',
															icon : 'resources/icons/save.png',
															sortable : false,
															menuDisabled : true,
															flex : 1,
															handler : function(
																	grid,
																	rowIndex,
																	colIndex,
																	actionItem,
																	event,
																	record, row) {
																_father = Ext.ComponentQuery
																		.query('adminDeployments #adminDeployGrid')[0];
																_chosens = _father
																		.getSelectionModel()
																		.getSelection();
																did = 1;
																var win = window
																		.open(
																				encodeURI("admin/downloadResource.action?id="
																						+ _chosens[0]
																								.get('id')
																						+ "&rn="
																						+ record
																								.get('name')),
																				'newwindow',
																				"height=100, width=400, toolbar=no, menubar=no, scrollbars=no, resizable=no, location=no, status=no");
																win.focus();

															}
														} ]
											} ]
								} ]
							}, ]

				});