Ext.define('sconsole.view.admin.suspendActiveProc', {
	extend : 'Ext.window.Window',
	alias : 'widget.suspendActiveProcWin',
	title : '挂起流程定义',
	layout : {
		type : 'vbox',
		align : 'stretch'
	},
	width : 400,
	height : 300,
	pid : '-1',
	items : [ {
		xtype : 'form',
		defaults : {
			xtype : 'textfield',
			margin : 10,
		},
		flex : 1,
		items : [ {
			xtype : 'displayfield',
			fieldLabel : '何时挂起此流程定义？',
			itemId : 'suspendInfoFld',
			labelWidth : 200,
			anchor : '100%'
		}, {
			xtype : 'radiogroup',
			name : 'suspendTime',
			itemId : 'suspendTimeFld',
			columns : 1,
			vertical : true,
			allowBlank : false,
			items : [ {
				boxLabel : '现在',
				name : 'suspendTime',
				inputValue : '0',
			// checked : true
			}, {
				boxLabel : '设定时间',
				name : 'suspendTime',
				inputValue : '1',
			} ],
			listeners : {
				change : function(thisobj, newval, oldval, eopts) {
					_father = thisobj.up('form');
					_next = _father.down('#suspendCustomTimeFld');
					_val = newval.suspendTime;
					if (Ext.String.startsWith(_val, '1')) {
						_next.setDisabled(false);
					} else {
						_next.setDisabled(true);
					}
				}

			}
		},
		// {
		// xtype : 'radiofield',
		// boxLabel : '现在',
		// name : 'suspendTime',
		// inputValue : '0',
		// itemId : 'suspendTimeFld1',
		// allowBlank : false
		//
		// }, {
		// xtype : 'radiofield',
		// boxLabel : '设定时间',
		// name : 'suspendTime',
		// inputValue : '1',
		// itemId : 'suspendTimeFld2',
		// allowBlank : false
		// },
		{
			xtype : 'customdatetimefield',
			name : 'suspendCustomTime',
			itemId : 'suspendCustomTimeFld',
			increment : 60,
			format : 'Y-m-d H:i:s',
			disabled : true,
		}, {
			boxLabel : '同时挂起和此流程定义相关的流程实例',
			xtype : 'checkboxfield',
			name : 'suspendBoth',
			itemId : 'suspendBothFld'
		} ],
		dockedItems : [ {
			xtype : 'toolbar',
			dock : 'bottom',
			items : [ {
				text : 'Submit',
				itemId : 'submitBtn'
			}, '-', {
				text : 'Cancel',
				itemId : 'cancelBtn',
				handler : function(thisobj) {
					_win = thisobj.up('window');
					_win.close();
				}
			} ]
		} ],

	} ],
	listeners : {
		beforerender : function(thisobj) {
			// console.log(thisobj.mid);
		}
	}
})