//Ext.define('sconsole.view.admin.groupUsers', {
//	extend : 'Ext.window.Window',
//	alias : 'widget.groupUsers',
//	groupId : -1,
//	title : 'Group Users',
//	layout : {
//		type : 'hbox',
//		align : 'stretch'
//	},
//	mid : -1,
//	mname : '',
//	width : 500,
//	height : 400,
//	items : {
//		xtype : 'grid',
//		store : 'admin.groupUsers',
//		flex : 1,
//		itemId : 'adminGroupUserGrid',
//		multiSelect : true,
//		columnLines : true,
//		dockedItems : [ {
//			xtype : 'pagingtoolbar',
//			dock : 'top',
//			displayInfo : true,
//			store : 'admin.groupUsers',
//			items : [ '-', {
//				xtype : 'button',
//				text : 'Add',
//				itemId : 'addBtn'
//			}, '-', {
//				text : 'Remove',
//				itemId : 'removeBtn'
//			} ],
//		} ],
//		columns : [ {
//			header : "ID",
//			dataIndex : "id",
//			resizable : true,
//			flex : 1,
//		}, {
//			header : "First Name",
//			dataIndex : "firstName",
//			resizable : true,
//			flex : 1,
//		}, {
//			header : "Last Name",
//			dataIndex : "lastName",
//			resizable : true,
//			flex : 1,
//		}, {
//			header : "Email",
//			dataIndex : "email",
//			resizable : true,
//			flex : 1,
//		}, {
//			header : "Password",
//			dataIndex : "password",
//			resizable : true,
//			flex : 1,
//		} ]
//	}
//});




//item selector
Ext.define('sconsole.view.admin.groupUsers', {
	extend : 'Ext.window.Window',
	alias : 'widget.groupUsers',
	groupId : '-1',
	title : 'Group Users',
	layout : {
		type : 'hbox',
		align : 'stretch'
	},
	mid : -1,
	mname : '',
	width : 800,
	height : 300,
	items : {
		 	title: 'Add or Remove Users for Group',
		 	xtype: 'form',
		 	alias : 'widget.groupUsersForm',
		 	//itemId : 'adminUserGroupGrid',
		 	//store: 'admin.userGroups',
	        width: 750,
	        bodyPadding: 10,
	        height: 400,
	        renderTo: Ext.getBody(),
	        layout: 'fit',
	        items:[{
	            xtype: 'itemselector',
	            name: 'groupUseritemselector',
	            id: 'adminGroupUserItemselector',
	            //id: 'userGroup_itemselector',
	            anchor: '100%',
	            //fieldLabel: 'Group',
	            //imagePath: 'ux/images/',
	            store: 'admin.users',
	            displayField: 'email',
	            valueField: 'id',
	            allowBlank: false,
	            msgTarget: 'side',
	            fromTitle: 'Available User',
	            toTitle: 'Selected User'
	        }],
	        dockedItems : [{
	        	xtype: 'toolbar',
 	            dock: 'bottom',
 	            ui: 'footer',
 	            defaults: {
 	                minWidth: 75
 	            },
				displayInfo : true,
				store : 'admin.groupUsers',
				items : [ '->', {
					xtype : 'button',
					text : 'Clear',
					itemId : 'clrBtn'
				},  {
					text : 'Reset',
					itemId : 'reBtn'
				},  {
					text : 'Submit',
					itemId : 'subBtn'
				}],
			}],
	}
});