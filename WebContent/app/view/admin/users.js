Ext.define('sconsole.view.admin.users', {
	extend : 'Ext.panel.Panel',
	alias : 'widget.adminUsers',
	title : 'Admin Users',
	layout : {
		type : 'hbox',
		align : 'stretch'
	},
	items : [ {
		xtype : 'grid',
		store : 'admin.users',
		flex : 1,
		itemId : 'adminUserGrid',
		//multiSelect: true,
		//columnLines : true,
		dockedItems : [ {
			xtype : 'pagingtoolbar',
			dock : 'top',
			displayInfo : true,
			store : 'admin.users',
			items : [ '-', {
				xtype : 'button',
				text : 'New',
				itemId : 'newBtn'
			}, '-', {
				text : 'Modify',
				itemId : 'modifyBtn'
			}, '-', {
				text : 'Delete',
				itemId : 'deleteBtn'
			} ]
		} ],
		columns : [ {
			header : "ID",
			dataIndex : "id",
			resizable : true,
			flex : 1,
		}, {
			header : "First Name",
			dataIndex : "firstName",
			resizable : true,
			flex : 1,
		}, {
			header : "Last Name",
			dataIndex : "lastName",
			resizable : true,
			flex : 1,
		}, {
			header : "Email",
			dataIndex : "email",
			resizable : true,
			flex : 1,
		}, {
			header : "Password",
			dataIndex : "password",
			resizable : true,
			flex : 1,
		}, {
			xtype : 'actioncolumn',
			items : [ {
				icon : 'resources/icons/advanced.png',
				tooltip : 'Show Groups',
				handler : function(_g, rowIdx, colIdx) {
					//alert('1');
					//console.log(_g);
					_rec = _g.getStore().getAt(rowIdx);
					console.log(_rec);
					win = Ext.create('sconsole.view.admin.userGroups');
					win.userId = _rec.get('id');
					console.log(win);
					_s = win.queryById('adminUserGroupItemselector');
					if (Ext.isEmpty(_s)) {
						alert('error');
					}
					
					var userGroup = Ext.create('Ext.data.ArrayStore', {
		    			autoLoad : false,
		    			proxy : {
		    				type : 'ajax',
		    				api : {
		    					read : "admin/getGroupsByUserId.action"
		    				},
		    				timeout : 0,
		    				reader : {
		    					type : 'json',
		    					totalProperty : "totalCount",
		    					root : "records",
		    					idProperty : 'id',
		    					messageProperty: 'userId'
		    				}
		    			},
		    			fields : [ 'id', 'name', 'type'],
		    			sortInfo: {
		    	            field: 'name',
		    	            direction: 'ASC'
		    	        }
		    		});
		    	
					_s.getStore().load();
					userGroup.getProxy().setExtraParam('userId', _rec.get('id'));
					userGroup.load(function(records, operation, success){
						var select = [];
						userGroup.each(function(record){
							//alert(record.get('id'));
				    		select.push(record.get('id'));
				    	});
				    	_s.setValue(select);
					});
					//_s.getStore().load();
					
					//alert(_s.getStore().getTotalCount());
					
//					userGroup.getTotalCount();
//					var select = [];
//					userGroup.each(function(record){
//						alert(record.get('id'));
//			    		select.push(record.get('id'));
//			    	});
					
					
					
					
					win.show();
			    	console.log(_s);
				}

			} ]
		} ]
	} ]

});