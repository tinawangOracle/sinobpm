Ext.define('sconsole.view.admin.addGroups', {
	extend : 'Ext.window.Window',
	alias : 'widget.addGroups',
	title : 'Add Groups',
	userId : '-1',
	layout : {
		type : 'hbox',
		align : 'stretch'
	},
	mid : -1,
	mname : '',
	width : 600,
	height : 500,
	items : [ {
		xtype : 'grid',
		store : 'admin.userNoGroups',
		flex : 1,
		itemId : 'addGroupGrid',
		//selModel :  Ext.create('Ext.selection.CheckboxModel'),
		multiSelect: true,
		columnLines : true,
		dockedItems : [ {
			xtype : 'pagingtoolbar',
			dock : 'top',
			displayInfo : true,
			store : 'admin.userNoGroups',
			
		},{
			xtype : 'toolbar',
			dock : 'bottom',
			items : [ {
				text : 'Submit',
				itemId : 'submitBtn'
			}, '-', {
				text : 'Cancel',
				itemId : 'cancelBtn',

			} ]
		}  ],
		columns : [
		           
		           {
		        	   header : "ID",
		        	   dataIndex : "id",
		        	   resizable : true,
		        	   flex : 1,
		           }, {
		        	   header : "Name",
		        	   dataIndex : "name",
		        	   resizable : true,
		        	   flex : 1,
		           }, {
		        	   header : "Type",
		        	   dataIndex : "type",
		        	   resizable : true,
		        	   flex : 1,
		           }]
	}]

});