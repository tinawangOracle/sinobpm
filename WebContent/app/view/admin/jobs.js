Ext
		.define(
				'sconsole.view.admin.jobs',
				{
					extend : 'Ext.panel.Panel',
					alias : 'widget.jobsPanel',
					title : '挂起的流程',
					layout : {
						type : 'border'
					},
					items : [
							{
								region : 'west',
								split : true,
								xtype : 'grid',
								itemId : 'jobsGrid',
								store : 'admin.jobs',
								flex : 2,
								dockedItems : [ {
									xtype : 'chgNotifyPagingbar',
									dock : 'top',
									displayInfo : false,
									store : 'admin.jobs',
									items : [ '-', {
										text : '执行',
										itemId : 'executeBtn'
									}, '-', {
										text : '删除',
										itemId : 'deleteBtn'
									} ]
								} ],
								columns : [ {
									header : "name",
									dataIndex : "name",
									resizable : true,
									flex : 1,
									renderer : function(val, meta, rec, ridx) {
										if (Ext.isEmpty(val))
											return rec.get('key');
										else
											return val;
									}

								} ],
								listeners : { // step 1
									beforerender : function(thisobj) {
										thisobj.getStore().load();
									},
									selectionchange : function(thisobj,
											selected, eOpts) {
										// console.log(record.get('id'));
										record = selected[0];
										if (Ext.isEmpty(record)) {
											_box = Ext.ComponentQuery
													.query('jobsPanel #detailTitleFld')[0];
											_box.update('');

											_box = Ext.ComponentQuery
													.query('jobsPanel #detailDuedateFld')[0];
											_box.update('');
											_p = Ext.ComponentQuery
													.query('jobsPanel #detailBody')[0];
											_p.remove('jobContentFld');
											return;
										}
										Ext.Ajax
												.request({
													url : "admin/showJob.action",
													success : function(a) {
														// _w.getEl().unmask();
														var c = sconsole.util.Util
																.decodeJSON(a.responseText);
														_box = Ext.ComponentQuery
																.query('jobsPanel #detailTitleFld')[0];
														_box.update(c.jobtype);

														_box = Ext.ComponentQuery
																.query('jobsPanel #detailDuedateFld')[0];
														_box.update(c.duedate);

														_p = Ext.ComponentQuery
																.query('jobsPanel #detailBody')[0];
														_p
																.remove('jobContentFld');
														_p
																.add({
																	xtype : 'box',
																	itemId : 'jobContentFld',
																	html : c.jobhtype
																			+ ' <a 														 href="oracle.com">'
																			+ c.processId
																			+ '</a>',
																	columnWidth : 1
																});
													},
													failure : function(a) {
														console
																.log(a.responseText);
														sconsole.util.Util
																.sendErrorMsg("Error");
													},
													params : {
														id : record.get('id')
													},
													waitMsg : "waiting...."
												});
									}
								},
							},
							{
								xtype : 'panel',
								region : 'center',
								itemId : 'myJob',
								margin : 10,
								header : false,
								// icon : 'resources/icons/job-50.png',
								layout : {
									xtype : 'vbox',
									align : 'stretch'
								},
								items : [
										{
											xtype : 'panel',
											margin : 10,
											border : false,
											componentCls : 'bottom-border',
											flex : 1,
											layout : {
												type : 'table',
												columns : 3
											},
											items : [
													{
														xtype : 'image',
														src : 'resources/icons/job-50.png',
														itemId : 'detailIconFld',
														width : 58,
														height : 50,
														rowspan : 2
													},
													{
														xtype : 'box',
														colspan : 2,
														itemId : 'detailTitleFld',
														componentCls : 'title-block'
													},
													{
														xtype : 'image',
														src : 'resources/icons/duedate-16.png',
														alt : 'dddd',
														width : 16,
														height : 16
													},
													{
														xtype : 'box',
														itemId : 'detailDuedateFld',
													} ]
										},
										{
											xtype : 'panel',
											flex : 4,
											border : false,
											itemId : 'detailBody',
											layout : {
												type : 'vbox',
												align : 'stretch'
											},
											padding : 10,
											items : [ {
												xtype : 'box',
												componentCls : 'block-holder-h3',
												html : translations.JOB_HEADER_EXECUTION
											} ]
										} ],
								flex : 5
							} ],

				});