Ext.define('sconsole.view.admin.groups', {
	extend : 'Ext.panel.Panel',
	alias : 'widget.adminGroups',
	title : 'Admin Groups',
	layout : {
		type : 'hbox',
		align : 'stretch'
	},
	items : [ {
		xtype : 'grid',
		store : 'admin.groups',
		flex : 1,
		itemId : 'adminGroupGrid',
		//multiSelect: true,
		//columnLines : true,
		dockedItems : [ {
			xtype : 'pagingtoolbar',
			dock : 'top',
			displayInfo : true,
			store : 'admin.groups',
			items : [ '-', {
				xtype : 'button',
				text : 'New',
				itemId : 'newBtn'
			}, '-', {
				text : 'Modify',
				itemId : 'modifyBtn'
			}, '-', {
				text : 'Delete',
				itemId : 'deleteBtn'
			} ]
	} ],
	columns : [{
	   				header : "ID",
	   				dataIndex : "id",
	   				resizable : true,
	   				flex : 1,
	   			}, {
	   				header : "Name",
	   				dataIndex : "name",
	   				resizable : true,
	   				flex : 1,
	   			}, {
	   				header : "Type",
	   				dataIndex : "type",
	   				resizable : true,
	   				flex : 1,
	   			}, {
	   				xtype : 'actioncolumn',
	   				items : [{
//	   					icon : 'resources/icons/advanced.png',
//	   					tooltip : 'Show Users',
//	   					handler : function(_g, rowIdx, colIdx) {
//	   						_rec = _g.getStore().getAt(rowIdx);
//	   						win = Ext.create('sconsole.view.admin.groupUsers');
//	   						win.groupId = _rec.get('id');
//	   						_s = win.queryById('adminGroupUserGrid').getStore();
//	   						_s.getProxy().setExtraParam('groupId',_rec.get('id'));
//	   						_s.load();
//							win.show();
//	   					}
	   					
	   					
	   					icon : 'resources/icons/advanced.png',
	   					tooltip : 'Show Users',
	   					handler : function(_g, rowIdx, colIdx) {
	   						//alert('1');
	   						//console.log(_g);
	   						_rec = _g.getStore().getAt(rowIdx);
	   						console.log(_rec);
	   						win = Ext.create('sconsole.view.admin.groupUsers');
	   						win.groupId = _rec.get('id');
	   						console.log(win);
	   						_s = win.queryById('adminGroupUserItemselector');
	   						if (Ext.isEmpty(_s)) {
	   							alert('error');
	   						}
	   						
	   						var groupUser = Ext.create('Ext.data.ArrayStore', {
	   			    			autoLoad : false,
	   			    			proxy : {
	   			    				type : 'ajax',
	   			    				api : {
	   			    					read : "admin/getUsersByGroupId.action"
	   			    				},
	   			    				timeout : 0,
	   			    				reader : {
	   			    					type : 'json',
	   			    					totalProperty : "totalCount",
	   			    					root : "records",
	   			    					idProperty : 'id',
	   			    					messageProperty: 'groupId'
	   			    				}
	   			    			},
	   			    			fields : [ 'id', 'email'],
	   			    			sortInfo: {
	   			    	            field: 'email',
	   			    	            direction: 'ASC'
	   			    	        }
	   			    		});
	   			    	
	   						_s.getStore().load();
	   						groupUser.getProxy().setExtraParam('groupId', _rec.get('id'));
	   						groupUser.load(function(records, operation, success){
	   							var select = [];
	   							groupUser.each(function(record){
	   								//alert(record.get('id'));
	   					    		select.push(record.get('id'));
	   					    	});
	   					    	_s.setValue(select);
	   						});
	   						//_s.getStore().load();
	   						
	   						//alert(_s.getStore().getTotalCount());
	   						
//	   						userGroup.getTotalCount();
//	   						var select = [];
//	   						userGroup.each(function(record){
//	   							alert(record.get('id'));
//	   				    		select.push(record.get('id'));
//	   				    	});
	   						
	   						
	   						
	   						
	   						win.show();
	   				    	console.log(_s);
	   					}

	   					
	   					
	   				}]
	   			}]
	   	} ]

	});