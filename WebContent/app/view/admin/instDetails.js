Ext.define('sconsole.view.admin.instDetails', {
	extend : 'Ext.panel.Panel',
	alias : 'widget.instDetailPanel',
	layout : {
		type : 'vbox',
		align : 'stretch'
	},
	defaults : {
		margin : 10
	},
	instId:-1,
	instType:0,
	autoScroll : true,
	items : [ {
		xtype : 'grid',
		store : 'workflow.instTasks',
		itemId : 'tasksGrid',
		emptyText : 'No tasks',
		title : 'Tasks',
		columns : [ {
			header : "name",
			dataIndex : "name",
			flex : 1
		}, {
			header : "Priority",
			dataIndex : "priority",
			flex : 1
		}, {
			header : "Assignee",
			dataIndex : "assignee",
			flex : 1,
		}, {
			header : "Due Date",
			dataIndex : "dueDate",
			flex : 1
		}, {
			header : "Start Date",
			dataIndex : "startTime",
			flex : 1
		}, {
			header : "End Date",
			dataIndex : "endTime",
			flex : 1
		} ],
		flex : 1
	}, {
		xtype : 'grid',
		store : 'workflow.instVars',
		itemId : 'varsGrid',
		title : 'Variables',
		emptyText : 'No Vairables',
		columns : [ {
			header : "name",
			dataIndex : "name",
			flex : 1
		}, {
			header : "Value",
			dataIndex : "value",
			emptyText : 'No variables',
			flex : 2
		} ],
		flex : 1
	}, {
		xtype : 'panel',
		layout : 'fit',
		flex : 2,
		itemId : 'instPngPanel',
		height : 500
	} ]
});