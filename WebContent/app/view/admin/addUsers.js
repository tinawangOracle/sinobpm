Ext.define('sconsole.view.admin.addUsers', {
	extend : 'Ext.window.Window',
	alias : 'widget.addUsers',
	title : 'Add Users',
	groupId : '-1',
	layout : {
		type : 'hbox',
		align : 'stretch'
	},
	mid : -1,
	mname : '',
	width : 600,
	height : 500,
	items : [ {
		xtype : 'grid',
		store : 'admin.groupNoUsers',
		flex : 1,
		itemId : 'addUserGrid',
		//selModel :  Ext.create('Ext.selection.CheckboxModel'),
		multiSelect: true,
		columnLines : true,
		dockedItems : [ {
			xtype : 'pagingtoolbar',
			dock : 'top',
			displayInfo : true,
			store : 'admin.groupNoUsers',
			
		},{
			xtype : 'toolbar',
			dock : 'bottom',
			items : [ {
				text : 'Submit',
				itemId : 'submitBtn'
			}, '-', {
				text : 'Cancel',
				itemId : 'cancelBtn',
			} ]
		}  ],
		columns : [
			   		{
			   			header : "ID",
			   			dataIndex : "id",
			   			resizable : true,
			   			flex : 1,
			   		}, {
			   			header : "First Name",
			   			dataIndex : "firstName",
			   			resizable : true,
			   			flex : 1,
			   		}, {
			   			header : "Last Name",
			   			dataIndex : "lastName",
			   			resizable : true,
			   			flex : 1,
			   		}, {
			   			header : "Email",
			   			dataIndex : "email",
			   			resizable : true,
			   			flex : 1,
			   		}
			   	]
	}]

});