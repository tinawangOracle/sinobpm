//grid
//Ext.define('sconsole.view.admin.userGroups', {
//	extend : 'Ext.window.Window',
//	alias : 'widget.userGroups',
//	userId : '-1',
//	title : 'User Groups',
//	layout : {
//		type : 'hbox',
//		align : 'stretch'
//	},
//	mid : -1,
//	mname : '',
//	width : 500,
//	height : 400,
//	items : {
//		xtype : 'grid',
//		store : 'admin.userGroups',
//		flex : 1,
//		itemId : 'adminUserGroupGrid',
//		multiSelect: true,
//		columnLines : true,
//		dockedItems : [{
//			xtype : 'pagingtoolbar',
//			dock : 'top',
//			displayInfo : true,
//			store : 'admin.userGroups',
//			items : [ '-', {
//				xtype : 'button',
//				text : 'Add',
//				itemId : 'addBtn'
//			}, '-', {
//				text : 'Remove',
//				itemId : 'removeBtn'
//			}],
//		}],
//		columns : [
//	   		{
//	   			header : "ID",
//	   			dataIndex : "id",
//	   			resizable : true,
//	   			flex : 1,
//	   		}, {
//	   			header : "Name",
//	   			dataIndex : "name",
//	   			resizable : true,
//	   			flex : 1,
//	   		}, {
//	   			header : "Type",
//	   			dataIndex : "type",
//	   			resizable : true,
//	   			flex : 1,
//	   		}
//	   	]
//	}
//});



//item selector
Ext.define('sconsole.view.admin.userGroups', {
	extend : 'Ext.window.Window',
	alias : 'widget.userGroups',
	userId : '-1',
	title : 'User Groups',
	layout : {
		type : 'hbox',
		align : 'stretch'
	},
	mid : -1,
	mname : '',
	width : 800,
	height : 300,
	items : {
		 	title: 'Add or Remove Groups for User',
		 	xtype: 'form',
		 	alias : 'widget.userGroupsForm',
		 	//itemId : 'adminUserGroupGrid',
		 	//store: 'admin.userGroups',
	        width: 750,
	        bodyPadding: 10,
	        height: 400,
	        renderTo: Ext.getBody(),
	        layout: 'fit',
	        items:[{
	            xtype: 'itemselector',
	            name: 'userGroupitemselector',
	            id: 'adminUserGroupItemselector',
	            //id: 'userGroup_itemselector',
	            anchor: '100%',
	            //fieldLabel: 'Group',
	            //imagePath: 'ux/images/',
	            store: 'admin.groups',
	            displayField: 'name',
	            valueField: 'id',
	            allowBlank: false,
	            msgTarget: 'side',
	            fromTitle: 'Available Group',
	            toTitle: 'Selected Group'
	        }],
	        dockedItems : [{
	        	xtype: 'toolbar',
 	            dock: 'bottom',
 	            ui: 'footer',
 	            defaults: {
 	                minWidth: 75
 	            },
				displayInfo : true,
				store : 'admin.userGroups',
				items : [ '->', {
					xtype : 'button',
					text : 'Clear',
					itemId : 'clrBtn'
				},  {
					text : 'Reset',
					itemId : 'reBtn'
				},  {
					text : 'Submit',
					itemId : 'subBtn'
				}],
			}],
	}
});