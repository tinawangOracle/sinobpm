Ext.define('sconsole.view.admin.modifyUser',{
	extend : 'Ext.window.Window',
	alias : 'widget.modifyUserWin',
	title : 'Modify User',
	layout : {
		type : 'vbox',
		align : 'stretch'
	},
	mid : -1,
	mname : '',
	width : 400,
	height : 500,
	items : [ {
		xtype : 'form',
		defaults : {
			xtype : 'textfield',
			margin : 10,
		},
		flex : 1,
		items : [ {
			fieldLabel : 'ID',
			itemId : 'userID',
			name : 'userID',
			allowBlank : false,
			readOnly : true
		}, {
			fieldLabel : 'First Name',
			itemId : 'firstName',
			name : 'firstName',
			allowBlank : false
		}, {
			fieldLabel : 'Last Name',
			itemId : 'lastName',
			name : 'lastName',
			allowBlank : false
		}, {
			fieldLabel : 'Email',
			itemId : 'email',
			name : 'email',
			allowBlank : false
		}, {
			fieldLabel : 'Password',
			itemId : 'password',
			name : 'password',
			inputType : 'password',
			emptyText : 'Keep Original'
		}, {
			fieldLabel : 'Confirm Password',
			itemId : 'confirmPassword',
			name : 'confirmPassword',
			inputType : 'password',
			emptyText : 'Keep Original'
		} ],
		dockedItems : [ {
			xtype : 'toolbar',
			dock : 'bottom',
			items : [ {
				text : 'Submit',
				itemId : 'submitBtn'
			}, '-', {
				text : 'Cancel',
				itemId : 'cancelBtn',

			} ]
		} ],

	} ],
	listeners : {
		beforerender : function(thisobj) {
			console.log(thisobj.mid);
		}
	}
})