Ext.define('sconsole.view.admin.instancePanel', {
	extend : 'Ext.window.Window',
	alias : 'widget.instPanelWin',
	title : 'Instance Detail',
	layout : 'fit',
	width : 800,
	height : 600,
	instId : -1,
	requires : [ 'sconsole.view.admin.instDetails' ],
	items : [ {
		xtype : 'instDetailPanel',
		dockedItems : [ {
			xtype : 'toolbar',
			dock : 'bottom',
			items : [ {
				text : 'Cancel',
				itemId : 'cancelBtn',
				handler : function(thisobj) {
					_win = thisobj.up('window');
					_win.close();
				}
			} ]
		} ],
	} ]
});