Ext
		.define(
				'sconsole.view.admin.activeProcs',
				{
					extend : 'Ext.panel.Panel',
					alias : 'widget.activeProcess',
					title : '挂起的流程',
					layout : {
						type : 'border'
					},
					items : [
							{
								region : 'west',
								split : true,
								xtype : 'grid',
								itemId : 'activeProcsGrid',
								store : 'admin.activeProcs',
								flex : 2,
								dockedItems : [ {
									xtype : 'chgNotifyPagingbar',
									dock : 'top',
									displayInfo : false,
									store : 'admin.activeProcs',
									items : [ '-', {
										text : '挂起',
										itemId : 'suspendBtn'
									} ]
								} ],
								columns : [ {
									header : "name",
									dataIndex : "name",
									resizable : true,
									flex : 1,
									renderer : function(val, meta, rec, ridx) {
										if (Ext.isEmpty(val))
											return rec.get('key');
										else
											return val;
									}

								} ],
								listeners : { // step 1
									// beforerender : function(thisobj) {
									// thisobj.getStore().load();
									// },
									selectionchange : function(thisobj,
											selected, eOpts) {
										record = selected[0];
										if (Ext.isEmpty(record)) {
											_p = Ext.ComponentQuery
													.query('activeProcess #myProcess')[0];
											_p.setTitle('');
											_p.removeAll();
											return;

										}
										Ext.Ajax
												.request({
													url : "workflow/showProc.action",
													success : function(a) {
														// _w.getEl().unmask();
														var c = Ext
																.decode(a.responseText);
														_p = Ext.ComponentQuery
																.query('activeProcess #myProcess')[0];
														_p
																.setTitle(c.pname
																		+ ' [版本'
																		+ c.pver
																		+ ', 部署于'
																		+ c.ptime
																		+ ']');

														if (c.success == "0") {
															_p.removeAll();
															_p
																	.add({
																		xtype : 'box',
																		itemId : 'procdetail',
																		autoEl : {
																			tag : 'iframe',
																			name : 'procdetail',
																			src : c.url
																		}
																	});
														} else if (c.success == "2") {
															_p.removeAll();
															_p
																	.add([ {
																		xtype : 'box',
																		itemId : 'procdetail',
																		flex : 4,
																		autoEl : {
																			tag : 'iframe',
																			name : 'procdetail',
																			src : 'nodiagram.html',

																		}
																	} ]);
														} else if (c.success == "1") {
															_p.removeAll();
															_p
																	.add({
																		xtype : 'box',
																		itemId : 'procdetail',
																		autoEl : {
																			tag : 'img',
																			src : 'workflow/showProcessDefintionPng.action?id='
																					+ record
																							.get('id'),
																		}
																	});
														}
													},
													failure : function(a) {
														console
																.log(a.responseText);
														sconsole.util.Util
																.sendErrorMsg("Error");
													},
													params : {
														id : record.get('id')
													},
													waitMsg : "waiting...."
												});
									}
								},
							}, {
								xtype : 'panel',
								region : 'center',
								itemId : 'myProcess',
								icon : 'resources/icons/process-50.png',
								layout : 'fit',
								items : [],
								flex : 5
							} ],

				});