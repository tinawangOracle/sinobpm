Ext.define('sconsole.view.admin.activateSuspendProc', {
	extend : 'Ext.window.Window',
	alias : 'widget.activateSuspendProcWin',
	title : '激活流程定义',
	layout : {
		type : 'vbox',
		align : 'stretch'
	},
	width : 400,
	height : 300,
	pid : '-1',
	items : [ {
		xtype : 'form',
		defaults : {
			xtype : 'textfield',
			margin : 10,
		},
		flex : 1,
		items : [ {
			xtype : 'displayfield',
			fieldLabel : '何时激活此流程定义？',
			itemId : 'activateInfoFld',
			labelWidth : 200,
			anchor : '100%'
		}, {
			xtype : 'radiofield',
			boxLabel : '现在',
			name : 'activeTime',
			inputValue : '0',
			itemId : 'activeTimeFld1'
		}, {
			xtype : 'radiofield',
			boxLabel : '设定时间',
			name : 'activeTime',
			inputValue : '1',
			itemId : 'activeTimeFld2',
			listeners : {
				change : function(thisobj, newval, oldval, eopts) {
					_father = thisobj.up('form');
					_next = _father.down('#customeDTimeFld');
					if (newval) {
						_next.setDisabled(false);
					} else {
						_next.setDisabled(true);
					}
				}

			}
		}, {
			xtype : 'customdatetimefield',
			itemId : 'customeDTimeFld',
			// xtype:'datetimecustomfield',
			disabled : true,
			increment : 60,
			format : 'Y-m-d H:i:s',
			name : 'activeCustomTime'
		}, {
			boxLabel : '同时激活和此流程定义相关的流程实例',
			xtype : 'checkboxfield',
			name : 'activeBoth',
			itemId : 'activeBothFld'
		} ],
		dockedItems : [ {
			xtype : 'toolbar',
			dock : 'bottom',
			items : [ {
				text : 'Submit',
				itemId : 'submitBtn'
			}, '-', {
				text : 'Cancel',
				itemId : 'cancelBtn',
				handler : function(thisobj) {
					_win = thisobj.up('window');
					_win.close();
				}
			} ]
		} ],

	} ],
	listeners : {
		beforerender : function(thisobj) {
			// console.log(thisobj.mid);
		}
	}
})