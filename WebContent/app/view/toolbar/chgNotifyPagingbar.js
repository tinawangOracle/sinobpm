Ext
		.define(
				'sconsole.view.toolbar.chgNotifyPagingbar',
				{
					extend : 'Ext.toolbar.Paging',
					alias : 'widget.chgNotifyPagingbar',
					dock : 'top',
					listeners : {
						beforechange : function(thisobj, page, eOpts) {
							_store = thisobj.getStore();
							_f = _store.getModifiedRecords();
							_proxy = _store.getProxy();
							_tcnt = _proxy.extraParams.tcnt;
							// alert(_store.getTotalCount());
							_proxy
									.setExtraParam('tcnt', _store
											.getTotalCount());
							_go = true;
							this.currentPage = 1;
							if (_f.length > 0) {
								_res = confirm("Some records have been modified , Click Ok to keep in current page, click Cancel to abandon changes?");
								if (_res) {
									return false;
								} else {
									_store.rejectChanges();
								}

							}
						}
					}
				});