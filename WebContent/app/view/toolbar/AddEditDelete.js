Ext.define('sconsole.view.toolbar.AddEditDelete', {
	extend : 'Ext.toolbar.Toolbar',
	alias : 'widget.addeditdelete',
	dock : 'top',
	items : [ {
		xtype : 'button',
		text : 'Add'
	}, {
		xtype : 'button',
		text : 'Edit'
	}, {
		xtype : 'button',
		text : 'Delete'
	}, {
		xtype : 'tbseparator'
	}, {
		xtype : 'button',
		text : 'Print'
	}, {
		xtype : 'button',
		text : 'Export to PDF'
	}, {
		xtype : 'button',
		text : 'Export to Excel'
	} ]
});