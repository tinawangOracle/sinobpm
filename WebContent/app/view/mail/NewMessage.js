Ext.define('sconsole.view.mail.NewMessage', {
	extend : 'Ext.window.Window',
	alias : 'widget.newmessage',

	height : 350,
	width : 666,
	autoShow : true,
	layout : {
		type : 'fit'
	},
	title : 'Untitled - Message',
	iconCls : 'new-mail',

	initComponent : function() {
		var me = this;

		Ext.applyIf(me, {
			dockedItems : [ {
				xtype : 'toolbar',
				dock : 'top',
				items : [ {
					xtype : 'button',
					text : 'Send',
					iconCls : 'send-mail',
					itemId : 'send'
				}, {
					xtype : 'button',
					text : 'Save',
					iconCls : 'save'
				}, {
					xtype : 'tbseparator'
				}, {
					xtype : 'button',
					iconCls : 'importance'
				}, {
					xtype : 'tbseparator'
				} ]
			} ],
			items : [ {
				xtype : 'form',
				frame : false,
				bodyPadding : 10,
				autoScroll : true,
				defaults:{
					margin : 5,
				},
				items : [ {
					xtype : 'panel',
					border : false,
					layout : {
						type : 'hbox',
						align : 'stretch'
					},
					defaults:{
						margin: '0 5 0 0'
					},
					items : [ {
						xtype : 'textfield',
	//					anchor : '100%',
						fieldLabel : 'To',
						name : 'to',
						flex : 6
					}, {
						xtype : 'button',
						text : 'search',
						flex : 1
					} ]
				}, {
					xtype : 'textfield',
					anchor : '100%',
					fieldLabel : 'Subject',
					name : 'subject'
				}, {
					xtype : 'htmleditor',
					anchor : '100%',
					height : 168,
					style : 'background-color: white;',
					name : 'content'
				} ]
			} ]
		});

		me.callParent(arguments);
	}

});