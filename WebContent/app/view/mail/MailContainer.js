Ext.define('sconsole.view.mail.MailContainer', {
	extend : 'Ext.container.Container',
	alias : 'widget.mailcontainer',

	requires : [ 'sconsole.view.mail.MailList',
			'sconsole.view.mail.MailPreview', 'sconsole.view.mail.MailMenu' ],

	layout : {
		type : 'border'
	},

	initComponent : function() {
		var me = this;

		// var mailPreview = {
		// xtype: 'mailpreview',
		// autoScroll: true
		// };

		me.items = [ 
		             {
			xtype : 'container',
			region : 'center',
			itemId : 'mailpanel',
			layout : {
				type : 'border'
			},
			items : [ {
				xtype : 'maillist',
				collapsible : false,
				region : 'center'
			} ]
		}, 
		{
			xtype : 'mailMenu',
			region : 'west',

		}
		];

		me.callParent(arguments);
	}

});