Ext
		.define(
				'sconsole.view.Dashboard',
				{
					requires : [ 'sconsole.util.Util' ],
					extend : 'Ext.panel.Panel',
					alias : 'widget.dashboard',
					requires : [ 'sconsole.util.Util' ],
					layout : {
						type : 'vbox',
						align : 'stretch'
					},
					items : [
					// {
					// xtype : 'users',
					// // html : 'test'
					// },
					{
						xtype : 'grid',
						id : 'dashAction',
						columnLines : true,
						viewConfig : {
							stripeRows : true
						},
						autoExpandColumn : 'value',
						features : [ {
							ftype : 'grouping',
							groupHeaderTpl : 'Build: {name} ({children.length})'
						} ],
						// plugins:['cellediting'], //not work
						plugins : [ Ext
								.create(
										'Ext.grid.plugin.CellEditing',
										{
											clicksToEdit : 2,
											listeners : {
												edit : function(editor, e,
														eOpts) {
													dd = e.record;
													if (e.column.text == "New Key"
															&& e.value != e.originalValue) {
														Ext.Ajax
																.request({
																	url : "common/existingKeyCheck.action",
																	success : function(
																			a) {
																		var b = Ext
																				.decode(a.responseText);
																		if (b.success == "true") {
																			Ext.MessageBox
																					.alert(
																							"Error",
																							"the new key is already used in the file, recover it!",
																							function() {
																								_a = e.grid;// Ext.ComponentQuery.query('searchResultAction')[0];
																								_a
																										.getStore()
																										.rejectChanges();
																							});
																		}

																	},
																	params : {
																		fid : dd
																				.get('fI'),
																		key : e.value
																	}
																});

													}
													if (e.column.text == "New Value")
														Ext.Ajax
																.request({
																	url : "common/checkT13y.action",
																	success : function(
																			a) {
																		var b = Ext
																				.decode(a.responseText);
																		if (b.success == "true") {
																			if (b.totalCount > 0) {
																				_win = Ext
																						.create('sconsole.view.t13yWin');
																				_win
																						.show();
																				_a = Ext.ComponentQuery
																						.query('t13yWindow #t13ygrid')[0];
																				_a
																						.getStore()
																						.loadData(
																								b.records);
																			}
																		}
																	},
																	params : {
																		checkVal : e.value
																	}
																});
												}
											}
										}) ],
						dockedItems : [ {
							// xtype : 'pagingtoolbar',
							xtype : 'chgNotifyPagingbar',
							dock : 'top',
							displayInfo : true,
							store : 'actions',
							items : [ {
								text : 'Modeler',
								itemId : 'modelerBtn',
								// iconCls : 'btn_commit',
								tooltip : 'open modeler window'
							}, {
								text : 'Commit',
								itemId : 'dashCommit',
								iconCls : 'btn_commit',
								tooltip : 'commit all unsaved changes'
							}, {
								text : 'Delete All',
								itemId : 'dashDelete',
								iconCls : 'delete',
								tooltip : 'delete all actions once'
							}, {
								text : 'Restore',
								itemId : 'dashRestore',
								iconCls : 'btn_restore',
								tooltip : 'Rollback unsaved changes'
							} ]
						} ],
						columns : [
								{
									header : 'File Name',
									dataIndex : 'fN',
									sortable : true,
									width : 100,
									renderer : function(value, meta, record,
											rowIndex, colIndex, store) {
										meta.tdAttr = ' data-qtip="'
												+ Ext.util.Format
														.htmlEncode(value)
												+ '" data-qclass="normalwhite"';
										if (rowIndex == 0)
											return value;
										lastRec = store.getAt(rowIndex - 1);
										if (value == lastRec.get('fN')) {
											return '';
										} else
											return value;
									}
								},
								{
									header : 'File Id',
									dataIndex : 'fI',
									sortable : true,
									width : 50,
									hidden : true
								},
								{
									header : 'Key',
									dataIndex : 'key',
									renderer : sconsole.util.Util.tipRenderer,
									sortable : true,
									width : 50,
									flex : 1
								},
								{
									header : 'Array Flag',
									dataIndex : 'aF',
									width : 30,
									hidden : true
								},
								{
									header : 'Dup Flag',
									dataIndex : 'dF',
									width : 30,
									hidden : true
								},
								{
									header : 'Created By',
									dataIndex : 'cB',
									hidden : true,
									width : 50
								},
								{
									header : 'Type',
									tooltip : 'Action Type',
									dataIndex : 'aT',
									width : 50,
									renderer : function(value, meta) {
										var type = parseInt(value);
										switch (type) {
										case 1:
											return 'Insert';
											break;
										case 2:
											return 'Update';
											break;
										case 3:
											return 'Delete';
											break;
										case 4:
											return 'Create';
											break;
										default:
											return 'None';
											break;
										}
									}
								},
								{
									header : 'Build Name',
									dataIndex : 'bN',
									width : 50,
									hidden : true
								},
								{
									header : 'Build Id',
									dataIndex : 'bI',
									width : 50,
									hidden : true
								},
								{
									header : 'Value',
									dataIndex : 'vL',
									renderer : sconsole.util.Util.formatLineWrap,// tipRenderer,
									width : 100,
									flex : 1
								},
								{
									header : 'New Key',
									dataIndex : 'nK',
									sortable : true,
									width : 50,
									flex : 1,
									editor : {
										xtype : 'textfield',
										allowBlank : false,
									}
								},
								{
									header : 'New Value',
									width : 200,
									dataIndex : 'nV',
									flex : 1,
									editor : {
										xtype : 'combo',
										allowBlank : false,
										minLength : 3,
										displayField : 'tvalue',
										valueField : 'tvalue',
										// typeAhead : false,
										// queryMode : 'remote',
										triggerAction : 'all',
										// hide arrow
										hideTrigger : true,
										store : 'SolrVerify'
									}
								// renderer : formatLineWrap,
								// renderer : tipRenderer,
								},
								{
									header : 'Update Cnt',
									dataIndex : 'uC',
									width : 30,
									hidden : true
								},
								{

									header : 'Delete Cnt',
									dataIndex : 'dC',
									width : 30,
									hidden : true
								},
								{
									// header : 'Modifier Cnt',
									header : 'Conflict',
									tooltip : 'Modification Count',
									align : 'center',
									width : 50,
								// flex:1

								// renderer : function(value, meta, record,
								// rowIndex) {
								// var sum = 0;
								// if (record.data['uC'] != null
								// && record.data['dC'] != null) {
								// var u = parseInt(record.data['uC']);
								// var d = parseInt(record.data['dC']);
								// if (u + d > 1) {
								// if (u > 0 && d > 0) {
								// meta.attr =
								// 'style="background-color:red;cursor:pointer;"';
								// } else {
								// meta.attr =
								// 'style="background-color:green;cursor:pointer;"';
								// }
								// meta.attr = meta.attr
								// + 'onclick="showOtherChangers()"';
								// } else if (u + d == 0) {
								// return '-';
								// }
								// sum = u + d - 1;
								// }
								// return sum;
								// }
								},
								{
									text : 'Delete',
									width : 60,
									menuDisabled : true,
									xtype : 'actioncolumn',
									align : 'center',
									icon : 'resources/icons/delete_ena.png',
									handler : function(grid, rowIndex,
											colIndex, actionItem, event,
											record, row) {
										grid.getStore().remove(record);
									},
								},
								{

									header : 'Health',
									tooltip : 'search similar values in repository',
									align : 'center',
									width : 50,
								// flex : 1
								// renderer : function(value, meta, c) {
								// meta.attr = 'style="cursor:pointer;"
								// onclick="showCheckValues('
								// + c.data["fI"] + ')"';
								// return '<img src="script/sgt/find.png"/>';
								//
								// }

								} ],
						store : 'actions'
					} ]
				});