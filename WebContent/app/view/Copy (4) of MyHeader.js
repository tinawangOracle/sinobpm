Ext
		.define(
				'sconsole.view.MyHeader',
				{
					statics : {
					// test : function(menuid) {
					// _menutree = Ext.ComponentQuery.query('mainmenu')[0];
					// if (Ext.isEmpty(_menutree)) {
					// alert('empty');
					// }
					// _menutree.removeAll();
					//							
					// console.log(menuid);
					// }
					},
					extend : 'Ext.Component',
					alias : 'widget.MyHeader',	collapsible : true,
					hideCollapseTool : false,
					// autoEl : {
					// tag : 'div',
					// },
					// baseCls : 'index_top',
					tpl : new Ext.XTemplate(
							'<div class="index_top">',
							'<div class="left_logo"><img src="resources/images/logo7.png" id="logoImg"></div>',
							'<div class="search_welcome">',
							'<div class="welcome_panel">',
							'<table border="0" style="display: inline-table;">',
							'<tbody><tr>',
							'<td nowrap="nowrap">',
							'<div style="padding: 5px;">',
							'<div onclick="setLink(\'default\')" id="welcomeContainer">',
							'<span></span>欢迎您，超级管理员',
							'</div>',
							'<div id="loginContainer">',
							'<a title="更多操作" id="loginButton" href="javascript:sconsole.util.Util.openTopMenu();"></a>',
							'<div style="clear: both"></div>',
							'<div class="itemDiv" id="loginBox"  onmouseleave="sconsole.util.Util.closemenu()">',
							'<ul>',
							'<tpl for="menus">',
							'<li><a href="javascript:{js}" resid="{id}" class="{cls}">{lbl}</a></li>',
							'</tpl>',
							'</ul>',
							'</div>',
							'<em id="shadeEm"></em>',
							'</div>',
							'</div>',
							'</td>',
							'<td>',
							'<div alt="站内消息" class="msg_div">',
							'<img src="resources/icons/msg_none.png" title="站内消息" id="inMsg">',
							'(<a id="labMsgSize" href="javascript:showReadMsgDlg()">0</a>)',
							'</div>',
							'</td>',
							'</tr></tbody>',
							'</table>',
							'</div>',
							'</div>',
							'<div class="index_menu">',
							'<div class="menuParent">',
							'<div class="menuPanel" id="menuPanel">',
							'<tpl for="btns">',
							'<a id="{id}" class="menuItem" href="javascript:sconsole.util.Util.changeMenu(\'{id}\',{menuId});"><img src="{src}"><span>{txt}</span></a>',
							'</tpl>', '</div>', '</div>', '</div>',
							'<div class="head_bg">', '</div>', '</div>'),
					data : {
						btns : [ {
							id : 'userGloBtn',
							src : 'resources/icons/user.gif',
							txt : 'User',
							menuId : 1
						}, {

							id : 'officeGloBtn',
							src : 'resources/icons/user.gif',
							txt : 'office',
							menuId : 2
						}, {

							id : 'workflowGloBtn',
							src : 'resources/icons/user.gif',
							txt : 'Workflow',
							menuId : 3
						}, {

							id : 'adminGloBtn',
							src : 'resources/icons/user.gif',
							txt : 'admin',
							menuId : 4
						} ],
						menus : [ {
							id : '-001',
							lbl : '修改密码',
							cls : 'more edit2',
							js : 'sconsole.util.Util.changepwd();'
						}, {
							id : '-002',
							lbl : '个人资料',
							cls : 'more detail2',
							js : 'sconsole.util.Util.editProfile();'
						}, {
							id : '-003',
							lbl : '退出系统',
							cls : 'more exit2',
							js : 'sconsole.util.Util.logout();'
						} ]
					},
					show_more_menu : function() {
						alert('text');
					},
					listeners : {
						afterrender : function(menuid) {
							_div = Ext.get('loginButton');
							if (Ext.isEmpty(_div))
								console.log('empty');
							else {
								_div.on({
									'mouseover' : {
										fn : this.openmenu,
										scope : this
									},
									'mouseup' : {
										fn : this.closemenu,
										scope : this
									}
								});

							}

						}
					},
					openmenu : function(e) {
						_div1 = Ext.get('loginBox');
						_div1.toggle();
						shade = Ext.get('shadeEm');
						shade.toggle();
						button = Ext.get('loginButton');
						button.toggleCls('active');
						// task = new Ext.util.DelayedTask(function() {
						// this.closemenu();
						// }, this);
						// task.delay(2000);
					},
					closemenu : function(e) {
						_div1 = Ext.get('loginBox');
						_div1.hide();
						shade = Ext.get('shadeEm');
						shade.hide();
						button = Ext.get('loginButton');
						button.removeCls('active');
					},

				});