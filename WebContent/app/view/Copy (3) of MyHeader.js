Ext
		.define(
				'sconsole.view.MyHeader',
				{
					extend : 'Ext.Component',
					alias : 'widget.MyHeader',
					// autoEl : {
					// tag : 'div',
					// },
					// baseCls : 'index_top',
					tpl : new Ext.XTemplate(
							'<div class="index_top">',
							'<div class="left_logo"><img src="resources/images/logo7.png" id="logoImg"></div>',
							'<div class="index_menu">',
							'<div class="menuParent">',
							'<div class="menuPanel" id="menuPanel">',
							'<tpl for="btns">',
							'<a id="{id}" class="menuItem"><img src="{src}"><span>{txt}</span></a>',
							'</tpl>', '</div>', '</div>', '</div>',
							'<div class="head_bg">', '</div>', '</div>'),
					data : {
						btns : [ {
							id : 'userBtn',
							src : 'resources/icons/user.gif',
							txt : 'User'
						}, {

							id : 'officeBtn',
							src : 'resources/icons/user.gif',
							txt : 'office'
						}, {

							id : 'workflowBtn',
							src : 'resources/icons/user.gif',
							txt : 'Workflow'
						}, {

							id : 'adminBtn',
							src : 'resources/icons/user.gif',
							txt : 'admin'
						} ]
					}

				});