Ext.define('sconsole.view.basegrid', {
   extend:'Ext.grid.Panel',
   alias:'widget.basegrid',
   columnLines: true,
   viewConfig: {
       stripeRows: true
   },
   dockedItems: [
                 {
                     xtype: 'addeditdelete'
                 }
             ],
});