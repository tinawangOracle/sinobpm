Ext.define('sconsole.view.Header', {
	extend : 'Ext.toolbar.Toolbar',
	alias : 'widget.appheader',

	height : 50,
	// ui : 'footer',
	style : 'border-bottom: 4px solid #4c72a4;',

	items : [ {
		xtype : 'image',
		width : 150,
		src : 'resources/images/Oracle-Logo1.png'
	}, {
		xtype : 'label',
		html : '<div id="titleHeader">String Console</div>'
	}, {
		xtype : 'tbfill'
	}, {
		xtype : 'tbseparator'
	} ]
});