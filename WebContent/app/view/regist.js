Ext.define('sconsole.view.regist', {
	// extend : 'Ext.container.Viewport',
	extend : 'Ext.panel.Panel',
	alias : 'widget.regist',
	layout : {
		type : 'border'
	},
	defaults : {
		border : false
	},
	items : [ {
		region : 'north',
		xtype : 'LoginHeader',
	// height:60

	}, {
		region : 'center',
		xtype : 'panel',
		// margins: '20 0 0 5',
		// height : 450,
		// width : 980,
		autoScroll : true,
		layout : {
			type : 'ux.center'
		},

		items : [ {
			xtype : 'panel',
			widthRatio : 0.65,
			layout : {
				type : 'vbox',
				align : 'stretch'
			},

			defaults : {
				margin : 15
			},
			border : false,
			height : 515,
			items : [ {
				xtype : 'panel',
				defaults : {
					margin : '0 10 20 10',
					bodyStyle : {
						background : 'white'
					}
				},

				height : 515,
				bodyStyle : {
					background : 'white'
				},
				items : [

				{
					xtype : 'panel',
					height : 120,
					layout : {
						type : 'fit'
					},
					ui : 'plain',
					html : translations.consoleTitle
				}, {
					xtype : 'panel',
					border : false,
					componentCls : 'top-border',
					layout : {
						type : 'hbox',
						align : 'stretch'
					},
					items : [ {
						xtype : 'form',
						width : 490,
						border : false,
						componentCls : 'right-border',
						baseCls : 'auth-border-right',
						defaults : {
							xtype : 'textfield',
							// anchor
							// :
							// '100%',
							allowBlank : false,
							labelStyle : 'margin-bottom:10px;',
							fieldStyle : 'margin-bottom:15px;',
							margin : 10,
							msgTarget : 'under'
						},
						items : [ {
							fieldLabel : translations.userEmail,
							name : 'userEmail',
							vtype : 'email'
						}, {
							fieldLabel : translations.userpwd,
							name : 'userpwd',
							itemId : 'userpwd',
							inputType : 'password',
							submitValue : false,
							listeners : {
								validitychange : function(field) {
									field.next().validate();
								},
								blur : function(field) {
									field.next().validate();
								}
							}
						}, {
							fieldLabel : translations.confirmPwd,
							name : 'confirmPwd',
							inputType : 'password',
							submitValue : false,
							vtype : 'password',
							initialPassField : 'userpwd'
						}, {
							fieldLabel : translations.chineseName,
							name : 'chineseName'
						}, {
							fieldLabel : translations.idcard,
							name : 'idcard'
						}, {
							fieldLabel : translations.phonenumber,
							name : 'phonenumber'
						}, {
							xtype : 'button',
							text : translations.registerBtn,
							scale : 'large',
							itemId : 'registerBtn'
						} ]
					}, {
						xtype : 'panel',
						width : 490,
						baseCls : 'auth-border-left',
						layout : {
							type : 'fit'
						},
						defaults : {
							margin : 15
						},
						items : [ {
							xtype : 'label',
							html : translations.loginMsg2
						}, {
							xtype : 'linkbutton',
							itemId : 'loginBtn',
							text : translations.signIn
						} ]
					} ]

				} ]
			}, {
				xtype : 'panel',
				layout : {
					type : 'hbox',
					pack : 'center',
					align : 'stretch'
				},
				border : false,
				defaults : {
					border : false,
				},
				items : [ {
					xtype : 'panel',
					width : 300,
					layout : {
						type : 'fit'
					},
					html : translations.copyright
				} ]
			} ]
		} ]
	} ]
});