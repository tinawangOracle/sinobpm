Ext.define('sconsole.view.user.changePwd', {
	extend : 'Ext.window.Window',
	alias : 'widget.changePwd',

	height : 200,
	width : 300,
	autoShow : true,
	layout : {
		type : 'fit'
	},
	title : 'Change Password',
	items : [ {
		xtype : 'form',
		frame : false,
		bodyPadding : 10,
		autoScroll : true,
		defaults : {
			xtype : 'textfield',
			inputType : 'password',
			submitValue : false,
			allowBlank : false,
			margin : 5,
		},
		items : [ {
			fieldLabel : 'Current Password',
			itemId:'currentPwd'
		}, {
			fieldLabel : 'New Password',
			itemId : 'pass',
			listeners : {
				validitychange : function(field) {
					field.next().validate();
				},
				blur : function(field) {
					field.next().validate();
				}
			}
		}, {
			fieldLabel : 'Confirm New Password',
			vtype : 'password',
			initialPassField : 'pass'
		} ],
		dockedItems : [ {
			xtype : 'toolbar',
			dock : 'top',
			items : [ {
				xtype : 'button',
				text : 'OK',
				itemId : 'okBtn'
			}, '-', {
				xtype : 'button',
				text : 'Cancel',
				itemId : 'cancelBtn'
			} ]
		} ],
	} ]
});