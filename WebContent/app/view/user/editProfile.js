Ext.define('sconsole.view.user.editProfile', {
	extend : 'Ext.form.Panel',
	alias : 'widget.editProfile',
	defaults : {
		afterLabelTextTpl : sconsole.util.Util.required,
		xtype : 'textfield',
		margin : '5,5,5,5',
		width : 400,
		allowBlank : false
	},
	items : [ {
		fieldLabel : '中文姓名',
		itemId : 'last',
		name : 'fullname',
	}, {
		fieldLabel : '电话号码',
		name : 'phone'
	}, {
		fieldLabel : '身份证号',
		name : 'idcard'
	}, {
		fieldLabel : '邮箱',
		name : 'email'
	} ],
	dockedItems : [ {
		xtype : 'toolbar',
		dock : 'top',
		items : [ {
			xtype : 'button',
			text : 'OK',
			itemId : 'okBtn'
		}, '-', {
			xtype : 'button',
			text : 'Cancel',
			itemId : 'cancelBtn'
		} ]
	} ],
});