Ext.define('sconsole.view.authentication.CapsLockTooltip', {
    extend: 'Ext.tip.QuickTip',
    alias: 'widget.capslocktooltip',

    target: 'password',
    anchor: 'top',
    anchorOffset: 0,
    width: 300,
    dismissDelay: 0,
    autoHide: false,
    title: '<div class="capslock">'+ 'capsLockTitle' + '</div>',
    html: '<div>'+ 'capsLockMsg1' + '</div>' +
        '<div>'+ 'capsLockMsg2' + '</div><br/>' +
        '<div>'+ 'capsLockMsg3' + '</div>' +
        '<div>'+ 'capsLockMsg4' + '</div>'
});