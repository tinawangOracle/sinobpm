Ext
		.define(
				'sconsole.view.tasks.todo',
				{
					extend : 'Ext.panel.Panel',
					alias : 'widget.todoJobsPanel',
					layout : {
						type : 'border'
					},
					items : [
							{
								region : 'west',
								split : true,
								flex : 2,
								xtype : 'grid',
								itemId : 'todoJobsList',
								store : 'tasks.todo',
								dockedItems : [ {
									xtype : 'chgNotifyPagingbar',
									dock : 'top',
									displayInfo : false,
									store : 'tasks.todo',
								} ],
								listeners : {
									beforerender : function(thisobj) {
										_store = thisobj.getStore();
										_store.load();
									},
									selectionchange : function(thisobj,
											_selected, eOpts) {
										record = _selected[0];
										if (Ext.isEmpty(record))
											return;
										Ext.Ajax
												.request({
													url : "workflow/getTaskDetail.action",
													success : function(a) {
														var c = sconsole.util.Util
																.decodeJSON(a.responseText);
														// _parent = thisobj
														// .up('todoJobsPanel');
														_parent = Ext.ComponentQuery
																.query('todoJobsPanel')[0];
														mcomp = _parent
																.down('#taskGeneral');
														mcomp.setVisible(true);
														_v = mcomp
																.down('#taskDesc');
														_v.setValue(c.desc);
														_v = mcomp
																.down('#taskDue');
														_v.setValue(c.dueto);
														_v = mcomp
																.down('#taskInstLink');
														_v
																.setValue('<a href="#" onclick="sconsole.util.Util.openProcInstance(\''
																		+ c.processInstanceId
																		+ '\');">click</a>');

														// form
														mcomp = _parent
																.down('#taskForm');
														mcomp.setVisible(true);
														sconsole.util.Util
																.openStartForm(
																		mcomp,
																		c.form,
																		c.enums);
														// attachments
														mcomp = _parent
																.down('#taskAttachs');
														mcomp.setVisible(true);
														_store = mcomp
																.getStore();
														_store.removeAll();
														_store
																.loadData(c.attachments);
														// subtasks
														mcomp = _parent
																.down('#taskSubTasks');
														mcomp.setVisible(true);
														_store = mcomp
																.getStore();
														_store.removeAll();
														_store
																.loadData(c.subtasks);

														mcomp = _parent
																.down('#usersPanel');
														mcomp.setVisible(true);
														mcomp.removeAll();
														Ext.Array
																.each(
																		c.people,
																		function(
																				item,
																				index,
																				countriesItSelf) {

																			_type = item.type;
																			_mtype = '参与者';
																			_btnTxt = '刪除';
																			switch (_type) {
																			case 'owner':
																				_mtype = '所有者';
																				break;
																			case 'assignee':
																				_mtype = '受理人';
																				_btnTxt = '重新分配';
																				break;
																			default:
																				break;
																			}
																			mcomp
																					.add(Ext
																							.create(
																									'Ext.form.FieldContainer',
																									{
																										layout : 'hbox',
																										items : [
																												{
																													xtype : 'image',
																													flex : 1,
																													border : 2,
																													style : {
																														borderColor : 'black',
																														borderStyle : 'solid'
																													},
																													width : 32,
																													height : 32
																												},
																												{
																													xtype : 'fieldcontainer',
																													layout : 'vbox',
																													items : [
																															{
																																xtype : 'displayfield',
																																value : '<b>'
																																		+ item.userId
																																		+ '</b>',
																																flex : 1
																															},
																															{
																																xtype : 'fieldcontainer',
																																layout : 'hbox',
																																flex : 1,
																																items : [
																																		{
																																			xtype : 'displayfield',
																																			value : _mtype,
																																			flex : 1
																																		},
																																		{
																																			xtype : 'button',
																																			text : _btnTxt,
																																			flex : 1
																																		} ]
																															} ],
																													flex : 1
																												} ],
																										columnWidth : 0.5
																									}));
																		});
													},
													failure : function(a) {

													},
													params : {
														taskId : record
																.get('id')
													},
													waitMsg : "waiting...."
												});
									}
								},
								columns : [ {
									header : "name",
									dataIndex : "name",
									resizable : true,
									flex : 1,
									renderer : function(val, meta, rec, ridx) {
										return val;
									},
								} ]
							},
							{
								region : 'center',
								xtype : 'panel',
								autoScroll : true,
								defaults : {
									margin : '10 20 20 10' // (top, right,
								// bottom, left).
								},
								itemId : 'todoJobDetail',
								layout : {
									type : 'vbox',
									align : 'stretch',
									defaultMargins : '{top: 0, right: 0, bottom: 20, left: 0}'
								},
								flex : 5,
								dockedItems : [ {
									xtype : 'toolbar',
									dock : 'top',
									items : [
											{
												text : 'Submit',
												itemId : 'submitBtn'
											},
											'-',
											{
												text : 'Reset',
												itemId : 'resetBtn',
												tooltip : 'It will clear all unsaved modification.'
											},
									// '-',
									// {
									// text : 'Complete',
									// itemId : 'completeBtn'
									// }
									]
								} ],
								items : [
										{
											xtype : 'panel',
											title : '基本信息',
											border : false,
											itemId : 'taskGeneral',
											layout : 'anchor',
											hidden : true,
											defaults : {
												labelWidth : 100,
												labelAlign : 'right',
												margin : 5
											},
											items : [
													{
														xtype : 'textareafield',
														fieldLabel : '描述',
														itemId : 'taskDesc',
														flex : 1,
														grow : true,
														growMax : 10,
														anchor : '100%'
													},
													{
														xtype : 'datefield',
														fieldLabel : '到期日',
														itemId : 'taskDue',
														anchor : '50%'
													},
													{
														xtype : 'displayfield',
														fieldLabel : '所属流程实例',
														itemId : 'taskInstLink',
														value : '<a href="#" onclick="sconsole.util.Util.openProcInstance();">click</a>',
														flex : 1
													} ]

										},
										{
											xtype : 'form',
											title : '表单',
											hidden : true,
											defaults : {
												labelWidth : 100,
												labelAlign : 'right',
												margin : '10 20 3 10' // (top,
											// right,
											// bottom,
											// left).
											},
											border : false,
											itemId : 'taskForm',
											items : [],
										// flex : 1
										},
										{
											xtype : 'panel',
											border : false,
											hidden : true,
											title : 'People',
											layout : 'column',
											itemId : 'usersPanel',
											items : []
										},
										{
											xtype : 'grid',
											hidden : true,
											itemId : 'taskAttachs',
											title : 'Attachements',
											store : 'tasks.taskAttachments',
											emptyText : 'No attachments',
											columns : [
													{
														header : "name",
														dataIndex : "name",
														resizable : true,
														flex : 1,
													},
													{
														header : "type",
														dataIndex : "type",
														resizable : true,
														flex : 1,
													},
													{
														header : "Url",
														dataIndex : "url",
														resizable : true,
														flex : 1,
													},
													{
														xtype : 'actioncolumn',
														items : [
																{
																	icon : 'resources/icons/delete_ena.png',
																	tooltip : 'Delete',
																	handler : function(
																			_g,
																			rowIdx,
																			colIdx) {
																		_rec = _g
																				.getStore()
																				.getAt(
																						rowIdx);
																		Ext.MessageBox
																				.confirm(
																						'Delete Model',
																						'Are you sure to delete this attachment?',
																						function(
																								bId,
																								text,
																								opt) {
																							if (bId == 'yes') {
																								Ext
																										.get(
																												_g
																														.getEl())
																										.mask(
																												'Deleting...',
																												'Waiting');
																								Ext.Ajax
																										.request({
																											url : 'workflow/deleteTaskAttachment.action',
																											success : function(
																													response,
																													opts) {
																												Ext
																														.get(
																																_g
																																		.getEl())
																														.unmask();
																												_ret = sconsole.util.Util
																														.decodeJSON(response.responseText);
																												if (_ret.success == 'true') {
																													sconsole.util.Util
																															.showErrorMsg(_ret.msg);
																													_g
																															.getStore()
																															.load();
																												} else {
																													Ext
																															.get(
																																	_g
																																			.getEl())
																															.unmask();
																													sconsole.util.Util
																															.showErrorMsg(_ret.msg);
																												}
																											},
																											failure : function(
																													response,
																													opts) {
																												sconsole.util.Util
																														.showErrorMsg(response.responseText);
																											},
																											params : {
																												aid : _rec
																														.get('id')
																											}
																										});

																							}
																						});

																	}
																},
																'-',
																{
																	icon : 'resources/icons/download.png',
																	tooltip : 'Download',
																	handler : function(
																			_g,
																			rowIdx,
																			colIdx) {
																		_rec = _g
																				.getStore()
																				.getAt(
																						rowIdx);
																		Ext.DomHelper
																				.append(
																						document.body,
																						{
																							tag : "iframe",
																							frameBorder : 0,
																							width : 0,
																							height : 0,
																							css : "display:none;visibility:hidden;height:0px;",
																							src : encodeURI("workflow/downloadTaskAttachment.action?aid="
																									+ _rec
																											.get('id'))
																						});

																	}
																} ]
													} ],
											dockedItems : [ {
												xtype : 'toolbar',
												dock : 'top',
												items : [ {
													text : 'Add',
													itemId : 'addBtn',
												// disabled : true,
												}, '-', {
													text : 'Refresh',
													itemId : 'refreshBtn',

												} ]
											} ],
										// flex : 1
										}, {
											xtype : 'grid',
											itemId : 'taskSubTasks',
											hidden : true,
											title : 'Sub Tasks',
											emptyText : 'No Subtasks',
											store : 'tasks.subTasks',
											columns : [ {
												header : "name",
												dataIndex : "name",
												resizable : true,
												flex : 1,
											} ],
											dockedItems : [ {
												xtype : 'toolbar',
												dock : 'top',
												items : [ {
													text : 'Add',
													itemId : 'addBtn',
													disabled : true
												}, '-', {
													text : 'Delete',
													itemId : 'delBtn',
													disabled : true
												}, '-', ]
											} ],
										// flex : 1
										} ]

							} ]
				});