Ext.define('sconsole.view.tasks.done', {
	extend : 'Ext.panel.Panel',
	alias : 'widget.doneJobsPanel',
	layout : {
		type : 'border'
	},
	items : [ {
		region : 'west',
		split : true,
		flex : 2,
		xtype : 'grid',
		itemId : 'doneJobsList',
		store : 'tasks.done',
		columns : [ {
			header : "name",
			dataIndex : "name",
			resizable : true,
			flex : 1,
			renderer : function(val, meta, rec, ridx) {
				return val;
			},
		} ],
		dockedItems : [ {
			xtype : 'pagingtoolbar',
			dock : 'top',
			displayInfo : false,
			store : 'tasks.done',
		} ],
		listeners : {
			beforerender : function(thisobj) {
				_store = thisobj.getStore();
				_store.load();
			},
		}
	}, {
		region : 'center',
		xtype : 'panel',
		flex : 5
	} ]

});