Ext
		.define(
				'sconsole.view.tasks.uploadContent',
				{
					extend : 'Ext.window.Window',
					alias : 'widget.uploadFilePanel',
					title : 'Upload File',
					width : 400,
					height : 200,
					layout : 'fit',
					tid : -1,
					sid : -1,
					items : [ {
						xtype : 'form',
						defaults : {
							labelWidth : 100,
							labelAlign : 'right',
							margin : '10 20 3 10' // (top, right, bottom,
						// left).
						},
						dockedItems : [ {
							xtype : 'toolbar',
							dock : 'top',
							items : [
									{
										text : 'OK',
										handler : function(thisobj) {
											var form = thisobj.up('form')
													.getForm();
											var _p = thisobj
													.up('uploadFilePanel');

											if (form.isValid()) {

												form
														.submit({
															url : 'workflow/uploadFile.action',
															waitMsg : 'Uploading file...',
															params : {
																tid : _p.tid,
																sid : _p.sid,
															},
															success : function(
																	fp, o) {

																_ret = o.result;
																if (_ret.success == 'true') {
																	sconsole.util.Util
																			.showInfoMsg(o.result.msg);
																	Ext.Ajax
																			.request({
																				url : 'workflow/getTaskAttachments.action',
																				success : function(
																						response,
																						opts) {
																					_ret = sconsole.util.Util
																							.decodeJSON(response.responseText);
																					_g = Ext.ComponentQuery
																							.query('todoJobsPanel #taskAttachs')[0];
																					if (_ret.success == 'true') {
																						_s = _g
																								.getStore();
																						_s
																								.removeAll();
																						_s
																								.loadData(_ret.attachments);
																					}

																				},
																				failure : function(response,
																						opts) {

																				},
																				params : {
																					tid : _p.tid,
																					sid : _p.sid,
																				}
																			});
																} else
																	sconsole.util.Util
																			.showErrorMsg(o.result.msg);
																_p.close();
															},
															failure : function(
																	fp, o) {
																sconsole.util.Util
																		.showErrorMsg(o.result.msg);
																_p.close();
															}
														});
											}
										}
									}, '-', 'Cancel' ]
						} ],
						items : [ {
							xtype : 'filefield',
							name : 'userfile',
							fieldLabel : 'File',
							labelWidth : 50,
							msgTarget : 'side',
							allowBlank : false,
							anchor : '100%',
							buttonText : 'Select...'
						} ]
					} ]
				});