Ext
		.define(
				'sconsole.view.LoginHeader',
				{
					// xtype : 'panel',
					extend : 'Ext.panel.Panel',
					alias : 'widget.LoginHeader',
					border : false,
					height : 50,
					layout : {
						type : 'hbox',
						// pack : 'center',
						align : 'stretch'
					},
					items : [
							{
								xtype : 'image',
								width : 150,
								height: 30,
								maxHeight:30,
								src : 'resources/images/logo7.png'
							},
							{
								// ui : 'plain',
								border : false,
							//	width:300,
								html : '<span class="university-label"> | <b>中洲BPM系统</b></span>'
							}, {
								xtype : 'tbfill'
							}, {
								xtype : 'tbseparator'
							}					]
				});