Ext.define('sconsole.view.menu.Accordion', {
	extend : 'Ext.panel.Panel',
	alias : 'widget.mainmenu',
	width : 200,
	layout : {
		type : 'accordion',
		pack : 'start',
		multi : false,
	},
	
	collapsible : true,
	hideCollapseTool : false,
	title : 'Menu'
});
