Ext.define('sconsole.view.menu.Item', {
	extend : 'Ext.tree.Panel',
	alias : 'widget.workflowMenu',
	border : 0,
	autoScroll : true,
	rootVisible : false
});