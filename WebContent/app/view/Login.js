Ext.define('sconsole.view.Login', {
	requires : [ 'sconsole.view.comp.LinkButton' ],
	extend : 'Ext.panel.Panel',
	alias : 'widget.login',
	layout : {
		type : 'border'
	},
	defaults : {
		border : false,
		autoScroll : true
	},
	requires : [ 'sconsole.view.Translation' ],
	items : [ {
		region : 'north',
		xtype : 'LoginHeader',
	//	flex : 1,
	}, {
		region : 'center',
		xtype : 'panel',
	//	flex : 4,
		autoScroll : true,
		layout : {
			type : 'ux.center'
		},
		items : [ {
			xtype : 'panel',
			widthRatio : 0.65,
			layout : {
				type : 'vbox',
				align : 'stretch'
			},

			defaults : {
				margin : 15
			},
			border : false,
		//	height : 1600,
			items : [ {
				xtype : 'panel',
				defaults : {
					margin : '0 10 20 10',
					bodyStyle : {
						background : 'white'
					}
				},
				layout : {
					type : 'fit'
				},
		//		height:600,
				autoScroll : true,
				bodyStyle : {
					background : 'white'
				},
				items : [
				{
					xtype : 'panel',
					height : 120,
					layout : {
						type : 'fit'
					},
					ui : 'plain',
					html : translations.consoleTitle
				}, {
					xtype : 'panel',
					border : false,
					componentCls : 'top-border',
					layout : {
						type : 'hbox',
						align : 'stretch'
					},
					items : [ {
						xtype : 'form',
						width : 490,
						height:450,
						border : false,
						componentCls : 'right-border',
						baseCls : 'auth-border-right',
						defaults : {
							xtype : 'textfield',
							// anchor
							// :
							// '100%',
							allowBlank : false,
							labelStyle : 'margin-bottom:10px;',
							fieldStyle : 'margin-bottom:15px;',
							margin : 10,
							msgTarget : 'under'
						},
						items : [ {
							name : 'user',
							fieldLabel : translations.user,
							labelAlign : 'top',
							maxLength : 25
						}, {
							inputType : 'password',
							name : 'password',
							fieldLabel : translations.password,
							enableKeyEvents : true,
							id : 'password',
							labelAlign : 'top',
							maxLength : 40,
							msgTarget : 'side'
						}, {
							xtype : 'combobox',
							name : 'logins',
							store : 'LoginStates',
							fieldLabel : translations.sessionStatus,
							labelAlign : 'top',
							labelPad : 30,
							queryMode : 'local',
							displayField : 'val',
							valueField : 'id',
							value : '0'
						}, {
							xtype : 'translation'
						}, {
							xtype : 'button',
							text : translations.signIn,
							scale : 'large',
							itemId : 'submit',
//							id : 'loginBtn'
						} ]
					}, {
						xtype : 'panel',
						width : 490,
						baseCls : 'auth-border-left',
						layout : {
							type : 'fit'
						},
						defaults : {
							margin : 15
						},
						items : [ {
							xtype : 'label',
							html : translations.loginMsg1
						}, {
							xtype : 'linkbutton',
							itemId : 'registerBtn',
							text : translations.registerTitle
						} ]
					} ]

				} ]
			}, {
				xtype : 'panel',
				layout : {
					type : 'hbox',
					pack : 'center',
					align : 'stretch'
				},
				border : false,
				defaults : {
					border : false,
				},
				items : [ {
					xtype : 'panel',
					width : 300,
					layout : {
						type : 'fit'
					},
					html : translations.copyright
				} ]
			} ]
		} ]
	} ]
});