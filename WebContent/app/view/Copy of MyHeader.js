Ext.define('sconsole.view.MyHeader', {
	// xtype : 'panel',
	extend : 'Ext.panel.Panel',
	alias : 'widget.MyHeader',
	border : false,
	height : 60,
	layout : {
		type : 'hbox',
		// pack : 'center',
		align : 'stretch'
	},
	items : [ {
		xtype : 'image',
		width : 150,
		src : 'resources/images/logo8.jpg'
	}, {
		// ui : 'plain',
		border : false,
		// width:300,
		html : translations.logoTitle
	}, {
		xtype : 'tbseparator'
	}, {
		xtype : 'button',
		text : '工作流管理',
		itemId : 'workflow',
//		height : 85,
		iconCls : 'logout',
		iconAlign : 'top'
	}, {
		xtype : 'tbfill'
	}, {
		xtype : 'tbseparator'
	}, {
		xtype : 'button',
		text : 'logout',
		itemId : 'logout',
		iconCls : 'logout',
	}

	]
});