Ext
		.define(
				'sconsole.util.Util',
				{

					requires : 'sconsole.store.setting',
					statics : {
						required : '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>',
						wrapLineStyle : 'white-space:pre-wrap !important;word-wrap: break-word;',
						decodeJSON : function(text) {
							var result = Ext.JSON.decode(text, true);

							if (!result) {
								result = {};
								result.success = false;
								result.msg = text;
							}

							return result;
						},
						objLevelRenderer : function(value, meta, record,
								rowIndex) {
							if (Ext.isEmpty(value))
								return '';
							if (value == 0)
								return 'Product';
							else if (value == 2)
								return 'Component';
							else if (value == 1)
								return 'Release';
							return value;
						},
						showInfoMsg : function(text) {

							Ext.Msg.show({
								title : 'Info',
								msg : text,
								icon : Ext.Msg.INFO,
								buttons : Ext.Msg.OK
							});
						},
						sendErrorMsg : function() {
							Ext.Msg
									.show({
										title : 'Error!',
										msg : 'Ajax Request fail, please open firebug console, and send me the error message!',
										icon : Ext.Msg.ERROR,
										buttons : Ext.Msg.OK
									});
						},
						showErrorMsg : function(text) {
							Ext.Msg.show({
								title : 'Error!',
								msg : text,
								icon : Ext.Msg.ERROR,
								buttons : Ext.Msg.OK
							});
						},
						boolGrpRenderer : function(data, metadata, record,
								rowIndex, columnIndex, store) {
							var _v = data;
							if (data == 0) {
								_v = 'no';
							} else if (data == 1)
								_v = 'yes';

							if (rowIndex == 0)
								return _v;

							lastRec = store.getAt(rowIndex - 1);
							if (record.get('pI') == lastRec.get('pI')) {
								return '';
							} else
								return _v;

						},
						boolRenderer : function(data, metadata, record,
								rowIndex, columnIndex, store) {
							if (data == 0) {
								return 'no';

							} else if (data == 1)
								return 'yes';
							return a;

						},
						tipRenderer : function(data, metadata, record,
								rowIndex, columnIndex, store) {
							metadata.tdAttr = ' data-qtip="'
									+ Ext.util.Format.htmlEncode(data)
									+ '" data-qclass="normalwhite"';
							return data;
						},
						formatLineWrap : function(value, meta) {
							str = '<div class="normalwhite">' + value
									+ '</div>';
							return str;
						},
						wordUpperCase : function(word) {

							var wLength = word.length;

							var ss = "";

							if (wLength > 0) {

								ss = (word.substring(0, 1).toUpperCase())

										+ (word.substring(1, wLength)
												.toLowerCase() + " ");

							}

							return ss;

						},
						sentenceUpperCase : function(sentence) {

							var upperSentence = '';

							var array = sentence.split(' ');

							for (var i = 0; i < array.length; i++) {

								upperSentence += sconsole.util.Util
										.wordUpperCase(array[i]);

							}

							return upperSentence;

						},
						lblUpperCase : function(label) {
							var lbl = label.replace(/_/g, " ");
							var rst = sconsole.util.Util.sentenceUpperCase(lbl);
							return rst;

						},
						redRenderer : function(a, b, c, d, e, f) {
							if (a > 0) {
								b.tdAttr = 'style="background-color:red;"';
								return a
										+ '('
										+ Ext.util.Format.round(c.get("sim100")
												* 100 / c.get("rowCnt"), 2)
										+ '%)';

							}

							return a;

						},

						yellowRenderer : function(a, b, c, d, e, f) {

							if (a > 0) {

								b.tdAttr = 'style="background-color:yellow;"';

								return a
										+ '('
										+ Ext.util.Format.round(c.get("sim90")
												* 100 / c.get("rowCnt"), 2)
										+ '%)';

							}

							return a;

						},

						blueRenderer : function(a, b, c, d, e, f) {

							if (a > 0) {

								b.tdAttr = 'style="background-color:RoyalBlue;"';

								return a
										+ '('
										+ Ext.util.Format.round(c.get("sim80")
												* 100 / c.get("rowCnt"), 2)
										+ '%)';

							}

							return a;

						},

						pinkRenderer : function(a, b, c, d, e, f) {

							if (a > 0) {

								b.tdAttr = 'style="background-color:Pink;"';

								return a
										+ '('
										+ Ext.util.Format.round(c.get("sim70")
												* 100 / c.get("rowCnt"), 2)
										+ '%)';

							}

							return a;

						},

						otherRenderer : function(a, b, c, d, e, f) {

							if (a > 0) {

								return a
										+ '('
										+ Ext.util.Format.round(a * 100
												/ c.get("rowCnt"), 2) + '%)';

							}

							return a;

						},

						disableFiltersInNewAction : function(thisobj, eopts) {
							d = Ext.ComponentQuery
									.query('newAction form button[text="Back"]')[0];
							d.setDisabled(false);
							d = Ext.ComponentQuery
									.query('newAction form button[text="Clear"]')[0];
							d.setDisabled(true);
							d = Ext.ComponentQuery
									.query('newAction form button[text="Search"]')[0];
							d.setDisabled(true);

							d = Ext.ComponentQuery
									.query('newAction form button[text="Search"]')[0];
							d.setDisabled(true);
							for (i = 1; i < 6; i++) {
								d = Ext.ComponentQuery
										.query('newAction combo:nth-child(' + i
												+ ')')[0];
								d.setDisabled(true);
							}
							d = Ext.ComponentQuery
									.query('newAction #searchKeywordFld')[0];
							d.setDisabled(true);
							d = Ext.ComponentQuery
									.query('newAction #fileNameLike')[0];
							d.setDisabled(true);
							d = Ext.ComponentQuery
									.query('newAction #returnRows')[0];
							d.setDisabled(true);

						},
						enableFiltersInNewAction : function(thisobj, eopts) {
							d = Ext.ComponentQuery
									.query('newAction form button[text="Back"]')[0];
							d.setDisabled(true);
							d = Ext.ComponentQuery
									.query('newAction form button[text="Clear"]')[0];
							d.setDisabled(false);
							d = Ext.ComponentQuery
									.query('newAction form button[text="Search"]')[0];
							d.setDisabled(false);
							for (i = 1; i < 6; i++) {
								d = Ext.ComponentQuery
										.query('newAction combo:nth-child(' + i
												+ ')')[0];
								d.setDisabled(false);
							}
							d = Ext.ComponentQuery
									.query('newAction #searchKeywordFld')[0];
							d.setDisabled(false);
							d = Ext.ComponentQuery
									.query('newAction #fileNameLike')[0];
							d.setDisabled(false);
							d = Ext.ComponentQuery
									.query('newAction #returnRows')[0];
							d.setDisabled(false);
						},

						recursiveMenu : function(pnode, clds, level) {
							// Ext.each(item.items(), function(itens) {
							// console.log(itens);//store
							// console.log(pnode);
							Ext.each(clds, function(item) {
								// console.log(item); // item
								if (level == 0)
									_node = pnode.appendChild({
										text : item.get('text'),
										id : item.get('id'),
										parentId : item.get('parent_id'),
										className : item.get('className'),
										iconCls : item.get('iconCls')

									});
								else
									_node = pnode.appendChild({
										text : item.text,
										// leaf : true,
										id : item.id,
										parentId : item.parent_id,
										className : item.className,
										iconCls : item.iconCls

									});
								if (!Ext.isEmpty(item.data)) {
									_cld = item.data.items;
									if (!Ext.isEmpty(_cld)) {
										// _node.expanded=true;
										_node.leaf = false;
										// console.log(_cld.length);
										sconsole.util.Util.recursiveMenu(_node,
												_cld, level + 1);
									} else
										_node.leaf = true;
								} else {
									_node.leaf = true;
								}

							});
							// console.log(pnode);
							pnode.expand(true);
							// });
						},
						changeMenu : function(bid, menuId) {
							// console.log(bid);
							var doc = document.getElementById('menuPanel');
							var c = doc.getElementsByTagName('a');

							for (var i = 0; i < c.length; i++) {
								c[i].className = "menuItem";
							}
							_d = document.getElementById(bid);
							_d.className += " menuItem_hover";

							_menutree = Ext.ComponentQuery.query('mainmenu')[0];
							if (Ext.isEmpty(_menutree)) {
								alert('empty');
							}
							_menutree.removeAll();

							if (menuId == 1) {
								_store = Ext.data.StoreManager
										.lookup('MenuUser');
							} else if (menuId == 2) {
								_store = Ext.data.StoreManager
										.lookup('MenuOffice');
							} else if (menuId == 3) {
								_store = Ext.data.StoreManager
										.lookup('MenuWorkflow');
							} else if (menuId == 4) {
								_store = Ext.data.StoreManager
										.lookup('MenuAdmin');
							}

							if (Ext.isEmpty(_store)) {
								alert('blank store');
								return;
							}
							var menuPanel = Ext.ComponentQuery
									.query("mainmenu")[0];
							_cnt = 0;
							_store.each(function(item, index, len) {
								var menu = Ext.create(
										'sconsole.view.menu.Item', {
											title : item.get('text')
										});
								if (_cnt > 0)
									menu.collapsed = true;
								_cnt++;

								Ext.each(item.items(), function(itens) {
									sconsole.util.Util
											.recursiveMenu(menu.getRootNode(),
													itens.data.items, 0);
								});
								// Ext.each(item.items(), function(itens) {
								//
								// Ext.each(itens.data.items, function(item) {
								//
								// menu.getRootNode().appendChild({
								// text : item.get('text'),
								// leaf : true,
								// id : item.get('id'),
								// parentId : item.get('parent_id'),
								// // icon : item.get('icon'),
								// iconCls : item.get('iconCls')
								//
								// });
								// });
								// });
								menuPanel.add(menu);
							});

						},
						logout : function(options) {
							fromWhere = 0;
							if (!Ext.isEmpty(options)) {
								fromWhere = options;
							}
							Ext.Ajax
									.request({
										url : 'login/logout.action',
										success : function(conn, response,
												options, eOpts) {
											var result = sconsole.util.Util
													.decodeJSON(conn.responseText);
											if (result.success) {
												if (fromWhere == 1) {
													Ext.MessageBox
															.alert(
																	"Info",
																	"Your session has expired!",
																	function(
																			btn) {
																		// window.location
																		// .reload();
																	});
													window.location.reload();
												} else {
													window.location.reload();
												}
											} else {

												sconsole.util.Util
														.showErrorMsg(conn.responseText);
											}
										},
										failure : function(conn, response,
												options, eOpts) {
											sconsole.util.Util
													.showErrorMsg(conn.responseText);
										}
									});
						},
						changepwd : function() {
							win = Ext.create('sconsole.view.user.changePwd');
							win.show();
						},
						editProfile : function() {
							win = Ext.create('sconsole.view.user.editProfile');
							win.show();
						},
						openTopMenu : function() {
							_div1 = Ext.get('loginBox');
							_div1.toggle();
							shade = Ext.get('shadeEm');
							shade.toggle();
							button = Ext.get('loginButton');
							button.toggleCls('active');
							task = new Ext.util.DelayedTask(function() {
								this.closemenu();
							}, this);
							task.delay(2000);
						},
						closemenu : function(e) {
							_div1 = Ext.get('loginBox');
							_div1.hide();
							shade = Ext.get('shadeEm');
							shade.hide();
							button = Ext.get('loginButton');
							button.removeCls('active');
						},
						openTab : function(cls, txt) {
							mainPanel = Ext.ComponentQuery.query('mainpanel')[0];
							var newTab = mainPanel.items.findBy(function(tab) {
								return tab.title == txt;
							});
							if (newTab) {
								mainPanel.setActiveTab(newTab);
								return newTab;
							}
							newTab = mainPanel.add({
								xtype : cls,
								closable : true,
								title : txt
							});
							if (newTab) {
								mainPanel.setActiveTab(newTab);
								return newTab;
							}
						},
						ajaxFailure : function(response) {
							var c = sconsole.util.Util
									.decodeJSON(response.responseText);
							sconsole.util.Util.showErrorMsg(c.msg);
						},
						openStartForm : function(mcomp, r, e) {
							mcomp.removeAll();
							Ext.Array
									.each(
											r,
											function(record, index,
													countriesItSelf) {
												_name = record.name;
												if (Ext.isEmpty(record.type)) {
													_type = 'NA';
												} else
													_type = record.type.name;
												// console.log(_type);

												switch (_type) {
												case "NA":
												case "string":
													mcomp
															.add(Ext
																	.create(
																			'Ext.form.field.Text',
																			{
																				id : record.id,
																				name : record.id,
																				fieldLabel : _name,
																				allowBlank : record.required == 'true' ? false
																						: true,
																				afterLabelTextTpl : record.required == 'true' ? sconsole.util.Util.required
																						: '',
																				validateBlank : true,
																				value : record.value,
																				disabled : record.writable == 'true' ? false
																						: true,
																				readOnly : record.writable == 'true' ? false
																						: true
																			}));
													break;
//												case "string":
//													mcomp
//															.add(Ext
//																	.create(
//																			'Ext.form.field.Text',
//																			{
//																				id : record.id,
//																				name : record.id,
//																				fieldLabel : _name,
//																				allowBlank : record.required == 'true' ? false
//																						: true,
//																				afterLabelTextTpl : record.required == 'true' ? sconsole.util.Util.required
//																						: '',
//																				validateBlank : true,
//																				value : record.value,
//																				disabled : record.writable == 'true' ? false
//																						: true,
//																				readOnly : record.writable == 'true' ? false
//																						: true
//																			}));
//													break;
												case "enum":
													_store = Ext
															.create('sconsole.store.tasks.enum');
													_store.loadData(eval('(e.'
															+ record.id + ')'));
													mcomp
															.add(Ext
																	.create(
																			'Ext.form.field.ComboBox',
																			{
																				id : record.id,
																				name : record.id,
																				fieldLabel : _name,
																				store : _store,
																				queryMode : 'local',

																				allowBlank : record.required == 'true' ? false
																						: true,
																				afterLabelTextTpl : record.required == 'true' ? sconsole.util.Util.required
																						: '',
																				validateBlank : true,
																				displayField : 'label',
																				valueField : 'key',
																			}));
													break;
												case "long":
													mcomp
															.add(Ext
																	.create(
																			'Ext.form.field.Number',
																			{
																				id : record.id,
																				name : record.id,
																				fieldLabel : _name,
																				value : record.value,
																				allowBlank : record.required == 'true' ? false
																						: true,
																				afterLabelTextTpl : record.required == 'true' ? sconsole.util.Util.required
																						: '',
																				validateBlank : true,
																				disabled : record.writable == 'true' ? false
																						: true
																			}));
													break;
												case "date":
													mcomp
															.add(Ext
																	.create(
																			'Ext.form.field.Date',
																			{
																				id : record.id,
																				name : record.id,
																				fieldLabel : _name,
																				value : record.value,
																				allowBlank : record.required == 'true' ? false
																						: true,
																				afterLabelTextTpl : record.required == 'true' ? sconsole.util.Util.required
																						: '',
																				disabled : record.writable == 'true' ? false
																						: true,
																				validateBlank : true,
																				format : sconsole.util.Util
																						.javaDateToExtjs(record.type.pattern),
																			}));

													break;
												case "user":
													mcomp
															.add(Ext
																	.create(
																			'Ext.form.field.ComboBox',
																			{
																				id : record.id,
																				name : record.id,
																				fieldLabel : 'Choose a user',
																				store : 'admin.users',
																				typeAhead : true,
																				allowBlank : record.required == 'true' ? false
																						: true,
																				afterLabelTextTpl : record.required == 'true' ? sconsole.util.Util.required
																						: '',
																				validateBlank : true,
																				displayField : 'id',
																				valueField : 'id',
																			}));
													break;
												case "double":
													mcomp
															.add(Ext
																	.create(
																			'Ext.form.field.Number',
																			{
																				id : record.id,
																				name : record.id,
																				fieldLabel : _name,
																				value : record.value,
																				disabled : record.writable == 'true' ? false
																						: true,
																				allowBlank : record.required == 'true' ? false
																						: true,
																				afterLabelTextTpl : record.required == 'true' ? sconsole.util.Util.required
																						: '',
																				validateBlank : true,
																				hideTrigger : true,
																				keyNavEnabled : false,
																				mouseWheelEnabled : false
																			}));

													break;
												case "month":
													mcomp
															.add(Ext
																	.create(
																			'Ext.form.field.Number',
																			{
																				id : record.id,
																				name : record.id,
																				fieldLabel : _name,
																				value : record.value,
																				allowBlank : record.required == 'true' ? false
																						: true,
																				afterLabelTextTpl : record.required == 'true' ? sconsole.util.Util.required
																						: '',
																				validateBlank : true,
																				disabled : record.writable == 'true' ? false
																						: true,
																				minValue : 1,
																				maxValue : 12,
																			}));
													break;
												}
											});
						},
						javaDateToExtjs : function(src) {
							// dd-MM-yyyy hh:mm
							tgt = src.replace(/dd/g, 'd').replace(/M{3,}/, 'M')
									.replace(/M{1,2}/g, 'm').replace(/y{3,}/g,
											'Y').replace(/y{1,2}/g, 'y')
									.replace(/mm/g, 'i').replace(/hh/g, 'h');
							// console.log(tgt);
							return tgt;
						},
						openProcInstance : function(sid) {
							console.log(sid);
							_tab = sconsole.util.Util.openTab(
									'myInstancesPanel',
									translations.wfMyInstsMenu);
							if (!Ext.isEmpty(_tab)) {
								_g = _tab.down('#instsList');
								_s = _g.getStore();
								_rec = _s.findExact('id', sid);
								if (_rec == -1) {
									_s.load({
										callback : function(records) {
											console.log(records.length);
											console.log(sid);
											_rec = _s.findExact('id', sid);
											if (_rec != -1)
												_g.getSelectionModel().select(
														_rec);
										}
									});
								} else
									_g.getSelectionModel().select(_rec);
							}
						},
						resetForm : function(formDom) {
							_panel = Ext.ComponentQuery.query(formDom)[0];
							_children = _panel.items;
							_children
									.each(function(item, idx, cnt) {
										_cname = Ext.getClassName(item);
										if (_cname
												.indexOf('Ext.form.field.ComboBox') >= 0)
											item.reset();
										else if (_cname
												.indexOf('Ext.form.field.Text') >= 0)
											item.reset();
										else if (_cname
												.indexOf('Ext.form.field.Date') >= 0)
											item.reset();
										else if (_cname
												.indexOf('Ext.form.field.Display') >= 0)
											item.reset();
									});
						},
						openProcess : function(pid) {
							sconsole.util.Util.changeMenu('workflowGloBtn', 3);
							_mainpanel = Ext.ComponentQuery.query('mainpanel')[0];

							var newTab = _mainpanel.items.findBy(function(tab) {
								return tab.title === '已部署的流程定义';
							});

							if (!newTab) {
								newTab = _mainpanel.add({
									xtype : 'workflowProcdefs',
									closable : true,
									title : '已部署的流程定义'
								});
							}
							_mainpanel.setActiveTab(newTab);
						},
						hasSelectedOneRow : function(_g) {
							var _chosens = _g.getSelectionModel()
									.getSelection();
							if (Ext.isEmpty(_chosens)) {
								Ext.Msg.alert('Waring',
										'Please select a row at first.');
								return false;
							}
							return true;
						}
					}
				});