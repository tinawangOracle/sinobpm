var t13yArray = new Array(5);
t13yArray[0] = new Array(4);
t13yArray[0][0] = "Non-Ascii Pattern";
t13yArray[0][1] = "ERROR";
t13yArray[0][2] = "Non-7-Bit ASCII characters MUST NOT be contained.";
t13yArray[0][3] = "1) Replace Non-7-Bit ASCII with 7-Bit ASCII alternatives. 2)Replace Unicode Escaped characters with 7-Bit ASCII alternatives.";

t13yArray[1] = new Array(4);
t13yArray[1][0] = "Date Pattern";
t13yArray[1][1] = "ERROR";t13yArray[1][2] = "Date format patterns MUST NOT be contained in messages.";
t13yArray[1][3] = "Locale sensitive format patterns should be retrieved from other dedicated resources.";

t13yArray[2] = new Array(4);
t13yArray[2][0] = "Time Pattern";
t13yArray[2][1] = "ERROR";
t13yArray[2][2] = "Time format patterns MUST NOT be contained in messages.";
t13yArray[2][3] = "Locale sensitive format patterns should be retrieved from other dedicated resources.";

t13yArray[3] = new Array(4);
t13yArray[3][0] = "Fonts Pattern";
t13yArray[3][1] = "ERROR";
t13yArray[3][2] = "Fonts MUST NOT be contained in messages.";
t13yArray[3][3] = "Locale sensitive patterns should be retrieved from other dedicated resources.";

t13yArray[4] = new Array(4);
t13yArray[4][0] = "Encoding Pattern";
t13yArray[4][1] = "ERROR";
t13yArray[4][2] = "Encodings MUST NOT be contained in messages.";
t13yArray[4][3] = "Locale sensitive patterns should be retrieved from other dedicated resources.";

t13yArray[5] = new Array(4);
t13yArray[5][0] = "Token Pattern";
t13yArray[5][1] = "ERROR";
t13yArray[5][2] = "Token patterns do not allow reordering during translation.";
t13yArray[5][3] = "Use sequential formats {0}, {1}, ... instead, in order to support reordering.";

t13yArray[6] = new Array(4);
t13yArray[6][0] = "All up case strings Pattern";
t13yArray[6][1] = "ERROR";
t13yArray[6][2] = "ALL-UPPER-CASE words will not be translated.";
t13yArray[6][3] = "Move to separate untranslatable files if it does not require translation, and make sure NOT to list untranslatable files in WPTG file. If you wish to have the word be translated, uncapitalize it. Otherwise WPTG will never translate it.";

t13yArray[7] = new Array(4);
t13yArray[7][0] = "No A-Z a-z strings";
t13yArray[7][1] = "ERROR";
t13yArray[7][2] = "Sentence contains no [A-Z] [a-z] chars, could not be translated.";
t13yArray[7][3] = "Move to separate untranslatable files, and make sure NOT to list untranslatable files in WPTG file.";

t13yArray[8] = new Array(4);
t13yArray[8][0] = "Crtl Characters Pattern";
t13yArray[8][1] = "WARN";
t13yArray[8][2] = "Control Characters in String.";
t13yArray[8][3] = "Do not put control chacters in WPTG files.";

t13yArray[9] = new Array(4);
t13yArray[9][0] = "Illegal String End Pattern";
t13yArray[9][1] = "WARN";
t13yArray[9][2] = "Strings should not end with underscores";
t13yArray[9][3] = "Remove trailing underscores.";

t13yArray[10] = new Array(4);
t13yArray[10][0] = "Prepositions Only Pattern";
t13yArray[10][1] = "WARN";
t13yArray[10][2] = "Prepositions should not be used alone because: They are very difficult to translate without having the full context. They might be concatenated with other strings.";
t13yArray[10][3] = "Provide complete sentences as much as possible.";

t13yArray[11] = new Array(4);
t13yArray[11][0] = "Token Pattern1";
t13yArray[11][1] = "WARN";
t13yArray[11][2] = "Word followed immediately (without any space) by the combination 's' to make a plural might be concatenated with other strings.";
t13yArray[11][3] = "Provide complete sentences as much as possible.";

t13yArray[12] = new Array(4);
t13yArray[12][0] = "Boolean Pattern";
t13yArray[12][1] = "WARN";
t13yArray[12][2] = "True/False values (if used as booleans) may cause malfunction when translated, and therefore should not exist in translatable files.";
t13yArray[12][3] = "Move to separate untranslatable files, and make sure NOT to list untranslatable files in WPTG file. However, if you wish to have the value translated (such as in cases they are literally used as captions), leave it as-is.";

t13yArray[13] = new Array(4);
t13yArray[13][0] = "Token Pattern2";
t13yArray[13][1] = "WARN";
t13yArray[13][2] = "Token {0, ...} is NOT a recommended format, since they may easily be broken during translation.";
t13yArray[13][3] = "Use simple formats {0}, {1}, ... instead.";

t13yArray[14] = new Array(4);
t13yArray[14][0] = "Token Pattern3";
t13yArray[14][1] = "WARN";
t13yArray[14][2] = "Token ${...} is NOT a recommended format, since they may easily be broken during translation.";
t13yArray[14][3] = "Use simple formats {0}, {1}, ... instead.";

t13yArray[15] = new Array(4);
t13yArray[15][0] = "Token Pattern4";
t13yArray[15][1] = "WARN";
t13yArray[15][2] = "Tokens $...$ or $... are NOT recommended formats, since they may easily be broken during translation.";
t13yArray[15][3] = "Use simple formats {0}, {1}, ... instead.";

t13yArray[16] = new Array(4);
t13yArray[16][0] = "URL Pattern";
t13yArray[16][1] = "WARN";
t13yArray[16][2] = "URLs are untranslatable.";
t13yArray[16][3] = "Move to separate untranslatable files, and make sure NOT to list untranslatable files in WPTG file.";

t13yArray[17] = new Array(4);
t13yArray[17][0] = "Path Pattern";
t13yArray[17][1] = "WARN";
t13yArray[17][2] = "Path/File names are untranslatable.";
t13yArray[17][3] = "Move to separate untranslatable files, and make sure NOT to list untranslatable files in WPTG file.";

t13yArray[18] = new Array(4);
t13yArray[18][0] = "None Word Pattern";
t13yArray[18][1] = "WARN";
t13yArray[18][2] = "Symbols are untranslatable.";
t13yArray[18][3] = "Move to separate untranslatable files, and make sure NOT to list untranslatable files in WPTG file.";

t13yArray[19] = new Array(4);
t13yArray[19][0] = "Regular Expression Pattern";
t13yArray[19][1] = "WARN";
t13yArray[19][2] = "Regexp patterns are untranslatable.";
t13yArray[19][3] = "Move to separate untranslatable files, and make sure NOT to list untranslatableufiles in WPTG file.";

t13yArray[20] = new Array(4);
t13yArray[20][0] = "Color Code Pattern";
t13yArray[20][1] = "WARN";
t13yArray[20][2] = "Color Codes are untranslatable.";
t13yArray[20][3] = "Move to separate untranslatable files, and make sure NOT to list untranslatable files in WPTG file.";

t13yArray[21] = new Array(4);
t13yArray[21][0] = "Numeric Entry Pattern";
t13yArray[21][1] = "WARN";
t13yArray[21][2] = "Numeric Entity References are untranslatable.";
t13yArray[21][3] = "Move to separate untranslatable files, and make sure NOT to list untranslatable files in WPTG file.";

t13yArray[22] = new Array(4);
t13yArray[22][0] = "Char Entry Pattern";
t13yArray[22][1] = "WARN";
t13yArray[22][2] = "Char References are untranslatable.";
t13yArray[22][3] = "Move to separate untranslatable files, and make sure NOT to list untranslatable files in WPTG file.";

t13yArray[23] = new Array(4);
t13yArray[23][0] = "Camel Case strings Pattern";
t13yArray[23][1] = "WARN";
t13yArray[23][2] = "Camel Case strings will not be translated.";
t13yArray[23][3] = "Move to separate untranslatable files, and make sure NOT to list untranslatable files in WPTG file.";

t13yArray[24] = new Array(4);
t13yArray[24][0] = "Words with connections Pattern";
t13yArray[24][1] = "WARN";
t13yArray[24][2] = "Words connected with '.', '_', or '$'' are untranslatable.";
t13yArray[24][3] = "Move to separate untranslatable files, and make sure NOT to list untranslatable files in WPTG file.";

t13yArray[25] = new Array(4);
t13yArray[25][0] = "Single world Pattern";
t13yArray[25][1] = "WARN";
t13yArray[25][2] = "Single word starting with lower case may not provide enough context for translation.";
t13yArray[25][3] = "Provide complete sentences as much as possible, if you wish to have the word be translated. Otherwise, move to separate untranslatable files , and make sure NOT to list untranslatable files in WPTG file.";

t13yArray[26] = new Array(4);
t13yArray[26][0] = "Leading CHOICE Pattern";
t13yArray[26][1] = "WARN";
t13yArray[26][2] = "Leading CHOICE should not exist in messages.";
t13yArray[26][3] = "Move the CHOICE to separate untranslatable files, and make sure NOT to list untranslatable files in WPTG file.";

t13yArray[27] = new Array(4);
t13yArray[27][0] = "Mnemonics Pattern";
t13yArray[27][1] = "WARN";
t13yArray[27][2] = "Mnemonics should not be specified alone in translatable files.";
t13yArray[27][3] = "The most standard practice is to specify the mnemonic along with the caption using an ampersand, e.g., &Help. Otherwise, move the mnemonic to separate untranslatable files, and make sure NOT to list untranslatable files in WPTG file (The latter is not a recommended workaround in this case).";

t13yArray[28] = new Array(4);
t13yArray[28][0] = "All number Pattern";
t13yArray[28][1] = "WARN";
t13yArray[28][2] = "Number could not be translated.";
t13yArray[28][3] = "Move to separate untranslatable files if it does not require translation, and make sure NOT to list untranslatable files in WPTG file. If you wish to have the word be translated, uncapitalize it. Otherwise WPTG will never translate it.";

t13yArray[29] = new Array(4);
t13yArray[29][0] = "End with Prepositions Pattern";
t13yArray[29][1] = "WARN";
t13yArray[29][2] = "Sentence end with a common preposition should not exist because: They are very difficult to translate without having the full context. They might be concatenated with other strings.";
t13yArray[29][3] = "Provide complete sentences as much as possible.";

t13yArray[30] = new Array(4);
t13yArray[30][0] = "Start with Prepositions Pattern";
t13yArray[30][1] = "WARN";
t13yArray[30][2] = "Sentence start with a common preposition should not exist because: They are very difficult to translate without having the full context. They might be concatenated with other strings.";
t13yArray[30][3] = "Provide complete sentences as much as possible.";

t13yArray[31] = new Array(4);
t13yArray[31][0] = "Start with verb Pattern";
t13yArray[31][1] = "WARN";
t13yArray[31][2] = "Sentence start with a common verb should not exist because: They are very difficult to translate without having the full context. They might be concatenated with other strings.";
t13yArray[31][3] = "Provide complete sentences as much as possible.";

t13yArray[32] = new Array(4);
t13yArray[32][0] = "end with verb Pattern";
t13yArray[32][1] = "WARN";
t13yArray[32][2] = "Sentence end with a common verb should not exist because: They are very difficult to translate without having the full context. They might be concatenated with other strings.";
t13yArray[32][3] = "Provide complete sentences as much as possible.";

t13yArray[33] = new Array(4);
t13yArray[33][0] = "end with , Pattern";
t13yArray[33][1] = "WARN";
t13yArray[33][2] = "Sentence end with (,) are not completed.";
t13yArray[33][3] = "Provide complete sentences as much as possible.";

t13yArray[34] = new Array(4);
t13yArray[34][0] = "Connected Case strings Pattern";
t13yArray[34][1] = "WARN";
t13yArray[34][2] = "Connected  Case strings can not be translated.";
t13yArray[34][3] = "Move to separate untranslatable files, and make sure NOT to list untranslatable files in WPTG file.";

t13yArray[35] = new Array(4);
t13yArray[35][0] = "Strings end with the Pattern";
t13yArray[35][1] = "WARN";
t13yArray[35][2] = "Sentence not completed.";
t13yArray[35][3] = "Move to separate untranslatable files, and make sure NOT to list untranslatable files in WPTG file.";

t13yArray[36] = new Array(4);
t13yArray[36][0] = "Strings end with num Pattern";
t13yArray[36][1] = "WARN";
t13yArray[36][2] = "Strings end with Number can not be translated.";
t13yArray[36][3] = "Move to separate untranslatable files, and make sure NOT to list untranslatable files in WPTG file.";

t13yArray[37] = new Array(4);
t13yArray[37][0] = "Single ing/ed String";
t13yArray[37][1] = "WARN";
t13yArray[37][2] = "Single Present/Past Continuous word hard to translate.";
t13yArray[37][3] = "Move to separate untranslatable files, and make sure NOT to list untranslatable files in WPTG file.";



//var t13yArray = new Array(5);
//t13yArray[0] = new Array(4);
//t13yArray[0][0] = 'Non-Ascii Pattern';
//t13yArray[0][1] = 'ERROR';
//t13yArray[0][2] = 'Non-7-Bit ASCII characters MUST NOT be contained.';
//t13yArray[0][3] = '1) Replace Non-7-Bit ASCII with 7-Bit ASCII alternatives. 2)Replace Unicode Escaped characters with 7-Bit ASCII alternatives.';
//
//t13yArray[1] = new Array(4);
//t13yArray[1][0] = 'Date Pattern';
//t13yArray[1][1] = 'ERROR';
//t13yArray[1][2] = "Date format patterns MUST NOT be contained in messages.";
//t13yArray[1][3] = "Locale sensitive format patterns should be retrieved from other dedicated resources.";
//
//t13yArray[2] = new Array(4);
//t13yArray[2][0] = "All up case strings Pattern";
//t13yArray[2][1] = 'ERROR';
//t13yArray[2][2] = "ALL-UPPER-CASE words will not be translated.";
//t13yArray[2][3] = "Move to separate untranslatable files if it does not require translation, and make sure NOT to list untranslatable files in WPTG file. If you wish to have the word be translated, uncapitalize it. Otherwise WPTG will never translate it.";
//
//t13yArray[3] = new Array(4);
//t13yArray[3][0] = "Token Pattern";
//t13yArray[3][1] = 'ERROR';
//t13yArray[3][2] = "Token patterns do not allow reordering during translation.";
//t13yArray[3][3] = "Use sequential formats {0}, {1}, ... instead, in order to support reordering.";
//
//t13yArray[4] = new Array(4);
//t13yArray[4][0] = "Encoding Pattern";
//t13yArray[4][1] = 'ERROR';
//t13yArray[4][2] = "Encodings MUST NOT be contained in messages.";
//t13yArray[4][3] = "Locale sensitive patterns should be retrieved from other dedicated resources.";



