Ext
		.define(
				'sconsole.util.DateTimePickerCustomBtn',
				{
					extend : 'Ext.picker.Date',
					alias : 'widget.datetimecustompicker',
					todayText : '现在',
					timeLabel : '时间',
					requires : [ 'sconsole.util.TimePickerField' ],
					renderTpl : [
							'<div id="{id}-innerEl" role="grid">',
							'<div role="presentation" class="{baseCls}-header">',
							'<div class="{baseCls}-prev"><a id="{id}-prevEl" href="#" role="button" title="{prevText}"></a></div>',
							'<div class="{baseCls}-month" id="{id}-middleBtnEl">{%this.renderMonthBtn(values, out)%}</div>',
							'<div class="{baseCls}-next"><a id="{id}-nextEl" href="#" role="button" title="{nextText}"></a></div>',
							'</div>',
							'<table id="{id}-eventEl" class="{baseCls}-inner" cellspacing="0" role="presentation">',
							'<thead role="presentation"><tr role="presentation">',
							'<tpl for="dayNames">',
							'<th role="columnheader" title="{.}"><span>{.:this.firstInitial}</span></th>',
							'</tpl>',
							'</tr></thead>',
							'<tbody role="presentation"><tr role="presentation">',
							'<tpl for="days">',
							'{#:this.isEndOfWeek}',
							'<td role="gridcell" id="{[Ext.id()]}">',
							'<a role="presentation" href="#" hidefocus="on" class="{parent.baseCls}-date" tabIndex="1">',
							'<em role="presentation"><span role="presentation"></span></em>',
							'</a>',
							'</td>',
							'</tpl>',
							'</tr></tbody>',
							'</table>',
							'<tpl if="showToday">',
							'<div id="{id}-footerEl" role="presentation" class="{baseCls}-footer">{%this.renderTodayBtn(values, out)%}{%this.renderCloseBtn(values, out)%}</div>',
							'</tpl>',
							'</div>',
							{
								firstInitial : function(value) {
									return Ext.picker.Date.prototype
											.getDayInitial(value);
								},
								isEndOfWeek : function(value) {
									// convert from 1 based index to 0 based
									// by decrementing value once.
									value--;
									var end = value % 7 === 0 && value !== 0;
									return end ? '</tr><tr role="row">' : '';
								},
								renderTodayBtn : function(values, out) {
									Ext.DomHelper.generateMarkup(
											values.$comp.todayBtn
													.getRenderTree(), out);
								},
								renderMonthBtn : function(values, out) {
									Ext.DomHelper.generateMarkup(
											values.$comp.monthBtn
													.getRenderTree(), out);
								},
								renderCloseBtn : function(values, out) {
									Ext.DomHelper.generateMarkup(
											values.$comp.closeBtn
													.getRenderTree(), out);
								}
							} ],
					initComponent : function() {
						// keep time part for value
						var value = this.value || new Date();
						this.callParent();
						this.value = value;

						var me = this;
						me.closeBtn = new Ext.button.Button({
							ownerCt : me,
							ownerLayout : me.getComponentLayout(),
							text : 'C',
							handler : function() {
								debug.console('need code for close click');
								me.hide();
							},
							scope : me
						});
						me.callParent(arguments);
					},
					onRender : function(container, position) {
						if (!this.timefield) {
							this.timefield = Ext.create(
									'sconsole.util.TimePickerField', {
										fieldLabel : this.timeLabel,
										labelWidth : 40,
										value : Ext.Date.format(this.value,
												'H:i:s')
									});
						}
						this.timefield.ownerCt = this;
						this.timefield.on('change', this.timeChange, this);
						this.callParent(arguments);
						var table = Ext.get(Ext.DomQuery.selectNode('table',
								this.el.dom));
						var tfEl = Ext.core.DomHelper.insertAfter(table, {
							tag : 'div',
							style : 'border:0px;',
							children : [ {
								tag : 'div',
								cls : 'x-datepicker-footer ux-timefield'
							} ]
						}, true);
						this.timefield.render(this.el
								.child('div div.ux-timefield'));
						var p = this.getEl().parent('div.x-layer');
						if (p) {
							p.setStyle("height", p.getHeight() + 31);
						}
					},
					finishRenderChildren: function() {
				        this.callParent(arguments);
				      //  this.childComponent.finishRender();
				    },
					// listener 时间域修改, timefield change
					timeChange : function(tf, time, rawtime) {
						// if(!this.todayKeyListener) { // before render
						this.value = this.fillDateTime(this.value);
						// } else {
						// this.setValue(this.value);
						// }
					},
					// @private
					fillDateTime : function(value) {
						if (this.timefield) {
							var rawtime = this.timefield.getRawValue();
							value.setHours(rawtime.h);
							value.setMinutes(rawtime.m);
							value.setSeconds(rawtime.s);
						}
						return value;
					},
					// @private
					changeTimeFiledValue : function(value) {
						this.timefield.un('change', this.timeChange, this);
						this.timefield.setValue(this.value);
						this.timefield.on('change', this.timeChange, this);
					},
					/* TODO 时间值与输入框绑定, 考虑: 创建this.timeValue 将日期和时间分开保存. */
					// overwrite
					setValue : function(value) {
						this.value = value;
						this.changeTimeFiledValue(value);
						return this.update(this.value);
					},
					// overwrite
					getValue : function() {
						return this.fillDateTime(this.value);
					},
					// overwrite : fill time before setValue
					handleDateClick : function(e, t) {
						var me = this, handler = me.handler;
						e.stopEvent();
						if (!me.disabled
								&& t.dateValue
								&& !Ext.fly(t.parentNode).hasCls(
										me.disabledCellCls)) {
							me.doCancelFocus = me.focusOnSelect === false;
							me.setValue(this
									.fillDateTime(new Date(t.dateValue))); // overwrite:
							// fill time
							// before
							// setValue
							delete me.doCancelFocus;
							me.fireEvent('select', me, me.value);
							if (handler) {
								handler.call(me.scope || me, me, me.value);
							}
							me.onSelect();
						}
					},
					// overwrite : fill time before setValue
					selectToday : function() {
						var me = this, btn = me.todayBtn, handler = me.handler;
						if (btn && !btn.disabled) {
							// me.setValue(Ext.Date.clearTime(new Date()));
							// //src
							me.setValue(new Date());// overwrite: fill time
							// before setValue
							me.fireEvent('select', me, me.value);
							if (handler) {
								handler.call(me.scope || me, me, me.value);
							}
							me.onSelect();
						}
						return me;
					}
				});