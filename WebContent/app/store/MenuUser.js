Ext.define('sconsole.store.MenuUser', {
	extend : 'Ext.data.Store',
	autoLoad : false,
	requires : [ 'sconsole.model.menu.Root' ],
	model : 'sconsole.model.menu.Root',
	proxy : {
		type : 'rest',
		url : 'json/userMenu.json',
		reader : {
			type : 'json',
			root : 'users'
		}
	},
});
