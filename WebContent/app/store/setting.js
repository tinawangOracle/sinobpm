Ext.define('sconsole.store.setting', {
	extend : 'Ext.data.Store',
	storeId : 'settingStore',
	fields : [ 'name', 'value', 'compType', 'minval', 'maxval', 'vtype' ],
	proxy : {
		type : 'ajax',
		api : {
			read : 'setting/view.action'
		},
		// url:'setting/view.action',
		reader : {
			type : 'json',
			idProperty : 'name',
			root : 'records'
		}

	}
});