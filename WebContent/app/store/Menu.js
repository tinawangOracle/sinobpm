Ext.define('sconsole.store.Menu', {
	extend : 'Ext.data.Store',
	requires : [ 'sconsole.model.menu.Root' ],
	model : 'sconsole.model.menu.Root'
});
