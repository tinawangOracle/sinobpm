Ext.define('sconsole.store.workflow.instTasks', {
	extend : 'Ext.data.Store',
	autoLoad : false,
	requires : [ 'sconsole.model.workflow.tasks' ],
	model : 'sconsole.model.workflow.tasks',
	proxy : {
		type : 'ajax',
		api : {
			read : "admin/getTasksByInst.action"
		},
		timeout : 0,
		reader : {
			type : 'json',
			totalProperty : "totalCount",
			root : "records",
			idProperty : 'id'
		}
	},
	pageSize : 100, 
});