Ext.define('sconsole.store.workflow.tasks', {
	extend : 'Ext.data.Store',
	autoLoad : false,
	requires : [ 'sconsole.model.workflow.tasks' ],
	model : 'sconsole.model.workflow.tasks',

});