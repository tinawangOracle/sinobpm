Ext.define('sconsole.store.workflow.myInsts', {
	extend : 'Ext.data.Store',
	autoLoad : false,
	proxy : {
		type : 'ajax',
		api : {
			read : "workflow/myInstances.action"
		},
		timeout : 0,
		reader : {
			type : 'json',
			totalProperty : "totalCnt",
			root : "insts",
			idProperty : 'id'
		}
	},
	pageSize : 50, // step 1

	fields : [ 'id', 'name' ],
});