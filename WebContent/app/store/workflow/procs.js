Ext.define('sconsole.store.workflow.procs', {
	extend : 'Ext.data.Store',
	autoLoad : false,
	proxy : {
		type : 'ajax',
		api : {
			read : "workflow/getProcs.action"
		},
		timeout : 0,
		reader : {
			type : 'json',
			totalProperty : "totalCount",
			root : "records",
			idProperty : 'id'
		}
	},
	pageSize:50, //step 1

	fields : [ 'id', 'name', 'category', 'description', 'diagramResourceName',
			'resourceName', 'key' ],
});