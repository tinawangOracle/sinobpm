Ext.define('sconsole.store.workflow.vars', {
	extend : 'Ext.data.Store',
	autoLoad : false,
	fields : [ 'name', 'value' ],

});