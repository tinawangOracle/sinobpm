Ext.define('sconsole.store.workflow.instVars', {
	extend : 'Ext.data.Store',
	autoLoad : false,
	fields : [ 'name', 'value' ],
	
	proxy : {
		type : 'ajax',
		api : {
			read : "admin/getVarsByInst.action"
		},
		timeout : 0,
		reader : {
			type : 'json',
			totalProperty : "totalCount",
			root : "records",
			idProperty : 'id'
		}
	},
	pageSize : 50, 
	
});