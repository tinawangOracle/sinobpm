Ext.define('sconsole.store.User', {
	extend : 'Ext.data.Store',
	fields : [ 'id', 'name', 'role' ],
	autoLoad: true,
	data : [ {
		id : '1',
		name : 'Tony',
		role : 'Programmer'
	}, {
		id : '2',
		name : 'Steve',
		role : 'Programmer'
	}, {
		id : '3',
		name : 'Alice',
		role : 'Manager'
	} ],
//	proxy : {
//		type : 'rest',
//		url : 'rest/users',
//		reader : {
//			type : 'json'
//		}
//	}
});