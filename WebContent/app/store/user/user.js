Ext.define('sconsole.store.user.user', {
	extend : 'Ext.data.Store',
	storeId : 'userStore',
	autoLoad : false,
	proxy : {
		type : 'ajax',
		api : {
			read : "user/getUser.action"
		},
		timeout : 0,
		reader : {
			type : 'json',
			totalProperty : "totalCount",
			root : "user",
			idProperty : 'id'
		}
	},
	pageSize : 50,
	fields : [ 'id', 'fullname', 'idcard', 'phone', 'email' ],
});