Ext.define('sconsole.store.MenuWorkflow', {
	extend : 'Ext.data.Store',
	autoLoad : true,
	requires : [ 'sconsole.model.menu.Root' ],
	model : 'sconsole.model.menu.Root',
	proxy : {
		type : 'rest',
		url : 'json/workflowMenu.json',
		reader : {
			type : 'json',
			root : 'workflows'
		}
	},
});