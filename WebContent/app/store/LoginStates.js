Ext.define('sconsole.store.LoginStates', {
	extend : 'Ext.data.ArrayStore',
	fields : [ 'id', 'val' ],
	data : [ [ '0', 'Do not auto login' ],
	[ '1', 'Auto login in a day' ], [ '2', 'Auto login in a week' ],
	[ '3', 'Auto login in a month' ], [ '4', 'Auto login in a year' ] ],
	autoLoad: true
});