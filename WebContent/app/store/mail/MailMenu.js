Ext.define('sconsole.store.mail.MailMenu', {
	extend : 'Ext.data.TreeStore',
	autoLoad : false,
	root : {
		expanded : true,
		text : "My Root",
		children : [ {
			"text" : "Inbox",
			"iconCls" : "folder-inbox",
			"expanded" : true,
			"leaf" : true
		}, {
			"text" : "Sent",
			"iconCls" : "folder-sent",
			"leaf" : true
		}, {
			"text" : "Draft",
			"iconCls" : "folder-drafts",
			"leaf" : true
		}, {
			"text" : "Trash",
			"iconCls" : "folder-trash",
			"leaf" : true
		} ]
	}
});
