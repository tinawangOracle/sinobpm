Ext.define('sconsole.store.mail.MailMessages', {
    extend: 'Ext.data.Store',

    requires: [
        'sconsole.model.mail.MailMessage',
        'sconsole.proxy.Sakila'
    ],

    model: 'sconsole.model.mail.MailMessage',

    pageSize: 20,

    storeId: 'films',

    autoLoad: false,

    proxy: {
        type: 'sakila',
        api: {
            read: 'php/mail/listInbox.php',
            update: 'php/mail/update.php'
        } 
    }
});