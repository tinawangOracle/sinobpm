Ext.define('sconsole.store.MenuOffice', {
	extend : 'Ext.data.Store',
	autoLoad : true,
//	requires : [ 'sconsole.model.menu.items' ],
//	model : 'sconsole.model.menu.items',
	requires : [ 'sconsole.model.menu.Root' ],
	model : 'sconsole.model.menu.Root',
	proxy : {
		type : 'rest',
		url : 'json/officeMenu.json',
		reader : {
			type : 'json',
			root : 'offices'
		}
	},
});