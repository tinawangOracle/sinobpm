Ext.define('sconsole.store.MenuAdmin', {
	extend : 'Ext.data.Store',
	autoLoad : true,
	requires : [ 'sconsole.model.menu.Root' ],
	model : 'sconsole.model.menu.Root',
	proxy : {
		type : 'rest',
		url : 'json/adminMenu.json',
		reader : {
			type : 'json',
			root : 'admins'
		}
	},
});