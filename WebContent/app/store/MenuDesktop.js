Ext.define('sconsole.store.MenuDesktop', {
	extend : 'Ext.data.TreeStore',
	autoLoad:true,
	
	//requires : [ 'sconsole.model.menu.node' ],
	//model : 'sconsole.model.menu.node',
	proxy : {
		type : 'ajax',
		url : 'json/desktopMenu.json',
		reader : {
			type : 'json',
			root : 'children',
			idProperty:'id'
		},
		 // Don't want proxy to include these params in request  
        pageParam: undefined,  
        startParam: undefined,  
        pageParam: undefined,  
        pageParam: undefined 
	},
	root: {
        text: 'Ext JS',
        expanded: true,
      //  loaded:true
    }
});