Ext.define('sconsole.store.tasks.enum', {
	extend : 'Ext.data.Store',
	autoLoad : false,
	fields : [ 'key', 'label' ],
});