Ext.define('sconsole.store.tasks.taskAttachments',
		{
			extend : 'Ext.data.Store',
			autoLoad : false,
			fields : [ 'id', 'name', 'description', 'type', 'url',
					'processInstanceId' ],
		});