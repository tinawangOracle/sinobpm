Ext.define('sconsole.store.tasks.subTasks', {
	extend : 'Ext.data.Store',
	autoLoad : false,
	fields : [ 'id', 'name', 'description', 'type', 'assignee',
			'processInstanceId' ],
});