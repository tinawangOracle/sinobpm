Ext.define('sconsole.store.tasks.todo', {
	extend : 'Ext.data.Store',
	autoLoad : false,
	proxy : {
		type : 'ajax',
		api : {
			read : "workflow/todoJobs.action"
		},
		timeout : 0,
		reader : {
			type : 'json',
			totalProperty : "totalCount",
			root : "tasks",
			idProperty : 'id'
		}
	},
	pageSize : 50,
	fields : [ 'id', 'name', 'description', 'priority', 'owner',
			'processInstanceId' ],
});