Ext.define('sconsole.store.tasks.subTasks', {
	extend : 'Ext.data.Store',
	autoLoad : false,
	fields : [ 'userId', 'groupId', 'taskId', 'type', 'processInstanceId',
			'processDefinitionId' ],
});