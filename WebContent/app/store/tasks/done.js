Ext.define('sconsole.store.tasks.done', {
	extend : 'Ext.data.Store',
	autoLoad : false,
	proxy : {
		type : 'ajax',
		api : {
			read : "workflow/doneJobs.action"
		},
		timeout : 0,
		reader : {
			type : 'json',
			totalProperty : "totalCnt",
			root : "tasks",
			idProperty : 'id'
		}
	},
	pageSize : 50,
	fields : [ 'id', 'name', 'description', 'priority', 'owner',
			'processInstanceId' ],
});