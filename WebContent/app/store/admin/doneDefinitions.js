Ext.define('sconsole.store.admin.doneDefinitions', {
	extend : 'Ext.data.Store',
	autoLoad : false,
	proxy : {
		type : 'ajax',
		api : {
			read : "admin/getFinishedDefinitions.action"
		},
		timeout : 0,
		reader : {
			type : 'json',
			totalProperty : "totalCount",
			root : "records",
			idProperty : 'id'
		}
	},
	pageSize : 50, // step 1

	fields : [ 'id', 'name', 'cnt' ],
});