Ext.define('sconsole.store.admin.userGroups', {
	extend : 'Ext.data.Store',
	autoLoad : false,
	proxy : {
		type : 'ajax',
		api : {
			read : "admin/getGroupsByUserId.action"
		},
		timeout : 0,
		reader : {
			type : 'json',
			totalProperty : "totalCount",
			root : "records",
			idProperty : 'id',
			messageProperty: 'userId'
		}
	},
	pageSize : 50,
	fields : [ 'id', 'name', 'type'],
});