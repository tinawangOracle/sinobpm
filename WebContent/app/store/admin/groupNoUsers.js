Ext.define('sconsole.store.admin.groupNoUsers', {
	extend : 'Ext.data.Store',
	autoLoad : false,
	proxy : {
		type : 'ajax',
		api : {
			read : "admin/getGroupNoUsers.action"
		},
		timeout : 0,
		reader : {
			type : 'json',
			totalProperty : "totalCount",
			root : "records",
			idProperty : 'id'
		}
	},
	pageSize : 50,
	fields : [ 'id', 'firstName','lastName','email'],
});