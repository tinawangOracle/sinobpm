Ext.define('sconsole.store.admin.activeInstances', {
	extend : 'Ext.data.Store',
	autoLoad : false,
	requires : [ 'sconsole.model.admin.instances' ],
	model : 'sconsole.model.admin.instances',
	proxy : {
		type : 'ajax',
		api : {
			read : "admin/getInstancesByProcId.action"
		},
		timeout : 0,
		reader : {
			type : 'json',
			totalProperty : "totalCount",
			root : "records",
			idProperty : 'id'
		}
	},
	pageSize : 50,
	listeners : {
		beforeload : function(a, b, c) {
			if (Ext.isEmpty(b.params)) {
				b.params = {};
			} else {
				b.params = b.params || {};
			}
			b.params.type=0;
			if (Ext.isEmpty(b.params.pid)) {
				_t = Ext.ComponentQuery.query('activeInstancesPanel #activeDefinitionsGrid')[0];
				var _chosens = _t
				.getSelectionModel()
				.getSelection();
				if (!Ext.isEmpty(_chosens))
					b.params.pid = _chosens[0].get('id');
			}
		}
	}

});