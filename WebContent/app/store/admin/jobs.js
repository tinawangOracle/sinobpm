Ext.define('sconsole.store.admin.jobs', {
	extend : 'Ext.data.Store',
	autoLoad : false,
	proxy : {
		type : 'ajax',
		api : {
			read : "admin/getJobs.action"
		},
		timeout : 0,
		reader : {
			type : 'json',
			totalProperty : "totalCount",
			root : "records",
			idProperty : 'id'
		}
	},
	pageSize : 50, // step 1

	fields : [ 'id', 'name', 'duedate', 'processDefinitionId' ],
});