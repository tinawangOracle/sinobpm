Ext.define('sconsole.store.admin.deploymentProcs', {
	extend : 'Ext.data.Store',
	autoLoad : false,
	fields : [ 'name', 'id' ],
});