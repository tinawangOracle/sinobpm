Ext.define('sconsole.model.admin.instances', {
	extend : 'Ext.data.Model',
	fields : [ 'id', 'bkey', 'sUid', 'sAid', 'st', 'et', 'dur' ]
});