Ext.define('sconsole.model.user', {
	extend : 'Ext.data.Model',
	fields : [ 'id', 'first', 'last', 'rev' ]
});