Ext.define('sconsole.model.workflow.tasks', {
	extend : 'Ext.data.Model',
	fields : [ 'id', 'name','priority', 'dueDate', 'startTime', 'assignee',
				'endTime' ],
});