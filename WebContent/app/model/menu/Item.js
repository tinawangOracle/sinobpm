Ext.define('sconsole.model.menu.Item', {
	extend : 'Ext.data.Model',
	uses : [ 'sconsole.model.menu.Root' ],
	idProperty : 'id',
	fields : [ {
		name : 'text'
	}, {
		name : 'id'
	}, {
		name : 'parent_id'
	},'icon','iconCls','items','className'  ],
	belongsTo : {
		model : 'sconsole.model.menu.Root',
		foreignKey : 'parent_id'
	}

});