Ext.define('sconsole.model.menu.Root', {
	extend : 'Ext.data.Model',
	uses : [ 'sconsole.model.menu.Item' ],
	idProperty : 'id',
	fields : [ {
		name : 'text'
	}, {
		name : 'id'
	}, 'icon', 'iconCls', 'items', 'className' ],
	hasMany : {
		model : 'sconsole.model.menu.Item',
		foreignKey : 'parent_id',
		name : 'items'
	}
});