/* Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.activiti.rest.editor.model;

import org.activiti.editor.constants.ModelDataJsonConstants;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.repository.Model;
import org.apache.commons.lang3.StringUtils;
import org.restlet.data.Status;
import org.restlet.resource.Get;
import org.restlet.resource.ServerResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ObjectNode;

//import com.fasterxml.jackson.databind.ObjectMapper;
//import com.fasterxml.jackson.databind.node.ObjectNode;

/**
 * @author Tijs Rademakers
 */
public class ModelEditorJsonRestResource extends ServerResource implements
		ModelDataJsonConstants {

	protected static final Logger LOGGER = LoggerFactory
			.getLogger(ModelEditorJsonRestResource.class);
	private ObjectMapper objectMapper = new ObjectMapper();

	@Get("json")
	public ObjectNode getEditorJson() {
		ObjectNode modelNode = null;
		String modelId = (String) getRequest().getAttributes().get("modelId");
		ProcessEngine _engine = ProcessEngines.getDefaultProcessEngine();
		if (_engine == null) {
			System.out.println("===========================");
		} else
			System.out.println("%%%%%%%%%%%%%%%%%%%%%%%%%%");
		RepositoryService repositoryService = _engine.getRepositoryService();
		Model model = repositoryService.getModel(modelId);

		if (model != null) {
			try {

				if (StringUtils.isNotEmpty(model.getMetaInfo())) {
//					System.out.println(model.getMetaInfo());
					modelNode = (ObjectNode) objectMapper.readTree(model
							.getMetaInfo());
				} else {
//					System.out.println(model.getId());
					modelNode = objectMapper.createObjectNode();
					modelNode.put(MODEL_NAME, model.getName());
				}
				modelNode.put(MODEL_ID, model.getId());
				String _s=new String(repositoryService
						.getModelEditorSource(model.getId()), "utf-8");
//				System.out.println(_s);
				ObjectNode editorJsonNode = (ObjectNode) objectMapper
						.readTree(_s);
				modelNode.put("model", editorJsonNode);
				//modelNode.put("model",_s);
			} catch (Exception e) {
				e.printStackTrace();
				LOGGER.error("Error creating model JSON", e);
				setStatus(Status.SERVER_ERROR_INTERNAL);
			}
		} else {
			System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXx");
		}
		return modelNode;
	}
}
