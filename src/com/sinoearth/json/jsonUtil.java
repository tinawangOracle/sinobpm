package com.sinoearth.json;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ListResourceBundle;
import java.util.Locale;
import java.util.ResourceBundle;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class jsonUtil {
	private static DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

	private static DateFormat dfString = new SimpleDateFormat("MM/dd/yyyy");

	/**
	 * Get list of Contacts from request.
	 * 
	 * @param data
	 *            - json data from request
	 * @return list of Contacts
	 */
	public List getObjectsFromRequest(Object data, Class myclass) {

		List list;

		// it is an array - have to cast to array object
		if (data.toString().indexOf('[') > -1) {

			list = (List) getListObjectsFromJSON(data, myclass);

		} else { // it is only one object - cast to object/bean
			// System.out.println(data);
			Object contact = getObjectFromJSON(data, myclass);

			list = new ArrayList();
			list.add(contact);
		}

		return list;
	}




	/**
	 * Transform json data format into Contact object
	 * 
	 * @param data
	 *            - json data from request
	 * @return
	 */
	public Object getObjectFromJSON(Object data, Class myclass) {
		JSONObject jsonObject = JSONObject.fromObject(data);
		Object newContact = JSONObject.toBean(jsonObject, myclass);
		return newContact;
	}

	public Object getObjectFromJSONC(Object data, Class mainCls, Class idCls) {
		JSONObject jsonObject = JSONObject.fromObject(data);
		Object idObj = JSONObject.toBean(jsonObject, idCls);
		Object mainObj =  JSONObject.toBean(jsonObject, mainCls);
		Method _setid;
		try {
			_setid = mainCls.getMethod("setId", idCls);
			_setid.invoke(mainObj, idObj);
			return mainObj;
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}


	/**
	 * Transform json data format into list of Contact objects
	 * 
	 * @param data
	 *            - json data from request
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Object getListObjectsFromJSON(Object data, Class myclass) {
		JSONArray jsonArray = JSONArray.fromObject(data);
		Object newContacts = JSONArray.toCollection(jsonArray, myclass);
		return newContacts;
	}


	public Object getListObjectsFromJSONC(Object data, Class mainCls,
			Class idCls) {
		System.out.println("ModifiedData : " + data.toString());
		JSONArray jsonArray = JSONArray.fromObject(data);

		List idObjs = (List) JSONArray.toCollection(jsonArray, idCls);
		List mainObjs = (List) JSONArray.toCollection(jsonArray, mainCls);
		Method _setid = null;
		try {
			_setid = mainCls.getMethod("setId", idCls);
			System.out.println("invoke setId");
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("SecurityException");
			return null;
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("NoSuchMethodException");
			return null;
		}
		try {
			for (int i = 0; i < mainObjs.size(); i++) {
				_setid.invoke(mainObjs.get(i), idObjs.get(i));
			}
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("IllegalArgumentException");
			return null;
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("IllegalAccessException");
			return null;
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("InvocationTargetException");
			return null;
		}
		return mainObjs;
	}

	public static void main(String[] args) {
		JSONObject jsonObject = JSONObject
				.fromObject("{\"key\":\"InstanceIdSimulator_LABEL\",\"dupFlag\":\"0\",\"arrayFlag\":\"0\",\"value\":\"Simulator Instance ID\"}");

		jsonUtil _util = new jsonUtil();

	}
}
