package com.sinoearth.bpm;

public class ConsoleApp {
	protected I18nManager i18nManager;
	protected transient static ThreadLocal<ConsoleApp> current = new ThreadLocal<ConsoleApp>();

	public ConsoleApp() {
		System.out.println("===================");
	}

	public static void set(ConsoleApp one) {
		current.set(one);
	}

	public static ConsoleApp get() {
		return current.get();
	}

	public I18nManager getI18nManager() {
		return i18nManager;
	}

	public void setI18nManager(I18nManager i18nManager) {
		this.i18nManager = i18nManager;
	}
}
