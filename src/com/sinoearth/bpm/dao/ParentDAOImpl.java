package com.sinoearth.bpm.dao;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
//import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projection;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.HibernateTemplate;

public class ParentDAOImpl implements ParentDAO {
	HibernateTemplate template;

	public ParentDAOImpl(HibernateTemplate template) {
		this.template = template;
	}

	public Object getEntity(Class entityClass, Serializable id) {
		return template.get(entityClass, id);
	}

	public List getListForPage(final String hql, final int offset,
			final int lengh) {
		try {
			List list = template.executeFind(new HibernateCallback() {

				public Object doInHibernate(Session session)
						throws HibernateException, SQLException {
					List list2 = session.createQuery(hql)
							.setFirstResult(offset).setMaxResults(lengh).list();
					return list2;
				}
			});
			return list;
		} catch (RuntimeException re) {

			throw re;
		}
	}

	public List getListForPage(final Class arg, final Criterion[] criterions,
			final int offset, final int length, final String[] sortAttr,
			final String[] sortDir, final Projection _project) {

		List list = template.executeFind(new HibernateCallback() {
			public Object doInHibernate(Session session)
					throws HibernateException, SQLException {
				Criteria criteria = session.createCriteria(arg);
				for (int i = 0; i < criterions.length; i++) {
					if (criterions[i] != null)
						criteria.add(criterions[i]);
				}
				if (sortDir != null && sortAttr != null) {
					int sortlen = sortDir.length;
					for (int j = 0; j < sortlen; j++) {
						if (!(sortDir[j] == null || sortDir[j].length() == 0))
							if (sortDir[j].equalsIgnoreCase("ASC"))
								criteria.addOrder(Order.asc(sortDir[j]));
							else
								criteria.addOrder(Order.desc(sortDir[j]));
					}
				}
				if (_project != null)
					criteria.setProjection(_project);
				criteria.setFirstResult(offset);
				criteria.setMaxResults(length);
				return criteria.list();
			}
		});
		return list;
	}

	public List getListForPage(final Class arg, final Criterion[] criterions,
			final int offset, final int length, final String sortAttr,
			final String sortDir, final Projection _project) {

		List list = template.executeFind(new HibernateCallback() {
			public Object doInHibernate(Session session)
					throws HibernateException, SQLException {
				Criteria criteria = session.createCriteria(arg);
				if (criterions != null)
					for (int i = 0; i < criterions.length; i++) {
						if (criterions[i] != null)
							criteria.add(criterions[i]);
					}
				if (sortDir != null && sortAttr != null) {
					String[] dirs = sortDir.split(",");
					String[] attrs = sortAttr.split(",");
					for (int j = 0; j < attrs.length; j++) {
						if (!(dirs[j] == null || dirs[j].length() == 0))
							if (dirs[j].equals("ASC"))
								criteria.addOrder(Order.asc(attrs[j]));
							else
								criteria.addOrder(Order.desc(attrs[j]));
					}
				}
				if (_project != null)
					criteria.setProjection(_project);
				criteria.setFirstResult(offset);
				criteria.setMaxResults(length);
				return criteria.list();
			}
		});
		return list;
	}

	public List getRowCnt(final Class arg, final String cattr,
			final Criterion[] criterions) {

		List list = template.executeFind(new HibernateCallback() {
			public Object doInHibernate(Session session)
					throws HibernateException, SQLException {
				Criteria criteria = session.createCriteria(arg);
				if (criterions != null)
					for (int i = 0; i < criterions.length; i++) {
						if (criterions[i] != null)
							criteria.add(criterions[i]);
					}

				// criteria.setProjection(Projections.rowCount());
				criteria.setProjection(Projections.count(cattr));
				return criteria.list();// ((Long)
										// criteria.uniqueResult()).intValue();
			}
		});
		return list;
	}

}
