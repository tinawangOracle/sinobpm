package com.sinoearth.bpm.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.activiti.engine.FormService;
import org.activiti.engine.HistoryService;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.TaskService;
import org.activiti.engine.form.FormProperty;
import org.activiti.engine.form.TaskFormData;
import org.activiti.engine.history.HistoricTaskInstance;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.task.Task;
import org.springframework.orm.hibernate3.HibernateTemplate;

public class TaskDAOImpl extends ActivitiParentDAOImpl implements TaskDAO {

	public TaskDAOImpl(ProcessEngine processEngine) {
		super(processEngine);
		// TODO Auto-generated constructor stub
	}

	@Override
	public Task getTask(String taskID) {

		// ProcessEngine processEngine =
		// ProcessEngines.getDefaultProcessEngine();
		// System.out.println(processEngine+"|success");
		TaskService taskService = processEngine.getTaskService();
		List<Task> tasks = taskService.createTaskQuery().taskId(taskID).list();
		if (tasks.size() > 0)
			return tasks.get(0);

		return null;
	}

	public ProcessDefinition getProcessDefinition(String taskId) {
		ProcessDefinition procdef = processEngine.getRepositoryService()
				.createProcessDefinitionQuery().processDefinitionId(taskId)
				.singleResult();
		return procdef;
	}

	

	@Override
	public List<FormProperty> getTaskForm(String taskID) {

		// ProcessEngine processEngine =
		// ProcessEngines.getDefaultProcessEngine();
		FormService formService = processEngine.getFormService();
		TaskFormData taskFormData = formService.getTaskFormData(taskID);
		List<FormProperty> pros = taskFormData.getFormProperties();

		return pros;
	}

	// @Override
	// public Map<String, List<FormProperty>> getAllTasksFormByAssignee(String
	// userID) {
	//
	// List<Task> tasks = getAllTasksByAssignee(userID);
	// Map<String, List<FormProperty>> allTaskForms = new HashMap<String,
	// List<FormProperty>>();
	// for(Task task : tasks)
	// allTaskForms.put(task.getId(), getTaskForm(task.getId()));
	//
	// return allTaskForms;
	// }

	@Override
	public Map<String, Object> getTaskVariables(String taskID) {

		// ProcessEngine processEngine =
		// ProcessEngines.getDefaultProcessEngine();
		TaskService taskService = processEngine.getTaskService();
		Map<String, Object> variables = taskService.getVariables(taskID);

		return variables;
	}

	@Override
	public String completeTask(String taskID, Map<String, Object> variables) {

		// ProcessEngine processEngine =
		// ProcessEngines.getDefaultProcessEngine();
		TaskService taskService = processEngine.getTaskService();
		taskService.complete(taskID, variables);

		return "0";
	}

	@Override
	public List<Task> getAllTasksByAssignee(String userID) {

		// ProcessEngine processEngine =
		// ProcessEngines.getDefaultProcessEngine();
		TaskService taskService = processEngine.getTaskService();
		List<Task> tasks = taskService.createTaskQuery().taskAssignee(userID)
				.list();

		return tasks;
	}

	@Override
	public List<Task> getAllTasksByOwner(String userID) {
		TaskService taskService = processEngine.getTaskService();
		List<Task> tasks = taskService.createTaskQuery().taskOwner(userID)
				.list();

		return tasks;
	}

	@Override
	public List<Task> getAllTasksByInvolved(String userID) {
		TaskService taskService = processEngine.getTaskService();
		List<Task> tasks = taskService.createTaskQuery()
				.taskInvolvedUser(userID).list();

		return tasks;
	}

	@Override
	public List<Task> getUnassginedTasksByGroup(String groupID) {
		TaskService taskService = processEngine.getTaskService();
		List<Task> tasks = taskService.createTaskQuery()
				.taskCandidateGroup(groupID).taskUnassigned().list();

		return tasks;
	}

	@Override
	public List<Task> getAllTasksByAssignee(String userID, String seq) {
		TaskService taskService = processEngine.getTaskService();
		List<Task> tasks = (List<Task>) getQuerySeq(
				taskService.createTaskQuery().taskAssignee(userID)
						.orderByTaskName(), seq).list();

		return tasks;
	}

	@Override
	public List<Task> getAllTasksByOwner(String userID, String seq) {
		TaskService taskService = processEngine.getTaskService();
		List<Task> tasks = (List<Task>) getQuerySeq(
				taskService.createTaskQuery().taskOwner(userID)
						.orderByTaskName(), seq).list();

		return tasks;
	}

	@Override
	public List<Task> getAllTasksByInvolved(String userID, String seq) {
		TaskService taskService = processEngine.getTaskService();
		List<Task> tasks = (List<Task>) getQuerySeq(
				taskService.createTaskQuery().taskInvolvedUser(userID)
						.orderByTaskName(), seq).list();

		return tasks;
	}

	@Override
	public List<Task> getUnassginedTasksByGroup(String groupID, String seq) {
		TaskService taskService = processEngine.getTaskService();
		List<Task> tasks = (List<Task>) getQuerySeq(
				taskService.createTaskQuery().taskCandidateGroup(groupID)
						.taskUnassigned().orderByTaskName(), seq).list();

		return tasks;
	}

	
	@Override
	public List<Task> getAllTasksByAssignee(String userID, String seq,
			int pageCount, int pageNumber) {
		TaskService taskService = processEngine.getTaskService();
		List<Task> tasks = (List<Task>) getListByPage(taskService
				.createTaskQuery().taskAssignee(userID).orderByTaskName(), seq,
				pageCount, pageNumber);

		return tasks;
	}

	@Override
	public List<Task> getAllTasksByOwner(String userID, String seq,
			int pageCount, int pageNumber) {
		TaskService taskService = processEngine.getTaskService();
		List<Task> tasks = (List<Task>) getListByPage(taskService
				.createTaskQuery().taskOwner(userID).orderByTaskName(), seq,
				pageCount, pageNumber);

		return tasks;
	}

	@Override
	public List<Task> getAllTasksByInvolved(String userID, String seq,
			int pageCount, int pageNumber) {
		TaskService taskService = processEngine.getTaskService();
		List<Task> tasks = (List<Task>) getListByPage(taskService
				.createTaskQuery().taskInvolvedUser(userID).orderByTaskName(),
				seq, pageCount, pageNumber);

		return tasks;
	}

	@Override
	public List<Task> getUnassginedTasksByGroup(String groupID, String seq,
			int pageCount, int pageNumber) {
		TaskService taskService = processEngine.getTaskService();
		List<Task> tasks = (List<Task>) getListByPage(taskService
				.createTaskQuery().taskCandidateGroup(groupID).taskUnassigned()
				.orderByTaskName(), seq, pageCount, pageNumber);

		return tasks;
	}

	@Override
	public List<HistoricTaskInstance> getAllFinishedHistoricTask() {
		HistoryService historyService = processEngine.getHistoryService();
		List<HistoricTaskInstance> htis = historyService
				.createHistoricTaskInstanceQuery().finished().list();
		return htis;
	}

	@Override
	public List<HistoricTaskInstance> getAllFinishedHistoricTask(String seq) {
		HistoryService historyService = processEngine.getHistoryService();
		List<HistoricTaskInstance> htis = (List<HistoricTaskInstance>) getQuerySeq(
				historyService.createHistoricTaskInstanceQuery().finished()
						.orderByTaskName(), seq).list();
		return htis;
	}

	@Override
	public List<HistoricTaskInstance> getAllFinishedHistoricTask(String seq,
			int pageCount, int pageNumber) {
		HistoryService historyService = processEngine.getHistoryService();
		List<HistoricTaskInstance> htis = (List<HistoricTaskInstance>) getListByPage(
				historyService.createHistoricTaskInstanceQuery().finished()
						.orderByTaskName(), seq, pageCount, pageNumber);
		return htis;
	}

	@Override
	public List<HistoricTaskInstance> getFinishedHistoricTaskByOwnerByOwner(
			String userID) {
		HistoryService historyService = processEngine.getHistoryService();
		List<HistoricTaskInstance> htis = historyService
				.createHistoricTaskInstanceQuery().finished().taskOwner(userID)
				.list();
		return htis;
	}

	@Override
	public List<HistoricTaskInstance> getFinishedHistoricTaskByOwner(
			String userID, String seq) {
		HistoryService historyService = processEngine.getHistoryService();
		List<HistoricTaskInstance> htis = (List<HistoricTaskInstance>) getQuerySeq(
				historyService.createHistoricTaskInstanceQuery().finished()
						.taskOwner(userID).orderByTaskName(), seq).list();
		return htis;
	}

	@Override
	public List<HistoricTaskInstance> getFinishedHistoricTaskByOwner(
			String userID, String seq, int pageCount, int pageNumber) {
		HistoryService historyService = processEngine.getHistoryService();
		List<HistoricTaskInstance> htis = (List<HistoricTaskInstance>) getListByPage(
				historyService.createHistoricTaskInstanceQuery().finished()
						.taskOwner(userID).orderByTaskName(), seq, pageCount,
				pageNumber);
		return htis;
	}

}
