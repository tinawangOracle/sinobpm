package com.sinoearth.bpm.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.activiti.engine.IdentityService;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.identity.Group;
import org.activiti.engine.identity.User;

import com.sinoearth.bpm.domain.ActIdGroup;

public class IdentityDAOImpl extends ActivitiParentDAOImpl implements
		IdentityDAO {

	public IdentityDAOImpl(ProcessEngine processEngine) {
		super(processEngine);
		// TODO Auto-generated constructor stub
	}

	@Override
	public Map<String, Object> getAllUsers() {
		System.out.println("getAllUsersDAO");
		List<User> users = processEngine.getIdentityService().createUserQuery()
				.list();
		List<User> usersWithNoPassword = new ArrayList<User>();
		for (User user : users) {
			user.setPassword("*");
			usersWithNoPassword.add(user);
		}

		Map<String, Object> modelMap = new HashMap<String, Object>(2);

		if (users != null)
			if (users.size() == 0)
				modelMap.put("totalCount", 0);
			else
				modelMap.put("totalCount", usersWithNoPassword.size());
		modelMap.put("records", usersWithNoPassword);

		System.out.println(users.size());

		return modelMap;
	}

	@Override
	public Map<String, Object> getAllUsers(String seq) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<String, Object> getAllUsers(String seq, int pageCount,
			int pageNumber) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String deleteUserById(String id) {
		processEngine.getIdentityService().deleteUser(id);
		return "1";
	}

	@SuppressWarnings("finally")
	@Override
	public String createUser(String userID, String firstName, String lastName,
			String email, String password) {
		IdentityService is = processEngine.getIdentityService();
		User newUser = is.newUser(userID);
		newUser.setFirstName(firstName);
		newUser.setLastName(lastName);
		newUser.setEmail(email);
		newUser.setPassword(password);
		String r = null;
		try {
			is.saveUser(newUser);
			r = "1";
		} catch (Exception e) {
			r = "0";
		} finally {
			return r;
		}
	}

	@SuppressWarnings("finally")
	@Override
	public String updateUser(String userID, String firstName, String lastName,
			String email, String password) {
		IdentityService is = processEngine.getIdentityService();
		User newUser = is.createUserQuery().userId(userID).singleResult();
		newUser.setFirstName(firstName);
		newUser.setLastName(lastName);
		newUser.setEmail(email);

		// System.out.println("password|" + password);
		// System.out.println(!password.equals("d41d8cd98f00b204e9800998ecf8427e"));
		if (!password.equals("d41d8cd98f00b204e9800998ecf8427e"))
			newUser.setPassword(password);
		String r = null;
		try {
			is.saveUser(newUser);
			r = "1";
		} catch (Exception e) {
			r = "0";
		} finally {
			return r;
		}
	}

	@Override
	public Map<String, Object> getAllGroups() {
		System.out.println("getAllUsersDAO");
		List<Group> groups = processEngine.getIdentityService()
				.createGroupQuery().list();

		Map<String, Object> modelMap = new HashMap<String, Object>(2);

		if (groups != null)
			if (groups.size() == 0)
				modelMap.put("totalCount", 0);
			else
				modelMap.put("totalCount", groups.size());
		modelMap.put("records", groups);

		System.out.println(groups.size());

		return modelMap;
	}

	@Override
	public Map<String, Object> getAllGroups(String seq) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<String, Object> getAllGroups(String seq, int pageCount,
			int pageNumber) {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("finally")
	@Override
	public String createGroup(String id, String name, String type) {
		IdentityService is = processEngine.getIdentityService();
		Group newGroup = is.newGroup(id);
		newGroup.setName(name);
		newGroup.setType(type);
		String r = null;
		try {
			is.saveGroup(newGroup);
			r = "1";
		} catch (Exception e) {
			r = "0";
		} finally {
			return r;
		}

	}

	@Override
	public String deleteGroupById(String id) {
		processEngine.getIdentityService().deleteGroup(id);
		return "1";
	}

	@SuppressWarnings("finally")
	@Override
	public String updateGroup(String id, String name, String type) {
		IdentityService is = processEngine.getIdentityService();
		Group newGroup = is.createGroupQuery().groupId(id).singleResult();
		newGroup.setName(name);
		newGroup.setType(type);
		String r = null;
		try {
			is.saveGroup(newGroup);
			r = "1";
		} catch (Exception e) {
			r = "0";
		} finally {
			return r;
		}
	}

	@Override
	public Map<String, Object> getUsersByGroupId(String groupId) {
		List<User> users = processEngine.getIdentityService().createUserQuery()
				.memberOfGroup(groupId).list();
		List<User> usersWithNoPassword = new ArrayList<User>();
		for (User user : users) {
			user.setPassword("*");
			usersWithNoPassword.add(user);
		}

		Map<String, Object> modelMap = new HashMap<String, Object>(2);

		if (users != null)
			if (users.size() == 0)
				modelMap.put("totalCount", 0);
			else
				modelMap.put("totalCount", usersWithNoPassword.size());
		modelMap.put("records", usersWithNoPassword);

		System.out.println(users.size());

		return modelMap;
	}

	@Override
	public Map<String, Object> getGroupsByUserId(String userId) {

		List<Group> groups = processEngine.getIdentityService()
				.createGroupQuery().groupMember(userId).list();
		Map<String, Object> modelMap = new HashMap<String, Object>(3);
		if (groups != null)
			if (groups.size() == 0)
				modelMap.put("totalCount", 0);
			else
				modelMap.put("totalCount", groups.size());
		modelMap.put("userId", userId);
		modelMap.put("records", groups);
		System.out.println(groups.size());

		return modelMap;
	}

	@Override
	public List<Group> getGroupsSetByUserId(String userId) {

		return processEngine.getIdentityService().createGroupQuery()
				.groupMember(userId).list();
	}

	@Override
	public Map<String, Boolean> addUsersToGroup(String[] userIds, String groupId) {

//		Map<String, Boolean> result = new HashMap<String, Boolean>();
//
//		for (String userId : userIds)
//			try {
//				processEngine.getIdentityService().createMembership(userId,
//						groupId);
//				result.put(userId, true);
//			} catch (RuntimeException e) {
//				result.put(userId, false);
//			}
//
//		return result;
		
		Map<String, Boolean> result = new HashMap<String, Boolean>();

		//是否已存在 已存在则不添加
		//是否原有的不存在了 不存在则删除
		
		ArrayList<String> currents = new ArrayList<String>();
		ArrayList<String> selects = new ArrayList<String>();
		ArrayList<String> adds = new ArrayList<String>();
		ArrayList<String> deletes = new ArrayList<String>();
		
		List<User> users =  processEngine.getIdentityService()
				.createUserQuery().memberOfGroup(groupId).list();
		
		for (User user : users)
			currents.add(user.getId());
		for (String select : userIds)
			selects.add(select);
		
		for (String current : currents)
		{
			deletes.add(current);
			for(String select : selects)
				if(current.equals(select))
				{
					deletes.remove(select);
					break;
				}
		}
		
		for (String select : selects)
		{
			adds.add(select);
			for(String current : currents)
				if(select.equals(current))
				{
					adds.remove(current);
					break;
				}
		}
		
		
		for (String userId : adds)
			try {
				System.out.println("create : "+userId+" "+groupId);
				processEngine.getIdentityService().createMembership(userId,
						groupId);
				result.put(userId, true);
			} catch (RuntimeException e) {
				result.put(userId, false);
			}
		
		for (String userId : deletes)
			try {
				System.out.println("delete : "+userId+" "+groupId);
				processEngine.getIdentityService().deleteMembership(userId,
						groupId);
				result.put(userId, true);
			} catch (RuntimeException e) {
				result.put(userId, false);
			}

		return result;
	}

	@Override
	public Map<String, Boolean> addGroupsToUser(String userId, String[] groupIds) {

		Map<String, Boolean> result = new HashMap<String, Boolean>();

		//是否已存在 已存在则不添加
		//是否原有的不存在了 不存在则删除
		
		ArrayList<String> currents = new ArrayList<String>();
		ArrayList<String> selects = new ArrayList<String>();
		ArrayList<String> adds = new ArrayList<String>();
		ArrayList<String> deletes = new ArrayList<String>();
		
		List<Group> groups = processEngine.getIdentityService()
				.createGroupQuery().groupMember(userId).list();
		
		for (Group group : groups)
			currents.add(group.getId());
		for (String select : groupIds)
			selects.add(select);
		
		for (String current : currents)
		{
			deletes.add(current);
			for(String select : selects)
				if(current.equals(select))
				{
					deletes.remove(select);
					break;
				}
		}
		
		for (String select : selects)
		{
			adds.add(select);
			for(String current : currents)
				if(select.equals(current))
				{
					adds.remove(current);
					break;
				}
		}
		
		
		for (String groupId : adds)
			try {
				System.out.println("create : "+userId+" "+groupId);
				processEngine.getIdentityService().createMembership(userId,
						groupId);
				result.put(groupId, true);
			} catch (RuntimeException e) {
				result.put(groupId, false);
			}
		
		for (String groupId : deletes)
			try {
				System.out.println("delete : "+userId+" "+groupId);
				processEngine.getIdentityService().deleteMembership(userId,
						groupId);
				result.put(groupId, true);
			} catch (RuntimeException e) {
				result.put(groupId, false);
			}

		return result;
	}

	@Override
	public Object addUserToGroups(String userId, String[] groupIds) {
		Map<String, Boolean> result = new HashMap<String, Boolean>();

		for (String groupId : groupIds)
			try {
				processEngine.getIdentityService().createMembership(userId,
						groupId);
				result.put(groupId, true);
			} catch (RuntimeException e) {
				result.put(groupId, false);
			}

		return result;

	}

	@Override
	public Map<String, Object> getUserNoGroups(String userId) {

		System.out.println("getUserNoGroupDAO : userId-" + userId);

		List<Group> groups = processEngine.getIdentityService()
				.createGroupQuery().groupMember(userId).list();
		List<Group> allGroups = processEngine.getIdentityService()
				.createGroupQuery().list();
		System.out.println("Groups Size : " + groups.size());
		System.out.println("AllGroups Size : " + allGroups.size());

		// for(Group group : groups)
		// {
		// allGroups.remove(group);
		// }
		// allGroups.removeAll(groups);

		List<Group> userNoGroups = new ArrayList<Group>();

		for (Group group : allGroups) {
			// System.out.println(!groups.contains(group));
			// if(!groups.contains(group))
			// userNoGroups.add(group);
			boolean contain = false;
			for (Group userGroup : groups) {
				if (userGroup.getId().equals(group.getId()))
					contain = true;
			}
			if (!contain)
				userNoGroups.add(group);
		}

		Map<String, Object> modelMap = new HashMap<String, Object>(2);

		if (userNoGroups != null)
			if (userNoGroups.size() == 0)
				modelMap.put("totalCount", 0);
			else
				modelMap.put("totalCount", userNoGroups.size());
		modelMap.put("records", userNoGroups);

		System.out.println(userNoGroups.size());

		return modelMap;
	}

	@Override
	public String removeGroup(String userId, String groupId) {

		processEngine.getIdentityService().deleteMembership(userId, groupId);

		return "1";
	}

	@Override
	public String removeUser(String userId, String groupId) {

		processEngine.getIdentityService().deleteMembership(userId, groupId);

		return "1";
	}

	@Override
	public Map<String, Object> getGroupNoUsers(String groupId) {
		System.out.println("getGroupNoUsers : groupId-" + groupId);

		List<User> users = processEngine.getIdentityService().createUserQuery()
				.memberOfGroup(groupId).list();
		List<User> allUsers = processEngine.getIdentityService()
				.createUserQuery().list();

		List<User> groupNoUsers = new ArrayList<User>();

		for (User user : allUsers) {
			boolean contain = false;
			for (User groupUser : users) {
				if (user.getId().equals(groupUser.getId()))
					contain = true;
			}
			if (!contain)
				groupNoUsers.add(user);
		}

		Map<String, Object> modelMap = new HashMap<String, Object>(2);

		if (groupNoUsers != null)
			if (groupNoUsers.size() == 0)
				modelMap.put("totalCount", 0);
			else
				modelMap.put("totalCount", groupNoUsers.size());
		modelMap.put("records", groupNoUsers);

		System.out.println(groupNoUsers.size());

		return modelMap;
	}

	@Override
	public String createUser(String userID, String firstName, String lastName,
			String email, String password, String groupID,
			Map<String, String> userInfos) {

		String resultCreateUser = createUser(userID, firstName, lastName,
				email, password);

		for (Entry<String, String> userInfo : userInfos.entrySet())
			processEngine.getIdentityService().setUserInfo(userID,
					userInfo.getKey(), userInfo.getValue());

		processEngine.getIdentityService().createMembership(userID, groupID);

		return resultCreateUser;
	}

	@Override
	public String updateUser(String userID, String firstName, String lastName,
			String email, String password, String group,
			Map<String, String> userInfo) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User checkUser(String userID, String password) {

		User user = processEngine.getIdentityService().createUserQuery()
				.userId(userID).singleResult();
		if (user == null)
			return null;

		if (user.getPassword().equals(password))
			return user;

		return null;
	}

}
