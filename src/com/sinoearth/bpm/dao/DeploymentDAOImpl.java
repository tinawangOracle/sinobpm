package com.sinoearth.bpm.dao;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.activiti.engine.ProcessEngine;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.impl.util.ReflectUtil;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.DeploymentBuilder;
import org.activiti.engine.repository.DeploymentQuery;
import org.activiti.engine.repository.ProcessDefinition;
import org.springframework.orm.hibernate3.HibernateTemplate;

public class DeploymentDAOImpl extends ActivitiParentDAOImpl implements
		DeploymentDAO {

	public DeploymentDAOImpl(ProcessEngine processEngine) {
		super(processEngine);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String deployment(String deploymentName, String filePath) {
		File sourceFile = new File(filePath);
		String resourceName = sourceFile.getName().substring(0,
				sourceFile.getName().lastIndexOf("."))
				+ ".bpmn20.xml";

		return deployment(deploymentName, resourceName, filePath);
	}

	@Override
	public String deployment(String deploymentName, String resourceName,
			String filePath) {
		RepositoryService repositoryService = processEngine
				.getRepositoryService();
		DeploymentBuilder db = repositoryService.createDeployment();
		db.name(deploymentName);
		db.addInputStream(resourceName,
				ReflectUtil.getResourceAsStream(filePath)).deploy();
		return "0";
	}

	@Override
	public List<Deployment> getAllDeployment() {
		RepositoryService repositoryService = processEngine
				.getRepositoryService();

		DeploymentQuery _query = repositoryService.createDeploymentQuery()
				.orderByDeploymentName().asc().orderByDeploymentId().asc();
		List<Deployment> deployments = _query.list();
		return deployments;
	}

	private String getProcessDisplayName(ProcessDefinition processDefinition) {
		if (processDefinition.getName() != null) {
			return processDefinition.getName();
		} else {
			return processDefinition.getKey();
		}
	}

	@Override
	public Map<String, Object> getDeployment(String did) {
		Map<String, Object> map = new HashMap<String, Object>();

		// processes
		List<ProcessDefinition> processDefinitions = repositoryService
				.createProcessDefinitionQuery().deploymentId(did)
				.orderByProcessDefinitionName().asc().list();
		ArrayList<HashMap> _procsList = new ArrayList<HashMap>(
				processDefinitions.size());
		if (processDefinitions.size() > 0) {
			for (final ProcessDefinition processDefinition : processDefinitions) {
				HashMap<String, String> _proc = new HashMap<String, String>();
				String _pname = getProcessDisplayName(processDefinition);
				_proc.put("name", _pname);
				_proc.put("id", processDefinition.getId());
				_procsList.add(_proc);
			}
		}
		map.put("procs", _procsList);
		map.put("pcnt", _procsList.size());

		// resources
		List<String> resourceNames = repositoryService
				.getDeploymentResourceNames(did);

		HashMap<String, String> _res = null;
		ArrayList<HashMap> _resList = new ArrayList<HashMap>(
				resourceNames.size());
		if (resourceNames.size() > 0) {
			for (final String resourceName : resourceNames) {
				_res = new HashMap<String, String>(1);
				System.out.println(resourceName);
				_res.put("name", resourceName);
				_resList.add(_res);
			}
		}
		map.put("resources", _resList);
		map.put("rcnt", resourceNames.size());

		return map;
	}

	@Override
	public void deleteDeployment(String did) {
		repositoryService.deleteDeployment(did, true);
	}
	@Override
	public int addDeleteWarning(String did) {
		List<ProcessDefinition> processDefinitions = repositoryService
				.createProcessDefinitionQuery().deploymentId(did).list();

		int nrOfProcessInstances = 0;
		for (ProcessDefinition processDefinition : processDefinitions) {
			nrOfProcessInstances += runtimeService.createProcessInstanceQuery()
					.processDefinitionId(processDefinition.getId()).count();
		}

		return nrOfProcessInstances;

	}
}
