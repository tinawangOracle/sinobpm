package com.sinoearth.bpm.dao;

import java.util.Map;
import java.util.List;
import org.apache.ibatis.annotations.Select;

public interface HistoryCountByProcSQL {
	@Select("select a.ID_ as id ,a.NAME_ as name, b.cnt from act_re_procdef a, act_v_doneProcInsts b where a.ID_=b.proc_def_id_")
	List<Map<String, Object>> selectDoneProcInstCnt();

	@Select("select a.ID_ as id,a.NAME_ as name, b.cnt from act_re_procdef a, act_v_activeProcInsts b where a.ID_=b.proc_def_id_")
	List<Map<String, Object>> selectActiveProcInstCnt();

}
