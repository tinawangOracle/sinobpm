package com.sinoearth.bpm.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.activiti.engine.ActivitiException;
import org.activiti.engine.FormService;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.form.FormProperty;
import org.activiti.engine.form.StartFormData;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.Job;
import org.activiti.engine.impl.persistence.entity.JobEntity;
import org.activiti.engine.impl.persistence.entity.MessageEntity;
import org.activiti.engine.impl.persistence.entity.TimerEntity;
import org.activiti.engine.impl.jobexecutor.TimerActivateProcessDefinitionHandler;
import org.activiti.engine.impl.jobexecutor.TimerSuspendProcessDefinitionHandler;

import com.sinoearth.bpm.Messages;

import org.springframework.beans.factory.annotation.Autowired;

import com.sinoearth.bpm.ConsoleApp;
import com.sinoearth.bpm.I18nManager;
import com.sinoearth.util.PrettyTimeLabel;

import java.util.Date;

public class JobDAOImpl extends ActivitiParentDAOImpl implements JobDAO {
	private static final long serialVersionUID = 1L;

	public JobDAOImpl(ProcessEngine processEngine) {
		super(processEngine);
		// i18nManager = ConsoleApp.get().getI18nManager();
	}

	@Override
	public List listJobs(int pnum, int count) {
		List<Job> jobs = managementService.createJobQuery().orderByJobDuedate()
				.asc().orderByJobId().asc().listPage(pnum * count, count);
		ArrayList<HashMap<String, Object>> _objs = new ArrayList<HashMap<String, Object>>(
				jobs.size());
		for (Job j : jobs) {
			HashMap<String, Object> _item = new HashMap<String, Object>();
			_item.put("name", getName(j));
			_item.put("id", j.getId());
			_item.put("duedate", j.getDuedate());
			_objs.add(_item);
		}
		return _objs;
	}

	private String getName(Job theJob) {
		if (theJob instanceof TimerEntity) {
			return "Timer job " + theJob.getId();
		} else if (theJob instanceof MessageEntity) {
			return "Message job " + theJob.getId();
		} else {
			return "Job " + theJob.getId();
		}
	}

	@Override
	public void deleteJob(String id) {
		managementService.deleteJob(id);
	}

	@Override
	public void executeJob(String id) throws Exception {

		managementService.executeJob(id);

	}

	@Override
	public HashMap getJob(String id) {
		Job job = managementService.createJobQuery().jobId(id).singleResult();
		HashMap _map = new HashMap();
		// i18nManager = ConsoleApp.get().getI18nManager();
		if (job.getExceptionMessage() != null) {
			_map.put("exception", job.getExceptionMessage());
			String stack = managementService.getJobExceptionStacktrace(id);
			_map.put("exceptionstack", stack);
		} else {
			if (job.getProcessDefinitionId() != null) {

				if (job instanceof TimerEntity) {

					_map.put(
							"jobtype",
							i18nManager.getMessage(Messages.JOB_TIMER,
									job.getId()));
				} else if (job instanceof MessageEntity) {
					_map.put(
							"jobtype",
							i18nManager.getMessage(Messages.JOB_MESSAGE,
									job.getId()));
				} else {
					_map.put("jobtype", i18nManager.getMessage(
							Messages.JOB_DEFAULT_NAME, job.getId()));
				}

				// This is a hack .. need to cleanify this in the engine
				JobEntity jobEntity = (JobEntity) job;

				_map.put("duedate", PrettyTimeLabel.getValue(i18nManager,
						i18nManager.getMessage(Messages.JOB_DUEDATE),
						job.getDuedate(),
						i18nManager.getMessage(Messages.JOB_NO_DUEDATE), false));
				if (jobEntity.getJobHandlerType().equals(
						TimerSuspendProcessDefinitionHandler.TYPE)) {
					_map.put("jobhtype", i18nManager
							.getMessage(Messages.JOB_SUSPEND_PROCESSDEFINITION)); // suspend
					_map.put("processId", job.getProcessDefinitionId());
				} else if (jobEntity.getJobHandlerType().equals(
						TimerActivateProcessDefinitionHandler.TYPE)) {
					_map.put(
							"jobhtype",
							i18nManager
									.getMessage(Messages.JOB_ACTIVATE_PROCESSDEFINITION)); // activate
					_map.put("processId", job.getProcessDefinitionId());
				} else {
					_map.put("jobhtype",
							i18nManager.getMessage(Messages.JOB_NOT_EXECUTED));
				}

			} else {
				_map.put("jobtype", "-1");
			}
		}
		return _map;
	}
}
