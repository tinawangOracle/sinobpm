package com.sinoearth.bpm.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.activiti.engine.ProcessEngine;
import org.activiti.engine.identity.User;
import org.hibernate.Transaction;
import org.springframework.orm.hibernate3.HibernateTemplate;

import com.sinoearth.bpm.domain.ActIdInfo;
import com.sinoearth.bpm.domain.ActIdUser;
import com.sinoearth.bpm.domain.ActIdUserND;

public class ActIdUserDAOImpl extends ParentDAOImpl implements ActIdUserDAO {

	private ProcessEngine processEngine;
	
	public ProcessEngine getProcessEngine() {
		return processEngine;
	}

	public void setProcessEngine(ProcessEngine processEngine) {
		this.processEngine = processEngine;
	}

	public ActIdUserDAOImpl(HibernateTemplate template) {
		super(template);
		// TODO Auto-generated constructor stub
	}

	@Override
	public Object getEntity(Class entityClass, Serializable id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List getListForPage(String hql, int offset, int lengh) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ActIdUser getUser(String email, String pwd) {
		List cnt = null;
		if (pwd != null)
			cnt = template.find("from ActIdUser where email=? and pwd=?",
					new Object[] { email, pwd });
		else
			cnt = template.find("from ActIdUser where email=?",
					new Object[] { email });
		if (cnt == null || cnt.size() == 0)
			return null;

		ActIdUser _user = (ActIdUser) cnt.get(0);
		List<ActIdInfo> _infos = (List<ActIdInfo>) template.find(
				"from ActIdInfo where actIdUser=?", new Object[] { _user });
		if (_infos == null || _infos.size() == 0)
			return _user;
		for (ActIdInfo info : _infos) {
			if (info.getKey().equals("idcard")) {
				_user.setIdcard(info.getValue());
			} else if (info.getKey().equals("phone")) {
				_user.setPhone(info.getValue());
			}
		}
		return _user;
	}

	@Override
	public String createUser(ActIdUser _user) {
		List cnt = template.find("select(1) from ActIdUser where email=?",
				new Object[] { _user.getEmail() });
		if (cnt != null && cnt.size() > 0) {
			int _i = (Integer) cnt.get(0);
			if (_i > 0)
				return "1:The email is already registered.";
		}
		try {
			Transaction _t = template.getSessionFactory().openSession()
					.beginTransaction();
			// _t.begin();
			template.save(_user);
			Set<ActIdInfo> _infos = _user.getActIdInfos();
			for (ActIdInfo _i : _infos) {
				System.out.println(_i.getValue());
				template.save(_i);
			}
			_t.commit();

		} catch (Exception ex) {
			ex.printStackTrace();
			return "1:" + ex.getMessage();
		}
		return "0";
	}

	@Override
	public String changePwd(String id, String cpass, String npass) {
		List _c = template.find("from ActIdUserND where id=?", id);
		if (_c == null || _c.size() == 0) {
			return "0:Cannot find the user.";
		}
		ActIdUserND _user = (ActIdUserND) _c.get(0);
		if (!_user.getPwd().equals(cpass)) {
			return "0:Current password is wrong!";
		}
		_user.setPwd(npass);
		template.save(_user);
		return "1:Save Successfully.";
	}

	@Override
	public String saveUser(ActIdUser _user) {
		List cnt = template.find("select(1) from ActIdUser where id=?",
				new Object[] { _user.getEmail() });
		if (cnt == null || cnt.size() == 0) {
			return "1:The user does not exist!";
		}
		try {
			Transaction _t = template.getSessionFactory().openSession()
					.beginTransaction();
			// _t.begin();
			template.bulkUpdate(
					"update ActIdUser set last=?,first=?,email=? where id=?",
					new Object[] { _user.getLast(), _user.getFirst(),
							_user.getEmail(), _user.getId() });
			Set<ActIdInfo> _infos = _user.getActIdInfos();
			for (ActIdInfo _i : _infos) {
				System.out.println(_i.getValue());
				template.merge(_i);
			}
			_t.commit();

		} catch (Exception ex) {
			ex.printStackTrace();
			return "1:" + ex.getMessage();
		}
		return "0";
	}

	@Override
	public Map<String, Object> getUsers() {
		
		List<User> users = processEngine.getIdentityService().createUserQuery().list();
		List<User> usersWithNoPassword = new ArrayList<User>();
		for(User user : users)
		{
			user.setPassword("*");
			usersWithNoPassword.add(user);
		}
		
		
		Map<String, Object> modelMap = new HashMap<String, Object>(2);
		
		if (users != null)
			if (users.size() == 0)
				modelMap.put("totalCount", 0);
			else
				modelMap.put("totalCount", usersWithNoPassword.size());
		modelMap.put("records", usersWithNoPassword);
		
		return modelMap;
	}
}
