package com.sinoearth.bpm.dao;

import java.util.Date;
import java.util.List;

import org.activiti.engine.form.FormProperty;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.Job;

public interface ProcessDefinitionDAO extends ActivitiParentDAO {
	public List<ProcessDefinition> getAllProcessDefinition();

	public List<ProcessDefinition> getAllProcessDefinition(String seq);

	public List<ProcessDefinition> getAllProcessDefinition(String seq,
			int pageCount, int pageNumber);

	public List<ProcessDefinition> getUserProcessDefinitionByUserID(
			String userID);

	public List<ProcessDefinition> getUserProcessDefinitionByUserID(
			String userID, String seq);

	public List<ProcessDefinition> getUserProcessDefinitionByUserID(
			String userID, String seq, int pageCount, int pageNumber);

	public List<ProcessDefinition> getProcessDefinitionByProcessDeploymentID(
			String deploymentID);

	public List<ProcessDefinition> getProcessDefinitionByProcessDeploymentID(
			String deploymentID, String seq);

	public List<ProcessDefinition> getProcessDefinitionByProcessDeploymentID(
			String deploymentID, String seq, int pageCount, int pageNumber);

	public ProcessDefinition getProcessDefinitionByProcessDefinitionID(
			String processDefinitionID);

	public ProcessDefinition getProcessDefinitionByProcessDefinitionKey(
			String processDefinitionKey);

	public List<FormProperty> getProcessDefinitionStartFormDataByProcessDefinitionID(
			String processDefinitionID);

	// ////////////////////////////////////////////////////////////////////////////////////////////////
	public List<ProcessDefinition> getSuspendedProcs(String seq, int pageCount,
			int pageNumber);

	public List<ProcessDefinition> getActiveProcs(String seq, int pageCount,
			int pageNumber);

	public boolean hasSuspendJobs(String pid);
	public boolean hasActiveJobs(String pid);

	public void suspendProcess(String pid, boolean isBoth, Date sdate);

	public void activateProcess(String pid, boolean isBoth, Date sdate);

	public List<ProcessDefinition> getAllBaseProcessDefinition();

	public List<ProcessDefinition> getAllBaseProcessDefinition(String seq);

	public List<ProcessDefinition> getAllBaseProcessDefinition(String seq,
			int pageCount, int pageNumber);

	public List<ProcessDefinition> getBaseUserProcessDefinitionByUserID(
			String userID);

	public List<ProcessDefinition> getBaseUserProcessDefinitionByUserID(
			String userID, String seq);

	public List<ProcessDefinition> getBaseUserProcessDefinitionByUserID(
			String userID, String seq, int pageCount, int pageNumber);

	public List<ProcessDefinition> getBaseProcessDefinitionByProcessDeploymentID(
			String deploymentID);

	public List<ProcessDefinition> getBaseProcessDefinitionByProcessDeploymentID(
			String deploymentID, String seq);

	public List<ProcessDefinition> getBaseProcessDefinitionByProcessDeploymentID(
			String deploymentID, String seq, int pageCount, int pageNumber);

	public ProcessDefinition getBaseProcessDefinitionByProcessDefinitionKey(
			String processDefinitionKey);

}
