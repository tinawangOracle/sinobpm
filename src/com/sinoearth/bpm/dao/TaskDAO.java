package com.sinoearth.bpm.dao;

import java.util.List;
import java.util.Map;

import org.activiti.engine.form.FormProperty;
import org.activiti.engine.history.HistoricTaskInstance;
import org.activiti.engine.task.Task;

public interface TaskDAO extends ActivitiParentDAO {

	public Task getTask(String taskID);
	public List<FormProperty> getTaskForm(String taskID);
	
	public List<Task> getAllTasksByAssignee(String userID);
	public List<Task> getAllTasksByOwner(String userID);
	public List<Task> getAllTasksByInvolved(String userID);
	public List<Task> getUnassginedTasksByGroup(String groupID);
	
	public List<Task> getAllTasksByAssignee(String userID, String seq);
	public List<Task> getAllTasksByOwner(String userID, String seq);
	public List<Task> getAllTasksByInvolved(String userID, String seq);
	public List<Task> getUnassginedTasksByGroup(String groupID, String seq);
	
	public List<Task> getAllTasksByAssignee(String userID, String seq, int pageCount, int pageNumber);
	public List<Task> getAllTasksByOwner(String userID, String seq, int pageCount, int pageNumber);
	public List<Task> getAllTasksByInvolved(String userID, String seq, int pageCount, int pageNumber);
	public List<Task> getUnassginedTasksByGroup(String groupID, String seq, int pageCount, int pageNumber);
	
//	public Map<String, List<FormProperty>> getAllTasksFormByAssignee(String userID);
//	public Map<String, List<FormProperty>> getAllTasksFormByOwner(String userID);
//	public Map<String, List<FormProperty>> getAllTasksFormByInvolved(String userID);
	public Map<String, Object> getTaskVariables(String taskID);
	public String completeTask(String taskID, Map<String, Object> variables);
	
	public List<HistoricTaskInstance> getAllFinishedHistoricTask();
	public List<HistoricTaskInstance> getAllFinishedHistoricTask(String seq);
	public List<HistoricTaskInstance> getAllFinishedHistoricTask(String seq, int pageCount, int pageNumber);
	
	public List<HistoricTaskInstance> getFinishedHistoricTaskByOwnerByOwner(String userID);
	public List<HistoricTaskInstance> getFinishedHistoricTaskByOwner(String userID, String seq);
	public List<HistoricTaskInstance> getFinishedHistoricTaskByOwner(String userID, String seq, int pageCount, int pageNumber);
}
