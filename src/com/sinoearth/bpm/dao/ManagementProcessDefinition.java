package com.sinoearth.bpm.dao;

import java.io.Serializable;
import java.util.List;

import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.repository.ProcessDefinition;

public class ManagementProcessDefinition implements Serializable {
	public ProcessDefinition processDefinition;
	public List<HistoricProcessInstance> runningInstances;
}
