package com.sinoearth.bpm.dao;

import java.util.List;

import org.activiti.engine.query.Query;

public interface ActivitiParentDAO {

	public Query<?,?> getQuerySeq(Query<?,?> query, String seq);
	public List<?> getListByCount(Query<?,?> query, String seq, int firstResult, int maxResults);
	public List<?> getListByPage(Query<?,?> query, String seq, int pageCount, int pageNumber);
}
