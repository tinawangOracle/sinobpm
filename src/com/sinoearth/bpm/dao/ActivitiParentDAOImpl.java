package com.sinoearth.bpm.dao;

import java.util.List;

import org.activiti.engine.HistoryService;
import org.activiti.engine.ManagementService;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.query.Query;
import org.springframework.beans.factory.annotation.Autowired;

import com.sinoearth.bpm.I18nManager;

public class ActivitiParentDAOImpl implements ActivitiParentDAO {
	ProcessEngine processEngine;
	RepositoryService repositoryService;
	RuntimeService runtimeService;
	ManagementService managementService;
	HistoryService historyService;
	@Autowired
	protected I18nManager i18nManager;

	public ActivitiParentDAOImpl(ProcessEngine processEngine) {
		this.processEngine = processEngine;
		repositoryService = this.processEngine.getRepositoryService();
		runtimeService = this.processEngine.getRuntimeService();
		managementService = this.processEngine.getManagementService();
		historyService = this.processEngine.getHistoryService();
	}

	public RepositoryService getRepositoryService() {
		return repositoryService;
	}

	public ProcessEngine getProcessEngine() {
		return processEngine;
	}

	public Query<?, ?> getQuerySeq(Query<?, ?> query, String seq) {
		if (seq == null)
			return query;
		if (seq.equals("asc"))
			return query.asc();
		else if (seq.equals("desc"))
			return query.desc();
		return query;
	}

	public List<?> getListByCount(Query<?, ?> query, String seq,
			int firstResult, int maxResults) {
		return getQuerySeq(query, seq).listPage(firstResult, maxResults);
	}

	public List<?> getListByPage(Query<?, ?> query, String seq, int pageCount,
			int pageNumber) {
		return getListByCount(query, seq, pageNumber * pageCount, pageCount);
	}
}
