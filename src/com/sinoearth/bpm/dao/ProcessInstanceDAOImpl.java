package com.sinoearth.bpm.dao;

import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamReader;

import org.activiti.bpmn.converter.BpmnXMLConverter;
import org.activiti.bpmn.model.BpmnModel;
import org.activiti.bpmn.model.GraphicInfo;
import org.activiti.engine.FormService;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.form.StartFormData;
import org.activiti.engine.history.HistoricDetail;
import org.activiti.engine.history.HistoricFormProperty;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.history.HistoricProcessInstanceQuery;
import org.activiti.engine.history.HistoricTaskInstance;
import org.activiti.engine.history.HistoricVariableUpdate;
import org.activiti.engine.impl.RepositoryServiceImpl;
import org.activiti.engine.impl.cmd.AbstractCustomSqlExecution;
import org.activiti.engine.impl.cmd.CustomSqlExecution;
import org.activiti.engine.impl.persistence.entity.ProcessDefinitionEntity;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.ProcessInstance;

import com.sinoearth.bpm.Messages;

import org.springframework.orm.hibernate3.HibernateTemplate;
import org.activiti.engine.task.Task;

public class ProcessInstanceDAOImpl extends ActivitiParentDAOImpl implements
		ProcessInstanceDAO {

	public ProcessInstanceDAOImpl(ProcessEngine processEngine) {
		super(processEngine);
		// TODO Auto-generated constructor stub
	}

	// private ProcessEngine processEngine;
	// private RepositoryService repositoryService;
	protected Map<String, ProcessDefinition> cachedProcessDefinitions = new HashMap<String, ProcessDefinition>();

	// public RepositoryService getRepositoryService() {
	// return repositoryService;
	// }
	//
	// public void setRepositoryService(RepositoryService repositoryService) {
	// this.repositoryService = repositoryService;
	// }
	//
	// public ProcessEngine getProcessEngine() {
	// return processEngine;
	// }
	//
	// public void setProcessEngine(ProcessEngine processEngine) {
	// this.processEngine = processEngine;
	// }

	// public ProcessInstanceDAOImpl(HibernateTemplate template) {
	// super(template);
	// cachedProcessDefinitions = new HashMap<String, ProcessDefinition>();
	// }

	@Override
	public int myInstancesCnt(String userid) {
		return (int) processEngine.getHistoryService()
				.createHistoricProcessInstanceQuery().startedBy(userid)
				.unfinished().count();
	}

	@Override
	public ProcessInstance startProcessInstanceByProcessDefinitionID(
			String ProcessDefinitionID) {
		RuntimeService runtimeService = processEngine.getRuntimeService();
		ProcessInstance processInstance = runtimeService
				.startProcessInstanceById(ProcessDefinitionID);
		return processInstance;
	}

	@Override
	public ProcessInstance startProcessInstanceByProcessDefinitionID(
			String ProcessDefinitionID, Map<String, Object> variables) {
		RuntimeService runtimeService = processEngine.getRuntimeService();
		ProcessInstance processInstance = runtimeService
				.startProcessInstanceById(ProcessDefinitionID, variables);
		return processInstance;
	}

	@Override
	public ProcessInstance startProcessInstanceByProcessDefinitionID(
			String ProcessDefinitionID, String curuser) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<HashMap<String, String>> getMyInstance(String userid,
			int start, int pagesize) {
		List<HistoricProcessInstance> processInstances = null;
		if (start >= 0)
			processInstances = processEngine.getHistoryService()
					.createHistoricProcessInstanceQuery().startedBy(userid)
					.unfinished().listPage(start, pagesize);
		else
			processInstances = processEngine.getHistoryService()
					.createHistoricProcessInstanceQuery().startedBy(userid)
					.unfinished().list();
		ArrayList<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>(
				processInstances.size());
		for (HistoricProcessInstance i : processInstances) {
			HashMap<String, String> ret = new HashMap<String, String>(2);
			ProcessDefinition _p = getProcessDefinition(i
					.getProcessDefinitionId());
			String itemName = getProcessDisplayName(_p) + " (" + i.getId()
					+ ")"
					+ (i.getBusinessKey() != null ? i.getBusinessKey() : "");
			ret.put("name", itemName);
			ret.put("id", i.getId());
			list.add(ret);
		}
		return list;
	}

	@Override
	public ProcessDefinition getProcessDefinition(String id) {
		ProcessDefinition processDefinition = cachedProcessDefinitions.get(id);
		if (processDefinition == null) {
			processDefinition = repositoryService
					.createProcessDefinitionQuery().processDefinitionId(id)
					.singleResult();
			cachedProcessDefinitions.put(id, processDefinition);
		}
		return processDefinition;
	}

	protected String getProcessDisplayName(ProcessDefinition processDefinition) {
		if (processDefinition.getName() != null) {
			return processDefinition.getName();
		} else {
			return processDefinition.getKey();
		}
	}

	@Override
	public List<HistoricProcessInstance> getMyInstance(String userid) {
		List<HistoricProcessInstance> processInstances = null;

		processInstances = processEngine.getHistoryService()
				.createHistoricProcessInstanceQuery().startedBy(userid)
				.unfinished().list();

		return processInstances;
	}

	@Override
	public ProcessInstance getProcessInstance(String id) {
		return processEngine.getRuntimeService().createProcessInstanceQuery()
				.processInstanceId(id).singleResult();
	}

	@Override
	public List<Map<String, Object>> getFinishedDefinitions(int fromidx,
			int toidx) {
		// List<HistoricProcessInstance> instanceList = historyService
		// .createHistoricProcessInstanceQuery().finished().list();
		// return compositeInstances(instanceList);
		CustomSqlExecution<HistoryCountByProcSQL, List<Map<String, Object>>> customSqlExecution = new AbstractCustomSqlExecution<HistoryCountByProcSQL, List<Map<String, Object>>>(
				HistoryCountByProcSQL.class) {

			public List<Map<String, Object>> execute(
					HistoryCountByProcSQL customMapper) {
				return customMapper.selectDoneProcInstCnt();
			}
		};
		List<Map<String, Object>> r = managementService
				.executeCustomSql(customSqlExecution);
		return r.subList(fromidx, toidx < r.size() ? toidx : r.size());
	}

	private List<HashMap<String, String>> compositeInstances(
			List<HistoricProcessInstance> instanceList) {
		Map<String, ManagementProcessDefinition> completedDefinitions = new HashMap<String, ManagementProcessDefinition>();
		for (HistoricProcessInstance instance : instanceList) {
			String processDefinitionId = instance.getProcessDefinitionId();
			ManagementProcessDefinition managementDefinition = null;
			if (completedDefinitions.containsKey(processDefinitionId)) {
				managementDefinition = completedDefinitions
						.get(processDefinitionId);

			} else {
				ProcessDefinition definition = repositoryService
						.createProcessDefinitionQuery()
						.processDefinitionId(processDefinitionId)
						.singleResult();
				if (definition == null) {
					// this process has a missing definition - skip
					continue;
				}
				managementDefinition = new ManagementProcessDefinition();
				managementDefinition.processDefinition = definition;
				managementDefinition.runningInstances = new ArrayList<HistoricProcessInstance>();
				completedDefinitions.put(definition.getId(),
						managementDefinition);
			}

			managementDefinition.runningInstances.add(instance);
		}
		ArrayList<HashMap<String, String>> _rets = new ArrayList<HashMap<String, String>>(
				completedDefinitions.size());
		for (ManagementProcessDefinition managementDefinition : completedDefinitions
				.values()) {
			System.out.println(historyService
					.createHistoricProcessInstanceQuery()
					.finished()
					.processDefinitionId(
							managementDefinition.processDefinition.getId())
					.count());

			HashMap<String, String> _one = new HashMap<String, String>();
			_one.put("id", managementDefinition.processDefinition.getId());
			_one.put("name", managementDefinition.processDefinition.getName());
			_one.put("cnt", String
					.valueOf(managementDefinition.runningInstances.size()));
			_rets.add(_one);
		}
		System.out.println("==========");

		return _rets;
	}

	@Override
	public List<Map<String, Object>> getRunningDefinitions(int fromidx,
			int toidx) {

		// List<HistoricProcessInstance> instanceList = historyService
		// .createHistoricProcessInstanceQuery().unfinished().list();
		// return compositeInstances(instanceList);
		CustomSqlExecution<HistoryCountByProcSQL, List<Map<String, Object>>> customSqlExecution = new AbstractCustomSqlExecution<HistoryCountByProcSQL, List<Map<String, Object>>>(
				HistoryCountByProcSQL.class) {

			public List<Map<String, Object>> execute(
					HistoryCountByProcSQL customMapper) {
				return customMapper.selectActiveProcInstCnt();
			}
		};
		List<Map<String, Object>> r = managementService
				.executeCustomSql(customSqlExecution);

		return r.subList(fromidx, toidx < r.size() ? toidx : r.size());
	}

	@Override
	public Map<String, Object> getInstancesByProcId(String type, String pid,
			int fromidx, int toidx) {

		HistoricProcessInstanceQuery _q = historyService
				.createHistoricProcessInstanceQuery().processDefinitionId(pid);
		if (type.equals("1"))
			_q = _q.finished();
		else
			_q = _q.unfinished();

		List<HistoricProcessInstance> instanceList = _q
				.listPage(fromidx, toidx);
		ArrayList<HashMap<String, String>> _records = new ArrayList<HashMap<String, String>>();
		for (HistoricProcessInstance _i : instanceList) {
			HashMap<String, String> _one = new HashMap<String, String>();
			_one.put("id", _i.getId());
			_one.put("bkey", _i.getBusinessKey());
			_one.put("sUid", _i.getStartUserId());
			_one.put("sAid", _i.getStartActivityId());
			_one.put("st", _i.getStartTime().toString());
			_one.put("et", _i.getEndTime() == null ? "" : _i.getEndTime()
					.toString());
			_one.put("dur", _i.getDurationInMillis() == null ? "" : _i
					.getDurationInMillis().toString());
			_records.add(_one);
		}
		instanceList.clear();
		HashMap<String, Object> _ret = new HashMap<String, Object>();
		_ret.put("totalCount", _q.count());
		_ret.put("records", _records);
		return _ret;
	}

	@Override
	public Map<String, Object> getTasksByInst(String inst) {
		List<HistoricTaskInstance> tasks = historyService
				.createHistoricTaskInstanceQuery().processInstanceId(inst)
				.orderByHistoricTaskInstanceEndTime().desc()
				.orderByHistoricTaskInstanceStartTime().desc().list();
		HashMap<String, Object> _records = new HashMap<String, Object>();
		ArrayList<HashMap<String, Object>> _all = new ArrayList<HashMap<String, Object>>();
		for (HistoricTaskInstance t : tasks) {
			HashMap<String, Object> _one = new HashMap<String, Object>();
			_one.put("id", t.getId());
			_one.put("name", t.getName());
			_one.put("priority", t.getPriority());
			_one.put("dueDate", t.getDueDate());
			_one.put("startTime", t.getStartTime());
			_one.put("assignee", t.getAssignee());
			_one.put("endTime", t.getEndTime());
			_all.add(_one);
		}
		_records.put("records", _all);
		_records.put("totalCount", tasks.size());
		return _records;
	}

	@Override
	public Map<String, Object> getHVarsByInst(String inst) {
		// variable sorting is done in-memory (which is ok, since normally there
		// aren't that many vars)
		List<HistoricDetail> variables = historyService
				.createHistoricDetailQuery().processInstanceId(inst)
				.orderByTime().desc().list();

		List<HashMap<String, Object>> _records = new ArrayList<HashMap<String, Object>>();
		List<String> variableNames = new ArrayList<String>();
		for (HistoricDetail detail : variables) {
			if (detail instanceof HistoricVariableUpdate) {
				HistoricVariableUpdate variable = (HistoricVariableUpdate) detail;
				if (variableNames.contains(variable.getId()) == false) {
					variableNames.add(variable.getId());
					HashMap<String, Object> _one = new HashMap<String, Object>();
					_one.put("name", variable.getId());
					// String theValue =
					// variableRendererManager.getStringRepresentation(variable.getValue());
					_one.put("val", variable.getValue());
					_one.put("type", "variable");
					_records.add(_one);
				}
			} else {
				HistoricFormProperty form = (HistoricFormProperty) detail;
				if (variableNames.contains(form.getPropertyId()) == false) {
					variableNames.add(form.getPropertyId());
					HashMap<String, Object> _one = new HashMap<String, Object>();
					_one.put("name", form.getPropertyId());
					_one.put("val", form.getPropertyValue());
					_one.put("type", "form property");
					_records.add(_one);
				}
			}
		}
		HashMap<String, Object> _ret = new HashMap<String, Object>(2);
		_ret.put("totalCount", _records.size());
		_ret.put("records", _records);
		return _ret;
	}

	@Override
	public Map<String, Object> getVarsByInst(String inst) {
		Map<String, Object> variables = new TreeMap<String, Object>(
				runtimeService.getVariables(inst));
		ArrayList<HashMap<String, String>> vars = new ArrayList<HashMap<String, String>>(
				variables.size());
		for (String _key : variables.keySet()) {
			HashMap<String, String> _item = new HashMap<String, String>(2);
			_item.put("name", _key);
			Object _v = variables.get(_key);
			_item.put("value", _v == null ? "" : _v.toString());
			vars.add(_item);
		}
		HashMap<String, Object> _ret = new HashMap<String, Object>(2);
		_ret.put("totalCount", vars.size());
		_ret.put("records", vars);
		return _ret;
	}

	protected List<String> initImage(ProcessDefinition processDefinition,
			String instId) {
		// boolean didDrawImage = false;

		// ExplorerApp.get().isUseJavascriptDiagram()
		boolean useJS = true;
		if (useJS) {
			try {
				final InputStream definitionStream = repositoryService
						.getResourceAsStream(
								processDefinition.getDeploymentId(),
								processDefinition.getResourceName());
				XMLInputFactory xif = XMLInputFactory.newInstance();
				XMLStreamReader xtr = xif
						.createXMLStreamReader(definitionStream);
				BpmnModel bpmnModel = new BpmnXMLConverter()
						.convertToBpmnModel(xtr);

				if (bpmnModel.getFlowLocationMap().size() > 0) {
					ArrayList<String> _ret = new ArrayList<String>(3);
					String _url = null;
					if (instId == null) {
						// url = new URL("http", "localhost", 8888,
						// "sinobpm/diagram-viewer/index.html?processDefinitionId="
						// + processDefinition.getId());
						_url = "diagram-viewer/index.html?processDefinitionId="
								+ processDefinition.getId();
						_ret.add(_url);
					} else {
						// url = new URL("http", "localhost", 8888,
						// "sinobpm/diagram-viewer/index.html?processDefinitionId="
						// + processDefinition.getId()
						// + "&processInstanceId=" + instId);
						_url = "diagram-viewer/index.html?processDefinitionId="
								+ processDefinition.getId()
								+ "&processInstanceId=" + instId;
						_ret.add(_url);

					}
					int maxX = 0;
					int maxY = 0;
					for (String key : bpmnModel.getLocationMap().keySet()) {
						GraphicInfo graphicInfo = bpmnModel.getGraphicInfo(key);
						double elementX = graphicInfo.getX()
								+ graphicInfo.getWidth();
						if (maxX < elementX) {
							maxX = (int) elementX;
						}
						double elementY = graphicInfo.getY()
								+ graphicInfo.getHeight();
						if (maxY < elementY) {
							maxY = (int) elementY;
						}
					}
					System.out.println(maxY);
					_ret.add((maxX + 350) + "");
					_ret.add((maxY + 220) + "");
					return _ret;
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return null;
	}

	@Override
	public Map<String, Object> getPngByInst(String sid) throws Exception {
		ProcessInstance _inst = getProcessInstance(sid);

		ProcessDefinition _pdef = getProcessDefinition(_inst
				.getProcessDefinitionId());
		if (_pdef == null) {
			return null;
		}

		ProcessDefinitionEntity processDefinitionEntity = (ProcessDefinitionEntity) ((RepositoryServiceImpl) repositoryService)
				.getDeployedProcessDefinition(_pdef.getId());
		boolean didDrawImage = false;
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("pname",
				(_pdef.getName() == null) ? _pdef.getKey() : _pdef.getName());
		map.put("pver", _pdef.getVersion());

		if (processDefinitionEntity != null) {
			List<String> _url = initImage(_pdef, _inst.getId());
			if (_url != null) {
				didDrawImage = true;
				map.put("success", "0");
				map.put("url", _url.get(0));
				map.put("maxX", _url.get(1));
				map.put("maxY", _url.get(2));
				return map;
			}

		}

		if (didDrawImage == false)
			if (processDefinitionEntity.isGraphicalNotationDefined()) {
				map.put("success", "1");
				map.put("msg", "try");
				return map;
			}

		map.put("success", "2");
		map.put("msg", "no result");
		return map;
	}

	@Override
	public HistoricProcessInstance getHistoricProcessInstance(String id) {
		HistoricProcessInstanceQuery _q = historyService
				.createHistoricProcessInstanceQuery().processInstanceId(id);
		return _q.singleResult();
	}
}
