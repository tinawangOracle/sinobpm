package com.sinoearth.bpm.dao;


import java.util.Map;

import com.sinoearth.bpm.domain.ActReModel;

public interface ActReModelDAO extends ParentDAO {

	public  Map<String, Object> getAllModels(String sortdir, String sortattr,
			int start, int length);
}
