package com.sinoearth.bpm.dao;

import java.util.List;
import java.util.HashMap;

public interface JobDAO extends ActivitiParentDAO {
	public List listJobs(int pnum, int count);

	public void deleteJob(String id);

	public void executeJob(String id) throws Exception;

	public HashMap getJob(String id);
}
