package com.sinoearth.bpm.dao;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.activiti.engine.identity.Group;
import org.activiti.engine.identity.User;

public interface IdentityDAO extends ActivitiParentDAO {
	public Map<String, Object> getAllUsers();
	public Map<String, Object> getAllUsers(String seq);
	public Map<String, Object> getAllUsers(String seq, int pageCount, int pageNumber);
	
	public User checkUser(String userID, String password);
	
	public String deleteUserById(String id);
	public String createUser(String userID, String firstName, String lastName, String email, String password);
	public String createUser(String userID, String firstName, String lastName, String email, String password, String group, Map<String,String> userInfos);
	public String updateUser(String userID, String firstName, String lastName, String email, String password);
	public String updateUser(String userID, String firstName, String lastName, String email, String password, String group, Map<String,String> userInfos);
	
	public Map<String, Object> getAllGroups();
	public Map<String, Object> getAllGroups(String seq);
	public Map<String, Object> getAllGroups(String seq, int pageCount, int pageNumber);
	public String createGroup(String id, String name, String type);
	public String deleteGroupById(String id);
	public String updateGroup(String id, String name, String type);
	public Map<String, Object> getUsersByGroupId(String groupId);
	public Map<String, Object> getGroupsByUserId(String userId);
	public Map<String, Boolean> addUsersToGroup(String[] userIds, String groupId);
	public Map<String, Boolean> addGroupsToUser(String userId, String[] groupIds);
	public Object addUserToGroups(String userID, String[] groupIDs);
	public Map<String, Object> getUserNoGroups(String userId);
	public String removeGroup(String userId, String groupId);
	public String removeUser(String userId, String groupId);
	public Map<String, Object> getGroupNoUsers(String groupId);
	List<Group> getGroupsSetByUserId(String userId);
	
}
