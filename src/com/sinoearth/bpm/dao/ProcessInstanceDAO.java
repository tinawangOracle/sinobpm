package com.sinoearth.bpm.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.ProcessInstance;

public interface ProcessInstanceDAO extends ActivitiParentDAO {

	public ProcessInstance startProcessInstanceByProcessDefinitionID(
			String ProcessDefinitionID, Map<String, Object> variables);

	public ProcessInstance startProcessInstanceByProcessDefinitionID(
			String ProcessDefinitionID);

	public List<HistoricProcessInstance> getMyInstance(String userid);

	public ArrayList<HashMap<String, String>> getMyInstance(String userid,
			int start, int pagesize);

	public int myInstancesCnt(String userid);

	public ProcessInstance getProcessInstance(String id);

	public HistoricProcessInstance getHistoricProcessInstance(String id);
	
	public ProcessDefinition getProcessDefinition(String id);

	public ProcessInstance startProcessInstanceByProcessDefinitionID(
			String ProcessDefinitionID, String curuser);

	public List<Map<String, Object>> getFinishedDefinitions(int fromidx,
			int toidx);

	public List<Map<String, Object>> getRunningDefinitions(int fromidx,
			int toidx);

	public Map<String, Object> getInstancesByProcId(String type, String pid,
			int fromidx, int toidx);

	public Map<String, Object> getTasksByInst(String inst);

	public Map<String, Object> getHVarsByInst(String inst);

	public Map<String, Object> getVarsByInst(String inst);

	public Map<String, Object> getPngByInst(String sid) throws Exception;
}
