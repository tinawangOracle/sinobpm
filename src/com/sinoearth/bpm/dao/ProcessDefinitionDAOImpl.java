package com.sinoearth.bpm.dao;

import java.util.List;

import org.activiti.engine.FormService;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.form.FormProperty;
import org.activiti.engine.form.StartFormData;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.Job;
import org.activiti.engine.impl.persistence.entity.JobEntity;
import org.activiti.engine.impl.jobexecutor.TimerActivateProcessDefinitionHandler;
import org.activiti.engine.impl.jobexecutor.TimerSuspendProcessDefinitionHandler;

import java.util.Date;

public class ProcessDefinitionDAOImpl extends ActivitiParentDAOImpl implements
		ProcessDefinitionDAO {

	public ProcessDefinitionDAOImpl(ProcessEngine processEngine) {
		super(processEngine);
	}

	@Override
	public List<ProcessDefinition> getAllProcessDefinition() {
		RepositoryService repositoryService = processEngine
				.getRepositoryService();
		List<ProcessDefinition> processDefinitions = (List<ProcessDefinition>) getQuerySeq(
				repositoryService.createProcessDefinitionQuery(), null).list();
		return processDefinitions;
	}

	@Override
	public List<ProcessDefinition> getAllProcessDefinition(String seq) {
		RepositoryService repositoryService = processEngine
				.getRepositoryService();
		List<ProcessDefinition> processDefinitions = (List<ProcessDefinition>) getQuerySeq(
				repositoryService.createProcessDefinitionQuery()
						.orderByProcessDefinitionName(), seq).list();
		return processDefinitions;
	}

	@Override
	public List<ProcessDefinition> getAllProcessDefinition(String seq,
			int pageCount, int pageNumber) {
		RepositoryService repositoryService = processEngine
				.getRepositoryService();
		List<ProcessDefinition> processDefinitions = (List<ProcessDefinition>) getListByPage(
				repositoryService.createProcessDefinitionQuery()
						.orderByProcessDefinitionName(), seq, pageCount,
				pageNumber);
		return processDefinitions;
	}

	@Override
	public List<ProcessDefinition> getUserProcessDefinitionByUserID(
			String userID) {
		RepositoryService repositoryService = processEngine
				.getRepositoryService();
		List<ProcessDefinition> processDefinitions = (List<ProcessDefinition>) getQuerySeq(
				repositoryService.createProcessDefinitionQuery()
						.startableByUser(userID), null).list();
		return processDefinitions;
	}

	@Override
	public List<ProcessDefinition> getUserProcessDefinitionByUserID(
			String userID, String seq) {
		RepositoryService repositoryService = processEngine
				.getRepositoryService();
		List<ProcessDefinition> processDefinitions = (List<ProcessDefinition>) getQuerySeq(
				repositoryService.createProcessDefinitionQuery()
						.startableByUser(userID).orderByProcessDefinitionName(),
				seq).list();
		return null;
	}

	@Override
	public List<ProcessDefinition> getUserProcessDefinitionByUserID(
			String userID, String seq, int pageCount, int pageNumber) {
		RepositoryService repositoryService = processEngine
				.getRepositoryService();
		List<ProcessDefinition> processDefinitions = (List<ProcessDefinition>) getListByPage(
				repositoryService.createProcessDefinitionQuery()
						.startableByUser(userID).orderByProcessDefinitionName(),
				seq, pageCount, pageNumber);
		return null;
	}

	@Override
	public List<ProcessDefinition> getProcessDefinitionByProcessDeploymentID(
			String deploymentID) {
		RepositoryService repositoryService = processEngine
				.getRepositoryService();
		List<ProcessDefinition> processDefinitions = (List<ProcessDefinition>) getQuerySeq(
				repositoryService.createProcessDefinitionQuery().deploymentId(
						deploymentID), null).list();
		return processDefinitions;
	}

	@Override
	public List<ProcessDefinition> getProcessDefinitionByProcessDeploymentID(
			String deploymentID, String seq) {
		RepositoryService repositoryService = processEngine
				.getRepositoryService();
		List<ProcessDefinition> processDefinitions = (List<ProcessDefinition>) getQuerySeq(
				repositoryService.createProcessDefinitionQuery()
						.deploymentId(deploymentID)
						.orderByProcessDefinitionName(), seq).list();
		return processDefinitions;
	}

	@Override
	public List<ProcessDefinition> getProcessDefinitionByProcessDeploymentID(
			String deploymentID, String seq, int pageCount, int pageNumber) {
		RepositoryService repositoryService = processEngine
				.getRepositoryService();
		List<ProcessDefinition> processDefinitions = (List<ProcessDefinition>) getListByPage(
				repositoryService.createProcessDefinitionQuery()
						.deploymentId(deploymentID)
						.orderByProcessDefinitionName(), seq, pageCount,
				pageNumber);
		return processDefinitions;
	}

	@Override
	public ProcessDefinition getProcessDefinitionByProcessDefinitionID(
			String processDefinitionID) {
		RepositoryService repositoryService = processEngine
				.getRepositoryService();
		List<ProcessDefinition> processDefinitions = repositoryService
				.createProcessDefinitionQuery()
				.processDefinitionId(processDefinitionID).list();
		if (processDefinitions.size() > 0)
			return processDefinitions.get(0);
		return null;
	}

	@Override
	public ProcessDefinition getProcessDefinitionByProcessDefinitionKey(
			String processDefinitionKey) {
		RepositoryService repositoryService = processEngine
				.getRepositoryService();
		List<ProcessDefinition> processDefinitions = repositoryService
				.createProcessDefinitionQuery()
				.processDefinitionKey(processDefinitionKey).list();
		if (processDefinitions.size() > 0)
			return processDefinitions.get(0);
		return null;
	}

	@Override
	public List<FormProperty> getProcessDefinitionStartFormDataByProcessDefinitionID(
			String processDefinitionID) {
		FormService formService = processEngine.getFormService();
		StartFormData startFormData = formService
				.getStartFormData(processDefinitionID);
		List<FormProperty> pros = startFormData.getFormProperties();
		return null;
	}

	// ----------------------------------------------------

	@Override
	public List<ProcessDefinition> getAllBaseProcessDefinition() {
		RepositoryService repositoryService = processEngine
				.getRepositoryService();
		List<ProcessDefinition> processDefinitions = (List<ProcessDefinition>) getQuerySeq(
				repositoryService.createProcessDefinitionQuery()
						.latestVersion().active(), null).list();
		return processDefinitions;
	}

	@Override
	public List<ProcessDefinition> getAllBaseProcessDefinition(String seq) {
		RepositoryService repositoryService = processEngine
				.getRepositoryService();
		List<ProcessDefinition> processDefinitions = (List<ProcessDefinition>) getQuerySeq(
				repositoryService.createProcessDefinitionQuery()
						.orderByProcessDefinitionName().latestVersion()
						.active(), seq).list();
		return processDefinitions;
	}

	@Override
	public List<ProcessDefinition> getAllBaseProcessDefinition(String seq,
			int pageCount, int pageNumber) {
		RepositoryService repositoryService = processEngine
				.getRepositoryService();
		List<ProcessDefinition> processDefinitions = (List<ProcessDefinition>) getListByPage(
				repositoryService.createProcessDefinitionQuery()
						.latestVersion().active(), null, pageCount, pageNumber);
		return processDefinitions;
	}

	@Override
	public List<ProcessDefinition> getBaseUserProcessDefinitionByUserID(
			String userID) {
		RepositoryService repositoryService = processEngine
				.getRepositoryService();
		List<ProcessDefinition> processDefinitions = (List<ProcessDefinition>) getQuerySeq(
				repositoryService.createProcessDefinitionQuery()
						.startableByUser(userID).latestVersion().active(), null)
				.list();
		return processDefinitions;
	}

	@Override
	public List<ProcessDefinition> getBaseUserProcessDefinitionByUserID(
			String userID, String seq) {
		RepositoryService repositoryService = processEngine
				.getRepositoryService();
		List<ProcessDefinition> processDefinitions = (List<ProcessDefinition>) getQuerySeq(
				repositoryService.createProcessDefinitionQuery()
						.startableByUser(userID).latestVersion().active()
						.orderByProcessDefinitionName(), seq).list();
		return null;
	}

	@Override
	public List<ProcessDefinition> getBaseUserProcessDefinitionByUserID(
			String userID, String seq, int pageCount, int pageNumber) {
		RepositoryService repositoryService = processEngine
				.getRepositoryService();
		List<ProcessDefinition> processDefinitions = (List<ProcessDefinition>) getListByPage(
				repositoryService.createProcessDefinitionQuery()
						.startableByUser(userID).latestVersion().active()
						.orderByProcessDefinitionName(), seq, pageCount,
				pageNumber);
		return null;
	}

	@Override
	public List<ProcessDefinition> getBaseProcessDefinitionByProcessDeploymentID(
			String deploymentID) {
		RepositoryService repositoryService = processEngine
				.getRepositoryService();
		List<ProcessDefinition> processDefinitions = (List<ProcessDefinition>) getQuerySeq(
				repositoryService.createProcessDefinitionQuery()
						.deploymentId(deploymentID).latestVersion().active(),
				null).list();
		return processDefinitions;
	}

	@Override
	public List<ProcessDefinition> getBaseProcessDefinitionByProcessDeploymentID(
			String deploymentID, String seq) {
		RepositoryService repositoryService = processEngine
				.getRepositoryService();
		List<ProcessDefinition> processDefinitions = (List<ProcessDefinition>) getQuerySeq(
				repositoryService.createProcessDefinitionQuery()
						.deploymentId(deploymentID).latestVersion().active()
						.orderByProcessDefinitionName(), seq).list();
		return processDefinitions;
	}

	@Override
	public List<ProcessDefinition> getBaseProcessDefinitionByProcessDeploymentID(
			String deploymentID, String seq, int pageCount, int pageNumber) {
		RepositoryService repositoryService = processEngine
				.getRepositoryService();
		List<ProcessDefinition> processDefinitions = (List<ProcessDefinition>) getListByPage(
				repositoryService.createProcessDefinitionQuery()
						.deploymentId(deploymentID).latestVersion().active()
						.orderByProcessDefinitionName(), seq, pageCount,
				pageNumber);
		return processDefinitions;
	}

	@Override
	public ProcessDefinition getBaseProcessDefinitionByProcessDefinitionKey(
			String processDefinitionKey) {
		RepositoryService repositoryService = processEngine
				.getRepositoryService();
		List<ProcessDefinition> processDefinitions = repositoryService
				.createProcessDefinitionQuery()
				.processDefinitionKey(processDefinitionKey).latestVersion()
				.active().list();
		if (processDefinitions.size() > 0)
			return processDefinitions.get(0);
		return null;
	}

	@Override
	public List<ProcessDefinition> getSuspendedProcs(String seq, int pageCount,
			int pageNumber) {

		List<ProcessDefinition> processDefinitions = repositoryService
				.createProcessDefinitionQuery().suspended()
				.orderByProcessDefinitionName().asc()
				.orderByProcessDefinitionVersion().asc()
				.listPage(pageNumber * pageCount, pageCount);
		return processDefinitions;
	}

	@Override
	public List<ProcessDefinition> getActiveProcs(String seq, int pageCount,
			int pageNumber) {
		List<ProcessDefinition> processDefinitions = repositoryService
				.createProcessDefinitionQuery().active()
				.orderByProcessDefinitionName().asc()
				.orderByProcessDefinitionVersion().asc()
				.listPage(pageNumber * pageCount, pageCount);
		return processDefinitions;
	}

	@Override
	public boolean hasSuspendJobs(String pid) {
		boolean suspendJobPending = false;
		List<Job> jobs = processEngine.getManagementService().createJobQuery()
				.processDefinitionId(pid).list();
		
		for (Job job : jobs) {
			// TODO: this is a hack. Needs to be cleaner in engine!
			if (((JobEntity) job).getJobHandlerType().equals(
					TimerSuspendProcessDefinitionHandler.TYPE)) {
				suspendJobPending = true;
				break;
			}
		}
		return suspendJobPending;
	}

	@Override
	public boolean hasActiveJobs(String pid) {
		boolean activeJobPending = false;
		List<Job> jobs = processEngine.getManagementService().createJobQuery()
				.processDefinitionId(pid).list();
	
		for (Job job : jobs) {
			// TODO: this is a hack. Needs to be cleaner in engine!
			if (((JobEntity) job).getJobHandlerType().equals(
					TimerActivateProcessDefinitionHandler.TYPE)) {
				activeJobPending = true;
				break;
			}
		}
		return activeJobPending;
	}

	@Override
	public void suspendProcess(String pid, boolean isBoth, Date sdate) {
		repositoryService.suspendProcessDefinitionById(pid, isBoth, sdate);
	}

	@Override
	public void activateProcess(String pid, boolean isBoth, Date sdate) {
		repositoryService.activateProcessDefinitionById(pid, isBoth, sdate);
	}

}
