package com.sinoearth.bpm.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Projection;
import org.springframework.orm.hibernate3.HibernateTemplate;

import com.sinoearth.bpm.domain.ActReModel;

public class ActReModelDAOImpl extends ParentDAOImpl implements ActReModelDAO {

	public ActReModelDAOImpl(HibernateTemplate template) {
		super(template);
		// TODO Auto-generated constructor stub
	}

	@Override
	public Map<String, Object> getAllModels(String sortdir, String sortattr,
			int start, int length) {

		List _list = getListForPage(ActReModel.class, null, start, length,
				sortattr, sortdir, null);
		List cnt = null;
		if (start == 0)
			cnt = getRowCnt(ActReModel.class, "id", null);

		Map<String, Object> modelMap = new HashMap<String, Object>(2);

		if (cnt != null)
			if (cnt.size() == 0)
				modelMap.put("totalCount", 0);
			else
				modelMap.put("totalCount", cnt.get(0));
		modelMap.put("records", _list);
		return modelMap;
	}

}
