package com.sinoearth.bpm.dao;

import java.util.List;
import java.util.Map;

import org.activiti.engine.repository.Deployment;

public interface DeploymentDAO extends ActivitiParentDAO {

	public int addDeleteWarning(String did);

	public void deleteDeployment(String did);

	public Map<String, Object> getDeployment(String did);

	public String deployment(String deploymentName, String filePath);

	public String deployment(String deploymentName, String resourceName,
			String filePath);

	public List<Deployment> getAllDeployment();

}
