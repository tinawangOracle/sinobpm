package com.sinoearth.bpm.dao;

import java.util.List;
import java.util.Map;

import org.activiti.engine.identity.User;

import com.sinoearth.bpm.domain.ActIdUser;

public interface ActIdUserDAO extends ParentDAO {

	public String createUser(ActIdUser _user);

	public String saveUser(ActIdUser _user);

	public String changePwd(String id, String cpass, String npass);

	public ActIdUser getUser(String email, String pwd);
	
	public Map<String, Object> getUsers();
}
