package com.sinoearth.bpm.dao;

import java.io.Serializable;
import java.util.List;

public interface ParentDAO
{
	public Object getEntity(Class entityClass, Serializable id);
	public List getListForPage(final String hql, final int offset, final int lengh);
}
