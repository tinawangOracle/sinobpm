package com.sinoearth.bpm;

public class FormPropertyImpl implements java.io.Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected String id;
	protected String name;
	
	public void setId(String id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setValue(String value) {
		this.value = value;
	}

	protected boolean isRequired;
	
	public void setRequired(boolean isRequired) {
		this.isRequired = isRequired;
	}

	public void setReadable(boolean isReadable) {
		this.isReadable = isReadable;
	}

	public void setWritable(boolean isWritable) {
		this.isWritable = isWritable;
	}

	protected boolean isReadable;
	protected boolean isWritable;
	protected String value;

	public FormPropertyImpl(String id, String name, boolean isRequired,
			boolean isReadable, boolean isWritable) {
		this.id = id;
		this.name = name;
		this.isRequired = isRequired;
		this.isReadable = isReadable;
		this.isWritable = isWritable;
	}

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getValue() {
		return value;
	}

//	public boolean isRequired() {
//		return isRequired;
//	}
//
//	public boolean isReadable() {
//		return isReadable;
//	}
//
//	public void setValue(String value) {
//		this.value = value;
//	}
//
//	public boolean isWritable() {
//		return isWritable;
//	}
}
