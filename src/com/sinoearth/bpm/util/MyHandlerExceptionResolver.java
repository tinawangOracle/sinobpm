package com.sinoearth.bpm.util;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

public class MyHandlerExceptionResolver implements HandlerExceptionResolver {  
  
    public ModelAndView resolveException(HttpServletRequest request,
			HttpServletResponse response, Object handler, Exception ex) {  
     
                
        Map model = new HashMap();  
        model.put("ex", ex.getClass().getSimpleName());  
        model.put("error", ex.getMessage());  
        return new ModelAndView("error", model);  
    }
}