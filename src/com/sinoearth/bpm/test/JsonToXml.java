package com.sinoearth.bpm.test;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.activiti.bpmn.converter.BpmnXMLConverter;
import org.activiti.bpmn.model.BpmnModel;
import org.activiti.bpmn.model.FlowElement;
import org.activiti.bpmn.model.ItemDefinition;
import org.activiti.editor.language.json.converter.BpmnJsonConverter;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngineConfiguration;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.repository.Deployment;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ObjectNode;
import org.activiti.bpmn.model.Process;

public class JsonToXml {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ProcessEngine processEngine = ProcessEngineConfiguration
				.createStandaloneInMemProcessEngineConfiguration()
				.setDatabaseSchemaUpdate(
						ProcessEngineConfiguration.DB_SCHEMA_UPDATE_FALSE)
				.setJdbcDriver("com.mysql.jdbc.Driver")
				.setJdbcUrl(
						"jdbc:mysql://localhost:3306/abpm?autoReconnect=true")
				.setJdbcUsername("root").setJdbcPassword("welcome1")
				.setJobExecutorActivate(true).buildProcessEngine();

		RepositoryService repositoryService = processEngine
				.getRepositoryService();
		System.out.println("----");
		byte[] jsons = repositoryService.getModelEditorSource("9173");

		try {
			JsonNode jsonNode = new ObjectMapper().readTree(jsons);
			final ObjectNode modelNode = (ObjectNode) jsonNode;
			System.out.println(modelNode);
			BpmnModel model = new BpmnJsonConverter()
					.convertToBpmnModel(modelNode);
			byte[] bpmnBytes = new BpmnXMLConverter().convertToXML(model);
			System.out.println(new String(bpmnBytes,"UTF8"));

			String processName = "tina1.bpmn20.xml";
			Deployment deployment = repositoryService.createDeployment()
					.name("tina1").addString(processName, new String(bpmnBytes,"UTF8"))
					.deploy();
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		processEngine.close();
	}
}
