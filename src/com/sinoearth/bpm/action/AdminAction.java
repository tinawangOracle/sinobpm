package com.sinoearth.bpm.action;

import java.io.InputStream;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.activiti.engine.RepositoryService;
//import org.activiti.engine.identity.User;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.Job;
import org.springframework.web.servlet.ModelAndView;

//import com.sinoearth.bpm.dao.ActIdUserDAO;
import com.sinoearth.bpm.dao.DeploymentDAO;
import com.sinoearth.bpm.dao.IdentityDAO;
import com.sinoearth.bpm.dao.JobDAO;
import com.sinoearth.bpm.dao.ProcessDefinitionDAO;
import com.sinoearth.bpm.dao.ProcessInstanceDAO;

//import com.sinoearth.bpm.domain.ActIdUser;

public class AdminAction extends ParentAction {

	private IdentityDAO identityDAO;
	private DeploymentDAO deploymentDAO;
	// private ProcessEngine processEngine;
	private RepositoryService repositoryService;
	private ProcessDefinitionDAO processDefinitionDAO;
	private ProcessInstanceDAO processInstanceDAO;

	private JobDAO jobDAO;

	// private I18nManager i18nManager;
	//
	// public I18nManager getI18nManager() {
	// return i18nManager;
	// }
	//
	// public void setI18nManager(I18nManager i18nManager) {
	// this.i18nManager = i18nManager;
	// }

	public ProcessInstanceDAO getProcessInstanceDAO() {
		return processInstanceDAO;
	}

	public void setProcessInstanceDAO(ProcessInstanceDAO processInstanceDAO) {
		this.processInstanceDAO = processInstanceDAO;
	}

	public ProcessDefinitionDAO getProcessDefinitionDAO() {
		return processDefinitionDAO;
	}

	public void setProcessDefinitionDAO(
			ProcessDefinitionDAO processDefinitionDAO) {
		this.processDefinitionDAO = processDefinitionDAO;
	}

	public RepositoryService getRepositoryService() {
		return repositoryService;
	}

	public void setRepositoryService(RepositoryService repositoryService) {
		this.repositoryService = repositoryService;
	}

	public JobDAO getJobDAO() {
		return jobDAO;
	}

	public void setJobDAO(JobDAO jobDAO) {
		this.jobDAO = jobDAO;
	}

	public IdentityDAO getIdentityDAO() {
		return identityDAO;
	}

	public void setIdentityDAO(IdentityDAO identityDAO) {
		this.identityDAO = identityDAO;
	}

	public ModelAndView getUsers(HttpServletRequest request,
			HttpServletResponse response) {

		System.out.println("getUsersAction");

		if (identityDAO == null)
			return notFoundDAO();

		Map<String, Object> _users = identityDAO.getAllUsers();
		ModelAndView objs = new ModelAndView("jsonView", _users);
		return objs;
	}

	public ModelAndView getUsersByGroupId(HttpServletRequest request,
			HttpServletResponse response) {

		System.out.println("getUsersByGroupIdAction");
		System.out.println(request.getParameter("groupId"));

		if (identityDAO == null)
			return notFoundDAO();
		String groupId = request.getParameter("groupId");
		Map<String, Object> _users = identityDAO.getUsersByGroupId(groupId);
		ModelAndView objs = new ModelAndView("jsonView", _users);
		return objs;
	}

	public ModelAndView newUser(HttpServletRequest request,
			HttpServletResponse response) {
		if (identityDAO == null)
			return notFoundDAO();

		System.out.println("new user action");

		String userID = request.getParameter("userID");
		String firstName = request.getParameter("firstName");
		String lastName = request.getParameter("lastName");
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		String confirmPassword = request.getParameter("confirmPassword");
		String[] groupIDs = request.getParameterValues("groupID");

		// System.out.println(userID+"|"+firstName+"|"+lastName+"|"+email+"|"+password+"|"+confirmPassword+"|"+groupIDs[0]+"|"+groupIDs[1]);

		Map<String, Object> map = new HashMap<String, Object>();
		if (password.equals(confirmPassword)) {
			if (identityDAO.createUser(userID, firstName, lastName, email,
					password).equals("1")) {
				if (groupIDs.length != 0) {
					Map<String, Boolean> results = identityDAO.addGroupsToUser(
							userID, groupIDs);
					Boolean r = true;
					for (Boolean result : results.values())
						if (!result)
							r = false;

					if (r) {
						String msg = "创建新用户: " + email + " 并添加进组:";
						String msg1 = "";
						for (String groupID : groupIDs)
							msg1 = msg1 + groupID + " ";
						msg = msg + msg1;
						map.put("success", true);
						map.put("msg", msg);
					} else {
						String msg = "创建新用户: " + email + " 成功，添加进组失败";
						map.put("success", false);
						map.put("msg", msg);
					}
				} else {
					map.put("success", true);
					map.put("msg", "创建新用户: " + email + " 成功");
				}
			} else {
				map.put("success", false);
				map.put("msg", "创建新用户: " + email + " 失败");
			}
		} else {
			map.put("success", false);
			map.put("msg", "两次输入密码不一致");
		}
		ModelAndView objs = new ModelAndView("jsonView", map);
		return objs;
		// return null;
	}

	public ModelAndView deleteUser(HttpServletRequest request,
			HttpServletResponse response) {
		if (identityDAO == null)
			return notFoundDAO();
		String[] userIDs = request.getParameterValues("userIDs");
		String[] emails = request.getParameterValues("emails");

		for (String userID : userIDs)
			System.out.println("delete:" + userID);
		Map<String, Object> map = new HashMap<String, Object>();
		String msg = "删除用户: ";
		String msgErr = "删除用户: ";
		boolean success = true;

		for (int i = 0; i < userIDs.length; i++)
			if (identityDAO.deleteUserById(userIDs[i]).equals("1")) {
				msg = msg + emails[i] + " ";
			} else {
				msgErr = msgErr + emails[i] + " ";
				map.put("success", false);
			}

		msg = msg + "成功";
		msgErr = msgErr + "失败";

		map.put("success", success);
		map.put("msg", msg);
		map.put("msgErr", msgErr);
		ModelAndView objs = new ModelAndView("jsonView", map);
		return objs;
	}

	public ModelAndView updateUser(HttpServletRequest request,
			HttpServletResponse response) {
		if (identityDAO == null)
			return notFoundDAO();

		String userID = request.getParameter("userID");
		String firstName = request.getParameter("firstName");
		String lastName = request.getParameter("lastName");
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		String confirmPassword = request.getParameter("confirmPassword");

		System.out.println("Update: " + userID + "|" + firstName + "|"
				+ lastName + "|" + email + "|" + password + "|"
				+ confirmPassword);

		Map<String, Object> map = new HashMap<String, Object>();
		if (password.equals(confirmPassword)) {
			if (identityDAO.updateUser(userID, firstName, lastName, email,
					password).equals("1")) {
				map.put("success", true);
				map.put("msg", "更新用户: " + email + " 成功");
				ModelAndView objs = new ModelAndView("jsonView", map);
				return objs;
			} else {
				map.put("success", false);
				map.put("msg", "更新用户: " + email + " 失败");
				ModelAndView objs = new ModelAndView("jsonView", map);
				return objs;
			}
		} else {
			map.put("success", false);
			map.put("msg", "两次输入密码不一致");
			ModelAndView objs = new ModelAndView("jsonView", map);
			return objs;
		}
	}

	public ModelAndView getGroups(HttpServletRequest request,
			HttpServletResponse response) {

		System.out.println("getGroupsAction");

		if (identityDAO == null)
			return notFoundDAO();

		Map<String, Object> _users = identityDAO.getAllGroups();
		ModelAndView objs = new ModelAndView("jsonView", _users);
		return objs;
	}

	public ModelAndView getUserNoGroups(HttpServletRequest request,
			HttpServletResponse response) {

		System.out.println("getUserNoGroupsAction");

		if (identityDAO == null)
			return notFoundDAO();

		String userId = request.getParameter("userId");

		Map<String, Object> _users = identityDAO.getUserNoGroups(userId);
		ModelAndView objs = new ModelAndView("jsonView", _users);
		return objs;
	}

	public ModelAndView getGroupNoUsers(HttpServletRequest request,
			HttpServletResponse response) {

		System.out.println("getGroupNoUsers");

		if (identityDAO == null)
			return notFoundDAO();

		String groupId = request.getParameter("groupId");

		Map<String, Object> _users = identityDAO.getGroupNoUsers(groupId);
		ModelAndView objs = new ModelAndView("jsonView", _users);
		return objs;
	}

	public ModelAndView getGroupsByUserId(HttpServletRequest request,
			HttpServletResponse response) {

		System.out.println("getGroupsByUserIdAction");

		if (identityDAO == null)
			return notFoundDAO();

		String userId = request.getParameter("userId");
		Map<String, Object> _groups = identityDAO.getGroupsByUserId(userId);
		ModelAndView objs = new ModelAndView("jsonView", _groups);
		return objs;
	}

	public ModelAndView newGroup(HttpServletRequest request,
			HttpServletResponse response) {
		if (identityDAO == null)
			return notFoundDAO();

		String id = request.getParameter("id");
		String name = request.getParameter("name");
		String type = request.getParameter("type");

		System.out.println(id + "|" + name + "|" + type);

		Map<String, Object> map = new HashMap<String, Object>();

		if (identityDAO.createGroup(id, name, type).equals("1")) {
			map.put("success", true);
			map.put("msg", "创建新用户组: " + name + " 成功");
			ModelAndView objs = new ModelAndView("jsonView", map);
			return objs;
		} else {
			map.put("success", false);
			map.put("msg", "创建新用户组: " + name + " 失败");
			ModelAndView objs = new ModelAndView("jsonView", map);
			return objs;
		}
	}

	public ModelAndView deleteGroup(HttpServletRequest request,
			HttpServletResponse response) {
		if (identityDAO == null)
			return notFoundDAO();
		String[] ids = request.getParameterValues("ids");
		String[] names = request.getParameterValues("names");

		for (String id : ids)
			System.out.println("delete:" + id);
		Map<String, Object> map = new HashMap<String, Object>();
		String msg = "删除用户组: ";
		String msgErr = "删除用户组: ";
		boolean success = true;

		for (int i = 0; i < ids.length; i++)
			if (identityDAO.deleteGroupById(ids[i]).equals("1")) {
				msg = msg + names[i] + " ";
			} else {
				msgErr = msgErr + names[i] + " ";
				map.put("success", false);
			}

		msg = msg + "成功";
		msgErr = msgErr + "失败";

		map.put("success", success);
		map.put("msg", msg);
		map.put("msgErr", msgErr);
		ModelAndView objs = new ModelAndView("jsonView", map);
		return objs;
	}

	public ModelAndView updateGroup(HttpServletRequest request,
			HttpServletResponse response) {
		if (identityDAO == null)
			return notFoundDAO();

		String id = request.getParameter("id");
		String name = request.getParameter("name");
		String type = request.getParameter("type");

		System.out.println("Update: " + id + "|" + name + "|" + type);

		Map<String, Object> map = new HashMap<String, Object>();
		if (identityDAO.updateGroup(id, name, type).equals("1")) {
			map.put("success", true);
			map.put("msg", "更新用户组: " + name + " 成功");
			ModelAndView objs = new ModelAndView("jsonView", map);
			return objs;
		} else {
			map.put("success", false);
			map.put("msg", "更新用户组: " + type + " 失败");
			ModelAndView objs = new ModelAndView("jsonView", map);
			return objs;
		}
	}

	public ModelAndView addGroupsToUser(HttpServletRequest request,
			HttpServletResponse response) {

		String userId = request.getParameter("userId");
		String[] groupIds = request.getParameterValues("groupIds");
		System.out.println("userId:" + userId);
		for (String groupId : groupIds)
			System.out.println("groupId:" + groupId);

		Map<String, Boolean> results = identityDAO.addGroupsToUser(userId,
				groupIds);
		Boolean r = true;
		for (Boolean result : results.values())
			if (!result)
				r = false;

		Map<String, Object> map = new HashMap<String, Object>();
		if (r) {
			String msg = "用户: " + userId + " 更新组:";
			String msg1 = "";
			for (String groupID : results.keySet())
				msg1 = msg1 + groupID + " ";
			msg = msg + msg1 + " 成功";
			map.put("success", true);
			map.put("msg", msg);
		} else {
			String msg = "用户: " + userId + " 更新组失败";
			map.put("success", false);
			map.put("msg", msg);
		}

		return new ModelAndView("jsonView", map);
	}

	public ModelAndView removeGroup(HttpServletRequest request,
			HttpServletResponse response) {
		if (identityDAO == null)
			return notFoundDAO();
		String userId = request.getParameter("userId");
		String[] groupIds = request.getParameterValues("groupIds");
		String[] groupNames = request.getParameterValues("names");

		for (String groupId : groupIds)
			System.out.println("Remove group: " + groupId + " from user: "
					+ userId);

		Map<String, Object> map = new HashMap<String, Object>();
		String msg = "移除组: ";
		String msgErr = "移除组: ";
		boolean success = true;

		for (int i = 0; i < groupIds.length; i++)
			if (identityDAO.removeGroup(userId, groupIds[i]).equals("1")) {
				msg = msg + groupNames[i] + " ";
			} else {
				success = false;
				msgErr = msgErr + groupNames[i] + " ";
			}

		msg = msg + "成功";
		msgErr = msgErr + "失败";
		map.put("success", success);
		map.put("msg", msg);
		map.put("msgErr", msgErr);

		ModelAndView objs = new ModelAndView("jsonView", map);
		return objs;

	}

	// ////////////////////////////
	public ModelAndView addUsersToGroup(HttpServletRequest request,
			HttpServletResponse response) {

		String groupId = request.getParameter("groupId");
		String[] userIds = request.getParameterValues("userIds");

		Map<String, Boolean> results = identityDAO.addUsersToGroup(userIds,
				groupId);
		Boolean r = true;
		for (Boolean result : results.values())
			if (!result)
				r = false;

		Map<String, Object> map = new HashMap<String, Object>();
		if (r) {
			String msg = "组: " + groupId + " 更新用户:";
			String msg1 = "";
			for (String userId : results.keySet())
				msg1 = msg1 + userId + " ";
			msg = msg + msg1 + " 成功";
			map.put("success", true);
			map.put("msg", msg);
		} else {
			String msg = "组: " + groupId + " 更新用户:";
			map.put("success", false);
			map.put("msg", msg);
		}

		return new ModelAndView("jsonView", map);
	}

	public ModelAndView removeUser(HttpServletRequest request,
			HttpServletResponse response) {
		if (identityDAO == null)
			return notFoundDAO();
		String userIds[] = request.getParameterValues("userIds");
		String groupId = request.getParameter("groupId");
		String emails[] = request.getParameterValues("emails");

		for (String userId : userIds)
			System.out.println("Remove group: " + groupId + " from user: "
					+ userId);

		Map<String, Object> map = new HashMap<String, Object>();
		String msg = "移除用户: ";
		String msgErr = "移除用户: ";
		boolean success = true;

		for (int i = 0; i < userIds.length; i++)
			if (identityDAO.removeGroup(userIds[i], groupId).equals("1")) {
				msg = msg + emails[i] + " ";
			} else {
				success = false;
				msgErr = msgErr + emails[i] + " ";
			}

		msg = msg + "成功";
		msgErr = msgErr + "失败";
		map.put("success", success);
		map.put("msg", msg);
		map.put("msgErr", msgErr);

		ModelAndView objs = new ModelAndView("jsonView", map);
		return objs;

	}

	public ModelAndView newDeployment(HttpServletRequest request,
			HttpServletResponse response) {

		if (deploymentDAO == null)
			return notFoundDAO();
		String _id = request.getParameter("id");
		Map<String, Object> map = new HashMap<String, Object>(2);
		try {
			deploymentDAO.deleteDeployment(_id);
			map.put("success", true);
			map.put("msg", "success");
		} catch (Exception e) {
			map.put("success", false);
			map.put("msg", e.getMessage());
		}
		ModelAndView objs = new ModelAndView("jsonView", map);
		return objs;
	}

	public ModelAndView deleteDeployment(HttpServletRequest request,
			HttpServletResponse response) {

		if (deploymentDAO == null)
			return notFoundDAO();
		String _id = request.getParameter("id");
		Map<String, Object> map = new HashMap<String, Object>(2);
		try {
			deploymentDAO.deleteDeployment(_id);
			map.put("success", true);
			map.put("msg", "success");
		} catch (Exception e) {
			map.put("success", false);
			map.put("msg", e.getMessage());
		}
		ModelAndView objs = new ModelAndView("jsonView", map);
		return objs;
	}

	public ModelAndView prepareDeleteDeployment(HttpServletRequest request,
			HttpServletResponse response) {

		if (deploymentDAO == null)
			return notFoundDAO();
		String _id = request.getParameter("id");
		int _cnt = deploymentDAO.addDeleteWarning(_id);

		Map<String, Object> map = new HashMap<String, Object>(1);
		map.put("icnt", _cnt);
		ModelAndView objs = new ModelAndView("jsonView", map);
		return objs;
	}

	public ModelAndView getDeployment(HttpServletRequest request,
			HttpServletResponse response) {

		if (deploymentDAO == null)
			return notFoundDAO();
		String _id = request.getParameter("id");
		Map<String, Object> map = deploymentDAO.getDeployment(_id);
		ModelAndView objs = new ModelAndView("jsonView", map);
		return objs;
	}

	public ModelAndView getDeployments(HttpServletRequest request,
			HttpServletResponse response) {

		if (deploymentDAO == null)
			return notFoundDAO();

		List<Deployment> _deploys = deploymentDAO.getAllDeployment();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("records", _deploys);
		map.put("totalCount", _deploys.size());
		ModelAndView objs = new ModelAndView("jsonView", map);
		return objs;
	}

	public DeploymentDAO getDeploymentDAO() {
		return deploymentDAO;
	}

	public void setDeploymentDAO(DeploymentDAO deploymentDAO) {
		this.deploymentDAO = deploymentDAO;
	}

	public void downloadResource(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		String _id = request.getParameter("id");
		String _rn = java.net.URLDecoder.decode(request.getParameter("rn"),
				"UTF-8");
		System.out.println(_rn);

		InputStream _in = repositoryService.getResourceAsStream(_id, _rn);
		ServletOutputStream _out = response.getOutputStream();
		response.setContentType("application/x-download");
		String filenamedisplay = URLEncoder.encode(_rn, "UTF-8");
		response.addHeader("Content-Disposition", "attachment;filename="
				+ filenamedisplay);

		byte[] bytes = new byte[1024];
		int read = 0;
		while ((read = _in.read(bytes)) != -1) {
			_out.write(bytes, 0, read);
		}
		_out.flush();
		_out.close();
	}

	public ModelAndView suspendedProcs(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		// System.out.println("enter");
		if (processDefinitionDAO == null)
			return notFoundDAO();

		// String _start = request.getParameter("start");
		String _page = request.getParameter("page");
		String _size = request.getParameter("limit");
		String _s = request.getParameter("sort");

		// ArrayList<String> _ss = jsonUtil.getSortDirForExtjsSorting(_s);
		// String _sort = null, _dir = null;
		// if (_ss != null) {
		// _sort = _ss.get(0);
		// _dir = _ss.get(1);
		// }
		//
		// if (_dir == null)
		// _dir = "ASC";
		// if (_sort == null)
		// _sort = "id";

		int _idx = 0;
		Map<String, Object> map = new HashMap<String, Object>(3);
		String user = getCurrentUser(request);
		System.out.println(user);
		List<ProcessDefinition> _procs = processDefinitionDAO
				.getSuspendedProcs(null, Integer.parseInt(_size),
						Integer.parseInt(_page) - 1);

		map.put("success", true);
		map.put("records", _procs);
		map.put("totalCount", _procs.size());
		ModelAndView objs = new ModelAndView("jsonView", map);
		return objs;
	}

	public ModelAndView activeProcs(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		// System.out.println("enter");
		if (processDefinitionDAO == null)
			return notFoundDAO();

		// String _start = request.getParameter("start");
		String _page = request.getParameter("page");
		String _size = request.getParameter("limit");
		Map<String, Object> map = new HashMap<String, Object>(3);
		String user = getCurrentUser(request);
		System.out.println(user);
		List<ProcessDefinition> _procs = processDefinitionDAO.getActiveProcs(
				null, Integer.parseInt(_size), Integer.parseInt(_page) - 1);

		map.put("success", true);
		map.put("records", _procs);
		map.put("totalCount", _procs.size());
		ModelAndView objs = new ModelAndView("jsonView", map);
		return objs;
	}

	public ModelAndView activeSuspendProc(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		if (processDefinitionDAO == null)
			return notFoundDAO();

		String _id = request.getParameter("id");
		String _both = request.getParameter("activeBoth");
		String _time = request.getParameter("activeTime");
		String _time1 = request.getParameter("activeCustomTime");

		Map<String, Object> map = new HashMap<String, Object>(3);
		Date _date = null;
		try {
			if (_time.equals("1")) {
				SimpleDateFormat formatter = new SimpleDateFormat(
						"yyyy-MM-dd HH:mm:ss");
				_date = formatter.parse(_time1);
			} else
				_date = new Date();

			processDefinitionDAO.activateProcess(_id, _both == null ? false
					: (_both.equalsIgnoreCase("on") ? true : false), _date);
			map.put("success", true);
			map.put("msg", "成功");
		} catch (Exception ex) {
			ex.printStackTrace();
			map.put("success", false);
			map.put("msg", ex.getMessage());
		}
		ModelAndView objs = new ModelAndView("jsonView", map);
		return objs;

	}

	public ModelAndView suspendActiveProc(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		// System.out.println("enter");
		if (processDefinitionDAO == null)
			return notFoundDAO();

		String _id = request.getParameter("id");
		String _both = request.getParameter("suspendBoth");
		String _time = request.getParameter("suspendTime");
		String _time1 = request.getParameter("suspendCustomTime");
		Map<String, Object> map = new HashMap<String, Object>(3);
		if (_time == null || _time.length() == 0) {
			map.put("success", false);
			map.put("msg","Not specify suspend time type!");
			return new ModelAndView("jsonView", map);
		}
		
		System.out.println(_time1);
		System.out.println(_time);

		Date _date = null;
		try {
			if (_time.equals("1")) {
				System.out.println(_time1);
				SimpleDateFormat formatter = new SimpleDateFormat(
						"yyyy-MM-dd HH:mm:ss");
				_date = formatter.parse(_time1);
				System.out.println(_date);
			} else
				_date = new Date();

			processDefinitionDAO.suspendProcess(_id, _both == null ? false
					: (_both.equalsIgnoreCase("on") ? true : false), _date);
			map.put("success", true);
			map.put("msg", "成功");
		} catch (Exception ex) {
			map.put("success", false);
			map.put("msg", ex.getMessage());
		}
		
		ModelAndView objs = new ModelAndView("jsonView", map);
		return objs;
	}

	public ModelAndView hasSuspendJobs(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		// System.out.println("enter");
		if (processDefinitionDAO == null)
			return notFoundDAO();

		String _id = request.getParameter("id");
		Map<String, Object> map = new HashMap<String, Object>(3);
		try {
			boolean _jobs = processDefinitionDAO.hasSuspendJobs(_id);

			map.put("success", true);
			map.put("jobs", _jobs);
		} catch (Exception e) {
			map.put("success", false);
			map.put("msg", e.getMessage());
		}
		ModelAndView objs = new ModelAndView("jsonView", map);
		return objs;
	}

	public ModelAndView hasActiveJobs(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		// System.out.println("enter");
		if (processDefinitionDAO == null)
			return notFoundDAO();

		String _id = request.getParameter("id");
		Map<String, Object> map = new HashMap<String, Object>(3);
		try {
			boolean _jobs = processDefinitionDAO.hasActiveJobs(_id);

			map.put("success", true);
			map.put("jobs", _jobs);
		} catch (Exception e) {
			map.put("success", false);
			map.put("msg", e.getMessage());
		}
		ModelAndView objs = new ModelAndView("jsonView", map);
		return objs;
	}

	public ModelAndView deleteJob(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		if (jobDAO == null)
			return notFoundDAO();

		// start=0,page=1
		String _id = request.getParameter("id");

		Map<String, Object> map = new HashMap<String, Object>(3);
		try {
			jobDAO.deleteJob(_id);
			map.put("success", true);
			map.put("msg", "删除成功！");
		} catch (Exception e) {
			map.put("success", true);
			map.put("msg", e.getMessage());

		}
		ModelAndView objs = new ModelAndView("jsonView", map);
		return objs;
	}

	public ModelAndView executeJob(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		if (jobDAO == null)
			return notFoundDAO();

		// start=0,page=1
		String _id = request.getParameter("id");

		Map<String, Object> map = new HashMap<String, Object>(3);
		try {
			jobDAO.executeJob(_id);
			map.put("success", true);
			map.put("msg", "执行任务成功！");
		} catch (Exception e) {
			map.put("success", true);
			map.put("msg", e.getMessage());

		}
		ModelAndView objs = new ModelAndView("jsonView", map);
		return objs;
	}

	public ModelAndView getJobs(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		// System.out.println("enter");
		if (jobDAO == null)
			return notFoundDAO();

		// start=0,page=1
		String _page = request.getParameter("page");
		String _size = request.getParameter("limit");// 25
		Map<String, Object> map = new HashMap<String, Object>(3);
		List<Job> _procs = jobDAO.listJobs(Integer.parseInt(_page) - 1,
				Integer.parseInt(_size));

		map.put("success", true);
		map.put("records", _procs);
		map.put("totalCount", _procs.size());
		ModelAndView objs = new ModelAndView("jsonView", map);
		return objs;
	}

	public ModelAndView showJob(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		if (jobDAO == null)
			return notFoundDAO();
		System.out.println(i18nManager.getLocale());
		String _id = request.getParameter("id");
		try {
			HashMap _map = jobDAO.getJob(_id);
			_map.put("success", true);
			return new ModelAndView("jsonView", _map);
		} catch (Exception e) {
			e.printStackTrace();
			Map<String, Object> map = new HashMap<String, Object>(3);
			map.put("success", false);
			map.put("msg", e.getMessage());
			return new ModelAndView("jsonView", map);
		}

	}

	public ModelAndView getRunningDefinitions(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		if (processInstanceDAO == null)
			return notFoundDAO();

		String _start = request.getParameter("start");
		String _limit = request.getParameter("limit");
		int start = Integer.parseInt(_start);
		int limit = Integer.parseInt(_limit);

		Map<String, Object> _map = new HashMap<String, Object>(3);
		try {
			List<Map<String, Object>> _insts = processInstanceDAO
					.getRunningDefinitions(start, start + limit);
			_map.put("records", _insts);
			_map.put("totalCount", _insts.size());
			_map.put("success", true);
			return new ModelAndView("jsonView", _map);
		} catch (Exception e) {
			e.printStackTrace();
			Map<String, Object> map = new HashMap<String, Object>(3);
			map.put("success", false);
			map.put("msg", e.getMessage());
			return new ModelAndView("jsonView", map);
		}
	}

	public ModelAndView getFinishedDefinitions(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		if (processInstanceDAO == null)
			return notFoundDAO();

		String _start = request.getParameter("start");
		String _limit = request.getParameter("limit");
		int start = Integer.parseInt(_start);
		int limit = Integer.parseInt(_limit);

		Map<String, Object> _map = new HashMap<String, Object>(3);
		try {
			List<Map<String, Object>> _insts = processInstanceDAO
					.getFinishedDefinitions(start, start + limit);
			_map.put("records", _insts);
			_map.put("totalCount", _insts.size());
			_map.put("success", true);
			return new ModelAndView("jsonView", _map);
		} catch (Exception e) {
			e.printStackTrace();
			Map<String, Object> map = new HashMap<String, Object>(3);
			map.put("success", false);
			map.put("msg", e.getMessage());
			return new ModelAndView("jsonView", map);
		}
	}

	public ModelAndView getInstancesByProcId(HttpServletRequest request,
			HttpServletResponse response) {

		if (processInstanceDAO == null)
			return notFoundDAO();
		// Map<String, Object> _map = new HashMap<String, Object>(3);
		String _pid = request.getParameter("pid");
		String _type = request.getParameter("type");
		String _start = request.getParameter("start");
		String _limit = request.getParameter("limit");
		int start = Integer.parseInt(_start);
		int limit = Integer.parseInt(_limit);

		try {
			Map<String, Object> _insts = processInstanceDAO
					.getInstancesByProcId(_type, _pid, start, start + limit);
			return new ModelAndView("jsonView", _insts);
		} catch (Exception e) {
			e.printStackTrace();
			Map<String, Object> map = new HashMap<String, Object>(3);
			map.put("success", false);
			map.put("msg", e.getMessage());
			return new ModelAndView("jsonView", map);
		}
	}

	public ModelAndView getTasksByInst(HttpServletRequest request,
			HttpServletResponse response) {
		if (processInstanceDAO == null)
			return notFoundDAO();

		String _id = request.getParameter("id");
		try {
			Map<String, Object> _insts = processInstanceDAO.getTasksByInst(_id);
			return new ModelAndView("jsonView", _insts);
		} catch (Exception e) {
			e.printStackTrace();
			Map<String, Object> map = new HashMap<String, Object>(3);
			map.put("success", false);
			map.put("msg", e.getMessage());
			return new ModelAndView("jsonView", map);
		}
	}

	private ModelAndView getHVarsByInst(HttpServletRequest request,
			HttpServletResponse response) {
		if (processInstanceDAO == null)
			return notFoundDAO();

		String _id = request.getParameter("id");
		try {
			Map<String, Object> _insts = processInstanceDAO.getHVarsByInst(_id);
			return new ModelAndView("jsonView", _insts);
		} catch (Exception e) {
			e.printStackTrace();
			Map<String, Object> map = new HashMap<String, Object>(3);
			map.put("success", false);
			map.put("msg", e.getMessage());
			return new ModelAndView("jsonView", map);
		}
	}

	public ModelAndView getVarsByInst(HttpServletRequest request,
			HttpServletResponse response) {
		if (processInstanceDAO == null)
			return notFoundDAO();

		String _id = request.getParameter("id");
		String _type = request.getParameter("ty");
		if (_type == null) {
			Map<String, Object> map = new HashMap<String, Object>(3);
			map.put("success", false);
			map.put("msg", "type参数为空，无法确定实例的类型！");
			return new ModelAndView("jsonView", map);
		}

		if (_type.equals("0"))
			return getRVarsByInst(request, response);
		else
			return getHVarsByInst(request, response);
	}

	private ModelAndView getRVarsByInst(HttpServletRequest request,
			HttpServletResponse response) {
		if (processInstanceDAO == null)
			return notFoundDAO();

		String _id = request.getParameter("id");
		try {
			Map<String, Object> _insts = processInstanceDAO.getVarsByInst(_id);
			return new ModelAndView("jsonView", _insts);
		} catch (Exception e) {
			e.printStackTrace();
			Map<String, Object> map = new HashMap<String, Object>(3);
			map.put("success", false);
			map.put("msg", e.getMessage());
			return new ModelAndView("jsonView", map);
		}
	}
}
