package com.sinoearth.bpm.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.activiti.engine.identity.User;
import org.activiti.engine.repository.Deployment;
import org.springframework.web.servlet.ModelAndView;

import com.sinoearth.bpm.dao.ActIdUserDAO;
import com.sinoearth.bpm.dao.DeploymentDAO;
import com.sinoearth.bpm.dao.IdentityDAO;
import com.sinoearth.bpm.domain.ActIdUser;

public class CopyOfAdminAction extends ParentAction {

	private IdentityDAO identityDAO;
	private DeploymentDAO deploymentDAO;

	public IdentityDAO getIdentityDAO() {
		return identityDAO;
	}

	public void setIdentityDAO(IdentityDAO identityDAO) {
		this.identityDAO = identityDAO;
	}

	public ModelAndView getDeployment(HttpServletRequest request,
			HttpServletResponse response) {

		if (deploymentDAO == null)
			return notFoundDAO();
		String _id = request.getParameter("id");
		Map<String, Object> map = deploymentDAO.getDeployment(_id);
		ModelAndView objs = new ModelAndView("jsonView", map);
		return objs;
	}

	public ModelAndView getDeployments(HttpServletRequest request,
			HttpServletResponse response) {

		if (deploymentDAO == null)
			return notFoundDAO();

		List<Deployment> _deploys = deploymentDAO.getAllDeployment();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("records", _deploys);
		map.put("totalCount", _deploys.size());
		ModelAndView objs = new ModelAndView("jsonView", map);
		return objs;
	}

	public DeploymentDAO getDeploymentDAO() {
		return deploymentDAO;
	}

	public void setDeploymentDAO(DeploymentDAO deploymentDAO) {
		this.deploymentDAO = deploymentDAO;
	}

	public ModelAndView getUsers(HttpServletRequest request,
			HttpServletResponse response) {

		// System.out.println("getUsersAction");

		if (identityDAO == null)
			return notFoundDAO();

		Map<String, Object> _users = identityDAO.getAllUsers();
		ModelAndView objs = new ModelAndView("jsonView", _users);
		return objs;
	}

	public ModelAndView getUsersByGroupId(HttpServletRequest request,
			HttpServletResponse response) {

		System.out.println("getUsersByGroupIdAction");
		System.out.println(request.getParameter("groupId"));

		if (identityDAO == null)
			return notFoundDAO();
		String groupId = request.getParameter("groupId");
		Map<String, Object> _users = identityDAO.getUsersByGroupId(groupId);
		ModelAndView objs = new ModelAndView("jsonView", _users);
		return objs;
	}

	public ModelAndView newUser(HttpServletRequest request,
			HttpServletResponse response) {
		if (identityDAO == null)
			return notFoundDAO();

		String userID = request.getParameter("userID");
		String firstName = request.getParameter("firstName");
		String lastName = request.getParameter("lastName");
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		String confirmPassword = request.getParameter("confirmPassword");
		String[] groupIDs = request.getParameterValues("groupID");

		// System.out.println(userID+"|"+firstName+"|"+lastName+"|"+email+"|"+password+"|"+confirmPassword+"|"+groupIDs[0]+"|"+groupIDs[1]);

		Map<String, Object> map = new HashMap<String, Object>();
		if (password.equals(confirmPassword)) {
			if (identityDAO.createUser(userID, firstName, lastName, email,
					password).equals("1")) {
				if (groupIDs.length != 0) {
					Map<String, Boolean> results = identityDAO.addGroupsToUser(
							userID, groupIDs);
					Boolean r = true;
					for (Boolean result : results.values())
						if (!result)
							r = false;

					if (r) {
						String msg = "创建新用户: " + email + " 并添加进组:";
						String msg1 = "";
						for (String groupID : groupIDs)
							msg1 = msg1 + groupID + " ";
						msg = msg + msg1;
						map.put("success", true);
						map.put("msg", msg);
					} else {
						String msg = "创建新用户: " + email + " 成功，添加进组失败";
						map.put("success", false);
						map.put("msg", msg);
					}
				} else {
					map.put("success", true);
					map.put("msg", "创建新用户: " + email + " 成功");
				}
			} else {
				map.put("success", false);
				map.put("msg", "创建新用户: " + email + " 失败");
			}
		} else {
			map.put("success", false);
			map.put("msg", "两次输入密码不一致");
		}
		ModelAndView objs = new ModelAndView("jsonView", map);
		return objs;
		// return null;
	}

	public ModelAndView deleteUser(HttpServletRequest request,
			HttpServletResponse response) {
		if (identityDAO == null)
			return notFoundDAO();
		String userID = request.getParameter("userID");
		String email = request.getParameter("email");
		System.out.println("delete:" + userID);
		Map<String, Object> map = new HashMap<String, Object>();
		if (identityDAO.deleteUserById(userID).equals("1")) {
			map.put("success", true);
			map.put("msg", "删除用户: " + email + " 成功");
			ModelAndView objs = new ModelAndView("jsonView", map);
			return objs;
		} else {
			map.put("success", false);
			map.put("msg", "删除用户: " + email + " 失败");
			ModelAndView objs = new ModelAndView("jsonView", map);
			return objs;
		}
	}

	public ModelAndView updateUser(HttpServletRequest request,
			HttpServletResponse response) {
		if (identityDAO == null)
			return notFoundDAO();

		String userID = request.getParameter("userID");
		String firstName = request.getParameter("firstName");
		String lastName = request.getParameter("lastName");
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		String confirmPassword = request.getParameter("confirmPassword");

		System.out.println("Update: " + userID + "|" + firstName + "|"
				+ lastName + "|" + email + "|" + password + "|"
				+ confirmPassword);

		Map<String, Object> map = new HashMap<String, Object>();
		if (password.equals(confirmPassword)) {
			if (identityDAO.updateUser(userID, firstName, lastName, email,
					password).equals("1")) {
				map.put("success", true);
				map.put("msg", "更新用户: " + email + " 成功");
				ModelAndView objs = new ModelAndView("jsonView", map);
				return objs;
			} else {
				map.put("success", false);
				map.put("msg", "更新用户: " + email + " 失败");
				ModelAndView objs = new ModelAndView("jsonView", map);
				return objs;
			}
		} else {
			map.put("success", false);
			map.put("msg", "两次输入密码不一致");
			ModelAndView objs = new ModelAndView("jsonView", map);
			return objs;
		}
	}

	public ModelAndView getGroups(HttpServletRequest request,
			HttpServletResponse response) {

		System.out.println("getGroupsAction");

		if (identityDAO == null)
			return notFoundDAO();

		Map<String, Object> _users = identityDAO.getAllGroups();
		ModelAndView objs = new ModelAndView("jsonView", _users);
		return objs;
	}

	public ModelAndView getUserNoGroups(HttpServletRequest request,
			HttpServletResponse response) {

		System.out.println("getUserNoGroupsAction");

		if (identityDAO == null)
			return notFoundDAO();

		String userId = request.getParameter("userId");

		Map<String, Object> _users = identityDAO.getUserNoGroups(userId);
		ModelAndView objs = new ModelAndView("jsonView", _users);
		return objs;
	}

	public ModelAndView getGroupNoUsers(HttpServletRequest request,
			HttpServletResponse response) {

		System.out.println("getGroupNoUsers");

		if (identityDAO == null)
			return notFoundDAO();

		String groupId = request.getParameter("groupId");

		Map<String, Object> _users = identityDAO.getGroupNoUsers(groupId);
		ModelAndView objs = new ModelAndView("jsonView", _users);
		return objs;
	}

	public ModelAndView getGroupsByUserId(HttpServletRequest request,
			HttpServletResponse response) {

		System.out.println("getGroupsByUserIdAction");

		if (identityDAO == null)
			return notFoundDAO();

		String userId = request.getParameter("userId");
		Map<String, Object> _groups = identityDAO.getGroupsByUserId(userId);
		ModelAndView objs = new ModelAndView("jsonView", _groups);
		return objs;
	}

	public ModelAndView newGroup(HttpServletRequest request,
			HttpServletResponse response) {
		if (identityDAO == null)
			return notFoundDAO();

		String id = request.getParameter("id");
		String name = request.getParameter("name");
		String type = request.getParameter("type");

		System.out.println(id + "|" + name + "|" + type);

		Map<String, Object> map = new HashMap<String, Object>();

		if (identityDAO.createGroup(id, name, type).equals("1")) {
			map.put("success", true);
			map.put("msg", "创建新用户组: " + name + " 成功");
			ModelAndView objs = new ModelAndView("jsonView", map);
			return objs;
		} else {
			map.put("success", false);
			map.put("msg", "创建新用户组: " + name + " 失败");
			ModelAndView objs = new ModelAndView("jsonView", map);
			return objs;
		}
	}

	public ModelAndView deleteGroup(HttpServletRequest request,
			HttpServletResponse response) {
		if (identityDAO == null)
			return notFoundDAO();
		String id = request.getParameter("id");
		String name = request.getParameter("name");
		System.out.println("delete:" + id);
		Map<String, Object> map = new HashMap<String, Object>();
		if (identityDAO.deleteGroupById(id).equals("1")) {
			map.put("success", true);
			map.put("msg", "删除用户组: " + name + " 成功");
			ModelAndView objs = new ModelAndView("jsonView", map);
			return objs;
		} else {
			map.put("success", false);
			map.put("msg", "删除用户组: " + name + " 失败");
			ModelAndView objs = new ModelAndView("jsonView", map);
			return objs;
		}
	}

	public ModelAndView updateGroup(HttpServletRequest request,
			HttpServletResponse response) {
		if (identityDAO == null)
			return notFoundDAO();

		String id = request.getParameter("id");
		String name = request.getParameter("name");
		String type = request.getParameter("type");

		System.out.println("Update: " + id + "|" + name + "|" + type);

		Map<String, Object> map = new HashMap<String, Object>();
		if (identityDAO.updateGroup(id, name, type).equals("1")) {
			map.put("success", true);
			map.put("msg", "更新用户组: " + name + " 成功");
			ModelAndView objs = new ModelAndView("jsonView", map);
			return objs;
		} else {
			map.put("success", false);
			map.put("msg", "更新用户组: " + type + " 失败");
			ModelAndView objs = new ModelAndView("jsonView", map);
			return objs;
		}
	}

	public ModelAndView addGroupsToUser(HttpServletRequest request,
			HttpServletResponse response) {

		String userId = request.getParameter("userId");
		String[] groupIds = request.getParameterValues("groupIds");
		System.out.println("userId:" + userId);
		for (String groupId : groupIds)
			System.out.println("groupId:" + groupId);

		Map<String, Boolean> results = identityDAO.addGroupsToUser(userId,
				groupIds);
		Boolean r = true;
		for (Boolean result : results.values())
			if (!result)
				r = false;

		Map<String, Object> map = new HashMap<String, Object>();
		if (r) {
			String msg = "用户: " + userId + " 添加进组:";
			String msg1 = "";
			for (String groupID : results.keySet())
				msg1 = msg1 + groupID + " ";
			msg = msg + msg1 + " 成功";
			map.put("success", true);
			map.put("msg", msg);
		} else {
			String msg = "用户: " + userId + " 添加进组失败";
			map.put("success", false);
			map.put("msg", msg);
		}

		return new ModelAndView("jsonView", map);
	}

	public ModelAndView removeGroup(HttpServletRequest request,
			HttpServletResponse response) {
		if (identityDAO == null)
			return notFoundDAO();
		String userId = request.getParameter("userId");
		String groupId = request.getParameter("groupId");
		String groupName = request.getParameter("name");
		System.out
				.println("Remove group: " + groupId + " from user: " + userId);
		Map<String, Object> map = new HashMap<String, Object>();
		if (identityDAO.removeGroup(userId, groupId).equals("1")) {
			map.put("success", true);
			map.put("msg", "移除组: " + groupName + " 成功");
			ModelAndView objs = new ModelAndView("jsonView", map);
			return objs;
		} else {
			map.put("success", false);
			map.put("msg", "移除组: " + groupName + " 失败");
			ModelAndView objs = new ModelAndView("jsonView", map);
			return objs;
		}
	}

	// ////////////////////////////
	public ModelAndView addUsersToGroup(HttpServletRequest request,
			HttpServletResponse response) {

		String groupId = request.getParameter("groupId");
		String[] userIds = request.getParameterValues("userIds");

		Map<String, Boolean> results = identityDAO.addUsersToGroup(userIds,
				groupId);
		Boolean r = true;
		for (Boolean result : results.values())
			if (!result)
				r = false;

		Map<String, Object> map = new HashMap<String, Object>();
		if (r) {
			String msg = "组: " + groupId + " 添加用户:";
			String msg1 = "";
			for (String userId : results.keySet())
				msg1 = msg1 + userId + " ";
			msg = msg + msg1 + " 成功";
			map.put("success", true);
			map.put("msg", msg);
		} else {
			String msg = "组: " + groupId + " 添加用户:";
			map.put("success", false);
			map.put("msg", msg);
		}

		return new ModelAndView("jsonView", map);
	}

	public ModelAndView removeUser(HttpServletRequest request,
			HttpServletResponse response) {
		if (identityDAO == null)
			return notFoundDAO();
		String userId = request.getParameter("userId");
		String groupId = request.getParameter("groupId");
		String email = request.getParameter("email");
		System.out
				.println("Remove group: " + groupId + " from user: " + userId);
		Map<String, Object> map = new HashMap<String, Object>();
		if (identityDAO.removeUser(userId, groupId).equals("1")) {
			map.put("success", true);
			map.put("msg", "移除用户: " + email + " 成功");
			ModelAndView objs = new ModelAndView("jsonView", map);
			return objs;
		} else {
			map.put("success", false);
			map.put("msg", "移除用户: " + email + " 失败");
			ModelAndView objs = new ModelAndView("jsonView", map);
			return objs;
		}
	}

}
