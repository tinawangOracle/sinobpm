//authoer: tina.wang
package com.sinoearth.bpm.action;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.DeploymentBuilder;

import java.util.zip.ZipInputStream;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamReader;

import oracle.sgt.util.jsonUtil;

import org.activiti.bpmn.converter.BpmnXMLConverter;
import org.activiti.bpmn.model.BpmnModel;
import org.activiti.bpmn.model.GraphicInfo;
import org.activiti.editor.language.json.converter.BpmnJsonConverter;
import org.activiti.engine.FormService;
import org.activiti.engine.HistoryService;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.form.FormProperty;
import org.activiti.engine.form.FormType;
import org.activiti.engine.form.StartFormData;
import org.activiti.engine.form.TaskFormData;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.history.HistoricTaskInstance;
import org.activiti.engine.history.HistoricTaskInstanceQuery;
import org.activiti.engine.impl.RepositoryServiceImpl;
import org.activiti.engine.impl.bpmn.diagram.ProcessDiagramGenerator;
import org.activiti.engine.impl.identity.Authentication;
import org.activiti.engine.impl.persistence.entity.ProcessDefinitionEntity;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.Model;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Attachment;
import org.activiti.engine.task.IdentityLink;
import org.activiti.engine.task.Task;
import org.activiti.explorer.ComponentFactories;
import org.activiti.explorer.ui.management.deployment.DeploymentFilter;
import org.activiti.explorer.ui.management.deployment.DeploymentFilterFactory;
import org.apache.commons.lang3.StringUtils;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ObjectNode;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.sinoearth.bpm.I18nManager;
import com.sinoearth.bpm.Messages;
import com.sinoearth.bpm.constants.ModelDataJsonConstants;
import com.sinoearth.bpm.dao.ActReModelDAO;
import com.sinoearth.bpm.dao.ProcessDefinitionDAO;
import com.sinoearth.bpm.dao.ProcessInstanceDAO;
import com.sinoearth.bpm.dao.TaskDAO;
import com.sinoearth.util.time.HumanTime;

public class WorkflowAction extends ParentAction implements
		ModelDataJsonConstants, HandlerExceptionResolver {

	private ActReModelDAO actReModelDAO;
	private ProcessInstanceDAO processInstanceDAO;
	private TaskDAO taskDAO;

	public TaskDAO getTaskDAO() {
		return taskDAO;
	}

	public void setTaskDAO(TaskDAO taskDAO) {
		this.taskDAO = taskDAO;
	}

	private ProcessEngine processEngine;
	private RepositoryService repositoryService;
	private FormService formService;
	private RuntimeService runtimeService;
	private HistoryService historyService;
	protected ComponentFactories componentFactories;

	public ComponentFactories getComponentFactories() {
		return componentFactories;
	}

	public void setComponentFactories(ComponentFactories componentFactories) {
		this.componentFactories = componentFactories;
	}

	public HistoryService getHistoryService() {
		return historyService;
	}

	public void setHistoryService(HistoryService historyService) {
		this.historyService = historyService;
	}

	private TaskService taskService;

	public TaskService getTaskService() {
		return taskService;
	}

	public void setTaskService(TaskService taskService) {
		this.taskService = taskService;
	}

	public ProcessInstanceDAO getProcessInstanceDAO() {
		return processInstanceDAO;
	}

	public void setProcessInstanceDAO(ProcessInstanceDAO processInstanceDAO) {
		this.processInstanceDAO = processInstanceDAO;
	}

	public RuntimeService getRuntimeService() {
		return runtimeService;
	}

	public void setRuntimeService(RuntimeService runtimeService) {
		this.runtimeService = runtimeService;
	}

	public FormService getFormService() {
		return formService;
	}

	public void setFormService(FormService formService) {
		this.formService = formService;
	}

	private I18nManager i18nManager;
	private HumanTime humanTime;
	private ResourceBundleMessageSource messageSource;

	public ResourceBundleMessageSource getMessageSource() {
		return messageSource;
	}

	public void setMessageSource(ResourceBundleMessageSource messageSource) {
		this.messageSource = messageSource;
	}

	public HumanTime getHumanTime() {
		return humanTime;
	}

	public void setHumanTime(HumanTime humanTime) {
		this.humanTime = humanTime;
	}

	public I18nManager getI18nManager() {
		return i18nManager;
	}

	public void setI18nManager(I18nManager i18nManager) {
		this.i18nManager = i18nManager;
	}

	public RepositoryService getRepositoryService() {
		return repositoryService;
	}

	public void setRepositoryService(RepositoryService repositoryService) {
		this.repositoryService = repositoryService;
	}

	private ProcessDefinitionDAO processDefinitionDAO;

	public ProcessDefinitionDAO getProcessDefinitionDAO() {
		return processDefinitionDAO;
	}

	public void setProcessDefinitionDAO(
			ProcessDefinitionDAO processDefinitionDAO) {
		this.processDefinitionDAO = processDefinitionDAO;
	}

	public ProcessEngine getProcessEngine() {
		return processEngine;
	}

	public void setProcessEngine(ProcessEngine processEngine) {
		this.processEngine = processEngine;
	}

	public ActReModelDAO getActReModelDAO() {
		return actReModelDAO;
	}

	public void setActReModelDAO(ActReModelDAO actReModelDAO) {
		this.actReModelDAO = actReModelDAO;
	}

	protected void deployModelerModel(RepositoryService repositoryService,
			final ObjectNode modelNode, String name) throws Exception {
		BpmnModel model = new BpmnJsonConverter().convertToBpmnModel(modelNode);
		// Map<String, List<ExtensionAttribute>>
		// _m=model.getDefinitionsAttributes();
		// Set<String> _keys=_m.keySet();
		// for (String _key:_keys)
		// System.out.println(_key);
		String processName = name + ".bpmn20.xml";
		Deployment deployment;

		deployment = repositoryService.createDeployment().name(name)
				.addBpmnModel(processName, model).deploy();

	}

	public ModelAndView deployModel(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		// System.out.println("enter");
		if (actReModelDAO == null || processEngine == null) {
			return notFoundDAO();
		}
		String _id = request.getParameter("id");
		String _name = request.getParameter("name");
		System.out.println(_id);
		RepositoryService _repos = processEngine.getRepositoryService();
		Map<String, Object> map = new HashMap<String, Object>();
		try {

			ObjectMapper objectMapper = new ObjectMapper();
			final ObjectNode modelNode = (ObjectNode) objectMapper
					.readTree(_repos.getModelEditorSource(_id));
			deployModelerModel(_repos, modelNode, _name);

			map.put("success", false);
			map.put("msg", "部署成功！");
		} catch (Exception e) {
			map.put("success", false);
			map.put("msg", e.getMessage());
		}
		return new ModelAndView("jsonView", map);
	}

	public ModelAndView newModel(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String _newname = request.getParameter("name");
		String _desc = request.getParameter("desc");
		Map<String, Object> map = new HashMap<String, Object>();
		if (_newname == null || _newname.trim().length() == 0) {
			map.put("success", false);
			map.put("msg", "name不能为空！");
			ModelAndView objs = new ModelAndView("jsonView", map);
			return objs;
		}
		try {
			RepositoryService repositoryService = processEngine
					.getRepositoryService();
			ObjectMapper objectMapper = new ObjectMapper();
			ObjectNode editorNode = objectMapper.createObjectNode();
			editorNode.put("id", "canvas");
			editorNode.put("resourceId", "canvas");
			ObjectNode stencilSetNode = objectMapper.createObjectNode();
			stencilSetNode.put("namespace",
					"http://b3mn.org/stencilset/bpmn2.0#");
			editorNode.put("stencilset", stencilSetNode);
			Model modelData = repositoryService.newModel();

			ObjectNode modelObjectNode = objectMapper.createObjectNode();
			modelObjectNode.put(MODEL_NAME, _newname);
			modelObjectNode.put(MODEL_REVISION, 1);
			if (_desc == null)
				_desc = "";
			modelObjectNode.put(MODEL_DESCRIPTION, _desc);
			modelData.setMetaInfo(modelObjectNode.toString());
			modelData.setName(_newname);

			repositoryService.saveModel(modelData);

			repositoryService.addModelEditorSource(modelData.getId(),
					editorNode.toString().getBytes("utf-8"));
		} catch (UnsupportedEncodingException e) {
			map.put("success", false);
			map.put("msg", e.getMessage());
			ModelAndView objs = new ModelAndView("jsonView", map);
			return objs;
		}
		map.put("success", true);
		map.put("msg", "Model创建成功。关闭后可以选择该model进行编辑！");
		ModelAndView objs = new ModelAndView("jsonView", map);
		return objs;

	}

	public void showProcessDefintionPng(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String _sid = request.getParameter("sid");
		ProcessDefinition processDefinition = null;

		if (_sid != null) {
			ProcessInstance _inst = processInstanceDAO.getProcessInstance(_sid);
			processDefinition = processInstanceDAO.getProcessDefinition(_inst
					.getProcessDefinitionId());
		} else {
			String _id = request.getParameter("id");
			if (_id != null)
				processDefinition = repositoryService.getProcessDefinition(_id);
		}
		String _type = request.getParameter("type");
		if (_type == null)
			_type = "1";

		if (processDefinition != null
				&& processDefinition.getDiagramResourceName() != null) {
			InputStream definitionImageStream = null;
			if (_type.equals("1"))
				definitionImageStream = repositoryService.getResourceAsStream(
						processDefinition.getDeploymentId(),
						processDefinition.getDiagramResourceName());
			else {
				BpmnModel bpmnModel = repositoryService
						.getBpmnModel(processDefinition.getDeploymentId());
				definitionImageStream = ProcessDiagramGenerator
						.generateDiagram(bpmnModel, "png",
								runtimeService.getActiveActivityIds(_sid));

			}
			OutputStream out = response.getOutputStream();
			// DatatypeConverter _c;
			byte[] b = new byte[4096];
			int n = 0;
			while ((n = definitionImageStream.read(b)) != -1) {
				out.write(b, 0, n); // 将数据已流的形式写出
			}
			out.flush();
			out.close();
		}
	}

	public void showProcInstPng(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String _sid = request.getParameter("sid");
		ProcessDefinition processDefinition = null;
		String _type = request.getParameter("type");
		if (_type == null)
			_type = "1";

		if (_sid != null) {
			if (_type.equals("0")) {
				ProcessInstance _inst = processInstanceDAO
						.getProcessInstance(_sid);
				processDefinition = processInstanceDAO
						.getProcessDefinition(_inst.getProcessDefinitionId());
			} else {
				HistoricProcessInstance _inst = processInstanceDAO
						.getHistoricProcessInstance(_sid);
				processDefinition = processInstanceDAO
						.getProcessDefinition(_inst.getProcessDefinitionId());
			}
		} else {
			String _id = request.getParameter("id");
			if (_id != null)
				processDefinition = repositoryService.getProcessDefinition(_id);
		}

		ProcessDefinitionEntity processDefinitionEntity = (ProcessDefinitionEntity) ((RepositoryServiceImpl) repositoryService)
				.getDeployedProcessDefinition(processDefinition.getId());

		if (processDefinition != null
				&& processDefinition.getDiagramResourceName() != null) {
			InputStream definitionImageStream = null;
			if (processDefinitionEntity != null
					&& processDefinitionEntity.isGraphicalNotationDefined()) {
				if (_type.equals("1")) {

					definitionImageStream = repositoryService
							.getResourceAsStream(
									processDefinition.getDeploymentId(),
									processDefinition.getDiagramResourceName());

				} else {
					BpmnModel bpmnModel = repositoryService
							.getBpmnModel(processDefinition.getId());
					definitionImageStream = ProcessDiagramGenerator
							.generateDiagram(bpmnModel, "png",
									runtimeService.getActiveActivityIds(_sid));

				}
			}
			if (definitionImageStream != null) {
				OutputStream out = response.getOutputStream();
				// DatatypeConverter _c;
				byte[] b = new byte[4096];
				int n = 0;
				while ((n = definitionImageStream.read(b)) != -1) {
					out.write(b, 0, n); // 将数据已流的形式写出
				}
				out.flush();
				out.close();
			}
		}
	}

	public Map<String, Object> getProcessDefintionPng(
			HistoricProcessInstance _inst, ProcessDefinition _pdef)
			throws Exception {
		ProcessDefinitionEntity processDefinitionEntity = (ProcessDefinitionEntity) ((RepositoryServiceImpl) repositoryService)
				.getDeployedProcessDefinition(_pdef.getId());
		boolean didDrawImage = false;
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("pname",
				(_pdef.getName() == null) ? _pdef.getKey() : _pdef.getName());
		map.put("pver", _pdef.getVersion());

		if (processDefinitionEntity != null) {
			List<String> _url = initImage(_pdef, null);
			if (_url != null) {
				didDrawImage = true;
				map.put("success", "0");
				map.put("url", _url.get(0));
				map.put("maxX", _url.get(1));
				map.put("maxY", _url.get(2));
				return map;
			}

		}

		if (didDrawImage == false)
			if (processDefinitionEntity.isGraphicalNotationDefined()) {
				map.put("success", "1");
				map.put("msg", "try");
				return map;
			}

		map.put("success", "2");
		map.put("msg", "no result");
		return map;
	}

	public Map<String, Object> getProcInstPng(ProcessInstance _inst,
			ProcessDefinition _pdef) throws Exception {
		ProcessDefinitionEntity processDefinitionEntity = (ProcessDefinitionEntity) ((RepositoryServiceImpl) repositoryService)
				.getDeployedProcessDefinition(_pdef.getId());
		boolean didDrawImage = false;
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("pname",
				(_pdef.getName() == null) ? _pdef.getKey() : _pdef.getName());
		map.put("pver", _pdef.getVersion());

		if (processDefinitionEntity != null) {
			List<String> _url = initImage(_pdef, _inst.getId());
			if (_url != null) {
				didDrawImage = true;
				map.put("success", "0");
				map.put("url", _url.get(0));
				map.put("maxX", _url.get(1));
				map.put("maxY", _url.get(2));
				return map;
			}

		}

		if (didDrawImage == false)
			if (processDefinitionEntity.isGraphicalNotationDefined()) {
				map.put("success", "1");
				map.put("msg", "try");
				return map;
			}

		map.put("success", "2");
		map.put("msg", "no result");
		return map;
	}

	public ModelAndView getProcInst(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		if (processInstanceDAO == null)
			return notFoundDAO();

		String _id = request.getParameter("id");
		String _type = "0";

		String _t = request.getParameter("type");
		if (_t != null)
			_type = _t;

		Map<String, Object> map = null;

		if (_type.equals("0")) {
			ProcessInstance _inst = processInstanceDAO.getProcessInstance(_id);
			if (_inst == null) {
				return null;
			}

			ProcessDefinition _pdef = processInstanceDAO
					.getProcessDefinition(_inst.getProcessDefinitionId());
			if (_pdef == null) {
				return null;
			}

			map = getProcInstPng(_inst, _pdef);

		} else {
			HistoricProcessInstance _inst = processInstanceDAO
					.getHistoricProcessInstance(_id);
			if (_inst == null) {
				return null;
			}

			ProcessDefinition _pdef = processInstanceDAO
					.getProcessDefinition(_inst.getProcessDefinitionId());
			if (_pdef == null) {
				return null;
			}

			map = getProcessDefintionPng(_inst, _pdef);

		}

		if (map == null)
			map = new HashMap<String, Object>();

		// Fetch all tasks
		List<HistoricTaskInstance> tasks = historyService
				.createHistoricTaskInstanceQuery().processInstanceId(_id)
				.orderByHistoricTaskInstanceEndTime().desc()
				.orderByHistoricTaskInstanceStartTime().desc().list();
		map.put("tasks", tasks);

		Map<String, Object> variables = new TreeMap<String, Object>(
				runtimeService.getVariables(_id));
		ArrayList<HashMap<String, String>> vars = new ArrayList<HashMap<String, String>>(
				variables.size());
		for (String _key : variables.keySet()) {
			HashMap<String, String> _item = new HashMap<String, String>(2);
			_item.put("name", _key);
			Object _v = variables.get(_key);
			_item.put("value", _v == null ? "" : _v.toString());
			vars.add(_item);
		}
		map.put("vars", vars);

		ModelAndView objs = new ModelAndView("jsonView", map);
		return objs;

	}

	public ModelAndView showProc(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		RepositoryService repositoryService = processEngine
				.getRepositoryService();
		String _id = request.getParameter("id");
		ProcessDefinition processDefinition = repositoryService
				.getProcessDefinition(_id);
		Deployment deployment = null;
		Map<String, Object> map = new HashMap<String, Object>();
		if (processDefinition != null) {
			deployment = repositoryService.createDeploymentQuery()
					.deploymentId(processDefinition.getDeploymentId())
					.singleResult();
			if (((ProcessDefinitionEntity) processDefinition)
					.isGraphicalNotationDefined() == false)
				map.put("editable", false);
			else
				map.put("editable", true);
			List<String> _url = initImage(processDefinition, null);
			if (_url != null) {
				map.put("success", "0");
				map.put("url", _url.get(0));
			} else if (processDefinition.getDiagramResourceName() == null) {
				map.put("success", "2");
				map.put("msg", "no result");
			} else {
				map.put("success", "1");
				map.put("msg", "try");
			}
			map.put("pname",
					(processDefinition.getName() == null) ? processDefinition
							.getKey() : processDefinition.getName());
			map.put("pver", processDefinition.getVersion());
			// System.out.println(humanTime.formatSimple(deployment
			// .getDeploymentTime()));
			// System.out.println(messageSource.getMessage(
			// Messages.TIME_UNIT_DAY,null,Locale.SIMPLIFIED_CHINESE));
			// ExplorerApp _app=ExplorerApp.get();
			// HumanTime _h=_app.getHumanTime();
			// String[] _d = _h.formatLocalized(null,
			// deployment.getDeploymentTime(), true);
			// System.out.println(_d[0]);
			map.put("ptime", deployment.getDeploymentTime());
			ModelAndView objs = new ModelAndView("jsonView", map);
			return objs;
		} else {
			map.put("success", "2");
			map.put("msg", "no result");
			return new ModelAndView("jsonView", map);
		}

	}

	protected List<String> initImage(ProcessDefinition processDefinition,
			String instId) {
		// boolean didDrawImage = false;

		// ExplorerApp.get().isUseJavascriptDiagram()
		boolean useJS = true;
		if (useJS) {
			try {
				final InputStream definitionStream = repositoryService
						.getResourceAsStream(
								processDefinition.getDeploymentId(),
								processDefinition.getResourceName());
				XMLInputFactory xif = XMLInputFactory.newInstance();
				XMLStreamReader xtr = xif
						.createXMLStreamReader(definitionStream);
				BpmnModel bpmnModel = new BpmnXMLConverter()
						.convertToBpmnModel(xtr);

				if (bpmnModel.getFlowLocationMap().size() > 0) {
					ArrayList<String> _ret = new ArrayList<String>(3);
					String _url = null;
					if (instId == null) {
						_url = "diagram-viewer/index.html?processDefinitionId="
								+ processDefinition.getId();
						_ret.add(_url);
					} else {
						// url = new URL("http", "localhost", 8888,
						// "sinobpm/diagram-viewer/index.html?processDefinitionId="
						// + processDefinition.getId()
						// + "&processInstanceId=" + instId);
						_url = "diagram-viewer/index.html?processDefinitionId="
								+ processDefinition.getId()
								+ "&processInstanceId=" + instId;
						_ret.add(_url);

					}
					int maxX = 0;
					int maxY = 0;
					for (String key : bpmnModel.getLocationMap().keySet()) {
						GraphicInfo graphicInfo = bpmnModel.getGraphicInfo(key);
						double elementX = graphicInfo.getX()
								+ graphicInfo.getWidth();
						if (maxX < elementX) {
							maxX = (int) elementX;
						}
						double elementY = graphicInfo.getY()
								+ graphicInfo.getHeight();
						if (maxY < elementY) {
							maxY = (int) elementY;
						}
					}
					System.out.println(maxY);
					_ret.add((maxX + 350) + "");
					_ret.add((maxY + 220) + "");
					return _ret;
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return null;
	}

	public ModelAndView deleteModel(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String _id = request.getParameter("id");
		RepositoryService repositoryService = processEngine
				.getRepositoryService();
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			repositoryService.deleteModel(_id);
		} catch (Exception e) {
			map.put("success", false);
			map.put("msg", e.getMessage());
			ModelAndView objs = new ModelAndView("jsonView", map);
			return objs;
		}
		map.put("success", true);
		map.put("msg", "Deletion Succeed.");
		ModelAndView objs = new ModelAndView("jsonView", map);
		return objs;
	}

	public ModelAndView copyModel(HttpServletRequest request,
			HttpServletResponse response) {
		String _id = request.getParameter("id");
		System.out.println(_id);
		String _newname = request.getParameter("name");
		String _desc = request.getParameter("desc");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			RepositoryService repositoryService = processEngine
					.getRepositoryService();
			Model newModelData = repositoryService.newModel();

			ObjectNode modelObjectNode = new ObjectMapper().createObjectNode();
			modelObjectNode.put(MODEL_NAME, _newname);
			if (_desc == null)
				_desc = "";

			modelObjectNode.put(MODEL_DESCRIPTION, _desc);
			newModelData.setMetaInfo(modelObjectNode.toString());
			newModelData.setName(_newname);
			repositoryService.saveModel(newModelData);

			repositoryService.addModelEditorSource(newModelData.getId(),
					repositoryService.getModelEditorSource(_id));
			repositoryService.addModelEditorSourceExtra(newModelData.getId(),
					repositoryService.getModelEditorSourceExtra(_id));
		} catch (Exception e) {
			map.put("success", false);
			map.put("msg", e.getMessage());
			ModelAndView objs = new ModelAndView("jsonView", map);
			return objs;
		}
		map.put("success", true);
		map.put("msg", "复制成功!");
		ModelAndView objs = new ModelAndView("jsonView", map);
		return objs;

	}

	public ModelAndView getModels(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		// System.out.println("enter");
		if (actReModelDAO == null)
			System.out.println("return null");
		else {
			String _start = request.getParameter("start");
			String _size = request.getParameter("limit");
			String _s = request.getParameter("sort");
			ArrayList<String> _ss = jsonUtil.getSortDirForExtjsSorting(_s);
			String _sort = null, _dir = null;
			if (_ss != null) {
				_sort = _ss.get(0);
				_dir = _ss.get(1);
			}

			if (_dir == null)
				_dir = "ASC";
			if (_sort == null)
				_sort = "id";

			int _idx = 0;
			Map<String, Object> map = null;
			if (_idx < 1)
				map = actReModelDAO.getAllModels(_dir, _sort,
						Integer.parseInt(_start), Integer.parseInt(_size));

			Object cnt = map.get("totalCount");
			if (cnt == null)
				map.put("totalCount", getTCnt(request));

			ModelAndView objs = new ModelAndView("jsonView", map);

			// -----------------------
			return objs;

		}
		return null;
	}

	public ModelAndView getProcs(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		// System.out.println("enter");
		if (processDefinitionDAO == null)
			return notFoundDAO();

		// String _start = request.getParameter("start");
		String _page = request.getParameter("page");
		String _size = request.getParameter("limit");
		String _s = request.getParameter("sort");
		ArrayList<String> _ss = jsonUtil.getSortDirForExtjsSorting(_s);
		String _sort = null, _dir = null;
		if (_ss != null) {
			_sort = _ss.get(0);
			_dir = _ss.get(1);
		}

		if (_dir == null)
			_dir = "ASC";
		if (_sort == null)
			_sort = "id";

		int _idx = 0;
		Map<String, Object> map = new HashMap<String, Object>(3);
		String user = getCurrentUser(request);
		System.out.println(user);
		List<ProcessDefinition> _procs = processDefinitionDAO
				.getAllBaseProcessDefinition(_dir, Integer.parseInt(_size),
						Integer.parseInt(_page) - 1);

		map.put("success", true);
		map.put("records", _procs);
		map.put("totalCount", _procs.size());
		ModelAndView objs = new ModelAndView("jsonView", map);
		return objs;
	}

	public ModelAndView startProcWithForm(HttpServletRequest request,
			HttpServletResponse response) {
		Map<String, Object> map = new HashMap<String, Object>();
		String pid = request.getParameter("pid");
		if (pid == null || pid.length() == 0) {
			return null;
		}
		Map _map1 = request.getParameterMap();
		Map<String, String> props = new HashMap<String, String>(_map1.size());
		Set _s = _map1.keySet();
		for (Object obj : _s) {
			String _key = obj.toString();
			if (_key.equals("pid"))
				continue;
			props.put(_key, ((String[]) _map1.get(_key))[0]);
		}

		try {
			Authentication.setAuthenticatedUserId(getCurrentUser(request));
			// props.put("initiatorVariableName", getCurrentUser(request));
			ProcessInstance _instance = formService.submitStartFormData(pid,
					props);
			if (_instance != null) {
				System.out.println(_instance.getId());
				map.put("success", true);
				map.put("msg", "start succeessfully");
			} else {
				map.put("success", false);
				map.put("msg", "start fail");
			}
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
			map.put("msg", e.getMessage());
		}
		return new ModelAndView("jsonView", map);
	}

	protected String getProcessDisplayName(ProcessDefinition processDefinition) {
		if (processDefinition.getName() != null) {
			return processDefinition.getName();
		} else {
			return processDefinition.getKey();
		}
	}

	// generate excel file
	public void getExcelFile(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String aid = request.getParameter("aid");
		InputStream bis = null;
		OutputStream bos = null;
		try {
			Attachment _a = taskService.getAttachment("aid");
			bis = taskService.getAttachmentContent(aid);

			// response.setContentType("application/vnd.ms-excel");
			response.setContentType("application/x-download");
			String filedisplay = _a.getName();
			String filenamedisplay = URLEncoder.encode(filedisplay, "UTF-8");
			response.addHeader("Content-Disposition", "attachment;filename="
					+ filenamedisplay);
			bos = response.getOutputStream();
			byte[] buff = new byte[2048];
			int bytesRead;
			while (-1 != (bytesRead = bis.read(buff, 0, buff.length))) {
				bos.write(buff, 0, bytesRead);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (bis != null)
				bis.close();
			if (bos != null)
				bos.close();
		}
	}

	public void downloadTaskAttachment(HttpServletRequest request,
			HttpServletResponse response) {
		String aid = request.getParameter("aid");
		Map<String, Object> map = new HashMap<String, Object>();
		if (aid == null) {
			map.put("success", false);
			map.put("msg", "附件编号为空！");

		}
		InputStream bis = null;
		OutputStream bos = null;
		try {
			Attachment _a = taskService.getAttachment(aid);
			if (_a == null) {
				map.put("success", false);
				map.put("msg", "无法找到编号为【" + aid + "】的附件！");
				// return new ModelAndView("jsonView", map);
			}
			bis = taskService.getAttachmentContent(aid);
			bos = new BufferedOutputStream(response.getOutputStream());
			response.setContentType("application/x-download");
			String filedisplay = _a.getName();
			System.out.println(filedisplay);
			String filenamedisplay = URLEncoder.encode(filedisplay, "UTF-8");
			response.addHeader("Content-Disposition", "attachment;filename="
					+ filenamedisplay);

			byte[] buff = new byte[2048];
			int bytesRead;
			while (-1 != (bytesRead = bis.read(buff, 0, buff.length))) {
				bos.write(buff, 0, bytesRead);
			}
			System.out.println(filedisplay);
			bos.flush();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			map.put("success", true);
			map.put("msg", "下載成功！");
			if (bis != null)
				try {
					bis.close();
				} catch (Exception e) {
					e.printStackTrace();
					map.put("success", false);
					map.put("msg", e.getMessage());
				}
			if (bos != null)
				try {
					bos.close();
				} catch (Exception e) {
					e.printStackTrace();
					map.put("success", false);
					map.put("msg", e.getMessage());
				}
		}

	}

	public ModelAndView deleteTaskAttachment(HttpServletRequest request,
			HttpServletResponse response) {
		String aid = request.getParameter("aid");
		Map<String, Object> map = new HashMap<String, Object>();
		if (aid == null) {
			map.put("success", false);
			map.put("msg", "附件编号为空！");
			return new ModelAndView("jsonView", map);
		}

		try {
			taskService.deleteAttachment(aid);
			map.put("success", true);
			map.put("msg", "删除附件成功！");
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
			map.put("msg", e.getMessage());
		}
		return new ModelAndView("jsonView", map);
	}

	public ModelAndView getTaskAttachments(HttpServletRequest request,
			HttpServletResponse response) {
		if (taskDAO == null)
			return null;
		String taskID = request.getParameter("tid");
		String sId = request.getParameter("sid");
		List<Attachment> attachments = null;
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			if (sId == null) {
				Task _task = taskDAO.getTask(taskID);

				if (_task.getProcessInstanceId() != null) {
					sId = _task.getProcessInstanceId();
				}
			}

			if (sId != null) {
				attachments = (taskService.getProcessInstanceAttachments(sId));
			} else {
				attachments = taskService.getTaskAttachments(taskID);
			}
			map.put("success", true);
			map.put("msg", "success.");
			map.put("attachments", attachments);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
			map.put("msg", e.getMessage());

		}
		return new ModelAndView("jsonView", map);
	}

	public ModelAndView submitTask(HttpServletRequest request,
			HttpServletResponse response) {
		if (taskDAO == null)
			return null;

		String taskID = request.getParameter("tid");
		System.out.println(taskID);
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String _desc = request.getParameter("desc");
			Map<String, String[]> properties = request.getParameterMap();
			Map<String, String> _props = new HashMap<String, String>(
					properties.size());
			Set<String> keys = properties.keySet();
			String[] _vals = null;
			for (String key : keys) {
				_vals = properties.get(key);
				// System.out.println(key);
				if (_vals != null && _vals.length > 0) {
					System.out.println(_vals[0]);
					_props.put(key, _vals[0]);
				}
			}
			Task _task = taskDAO.getTask(taskID);
			TaskFormData formData = formService.getTaskFormData(taskID);

			// modify desc
			_task.setDescription(_desc);
			taskService.saveTask(_task);

			if (formData != null && formData.getFormProperties() != null
					&& formData.getFormProperties().size() > 0) {
				formService.submitTaskFormData(taskID, _props);
			} else {

				if (_task.getOwner() == null) {
					_task.setOwner(_task.getAssignee());
					taskService.setOwner(taskID, _task.getAssignee());
				}
				taskService.complete(taskID);
			}

			map.put("msg", "Task Complete。");
			map.put("success", true);
		} catch (Exception ex) {
			ex.printStackTrace();
			map.put("msg", ex.getMessage());
			map.put("success", false);
		}

		return new ModelAndView("jsonView", map);
	}

	public ModelAndView getTaskReset(HttpServletRequest request,
			HttpServletResponse response) {
		if (taskDAO == null)
			return null;
		String userId = getCurrentUser(request);
		String taskID = request.getParameter("tid");
		Map<String, Object> map = new HashMap<String, Object>();

		Task _task = taskDAO.getTask(taskID);

		map.put("desc", _task.getDescription());
		map.put("processInstanceId", _task.getProcessInstanceId());
		Date _date = _task.getDueDate();
		if (_date != null) {
			SimpleDateFormat _format = new SimpleDateFormat("MM/dd/yyyy");
			map.put("dueto", _format.format(_date));
		}

		List<FormProperty> _props = taskDAO.getTaskForm(taskID);
		if (_props != null) {
			map.put("form", _props);
		}
		map.put("msg", "done");
		map.put("success", true);
		return new ModelAndView("jsonView", map);
	}

	public ModelAndView getTaskDetail(HttpServletRequest request,
			HttpServletResponse response) {
		if (taskDAO == null)
			return null;
		String userId = getCurrentUser(request);
		String taskID = request.getParameter("taskId");
		Map<String, Object> map = new HashMap<String, Object>();

		try {
			Task _task = taskDAO.getTask(taskID);

			map.put("desc", _task.getDescription());
			map.put("processInstanceId", _task.getProcessInstanceId());
			Date _date = _task.getDueDate();
			if (_date != null) {
				SimpleDateFormat _format = new SimpleDateFormat("MM/dd/yyyy");
				map.put("dueto", _format.format(_date));
			}
			ArrayList<Integer> _dels = new ArrayList<Integer>();
			List<IdentityLink> identityLinks = taskService
					.getIdentityLinksForTask(taskID);
			int len = identityLinks.size();
			for (int i = 0; i < len; i++) {
				IdentityLink identityLink = identityLinks.get(i);
				if (identityLink.getUserId() == null) { // only user identity
														// links,
														// ignoring the group
														// ids
					_dels.add(i);
					System.out.println(identityLink.getGroupId());
				}
			}
			len = _dels.size();
			for (int i = len - 1; i >= 0; i--) {
				identityLinks.remove(i);
			}
			map.put("people", identityLinks);

			List<FormProperty> _props = taskDAO.getTaskForm(taskID);

			HashMap<String, ArrayList<HashMap<String, String>>> enums = new HashMap<String, ArrayList<HashMap<String, String>>>();
			if (_props != null) {
				for (FormProperty _p : _props) {
					//System.out.println(_p);
					FormType _type = _p.getType();
					if (_type == null)
						continue;
					String _t = _p.getType().getName();
					if (_t.equalsIgnoreCase("enum")) {
						Map<String, String> values = (Map<String, String>) _p
								.getType().getInformation("values");
						ArrayList<HashMap<String, String>> ret = new ArrayList<HashMap<String, String>>(
								values.size());

						Set<String> _keys = values.keySet();
						for (String _k : _keys) {
							HashMap<String, String> _cur = new HashMap<String, String>(
									2);
							_cur.put("key", _k);
							_cur.put("label", values.get(_k));
							ret.add(_cur);
						}
						enums.put(_p.getId(), ret);
						// System.out.println(_p.getType().getInformation("values"));
					}
				}
			}

			if (_props != null) {
				map.put("form", _props);
			}
			if (enums.size() > 0)
				map.put("enums", enums);

			List<Attachment> attachments = null;
			if (_task.getProcessInstanceId() != null) {
				attachments = (taskService.getProcessInstanceAttachments(_task
						.getProcessInstanceId()));
			} else {
				attachments = taskService.getTaskAttachments(_task.getId());
			}
			map.put("attachments", attachments);

			List<HistoricTaskInstance> subTasks = historyService
					.createHistoricTaskInstanceQuery().taskParentTaskId(taskID)
					.list();
			map.put("subtasks", subTasks);

			ProcessDefinition procdef = repositoryService
					.createProcessDefinitionQuery()
					.processDefinitionId(_task.getProcessDefinitionId())
					.singleResult();
			map.put("pname", getProcessDisplayName(procdef));

			map.put("msg", "done");
			map.put("success", true);
			return new ModelAndView("jsonView", map);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("msg", e.getMessage());
			map.put("success", false);
			return new ModelAndView("jsonView", map);
		}
	}

	public ModelAndView truncateProcInsts(HttpServletRequest request,
			HttpServletResponse response) {
		Map<String, Object> map = new HashMap<String, Object>();

		try {
			String _user = getCurrentUser(request);
			// _user="kermit";
			List<HistoricProcessInstance> processInstances = processInstanceDAO
					.getMyInstance(_user);
			int cnt = processInstances.size();
			for (int i = 0; i < cnt; i++) {
				HistoricProcessInstance _cur = processInstances.get(i);
				runtimeService.deleteProcessInstance(_cur.getId(), null);
			}
			map.put("success", true);
			map.put("msg", "Truncation succeed.");
		} catch (Exception ex) {
			map.put("success", false);
			map.put("msg", ex.getMessage());
		}
		return new ModelAndView("jsonView", map);
	}

	public ModelAndView deleteProcInst(HttpServletRequest request,
			HttpServletResponse response) {
		Map<String, Object> map = new HashMap<String, Object>();

		String _id = request.getParameter("id");
		try {
			runtimeService.deleteProcessInstance(_id, null);
			map.put("success", true);
			map.put("msg", "Deletion succeed.");
		} catch (Exception ex) {
			ex.printStackTrace();
			map.put("success", false);
			map.put("msg", ex.getMessage());
		}
		return new ModelAndView("jsonView", map);
	}

	public ModelAndView myInstances(HttpServletRequest request,
			HttpServletResponse response) {
		if (processInstanceDAO == null)
			return null;
		Map<String, Object> map = new HashMap<String, Object>();

		String _start = request.getParameter("start");
		String _size = request.getParameter("limit");
		int start = 0, size = 50;
		if (_start != null)
			start = Integer.parseInt(_start);
		if (_size != null)
			size = Integer.parseInt(_size);
		try {
			String _user = getCurrentUser(request);
			// _user = "kermit";

			int cnt = processInstanceDAO.myInstancesCnt(_user);
			ArrayList<HashMap<String, String>> processInstances = processInstanceDAO
					.getMyInstance(_user, start, size);
			map.put("success", true);
			map.put("insts", processInstances);
			map.put("totalCnt", cnt);
		} catch (Exception ex) {
			ex.printStackTrace();
			map.put("success", false);
			map.put("msg", ex.getMessage());
		}
		return new ModelAndView("jsonView", map);
	}

	public ModelAndView todoJobs(HttpServletRequest request,
			HttpServletResponse response) {
		if (taskDAO == null)
			return null;
		String userId = getCurrentUser(request);
		String seq = "asc";
		int _start = 1, _limit = 50;

		String start = request.getParameter("page");
		if (start != null)
			_start = Integer.parseInt(start);
		String limit = request.getParameter("limit");
		if (limit != null)
			_limit = Integer.parseInt(limit);

		Map<String, Object> map = new HashMap<String, Object>();

		List<Task> tasks = taskDAO.getAllTasksByAssignee(userId, "desc",
				_limit, _start - 1);
		Attachment a;

		// System.out.println(tasks.size());
		map.put("tasks", tasks);
		map.put("success", true);
		map.put("msg", "good");
		map.put("totalCount", (tasks == null) ? 0 : tasks.size());
		return new ModelAndView("jsonView", map);

	}

	public ModelAndView newDeployment(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		boolean validFile = false;
		Deployment deployment = null;
		Map<String, Object> map = new HashMap<String, Object>();
		// try {

		map.put("success", false);
		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
		MultipartFile multipartFile = multipartRequest.getFile("bpmnFilePath");
		String fileName = multipartFile.getOriginalFilename();
		System.out.println(fileName);

		DeploymentBuilder deploymentBuilder = repositoryService
				.createDeployment().name(fileName);
		DeploymentFilter deploymentFilter = componentFactories.get(
				DeploymentFilterFactory.class).create();

		if (fileName.endsWith(".bpmn20.xml") || fileName.endsWith(".bpmn")) {
			validFile = true;
			deploymentBuilder.addInputStream(fileName,
					new ByteArrayInputStream(multipartFile.getBytes()));

		} else if (fileName.endsWith(".bar") || fileName.endsWith(".zip")) {
			validFile = true;
			deploymentBuilder.addZipInputStream(new ZipInputStream(
					new ByteArrayInputStream(multipartFile.getBytes())));
		} else {
			map.put("msg", Messages.MODEL_IMPORT_INVALID_FILE_EXPLANATION);
		}

		if (validFile) {
			deploymentFilter.beforeDeploy(deploymentBuilder);
			deployment = deploymentBuilder.deploy();
			map.put("success", true);
			map.put("msg", "succeed");
		}
		// } catch (Exception e) {
		// e.printStackTrace();
		// map.put("success", false);
		// map.put("msg", e.getMessage());
		// }
		return new ModelAndView("jsonView", map);
	}

	public ModelAndView deployUploadedFile(HttpServletRequest request,
			HttpServletResponse response) {
		System.out.println("=================");
		boolean validFile = false;
		Map<String, Object> map = new HashMap<String, Object>();
		try {

			map.put("success", false);
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile multipartFile = multipartRequest
					.getFile("bpmnFilePath");
			String fileName = multipartFile.getOriginalFilename();
			System.out.println(fileName);
			if (fileName.endsWith(".bpmn20.xml") || fileName.endsWith(".bpmn")) {
				String contentType = multipartFile.getContentType();
				int idx = fileName.lastIndexOf(".");
				String _ext = "";
				if (idx > 0)
					_ext = fileName.substring(idx + 1);
				System.out.println(_ext);
				validFile = true;
				BpmnXMLConverter xmlConverter = new BpmnXMLConverter();
				XMLInputFactory xif = XMLInputFactory.newInstance();
				XMLStreamReader xtr = xif.createXMLStreamReader(
						multipartFile.getInputStream(), "UTF-8");
				BpmnModel bpmnModel = new BpmnXMLConverter()
						.convertToBpmnModel(xtr);
				xmlConverter.convertToBpmnModel(xtr);

				if (bpmnModel.getMainProcess() == null
						|| bpmnModel.getMainProcess().getId() == null) {
					map.put("msg",
							Messages.MODEL_IMPORT_INVALID_BPMN_EXPLANATION);
				} else {
					System.out.println("=");
					if (bpmnModel.getLocationMap().size() == 0) {
						map.put("msg",
								Messages.MODEL_IMPORT_INVALID_BPMNDI_EXPLANATION);
					} else {

						String processName = null;
						if (StringUtils.isNotEmpty(bpmnModel.getMainProcess()
								.getName())) {
							processName = bpmnModel.getMainProcess().getName();
						} else {
							processName = bpmnModel.getMainProcess().getId();
						}

						Model modelData = repositoryService.newModel();
						ObjectNode modelObjectNode = new ObjectMapper()
								.createObjectNode();
						modelObjectNode.put(MODEL_NAME, processName);
						modelObjectNode.put(MODEL_REVISION, 1);
						modelData.setMetaInfo(modelObjectNode.toString());
						modelData.setName(processName);

						repositoryService.saveModel(modelData);

						BpmnJsonConverter jsonConverter = new BpmnJsonConverter();
						ObjectNode editorNode = jsonConverter
								.convertToJson(bpmnModel);

						repositoryService.addModelEditorSource(modelData
								.getId(),
								editorNode.toString().getBytes("utf-8"));
						map.put("success", true);
						map.put("msg", "Import Succeed.");
					}
				}
			} else {
				// System.out.println(Messages.MODEL_IMPORT_INVALID_FILE_EXPLANATION);
				map.put("msg", Messages.MODEL_IMPORT_INVALID_FILE_EXPLANATION);
				// Messages.MODEL_IMPORT_INVALID_FILE_EXPLANATION));
			}

		} catch (Exception e) {
			e.printStackTrace();
			map.put("msg", e.getMessage());
		}
		return new ModelAndView("jsonView", map);
	}

	public ModelAndView uploadFile(HttpServletRequest request,
			HttpServletResponse response) {
		String _tid = request.getParameter("tid");
		String _sid = request.getParameter("sid");
		Map<String, Object> map = new HashMap<String, Object>();

		try {// image/png;PNG
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile multipartFile = multipartRequest.getFile("userfile");
			String fileName = multipartFile.getOriginalFilename();
			String contentType = multipartFile.getContentType();
			int idx = fileName.lastIndexOf(".");
			String _ext = "";
			if (idx > 0)
				_ext = fileName.substring(idx + 1);

			taskService.createAttachment(contentType + ";" + _ext, _tid, _sid,
					fileName, "", multipartFile.getInputStream());
			map.put("msg", "上传文件成功！");
			map.put("success", true);

		} catch (Exception e) {
			e.printStackTrace();
			map.put("msg", e.getMessage());
			map.put("success", false);
		}
		return new ModelAndView("jsonView", map);
	}

	public ModelAndView doneJobs(HttpServletRequest request,
			HttpServletResponse response) {
		Map<String, Object> map = new HashMap<String, Object>();
		String userId = getCurrentUser(request);
		String seq = "asc";
		String start = request.getParameter("start");
		String limit = request.getParameter("limit");

		int _start = 1, _limit = 50;
		if (start != null)
			_start = Integer.parseInt(start);
		if (limit != null)
			_limit = Integer.parseInt(limit);

		try {
			HistoricTaskInstanceQuery _q = historyService
					.createHistoricTaskInstanceQuery().taskOwner(userId)
					.finished();
			List<HistoricTaskInstance> historicTaskInstances = _q.list(); // .listPage(_start,
																			// _limit);
			map.put("success", true);
			map.put("tasks", historicTaskInstances);
			map.put("totalCnt", historicTaskInstances.size());
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
			map.put("msg", e.getMessage());
		}
		return new ModelAndView("jsonView", map);
	}

	public ModelAndView startProc(HttpServletRequest request,
			HttpServletResponse response) {

		String ProcessDefinitionID = request.getParameter("pid");
		if (ProcessDefinitionID == null || ProcessDefinitionID.length() == 0) {
			return null;
		}
		// System.out.println(ProcessDefinitionID);
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			StartFormData startFormData = formService
					.getStartFormData(ProcessDefinitionID);
			String _user = getCurrentUser(request);
			Authentication.setAuthenticatedUserId(_user);
			if (startFormData != null) {
				List<FormProperty> _props = startFormData.getFormProperties();
				if ((_props != null && _props.size() > 0)
						|| startFormData.getFormKey() != null) {
					map.put("success", true);
					map.put("mode", 1);
					// FormProperty _p=_props.get(0);
					// List<FormPropertyImpl> _props1 = new
					// ArrayList<FormPropertyImpl>();
					// for (FormProperty p : _props)
					// _props1.add(new FormPropertyImpl(p.getId(),
					// p.getName(), p.isRequired(), p.isReadable(), p
					// .isWritable()));

					map.put("records", _props);
					map.put("formdataCnt", _props.size());
					return new ModelAndView("jsonView", map);
				}
			}

			// System.out.println("=====");

			ProcessInstance _instance = processInstanceDAO
					.startProcessInstanceByProcessDefinitionID(ProcessDefinitionID);
			// notification

			List<Task> loggedInUsersTasks = taskService.createTaskQuery()
					.taskAssignee(_user).processInstanceId(_instance.getId())
					.list();

			map.put("success", true);
			map.put("mode", 0);
			map.put("msg", "instance has started");
			map.put("tasks", loggedInUsersTasks);
			map.put("taskscnt", loggedInUsersTasks.size());
			Authentication.setAuthenticatedUserId(null);
			ModelAndView objs = new ModelAndView("jsonView", map);
			return objs;
		} catch (Exception e) {
			map.put("success", false);
			map.put("msg", e.getMessage());
			return new ModelAndView("jsonView", map);
		}
	}

	public void exportModel(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		byte[] bpmnBytes = null;
		String filename = null;
		String _id = request.getParameter("id");
		// System.out.println(_id);
		RepositoryService repositoryService = processEngine
				.getRepositoryService();

		JsonNode editorNode = null;
		try {
			editorNode = new ObjectMapper().readTree(repositoryService
					.getModelEditorSource(_id));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (editorNode == null)
			System.out.println("why");

		BpmnJsonConverter jsonConverter = new BpmnJsonConverter();
		BpmnModel model = null;
		// try {
		model = jsonConverter.convertToBpmnModel(editorNode);
		// } catch (Exception e) {
		// e.printStackTrace();
		// }

		if (model == null) {
			System.out.println("================");
			response.setContentType("text/plain; charset=utf-8");
			response.getWriter()
					.write("error when export the model, check whether the model is blank!");
			response.getWriter().close();

		} else {

			String processId = null;
			if (model.getPools().size() > 0) {
				processId = "Collaboration";
			} else {
				processId = model.getMainProcess().getId();
			}
			if (processId == null) {
				response.setContentType("text/plain; charset=utf-8");
				response.getWriter()
						.write("error when export the model, check whether the model is blank!");
				response.getWriter().close();
			} else {
				ServletOutputStream _out = response.getOutputStream();
				filename = model.getMainProcess().getId() + ".bpmn20.xml";
				bpmnBytes = new BpmnXMLConverter().convertToXML(model);
				response.setContentType("application/x-download");
				String filenamedisplay = URLEncoder.encode(filename, "UTF-8");
				response.addHeader("Content-Disposition",
						"attachment;filename=" + filenamedisplay);

				_out.write(bpmnBytes);
				_out.flush();
				_out.close();
			}
		}

	}

	// @ExceptionHandler(Exception.class)
	// public ModelAndView handleException(Exception ex, HttpServletRequest
	// request) {
	// Map<Object, Object> model = new HashMap<Object, Object>();
	// model.put("success", false);
	// System.out.println("ddddddddddddddd");
	// if (ex instanceof MaxUploadSizeExceededException) {
	// System.out.println("====+++++++++++++++++");
	// model.put("errors", "文件应不大于 3M");
	// } else {
	// System.out.println("====");
	// model.put("errors", "不知错误: " + ex.getMessage());
	// }
	// return new ModelAndView("jsonView", (Map) model);
	//
	// }

	@Override
	public ModelAndView resolveException(HttpServletRequest arg0,
			HttpServletResponse arg1, Object arg2, Exception arg3) {
		Map<Object, Object> model = new HashMap<Object, Object>();
		model.put("success", false);
		// System.out.println("ddddddddddddddd");
		if (arg3 instanceof MaxUploadSizeExceededException) {
			System.out.println("====+++++++++++++++++");
			model.put("errors", "文件应不大于 3M");
		} else {
			System.out.println("====");
			model.put("errors", "不知错误: " + arg3.getMessage());
		}
		return new ModelAndView("jsonView", (Map) model);

	}
}
