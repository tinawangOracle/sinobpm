//authoer: tina.wang
package com.sinoearth.bpm.action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import oracle.sgt.util.jsonUtil;

import org.springframework.web.servlet.ModelAndView;

import com.sinoearth.bpm.dao.ActReModelDAO;

public class MessagesAction extends ParentAction {

	private ActReModelDAO actReModelDAO;

	public ActReModelDAO getActReModelDAO() {
		return actReModelDAO;
	}

	public void setActReModelDAO(ActReModelDAO actReModelDAO) {
		this.actReModelDAO = actReModelDAO;
	}

	public ModelAndView sendMail(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		Map<String, Object> map = null;
		ModelAndView objs = new ModelAndView("jsonView", map);
		return objs;
	}

	public ModelAndView getPrivateInbox(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		// System.out.println("enter");
		if (actReModelDAO == null)
			System.out.println("return null");
		else {
			String _start = request.getParameter("start");
			String _size = request.getParameter("limit");
			String _s = request.getParameter("sort");
			ArrayList<String> _ss = jsonUtil.getSortDirForExtjsSorting(_s);
			String _sort = null, _dir = null;
			if (_ss != null) {
				_sort = _ss.get(0);
				_dir = _ss.get(1);
			}

			if (_dir == null)
				_dir = "ASC";
			if (_sort == null)
				_sort = "id";

			int _idx = 0;
			Map<String, Object> map = null;
			if (_idx < 1)
				map = actReModelDAO.getAllModels(_dir, _sort,
						Integer.parseInt(_start), Integer.parseInt(_size));

			Object cnt = map.get("totalCount");
			if (cnt == null)
				map.put("totalCount", getTCnt(request));

			ModelAndView objs = new ModelAndView("jsonView", map);

			// -----------------------
			return objs;
		}
		return null;
	}

}
