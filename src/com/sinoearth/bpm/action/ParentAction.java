package com.sinoearth.bpm.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import com.opensymphony.xwork2.ActionContext;
import com.sinoearth.bpm.I18nManager;

import oracle.sgt.json.jsonUtil;

public class ParentAction extends MultiActionController {

	@Autowired
	protected I18nManager i18nManager;
	
	Map parameters = null;
	private int length, start, end;
	public jsonUtil util;

	public jsonUtil getUtil() {
		return util;
	}

	public void setUtil(jsonUtil util) {
		this.util = util;
	}

	public int getEnd() {
		return end;
	}

	public int getLength() {
		return length;
	}

	public int getStart() {
		return start;
	}

	public ModelAndView notFoundDAO() {
		Map<String, Object> modelMap = new HashMap<String, Object>(2);
		modelMap.put("success", false);
		modelMap.put("msg", "Cannot find DAO!");
		ModelAndView objs = new ModelAndView("jsonView", modelMap);
		return objs;
	}

	public int getTCnt(HttpServletRequest request) {
		int _tcnt = 0;
		String _cnt = request.getParameter("tcnt");
		if (_cnt != null) {
			try {
				_tcnt = Integer.parseInt(_cnt);
				return _tcnt;
			} catch (Exception e) {
				return 0;
			}
		}
		return 0;
	}

	public Map getParameters() {
		parameters = ActionContext.getContext().getParameters();
		return parameters;
	}

	public String getParameterValue(String param) {
		Object paramObj = getParameters().get(param);
		if (paramObj == null)
			return null;
		return ((String[]) paramObj)[0];
	}

	public final Map getParameters(HttpServletRequest request) {
		parameters = request.getParameterMap();
		return parameters;
	}

	public String getParameterValue(HttpServletRequest request, String param) {
		Object paramObj = getParameters(request).get(param);
		if (paramObj == null)
			return null;
		return ((String[]) paramObj)[0];
	}

	public String getCurrentUser(HttpServletRequest request) {
		Object o = request.getSession().getAttribute("email");
		String mail = "admin";

		if (o != null)
			mail = o.toString().toLowerCase();
		return mail;
	}

	public ModelAndView getModelMap(List products) {

		Map<String, Object> modelMap = new HashMap<String, Object>(4);
		modelMap.put("success", true);
		if (products == null || products.size() == 0) {
			modelMap.put("totalCount", 0);
		} else {
			modelMap.put("totalCount", products.size());
			modelMap.put("records", products);
		}
		return new ModelAndView("jsonView", modelMap);
	}
}
