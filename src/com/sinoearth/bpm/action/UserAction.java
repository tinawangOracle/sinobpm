//authoer: tina.wang
package com.sinoearth.bpm.action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import oracle.sgt.util.jsonUtil;

import org.springframework.web.servlet.ModelAndView;

import com.sinoearth.bpm.dao.ActIdUserDAO;
import com.sinoearth.bpm.dao.ActReModelDAO;
import com.sinoearth.bpm.domain.ActIdGroup;
import com.sinoearth.bpm.domain.ActIdInfo;
import com.sinoearth.bpm.domain.ActIdUser;

public class UserAction extends ParentAction {

	private ActIdUserDAO actIdUserDAO;

	public ActIdUserDAO getActIdUserDAO() {
		return actIdUserDAO;
	}

	public void setActIdUserDAO(ActIdUserDAO actIdUserDAO) {
		this.actIdUserDAO = actIdUserDAO;
	}

	public ModelAndView getUser(HttpServletRequest request,
			HttpServletResponse response) {
		if (actIdUserDAO == null)
			return notFoundDAO();

		String uname = getCurrentUser(request);
		ActIdUser _user = actIdUserDAO.getUser(uname, null);
		Map<String, Object> modelMap = new HashMap<String, Object>(2);
		if (_user == null) {
			modelMap.put("success", false);
			modelMap.put("msg", "Cannot find the user.");
			return new ModelAndView("jsonView", modelMap);
		}
		modelMap.put("success", true);
		modelMap.put("user", _user);
		return new ModelAndView("jsonView", modelMap);

	}

	public ModelAndView changePwd(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		// System.out.println("enter");

		if (actIdUserDAO == null)
			return notFoundDAO();

		Map<String, Object> modelMap = new HashMap<String, Object>(2);
		String _id = getCurrentUser(request);
		String _cpass = request.getParameter("cpass");
		String _npass = request.getParameter("npass");

		String map = actIdUserDAO.changePwd(_id, _cpass, _npass);

		if (map.startsWith("0:")) {
			modelMap.put("success", false);
			modelMap.put("msg", map.substring(2));
		} else if (map.startsWith("1:")) {
			modelMap.put("success", true);
			modelMap.put("msg", map.substring(2));
		} else {
			modelMap.put("success", false);
		}

		ModelAndView objs = new ModelAndView("jsonView", modelMap);
		return objs;
	}

	public ModelAndView saveUser(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		String _email = request.getParameter("email");
		String chineseName = request.getParameter("fullname");
		String idcard = request.getParameter("idcard");
		String phonenumber = request.getParameter("phone");

		ActIdUser _user = new ActIdUser(_email, chineseName.substring(0, 1),
				chineseName.substring(1), _email);

		HashSet<ActIdInfo> _infos = new HashSet<ActIdInfo>();
		ActIdInfo _info = new ActIdInfo(_user, 1, "userinfo", "idcard", idcard,
				null, null);
		_infos.add(_info);
		_info = new ActIdInfo(_user, 1, "userinfo", "phone", phonenumber, null,
				null);
		_infos.add(_info);

		_user.setActIdInfos(_infos);

		String _res = actIdUserDAO.saveUser(_user);

		Map<String, Object> modelMap = new HashMap<String, Object>(3);
		if (_res.equals("0"))
			modelMap.put("success", "true");
		else {
			modelMap.put("success", "false");
			modelMap.put("msg", _res);
		}
		return new ModelAndView("jsonView", modelMap);
	}
}
