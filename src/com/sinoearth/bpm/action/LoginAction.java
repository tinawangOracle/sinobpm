package com.sinoearth.bpm.action;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.activiti.engine.identity.Group;
import org.activiti.engine.identity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import com.sinoearth.bpm.ConsoleApp;
import com.sinoearth.bpm.I18nManager;
import com.sinoearth.bpm.dao.ActIdUserDAO;
import com.sinoearth.bpm.dao.IdentityDAO;
import com.sinoearth.bpm.domain.ActIdGroup;
import com.sinoearth.bpm.domain.ActIdInfo;
import com.sinoearth.bpm.domain.ActIdUser;

public class LoginAction  extends ParentAction {

	private ActIdUserDAO actIdUserDAO;
//	private ConsoleApp consoleApp;

	
//	private I18nManager i18nManager;
//
//	public I18nManager getI18nManager() {
//		return i18nManager;
//	}
//	
//
//	public void setI18nManager(I18nManager i18nManager) {
//		this.i18nManager = i18nManager;
//	}

//	public ConsoleApp getConsoleApp() {
//		return consoleApp;
//	}
//
//	public void setConsoleApp(ConsoleApp consoleApp) {
//		this.consoleApp = consoleApp;
//	}

	private String javascript = "";
	private String msg;

	public ActIdUserDAO getActIdUserDAO() {
		return actIdUserDAO;
	}

	public void setActIdUserDAO(ActIdUserDAO actIdUserDAO) {
		this.actIdUserDAO = actIdUserDAO;
	}

	private IdentityDAO identityDAO;

	public IdentityDAO getIdentityDAO() {
		return identityDAO;
	}

	public void setIdentityDAO(IdentityDAO identityDAO) {
		this.identityDAO = identityDAO;
	}

	private boolean success;

	public String getJavascript() {
		return javascript;
	}

	public boolean isSuccess() {
		return success;
	}

	public ModelAndView inSession(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		System.out.println("enter");

		Cookie[] cks = null;
		String email = null;
		HttpSession session = ((HttpServletRequest) request).getSession(false);
		HttpServletResponse _response = (HttpServletResponse) response;
		HttpServletRequest _request = (HttpServletRequest) request;
		Map<String, Object> modelMap = new HashMap<String, Object>(3);

		if (session == null)// timeout
		{
			modelMap.put("success", 0);
		} else {
			cks = _request.getCookies();
			if (cks == null) { // still no cookies
				modelMap.put("success", 0);
			} else {
				Object _u = session.getAttribute("email");
				if (_u == null) {
					modelMap.put("success", 0);
					// if (_request.getHeader("x-requested-with") != null
					// &&
					// _request.getHeader("x-requested-with").equalsIgnoreCase(
					// "XMLHttpRequest"))
					// _response.setHeader("sessionstatus", "timeout");
				} else
					modelMap.put("success", 1);
			}
		}
		System.out.println(modelMap.get("success"));
		modelMap.put("msg", "in session");
		modelMap.put("script", "");
		return new ModelAndView("jsonView", modelMap);
	}

	public ModelAndView login(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession _session = request.getSession();
		String _user = request.getParameter("email");
		String _password = request.getParameter("passwd");

		int loginStateId = Integer.parseInt(request.getParameter("logins"));

		try {
			// if auth failed, it will throw exception
			if (checkUser(_user, _password, _session)) {
				Locale locale = LocaleContextHolder.getLocale();
				System.out.println(locale);
				// ConsoleApp.set(consoleApp);
				i18nManager.setLocale(locale);

				_session.setMaxInactiveInterval(60 * 15);// timeout is 15
															// minutes
				success = true;
				int time = 0;
				switch (loginStateId) {
				case 1:
					time = 24 * 3600;
					break;
				case 2:
					time = 24 * 3600 * 7;
					break;
				case 3:
					time = 24 * 3600 * 30;
					break;
				case 4:
					time = 24 * 3600 * 365;
					break;
				}

				if (time > 0) {

					Cookie cookieEmail = new Cookie("username", _user);
					cookieEmail.setMaxAge(time);
					cookieEmail.setPath(request.getContextPath());

					response.addCookie(cookieEmail);

					Cookie cookieAdmin = new Cookie("admin", _session
							.getAttribute("admin").toString());
					cookieAdmin.setMaxAge(time);
					cookieAdmin.setPath(request.getContextPath());
					response.addCookie(cookieAdmin);
				}

				msg = "Login Succeed";
				Map<String, Object> modelMap = new HashMap<String, Object>(3);
				modelMap.put("success", "true");
				modelMap.put("email", _user);
				modelMap.put("msg", msg);
				modelMap.put("script", "");
				return new ModelAndView("jsonView", modelMap);
			} else {
				msg = "Username or Password is not correct";
				Map<String, Object> modelMap = new HashMap<String, Object>(3);
				modelMap.put("success", "false");
				modelMap.put("msg", msg);
				modelMap.put("script", "");
				return new ModelAndView("jsonView", modelMap);
			}
		} catch (Exception e) {

			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	public boolean checkUser(String uname, String passwd, HttpSession _session) {
		String encodedpasswd = passwd;// MD5.Encrypt(passwd);

		User _user = identityDAO.checkUser(uname, encodedpasswd);
		if (_user == null)
			return false;
		_session.setAttribute("username", uname);
		_session.setAttribute("email", uname);
		_session.setAttribute("admin", "false");
		List<Group> _grps = identityDAO.getGroupsSetByUserId(uname);
		// ConsoleApp _app= ConsoleApp.get();
		// if (_app!=null) System.out.println("===========");

		_session.setAttribute("groups", _grps);
		for (Group _grp : _grps) {
			if (_grp.getId().equalsIgnoreCase("admin")) {
				_session.setAttribute("admin", "true");
				break;
			}
		}
		return true;
	}

	// get email address if user has logined
	public ModelAndView getUser(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession _session = request.getSession();
		String username = (String) _session.getAttribute("username");
		String blogin = username == null ? "false" : "true";
		System.out.println(username);
		Map<String, Object> modelMap = new HashMap<String, Object>(2);
		modelMap.put("blogin", blogin);
		modelMap.put("uname", username);
		return new ModelAndView("jsonView", modelMap);

	}

	public ModelAndView register(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		String _email = request.getParameter("userEmail");
		String userpwd = request.getParameter("newPass");
		String chineseName = request.getParameter("chineseName");
		String idcard = request.getParameter("idcard");
		String phonenumber = request.getParameter("phonenumber");

		Map<String, String> userInfos = new HashMap<String, String>();
		userInfos.put("idcard", idcard);
		userInfos.put("phone", phonenumber);

		String _res = identityDAO.createUser(_email,
				chineseName.substring(0, 1), chineseName.substring(1), _email,
				userpwd, "student", userInfos);

		// ActIdUser _user = new ActIdUser(_email, 2, chineseName.substring(0,
		// 1),
		// chineseName.substring(1), _email, userpwd, null, null, null);
		// ActIdGroup _group = new ActIdGroup("student");
		// HashSet<ActIdGroup> _grps = new HashSet<ActIdGroup>(1);
		// _grps.add(_group);
		// _user.setActIdGroups(_grps);
		// HashSet<ActIdInfo> _infos = new HashSet<ActIdInfo>();
		// ActIdInfo _info = new ActIdInfo(_user, 1, "userinfo", "idcard",
		// idcard,
		// null, null);
		// _infos.add(_info);
		// _info = new ActIdInfo(_user, 1, "userinfo", "phone", phonenumber,
		// null,
		// null);
		// _infos.add(_info);
		//
		// _user.setActIdInfos(_infos);
		//
		// String _res = actIdUserDAO.createUser(_user);

		Map<String, Object> modelMap = new HashMap<String, Object>(3);
		if (_res.equals("1"))
			modelMap.put("success", "true");
		else {
			modelMap.put("success", "false");
			modelMap.put("msg", _res);
		}
		return new ModelAndView("jsonView", modelMap);
	}

	// get email address if user has logined
	public ModelAndView logout(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		// remove session
		HttpSession _session = request.getSession();
		_session.removeAttribute("email");
		_session.removeAttribute("solr");
		// remove cookie
		Cookie killMyCookie = new Cookie("email", null);
		killMyCookie.setMaxAge(0);
		killMyCookie.setPath(request.getContextPath());
		response.addCookie(killMyCookie);
		Map<String, Object> modelMap = new HashMap<String, Object>(3);
		modelMap.put("success", "true");
		return new ModelAndView("jsonView", modelMap);
	}

}
