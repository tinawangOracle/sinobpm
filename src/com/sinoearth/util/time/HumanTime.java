/* Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sinoearth.util.time;

import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import com.sinoearth.bpm.Constants;
import com.sinoearth.bpm.I18nManager;
import com.sinoearth.bpm.Messages;
import com.sinoearth.util.time.timeunit.DayTimeUnit;
import com.sinoearth.util.time.timeunit.HourTimeUnit;
import com.sinoearth.util.time.timeunit.MinuteTimeUnit;
import com.sinoearth.util.time.timeunit.MonthTimeUnit;
import com.sinoearth.util.time.timeunit.WeekTimeUnit;
import com.sinoearth.util.time.timeunit.YearTimeUnit;

/**
 * @author Frederik Heremans
 */
public class HumanTime {

	private static final List<TimeUnit> timeUnits = Arrays.asList(
			new YearTimeUnit(), new MonthTimeUnit(), new WeekTimeUnit(),
			new DayTimeUnit(), new HourTimeUnit(), new MinuteTimeUnit());

	private Long baseDate;
	private I18nManager i18nManager;

	public I18nManager getI18nManager() {
		return i18nManager;
	}

	public void setI18nManager(I18nManager i18nManager) {
		this.i18nManager = i18nManager;
	}

	/**
	 * Create human time, relative to current time.
	 */
	public HumanTime() {
	}

	public HumanTime(I18nManager i18nManager) {
		this(null, i18nManager);
	}

	public HumanTime(Date date, I18nManager i18nManager) {
		if (i18nManager == null) {
			throw new IllegalArgumentException("I18NManager is required!");
		}

		this.i18nManager = i18nManager;
		if (date != null) {
			baseDate = date.getTime();
		} else {
			baseDate = new Date().getTime();
		}
	}

	/**
	 * Returns the human readable string of the duration between the given date
	 * and the base date.
	 */
	public String formatSimple(Date date) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(
				Constants.DEFAULT_TIME_FORMAT);
		return dateFormat.format(date);
	}

	public String format(Date date) {
		boolean future = true;
		baseDate = new Date().getTime();
		Long difference = date.getTime() - baseDate;
		if (difference < 0) {
			future = false;
			difference = -difference;
		} else if (difference == 0) {
			return i18nManager.getMessage(Messages.TIME_UNIT_JUST_NOW);
		}

		String unitMessage = getUnitMessage(difference);

		String messageKey = null;
		if (future) {
			messageKey = Messages.TIME_UNIT_FUTURE;
		} else {
			messageKey = Messages.TIME_UNIT_PAST;
		}

		return i18nManager.getMessage(messageKey, unitMessage);
	}

	public String[] formatLocalized(String labelTemplate, Date date,
			boolean showTime) {
		if (date != null) {
			DateFormat dateFormat = null;
			if (showTime) {
				dateFormat = new SimpleDateFormat(Constants.DEFAULT_TIME_FORMAT);
			} else {
				dateFormat = new SimpleDateFormat(Constants.DEFAULT_DATE_FORMAT);
			}

			if (labelTemplate != null) {
				return new String[] {
						MessageFormat.format(labelTemplate, format(date)),
						dateFormat.format(date) };
			} else {
				return new String[] { format(date), dateFormat.format(date) };
			}

		} else
			return null;
	}

	private String getUnitMessage(Long difference) {
		String unitMessage = null;
		TimeUnit unitToUse = null;
		TimeUnit currentUnit = null;

		for (int i = 0; i < timeUnits.size() && unitToUse == null; i++) {
			currentUnit = timeUnits.get(i);

			if (currentUnit.getNumberOfMillis() <= difference) {
				unitToUse = currentUnit;
			}
		}

		if (unitToUse == null) {
			// No unit found, so use "moments ago" of "moments from now"
			unitMessage = i18nManager.getMessage(Messages.TIME_UNIT_MOMENTS);
		} else {
			// Calculate number of units
			Long numberOfUnits = (difference - (difference % unitToUse
					.getNumberOfMillis())) / unitToUse.getNumberOfMillis();
			unitMessage = i18nManager.getMessage(
					unitToUse.getMessageKey(numberOfUnits), numberOfUnits);
		}

		return unitMessage;
	}

}
