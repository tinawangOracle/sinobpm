package oracle.sgt.nosql;

import oracle.kv.KVStore;
import oracle.kv.KVStoreConfig;
import oracle.kv.KVStoreFactory;
import oracle.kv.Key;
import oracle.kv.Value;
import oracle.kv.ValueVersion;

public class KVManager {
	private KVStore store;

	private void connStore(String[] argv) {
		String storeName = "kvstore";
		String hostName = "localhost";
		String hostPort = "5000";

		final int nArgs = argv.length;
		int argc = 0;

		while (argc < nArgs) {
			final String thisArg = argv[argc++];

			if (thisArg.equals("-store")) {
				if (argc < nArgs) {
					storeName = argv[argc++];
				} else {
					usage("-store requires an argument");
				}
			} else if (thisArg.equals("-host")) {
				if (argc < nArgs) {
					hostName = argv[argc++];
				} else {
					usage("-host requires an argument");
				}
			} else if (thisArg.equals("-port")) {
				if (argc < nArgs) {
					hostPort = argv[argc++];
				} else {
					usage("-port requires an argument");
				}
			} else {
				usage("Unknown argument: " + thisArg);
			}
		}

		store = KVStoreFactory.getStore(new KVStoreConfig(storeName, hostName
				+ ":" + hostPort));
	}

	private void usage(String message) {
		System.out.println("\n" + message + "\n");
		System.out.println("usage: " + getClass().getName());
		System.out.println("\t-store <instance name> (default: kvstore) "
				+ "-host <host name> (default: localhost) "
				+ "-port <port number> (default: 5000)");
		System.exit(1);
	}

	public void write() {

		final String keyString = "AAASdpAAEAAAMQHAAI";
		final String valueString = "scl58004.us.oracle.com-2012-03-05-15-34-51.png";

		store.put(Key.createKey(keyString),
				Value.createValue(valueString.getBytes()));

		store.close();
	}

	public void read(String keyString) {
		final ValueVersion valueVersion = store.get(Key.createKey(keyString));

		System.out.println(keyString + " "
				+ new String(valueVersion.getValue().getValue()));

		store.close();
	}

	public static void main(String[] args) {
		try {
			KVManager example = new KVManager();
			example.connStore(args);
			example.write();
			example.connStore(args);
			example.read("AAASdpAAEAAAMQHAAI");

		} catch (RuntimeException e) {
			e.printStackTrace();
		}
	}

}
