package oracle.sgt.web.tag;

import java.io.IOException;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;

public class LoginTag extends SimpleTagSupport
{

	@Override
	public void doTag() throws JspException, IOException
	{
		PageContext pageContext = (PageContext) this.getJspContext(); 
		HttpSession session = pageContext.getSession();
		String email =(String) session.getAttribute("email");
		System.out.println(email);
		if (email==null) {
			Cookie[]cks=((HttpServletRequest)pageContext.getRequest()).getCookies();
			if (cks!=null) {
				for (int i = 0; i < cks.length; i++) {
					if (cks[i].getName().equals("email")) {
						email = cks[i].getValue();
						session.setAttribute("email", email);
					}
					if (cks[i].getName().equals("admin")) {
						session.setAttribute("admin", cks[i].getValue().equals("true"));
					}
				}
			}
		}
		String bodyText = "";		
		if(email == null)
		{
			bodyText += "<script type='text/javascript'>";
			bodyText += "function init(){var login=Ext.create('stringconsole.view.Login');login.show();}; Ext.onReady(init)";
			bodyText += "</script>";
		}
		else
		{
			bodyText += "<script type='text/javascript'>bLogin = true;welcomeMsg = '欢迎";
			bodyText += email + "';</script>";			
		}
		    
		pageContext.getOut().write(bodyText); 
	}}
