package oracle.sgt.util.auth;

import java.io.UnsupportedEncodingException;
import java.security.spec.KeySpec;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;

public class DesCrypto {
    Cipher ecipher;
    Cipher dcipher;
    private static DesCrypto myDES = null;

    public static DesCrypto getInstance() {
        if (myDES == null) {
            byte[] theKey = null;
            theKey = hexToBytes("133457799BBCDFF1");
            KeySpec ks;
            SecretKey key = null;
            try {
                ks = new DESKeySpec(theKey);
                SecretKeyFactory kf = SecretKeyFactory.getInstance("DES");
                key = kf.generateSecret(ks);
                myDES = new DesCrypto(key);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return myDES;
    }

    public DesCrypto(SecretKey key) {
        try {
            ecipher = Cipher.getInstance("DES");
            dcipher = Cipher.getInstance("DES");
            ecipher.init(Cipher.ENCRYPT_MODE, key);
            dcipher.init(Cipher.DECRYPT_MODE, key);

        } catch (javax.crypto.NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (java.security.NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (java.security.InvalidKeyException e) {
            e.printStackTrace();
        }
    }

    public String encrypt(String str) {
        try {
            // Encode the string into bytes using utf-8
            byte[] utf8 = str.getBytes("UTF8");

            // Encrypt
            byte[] enc = ecipher.doFinal(utf8);

            // Encode bytes to base64 to get a string
            return new sun.misc.BASE64Encoder().encode(enc);
        } catch (javax.crypto.BadPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String decrypt(String str) {
        try {
            // Decode base64 to get bytes
            byte[] dec = new sun.misc.BASE64Decoder().decodeBuffer(str);

            // Decrypt
            byte[] utf8 = dcipher.doFinal(dec);

            // Decode using utf-8
            return new String(utf8, "UTF8");
        } catch (javax.crypto.BadPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static byte[] hexToBytes(String str) {
        if (str == null) {
            return null;
        } else if (str.length() < 2) {
            return null;
        } else {
            int len = str.length() / 2;
            byte[] buffer = new byte[len];
            for (int i = 0; i < len; i++) {
                buffer[i] =
                        (byte)Integer.parseInt(str.substring(i * 2, i * 2 + 2),
                                               16);
            }
            return buffer;
        }

    }

    public static void main(String[] args) {
        try {

            // Create encrypter/decrypter class
            DesCrypto encrypter = getInstance();

            // Encrypt
            String encrypted = encrypter.encrypt("AXPA49dS");
            System.out.println(encrypted);
            // Decrypt
            String decrypted = encrypter.decrypt(encrypted);
            System.out.println(decrypted);
            System.out.println(DesCrypto.getInstance().decrypt("J3OlQ2r2iNP98uF0SSki+A=="));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
