package oracle.sgt.util.auth;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;

public class MAuthenticator extends Authenticator {
    String uname = null, passwd = null;

    public MAuthenticator() {
    }

    public MAuthenticator(String uname, String passwd) {
        this.uname = uname;
        this.passwd = passwd;
    }

    protected PasswordAuthentication getPasswordAuthentication() {
        return new PasswordAuthentication(uname, passwd);
    }

    public static void main(String[] args) {
        MAuthenticator mAuthenticator = new MAuthenticator();
    }
}

