/*
 * $Log: ConnParams.java,v $
 * Revision 1.1  2006/12/06 08:06:23  zxu
 * Initial Import
 *
 * Revision 1.1  2006/08/06 08:37:51  Zhuang
 * Initial version: LDAP related
 *
 */
package oracle.sgt.util.ldap;

import java.util.Properties;

import org.apache.commons.lang.StringUtils;


/**
 * Contains static variables which define the connection parameters
 * for Oracle Internet Directory and Database. The parameters are loaded from
 * Connection.properties file.
 *
 */
public class ConnParams {

    // Default Root Context
    public static String identityMgmtRealm = "dc=oracle,dc=com";

    // Directory Server host name
    public static String dirHostName = "ldap.oracle.com";

    // Directory instance  port
    public static String dirPort = "389";

    // Load parameters from Connection.properties


    public static void populate(String host, String port, String myrealm) {
        dirHostName = host;
        dirPort = port;
        identityMgmtRealm = (String)myrealm;
        identityMgmtRealm =
                StringUtils.replaceChars(identityMgmtRealm, "_", ",");
        System.out.println(identityMgmtRealm);
    }
}
