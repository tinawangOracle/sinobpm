/*
 * $Log: LDAPNames.java,v $
 * Revision 1.1  2006/12/06 08:06:23  zxu
 * Initial Import
 *
 * Revision 1.1  2006/08/06 08:37:51  Zhuang
 * Initial version: LDAP related
 *
 *
 */

package oracle.sgt.util.ldap;

/**
 * This interface holds the context names used by the Grocery Store application.
 * All names are generated based on the Identity Management Realm.
 */
public class LDAPNames {

    /** Identity Management Realm */
    public static String rootContext = ConnParams.identityMgmtRealm;

    /** Group context under Identity Realm, this is where all group entries
    are present */
    public static String groupContext = "cn=Groups," + rootContext;

    /** User context under Identity Realm, this is where all user entries
      are present */
    //  public static  String userContext   = "cn=Users,"+rootContext;
    public static String userContext = rootContext;

    /** HR Manager group context name */
    public static String hrGrpContext = "cn=HR," + groupContext;

    /** Store Manager group context name */
    public static String smGrpContext = "cn=SM," + groupContext;

    /** Employee group context name */
    public static String empGrpContext = "cn=Employee," + groupContext;

    /** Supplier group context name */
    public static String suppGrpContext = "cn=Supplier," + groupContext;

    /** The relative distinguished name of user entries */
    //  public static  String RDN = "uname";
    public static String RDN = "cn";

}
