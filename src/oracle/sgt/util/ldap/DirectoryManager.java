package oracle.sgt.util.ldap;

import javax.naming.Context;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttributes;
import javax.naming.directory.Attribute;
import javax.naming.directory.SearchResult;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.DirContext;

import javax.naming.NamingException;
import javax.naming.NamingEnumeration;
import javax.naming.AuthenticationException;
import javax.naming.NameAlreadyBoundException;

import java.util.List;
import java.util.Map;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Collection;
import java.util.Hashtable;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpSession;

/**
 * This class manages all Directory operations.
 */
public class DirectoryManager {
	public static String[] regions = { "amer", "apac", "emea", "JPN" };
	private DirContext dirctx = null;
	Pattern patterns[] = { Pattern.compile("CN=(.*),L=(.*),DC=ORACLE,DC=COM"),
			Pattern.compile("cn=(.*),l=(.*),dc=oracle,dc=com") };
	private static DirectoryManager _manager = null;

	public DirectoryManager() {
	}

	public static DirectoryManager getInstance() {
		if (_manager == null)
			_manager = new DirectoryManager();
		return _manager;
	}

	/**
	 * Checks if the specified uname is a member of the specified group.
	 * 
	 * @param uname
	 *            Relative Distinguished name of the user
	 * @param groupDN
	 *            Distingushed name of the group
	 * @return true - if the user belongs to the group, else false
	 * @exception NamingException
	 *                if any directory operation fails
	 */
	public boolean isUserInGroup(String region, String uname, String groupDN)
			throws NamingException {

		boolean ingroup = false;

		// Get the Distinguished Name of the user
		String userDN = this.getUserDN(region, uname, null);

		// Filter to check if the user DN is a member
		// A user is a member of a group if the uniqueMember attribute of that
		// group entry
		// has the user DN value.
		String filter = "(uniqueMember=" + userDN + ")";

		// Initialize search controls to search with scope as sub tree
		SearchControls searchControls = new SearchControls();
		searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);
		// Set the attributes to be returned
		searchControls.setReturningAttributes(new String[] { "cn" });

		// Search under the specified group
		NamingEnumeration results = dirctx.search(groupDN, filter,
				searchControls);

		// If the search has results, then the user is a member
		if (results.hasMore()) {
			ingroup = true;
		} // else user not present, i.e defaulted

		return ingroup;
	}

	public boolean authenticateUserSimple(String region, String username,
			String passwd, HttpSession session) throws AuthenticationException,
			NamingException {

		return false;
	}

	/**
	 * Authenticates the user credentials with Directory.
	 * 
	 * @param username
	 *            User Name of the user
	 * @param passwd
	 *            Password of the user
	 * @return true - if the credentials are valid
	 * 
	 * @exception AuthenticationException
	 *                If credentials are invalid
	 * @exception NamingException
	 *                if any directory operation fails
	 */
	public boolean authenticateUser(String region, String username,
			String passwd, HttpSession session) throws AuthenticationException,
			NamingException {

		boolean authorized = false;

		// Get the Distinguished Name
		String dn = this.getUserDN(region, username, session);

		try {
			// Authenticate with Directory
			dirctx = this.getDirectoryContext(dn, passwd);

		} catch (AuthenticationException authEx) {

			throw new AuthenticationException(" Invalid Password ");
		}

		authorized = true;

		return authorized;
	}

	public String getManagerMail(String region, String uname)
			throws NamingException {
		DirContext dCtx = null;
		dCtx = this.getDirectoryContext("l=" + region + ","
				+ LDAPNames.rootContext, "");
		SearchResult searchResult = null;
		NamingEnumeration results = null;
		String userDN = null;
		String filter = "(" + LDAPNames.RDN + "=" + uname + ")";

		SearchControls searchControls = new SearchControls();
		searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);

		results = dCtx.search(LDAPNames.userContext, filter, searchControls);

		// If matching record found
		if (results.hasMore()) {

			searchResult = (SearchResult) results.next();
			// Build the User DN
			userDN = searchResult.getName() + "," + LDAPNames.userContext;

			// get manager mail
			Attributes attribs = searchResult.getAttributes();
			ArrayList<String> managers = new ArrayList<String>();
			NamingEnumeration values = attribs.get("mail").getAll();
			if (values.hasMore()) {
				return values.next().toString();
			}
		}
		return null;
	}

	public String getManagerMail(String mail) throws Exception {
		for (int i = 0; i < regions.length; i++) {
			String _temp = getManagerMailByMail(regions[i], mail
					+ "@oracle.com");
			if (_temp != null) {
				return _temp;
			}
		}
		return null;
	}

	public String getManagerMailByMail(String region, String mail)
			throws NamingException {

		DirContext dCtx = dCtx = this.getDirectoryContext("l=" + region + ","
				+ LDAPNames.rootContext, "");
		SearchResult searchResult = null;
		NamingEnumeration results = null;
		String userDN = null;
		String filter = "(mail=" + mail + ")";

		// To set search controls to search with subtree scope
		SearchControls searchControls = new SearchControls();
		searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);

		// Search the directory based on the search string from the specified
		// context
		results = dCtx.search(LDAPNames.userContext, filter, searchControls);

		// If matching record found
		if (results.hasMore()) {

			searchResult = (SearchResult) results.next();
			// Build the User DN
			userDN = searchResult.getName() + "," + LDAPNames.userContext;

			// get manager mail
			Attributes attribs = searchResult.getAttributes();
			Attribute attrManager = null;
			attrManager = attribs.get("manager");
			if (attrManager == null) {
				attrManager = attribs.get("orclcorprequestormailid");
			}
			if (attrManager != null) {
				NamingEnumeration values = attrManager.getAll();
				if (values.hasMore()) {
					String _manager = values.next().toString();
					System.out.println(region + " " + _manager);
					for (int u = 0; u < patterns.length; u++) {
						Matcher _mat = patterns[u].matcher(_manager);
						if (_mat.matches()) {
							String _temp = getManagerMail(_mat.group(2),
									_mat.group(1));
							return _temp;
						}
					}
				}
			}

		}
		return null;
	}

	/**
	 * Retrieves the Distinguished name of them of the specified RDN.
	 * 
	 * @param uname
	 *            Relative Distinguished name.
	 * @return Distinguished name of the user
	 * @exception NamingException
	 *                if directory operation fails
	 */
	public String getUserDN(String region, String uname, HttpSession session)
			throws NamingException {

		DirContext dCtx = null;
		if (uname.toUpperCase().endsWith("@ORACLE.COM"))
			uname = uname.substring(0, uname.length() - 11);

		// if Grocery context is available, use it, else create one as
		// application entity
		if (dirctx == null) {
			// dCtx =
			// this.getDirectoryContext("orclApplicationCommonName=GroceryStoreApp,cn=GroceryStore,cn=Products,cn=OracleContext,"+GroceryNames.rootContext,"");
			// dCtx = this.getDirectoryContext("cn=" + uname + "," +
			// GroceryNames.rootContext,"");
			dCtx = this.getDirectoryContext("l=" + region + ","
					+ LDAPNames.rootContext, "");
		} else {
			dCtx = dirctx;
		}

		SearchResult searchResult = null;
		NamingEnumeration results = null;
		String userDN = null;
		String filter = "(" + LDAPNames.RDN + "=" + uname + ")";

		// To set search controls to search with subtree scope
		SearchControls searchControls = new SearchControls();
		searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);

		// Search the directory based on the search string from the specified
		// context
		results = dCtx.search(LDAPNames.userContext, filter, searchControls);

		// If matching record found
		if (results.hasMore()) {

			searchResult = (SearchResult) results.next();
			// Build the User DN
			userDN = searchResult.getName() + "," + LDAPNames.userContext;

			// get manager mail
			Attributes attribs = searchResult.getAttributes();
			ArrayList<String> managers = new ArrayList<String>();

			Attribute attrManager = null;
			attrManager = attribs.get("manager");
			if (attrManager == null) {
				attrManager = attribs.get("orclcorprequestormailid");
			}
			if (attrManager != null) {
				NamingEnumeration values = attrManager.getAll();
				while (values.hasMore()) {
					String _manager = values.next().toString();
					System.out.println(_manager);
					for (int u = 0; u < patterns.length; u++) {
						Matcher _mat = patterns[u].matcher(_manager);
						if (_mat.matches()) {
							String _temp = getManagerMail(_mat.group(2),
									_mat.group(1));
							System.out.println(_temp);
							managers.add(_temp);
						}
					}
				}
			}

			if (session != null)
				session.setAttribute("managers", managers);

		}
		System.out.println(region + " " + uname + "  " + userDN);
		return userDN;

	}

	/**
	 * Initializes a Directory Context with the specified credentials and return
	 * it. If the password is blank(null), it binds as anonymous user and
	 * returns the context.
	 * 
	 * @param username
	 *            Directory user name
	 * @param password
	 *            Directory user password
	 * @return valid directory context, if credentials are valid
	 * @exception AuthenticationException
	 *                if credentails are invalid
	 * @exception NamingException
	 *                if directory operation fails
	 */
	public DirContext getDirectoryContext(String username, String password)
			throws AuthenticationException, NamingException {
		// System.out.println(username);
		DirContext dCtx = null;

		// Build the LDAP url
		String ldapurl = "ldap://" + ConnParams.dirHostName + ":"
				+ ConnParams.dirPort;
		// System.out.println(ldapurl);
		Hashtable env = new Hashtable();
		env.put(Context.INITIAL_CONTEXT_FACTORY,
				"com.sun.jndi.ldap.LdapCtxFactory");
		env.put(Context.PROVIDER_URL, ldapurl);

		// if password is specified, set the credentials
		if (password != null) {
			// if the username is null, which means no such user
			if (username == null) {
				throw new AuthenticationException("no such user");
			}
			env.put(Context.SECURITY_AUTHENTICATION, "simple");
			env.put(Context.SECURITY_PRINCIPAL, username);
			env.put(Context.SECURITY_CREDENTIALS, password);
		}

		// Bind and initialize the Directory context
		dCtx = new InitialDirContext(env);

		return dCtx;
	}

	/**
	 * Searchs the directory under the specified context with specified filter
	 * and returns the search results. The scope is set to one-level.
	 * 
	 * @param ctxname
	 *            Context under which seach has to be done.
	 * @param filter
	 *            Search filter
	 * @return Attributes matching the search specification
	 * @exception NamingException
	 *                if directory search fails
	 */
	public Collection search(String ctxname, String filter)
			throws NamingException {

		SearchResult searchResult = null;
		NamingEnumeration results = null;
		Collection retColl = new ArrayList();
		Attributes attrs = null;

		// Initialize search search controls with one-level scope
		SearchControls searchControls = new SearchControls();
		searchControls.setSearchScope(SearchControls.ONELEVEL_SCOPE);

		// Search the directory based on the search string under the specified
		// context
		results = dirctx.search(ctxname, filter, searchControls);

		// Check if any matching results were found in Directory
		if (results.hasMore()) {

			// Iterate through the results and populate the return collection
			do {

				searchResult = (SearchResult) results.next();
				// Get the attributes
				attrs = searchResult.getAttributes();

				retColl.add(attrs);

			} while (results.hasMore());
		}
		return retColl;
	}

	/**
	 * Creates an entry in Directory with the specified attributes and
	 * objectclass, with the specified Distingushed Name.
	 * 
	 * @param dn
	 *            Distinguished name of the entry to be created
	 * @param objCls
	 *            Object classes that the entry must use
	 * @param map
	 *            Attribute,value mappings of the entry
	 * @exception NamingException
	 *                if adding entry fails
	 * @exception NameAlreadyBoundException
	 *                if user already exists
	 */
	public void addDirectoryEntry(String dn, List objCls, Map map)
			throws NamingException, NameAlreadyBoundException {

		// Create attribute list, ignore case of attribute names
		Attributes attrs = new BasicAttributes(true);

		if (!objCls.isEmpty()) {
			Attribute objclass = new BasicAttribute("objectclass");
			// Iterate thriough the collection and add the object classes to the
			// attribute
			Iterator objclsIter = objCls.iterator();
			while (objclsIter.hasNext()) {
				// Add the object classes
				objclass.add(objclsIter.next());
			}
			// Add the object class attribute to list
			attrs.put(objclass);
		}

		// Iterate through other attributes and add to attributes list
		Iterator attrsIter = map.entrySet().iterator();

		while (attrsIter.hasNext()) {
			Map.Entry attr = (Map.Entry) attrsIter.next();
			attrs.put(new BasicAttribute((String) attr.getKey(), attr
					.getValue()));
		}

		// add the directory entry to the directory with the attributes
		dirctx.createSubcontext(dn, attrs);

	}

	/**
	 * Replaces the specified attributes of an entry.
	 * 
	 * @param dn
	 *            The distinguished name of the entry whose attributes have to
	 *            be modified
	 * @param map
	 *            Attribute,Value pair to be replaced
	 * @exception NamingException
	 *                if directory operation fails, or attribute is not present
	 */
	public void modifyDirectoryEntry(String dn, Map map) throws NamingException {

		// Get the attribute,newvalue mapping
		Iterator attrsIter = map.entrySet().iterator();

		// Construct the attributes list
		Attributes attrs = new BasicAttributes(true); // case-ignore

		while (attrsIter.hasNext()) {
			Map.Entry attr = (Map.Entry) attrsIter.next();
			attrs.put(new BasicAttribute((String) attr.getKey(), attr
					.getValue()));
		}

		// Replace the existing attribute values with the specified new values
		dirctx.modifyAttributes(dn, DirContext.REPLACE_ATTRIBUTE, attrs);

	}

	/**
	 * Adds the specified user to the group.
	 * 
	 * @param uid
	 *            Relative distinguished name of the entry
	 * @param groupdn
	 *            Group to which the user has to be added
	 * @exception NamingException
	 *                if adding to group fails, or user is already a member
	 */
	public void addToGroup(String region, String uid, String groupdn)
			throws NamingException {
		// Build the distinguished name of the entry
		String userdn = this.getUserDN(region, uid, null);

		Attributes attrs = new BasicAttributes(true);

		// The DN of the user has to be added to the uniqueMember attribute of
		// the group
		// to become a member
		attrs.put(new BasicAttribute("uniqueMember", userdn));

		// Add the user as member
		dirctx.modifyAttributes(groupdn, DirContext.ADD_ATTRIBUTE, attrs);
	}

	/**
	 * Removes the specified user from the group.
	 * 
	 * @param uid
	 *            User to be removed from group
	 * @param groupdn
	 *            Group from which the user has to be removed
	 * @exception NamingException
	 *                if directory opertaion fails, or the user is not a memeber
	 *                of this group
	 */
	public void removeFromGroup(String region, String uid, String groupdn)
			throws NamingException {
		// Build the user distinguished name
		String userdn = this.getUserDN(region, uid, null);

		Attributes attrs = new BasicAttributes(true); // case-ignore

		// Remove the user dn from the uniqueMember attribute
		attrs.put(new BasicAttribute("uniqueMember", userdn));

		// Modify the attributes in Directory
		dirctx.modifyAttributes(groupdn, DirContext.REMOVE_ATTRIBUTE, attrs);
	}

	/**
	 * Delete the specified entry from Directory.
	 * 
	 * @param dn
	 *            The distinguished name of the entry to be removed
	 * @exception NamingException
	 *                if entry not found
	 */
	public void deleteDirectoryEntry(String dn) throws NamingException {
		// delete the entry with this DN
		dirctx.destroySubcontext(dn);
	}

}
