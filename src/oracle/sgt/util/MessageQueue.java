package oracle.sgt.util;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * the customs put the data into this queue,
 * and there will be a thread for updating it to db.
 */

public class MessageQueue {

	private static MessageQueue inst = null;

	// the queue size will have no effect except for first time
	public static MessageQueue getInstance(int queueSize) {
		if (inst == null) {
			inst = new MessageQueue(queueSize);
		}
		return inst;
	}
	
	/**
	 * Map<String, Object> the actual buffer type
	 * 			fileId	:	String
	 * 			data	:	List<Map<String,Object>>
	 */
	
	
	private int maxSize,occupiedSize;

	//mapFile saves which file is analyzing
	/**
	 * mapFile's structure
	 * 		key		value
	 * 		fileId	Map		key		value
	 * 						flag	true(occupied)/false
	 * 						data	List<Map<String, Object>>
	 */
	private static Map<String, Map<String, Object>> mapFile=new HashMap<String, Map<String, Object>>();
	public static boolean containsFile(String fileId){
		return mapFile.containsKey(fileId);
	}
	
	private MessageQueue(int maxSize) {
		this.maxSize=maxSize;
		this.occupiedSize=0;
	}
	
	//get fileId which not occupied by any thread
	public String getFreeFile(){
		Set<String> keys=mapFile.keySet();
		//key is fileId
		for (String key : keys) {
			Map<String, Object> map=mapFile.get(key);
			boolean flag=(Boolean) map.get("flag");
			if (flag==false) {
				map.put("flag", true);
				return key;
			}
		}
		//there is no free fileId
		return null;
	}
	
	//msg is Map<String, Object>
	public synchronized void put(Object msg) {
		
		while (maxSize<occupiedSize+1) {
			try {
				wait();
			} catch (InterruptedException ie) {
				
			}
		}
		
		//plus count after putting
		Map<String, Object> data=(Map<String, Object>)msg;
		String fileId=(String) data.get("fileId");
		
		if (mapFile.containsKey(fileId)) {
			Map<String, Object> mapData=mapFile.get(fileId);
			List<Map<String, Object>> dtLst=(List<Map<String, Object>>) mapData.get("data");
			dtLst.add(data);
		}
		else {
			Map<String, Object> mapData=new HashMap<String, Object>();
			mapData.put("flag", false);
			List<Map<String, Object>> dtLst=new LinkedList<Map<String,Object>>();
			dtLst.add(data);
			mapData.put("data", dtLst);
			//put it to mapFile
			mapFile.put(fileId, mapData);
		}
		
		occupiedSize++;
		notifyAll();
	}

	public synchronized Object get(String fileId) {
		
		//if fileId is null, or no key in mapFile, get a new fileId which not occupied
		if (fileId==null||!mapFile.containsKey(fileId)) {
			//fileId points to new file
			while ((fileId=getFreeFile())==null) {
				try {
					wait();
				} catch (InterruptedException ie) {
					
				}
			}
		}
		
		Map<String, Object> mapData=mapFile.get(fileId);
		List<Map<String, Object>> dtLst=(List<Map<String, Object>>) mapData.get("data");
		Object obj=dtLst.get(0);
		dtLst.remove(0);
		if (dtLst.size()==0) {
			mapFile.remove(fileId);
		}
		
		occupiedSize--;
		notifyAll();
		
		return obj;
	}
}