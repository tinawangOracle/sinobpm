package oracle.sgt.util.similarity;

import java.util.ArrayList;
import java.util.List;

public class Similarity3 {
	
	private static String[] letterPairsToStrings(String str)
	{
		int numPairs = str.length()-1;
		String[] pairs = new String[numPairs];
		for(int i = 0; i<numPairs; i++)
		{
			pairs[i] = str.substring(i,i+2);
		}
		
		return pairs;
	}
	
	private static ArrayList<String> letterPairsToList(String str)
	{
		ArrayList<String> allPairs = new ArrayList<String>();
		for(int i = 0; i<str.length()-1;i++)
			allPairs.add(str.substring(i, i+2));
		return allPairs;
	}
	
	private static double SimNotIngoreCase(String s1, String s2)
	{
		ArrayList<String> pairs1 = letterPairsToList(s1);
		ArrayList<String> pairs2 = letterPairsToList(s2);
		int intersection = 0;
		int union = pairs1.size()>pairs2.size()?pairs1.size():pairs2.size();
		for(int i = 0; i<pairs1.size();i++)
		{
			Object pair1 = pairs1.get(i);
			for(int j=0;j<pairs2.size();j++)
			{
				Object pair2 = pairs2.get(j);
				if(pair1.equals(pair2))
				{
					intersection++;
					pairs2.remove(j);
					break;
				}
			}
		}
		
		return (2.0*intersection)/(2*union);	
	}
	
	private static double SimIngoreCase(String s1, String s2)
	{
		ArrayList<String> pairs1 = letterPairsToList(s1.toLowerCase());
		ArrayList<String> pairs2 = letterPairsToList(s2.toLowerCase());
		int intersection = 0;
		int union = pairs1.size()>pairs2.size()?pairs1.size():pairs2.size();
		for(int i = 0; i<pairs1.size();i++)
		{
			Object pair1 = pairs1.get(i);
			for(int j=0;j<pairs2.size();j++)
			{
				Object pair2 = pairs2.get(j);
				if(pair1.equals(pair2))
				{
					intersection++;
					pairs2.remove(j);
					break;
				}
			}
		}
		
		return (2.0*intersection)/(2*union);	
	}
	
	
	
	
	private static double SimIngoreWhiteSpace(String first, String second) {
		if (first == null || first.length() == 0 || second == null
				|| second.length() == 0) {
			return 0;
		}

		String[] firstWords = splitSentence(first);
		String[] secondWords = splitSentence(second);
		
		
		return lcsLength(firstWords, secondWords) * 1.0
				/ (Math.max(firstWords.length, secondWords.length) - 1);
	}

	private static int lcsLength(String[] x, String[] y) {

		int m = x.length;
		int n = y.length;
		
		List<String> list = new ArrayList<String>();
		
		
		int i, j;
		int[][] c = new int[m][n];
		for (i = 1; i < m; i++)
			c[i][0] = 0;
		for (j = 0; j < n; j++)
			c[0][j] = 0;
		for (i = 1; i < m; i++)
			for (j = 1; j < n; j++) {
				if (x[i].equals(y[j])) {
					c[i][j] = c[i - 1][j - 1] + 1;
				} else if (c[i - 1][j] >= c[i][j - 1]) {
					c[i][j] = c[i - 1][j];
				} else {
					c[i][j] = c[i][j - 1];
				}
			}
		return c[m - 1][n - 1];
	}

	private static String[] splitSentence(String sentence) {

		StringBuffer sbWords = new StringBuffer();
		for (int i = 0; i < sentence.length(); i++) {
			char ch = sentence.charAt(i);
			if (Character.isLetter(ch) || Character.isWhitespace(ch))
				sbWords.append(ch);
			//else
			//	sbWords.append(" " + ch);
		}
		sbWords.append(" 1");

		String[] words = sbWords.toString().split(" ");

		List<String> list = new ArrayList<String>();
		list.add(" ");
		for (int i = 0; i < words.length-1; i++) {
			list.add(words[i]);
		}

		return list.toArray(new String[1]);

	}
	
	
	
	
	
	
	public static double Sim(String s1, String s2)
	{
		double similarity = SimNotIngoreCase(s1,s2);
		if(similarity<0.5)
			similarity = SimIngoreCase(s1,s2);
		if(similarity<0.2)
		{
			double similarityTemp = SimIngoreWhiteSpace(s1,s2);
			if(similarityTemp == 0)
				similarity = 0;
		}
		
		return similarity;
	}

}
