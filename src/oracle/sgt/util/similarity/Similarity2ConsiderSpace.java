package oracle.sgt.util.similarity;

import java.util.ArrayList;
import java.util.List;

public class Similarity2ConsiderSpace {

	/**
	 * @param args
	 */
//	public static void main(String[] args) {
//
//		String sentence1 = "Collection frequency is invalid: {0}, it should be a positive integer!";
//		String sentence2 = "Collection frequency is invalid: {0}, it should be a positive integer.";
//
//		System.out.println("sentence1 : " + sentence1);
//		System.out.println("sentence2 : " + sentence2);
//		System.out.println("old : "
//				+ new Similarity().sim(sentence1, sentence2));
//		System.out.println("new : "
//				+ new Similarity2ConsiderSpace().sim(sentence1, sentence2));
//		System.out
//				.println("-------------------------------------------------------------------------------");
//
//		sentence1 = "collection frequency is invalid: {0}, it should be a positive integer.";
//		sentence2 = "Collection frequency is invalid: {0}, it should be a positive integer.";
//
//		System.out.println("sentence1 : " + sentence1);
//		System.out.println("sentence2 : " + sentence2);
//		System.out.println("old : "
//				+ new Similarity().sim(sentence1, sentence2));
//		System.out.println("new : "
//				+ new Similarity2ConsiderSpace().sim(sentence1, sentence2));
//		System.out
//				.println("-------------------------------------------------------------------------------");
//
//		sentence1 = "Collection frequency is invalid: {0}, it should be a positive integer.";
//		sentence2 = "Collection frequency is invalid: {0}, it should be a positive integer?";
//
//		System.out.println("sentence1 : " + sentence1);
//		System.out.println("sentence2 : " + sentence2);
//		System.out.println("old : "
//				+ new Similarity().sim(sentence1, sentence2));
//		System.out.println("new : "
//				+ new Similarity2ConsiderSpace().sim(sentence1, sentence2));
//		System.out
//				.println("-------------------------------------------------------------------------------");
//
//		sentence1 = "frequency Collection is invalid: {0}, it should be a positive integer.";
//		sentence2 = "Collection frequency is invalid: {0}, it should be a positive integer.";
//
//		System.out.println("sentence1 : " + sentence1);
//		System.out.println("sentence2 : " + sentence2);
//		System.out.println("old : "
//				+ new Similarity().sim(sentence1, sentence2));
//		System.out.println("new : "
//				+ new Similarity2ConsiderSpace().sim(sentence1, sentence2));
//		System.out
//				.println("-------------------------------------------------------------------------------");
//
//		sentence1 = "xxx xxx is invalid: {0}, it should be a positive integer.";
//		sentence2 = "Collection frequency is invalid: {0}, it should be a positive integer.";
//
//		System.out.println("sentence1 : " + sentence1);
//		System.out.println("sentence2 : " + sentence2);
//		System.out.println("old : "
//				+ new Similarity().sim(sentence1, sentence2));
//		System.out.println("new : "
//				+ new Similarity2ConsiderSpace().sim(sentence1, sentence2));
//		System.out
//				.println("-------------------------------------------------------------------------------");
//
//		sentence1 = "Copyright 1996, 2011, Oracle and/or its affiliates. All rights reserved. Legal Notices";
//		sentence2 = "The J2EE Server instance is down";
//
//		System.out.println("sentence1 : " + sentence1);
//		System.out.println("sentence2 : " + sentence2);
//		System.out.println("old : "
//				+ new Similarity().sim(sentence1, sentence2));
//		System.out.println("new : "
//				+ new Similarity2ConsiderSpace().sim(sentence1, sentence2));
//		System.out
//				.println("-------------------------------------------------------------------------------");
//		
//		sentence1 = "Copyright 1996, 2011, Oracle and/or its affiliates. All rights reserved. Legal Notices";
//		sentence2 = "The, J2EE, Server/ instance. is. down";
//
//		System.out.println("sentence1 : " + sentence1);
//		System.out.println("sentence2 : " + sentence2);
//		System.out.println("old : "
//				+ new Similarity().sim(sentence1, sentence2));
//		System.out.println("new : "
//				+ new Similarity2ConsiderSpace().sim(sentence1, sentence2));
//		System.out
//				.println("-------------------------------------------------------------------------------");
//		
//		sentence1 = "test test";
//		sentence2 = "test  test";
//
//		System.out.println("sentence1 : " + sentence1);
//		System.out.println("sentence2 : " + sentence2);
//		System.out.println("old : "
//				+ new Similarity().sim(sentence1, sentence2));
//		System.out.println("new : "
//				+ new Similarity2ConsiderSpace().sim(sentence1, sentence2));
//		System.out
//				.println("-------------------------------------------------------------------------------");
//		
//		sentence1 = "test test ";
//		sentence2 = "test test";
//
//		System.out.println("sentence1 : " + sentence1);
//		System.out.println("sentence2 : " + sentence2);
//		System.out.println("old : "
//				+ new Similarity().sim(sentence1, sentence2));
//		System.out.println("new : "
//				+ new Similarity2ConsiderSpace().sim(sentence1, sentence2));
//		System.out
//				.println("-------------------------------------------------------------------------------");
//		
//		sentence1 = " test test";
//		sentence2 = "test test";
//
//		System.out.println("sentence1 : " + sentence1);
//		System.out.println("sentence2 : " + sentence2);
//		System.out.println("old : "
//				+ new Similarity().sim(sentence1, sentence2));
//		System.out.println("new : "
//				+ new Similarity2ConsiderSpace().sim(sentence1, sentence2));
//		System.out
//				.println("-------------------------------------------------------------------------------");
//
//	}

	public double sim(String first, String second) {
		if (first == null || first.length() == 0 || second == null
				|| second.length() == 0) {
			return 0;
		}

		String[] firstWords = splitSentence(first);
		String[] secondWords = splitSentence(second);
		
		
//		
//		for(String word1 : firstWords)
//		{
//			System.out.print(word1 + "|");
//		}
//		System.out.println();
//		for(String word2 : secondWords)
//		{
//			System.out.print(word2 + "|");
//		}
//		System.out.println();
		
		
		
		
		
		return lcsLength(firstWords, secondWords) * 1.0
				/ (Math.max(firstWords.length, secondWords.length) - 1);
	}

	private int lcsLength(String[] x, String[] y) {

		int m = x.length;
		int n = y.length;
		
		List<String> list = new ArrayList<String>();
		
		
		int i, j;
		int[][] c = new int[m][n];
		for (i = 1; i < m; i++)
			c[i][0] = 0;
		for (j = 0; j < n; j++)
			c[0][j] = 0;
		for (i = 1; i < m; i++)
			for (j = 1; j < n; j++) {
				if (x[i].equals(y[j])) {
					c[i][j] = c[i - 1][j - 1] + 1;
				} else if (c[i - 1][j] >= c[i][j - 1]) {
					c[i][j] = c[i - 1][j];
				} else {
					c[i][j] = c[i][j - 1];
				}
			}
		return c[m - 1][n - 1];
	}

	private String[] splitSentence(String sentence) {

		StringBuffer sbWords = new StringBuffer();
		for (int i = 0; i < sentence.length(); i++) {
			char ch = sentence.charAt(i);
			if (Character.isLetter(ch) || Character.isWhitespace(ch))
				sbWords.append(ch);
			else
				sbWords.append(" " + ch);
		}
		sbWords.append(" 1");

		String[] words = sbWords.toString().split(" ");

		List<String> list = new ArrayList<String>();
		list.add(" ");
		for (int i = 0; i < words.length-1; i++) {
			list.add(words[i]);
		}

		return list.toArray(new String[1]);

	}

}
