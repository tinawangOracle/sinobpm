package oracle.sgt.util.similarity;

public class testSim {
	
	public static void main(String[] args) {

		Similarity tool = new Similarity();
		
		
		
		String sentence1 = "Search for Contacts...";
		String sentence2 = "Search For Contacts...";

		System.out.println("sentence1 : " + sentence1);
		System.out.println("sentence2 : " + sentence2);
		System.out.println("Old : "
				+ Similarity3.Sim(sentence1, sentence2));
		System.out.println("New : " + tool.Sim(sentence1, sentence2));
		System.out
				.println("-------------------------------------------------------------------------------");

//		sentence1 = "collection frequency is invalid: {0}, it should be a positive integer.";
//		sentence2 = "Collection frequency is invalid: {0}, it should be a positive integer.";
//
//		System.out.println("sentence1 : " + sentence1);
//		System.out.println("sentence2 : " + sentence2);
//		System.out.println("Old : "
//				+ Similarity3.Sim(sentence1, sentence2));
//		System.out.println("New : " + tool.Sim(sentence1, sentence2));
//		System.out
//				.println("-------------------------------------------------------------------------------");
//		
//		sentence1 = "Collection frequency is invalid: {0}, it should be a positive integer.";
//		sentence2 = "Collection frequency is invalid: {0}, it should be a positive integer?";
//
//		System.out.println("sentence1 : " + sentence1);
//		System.out.println("sentence2 : " + sentence2);
//		System.out.println("Old : "
//				+ Similarity3.Sim(sentence1, sentence2));
//		System.out.println("New : " + tool.Sim(sentence1, sentence2));
//		System.out
//				.println("-------------------------------------------------------------------------------");
//
//		sentence1 = "frequency Collection is invalid: {0}, it should be a positive integer.";
//		sentence2 = "Collection frequency is invalid: {0}, it should be a positive integer.";
//
//		System.out.println("sentence1 : " + sentence1);
//		System.out.println("sentence2 : " + sentence2);
//		System.out.println("Old : "
//				+ Similarity3.Sim(sentence1, sentence2));
//		System.out.println("New : " + tool.Sim(sentence1, sentence2));
//		System.out
//				.println("-------------------------------------------------------------------------------");
//
//		sentence1 = "xxx xxx is invalid: {0}, it should be a positive integer.";
//		sentence2 = "Collection frequency is invalid: {0}, it should be a positive integer.";
//
//		System.out.println("sentence1 : " + sentence1);
//		System.out.println("sentence2 : " + sentence2);
//		System.out.println("Old : "
//				+ Similarity3.Sim(sentence1, sentence2));
//		System.out.println("New : " + tool.Sim(sentence1, sentence2));
//		System.out
//				.println("-------------------------------------------------------------------------------");
//
//		sentence1 = "Copyright 1996, 2011, Oracle and/or its affiliates. All rights reserved. Legal Notices";
//		sentence2 = "The J2EE Server instance is down";
//
//		System.out.println("sentence1 : " + sentence1);
//		System.out.println("sentence2 : " + sentence2);
//		System.out.println("Old : "
//				+ Similarity3.Sim(sentence1, sentence2));
//		System.out.println("New : " + tool.Sim(sentence1, sentence2));
//		System.out
//				.println("-------------------------------------------------------------------------------");
//		
//		sentence1 = "Copyright 1996, 2011, Oracle and/or its affiliates. All rights reserved. Legal Notices";
//		sentence2 = "The, J2EE, Server/ instance. is. down";
//
//		System.out.println("sentence1 : " + sentence1);
//		System.out.println("sentence2 : " + sentence2);
//		System.out.println("Old : "
//				+ Similarity3.Sim(sentence1, sentence2));
//		System.out.println("New : " + tool.Sim(sentence1, sentence2));
//		System.out
//				.println("-------------------------------------------------------------------------------");
//		
//		sentence1 = "Copyright 1996, 2011, Oracle and/or its affiliates. All rights reserved. Legal Notices";
//		sentence2 = "Oracle Copyright. The J2EE Server instance is down";
//
//		System.out.println("sentence1 : " + sentence1);
//		System.out.println("sentence2 : " + sentence2);
//		System.out.println("Old : "
//				+ Similarity3.Sim(sentence1, sentence2));
//		System.out.println("New : " + tool.Sim(sentence1, sentence2));
//		System.out
//				.println("-------------------------------------------------------------------------------");
//		
//		sentence1 = "Copyright 1996, 2011, Oracle and/or its affiliates. All rights reserved. Legal Notices";
//		sentence2 = "Copyright Oracle. The J2EE Server instance is down";
//
//		System.out.println("sentence1 : " + sentence1);
//		System.out.println("sentence2 : " + sentence2);
//		System.out.println("Old : "
//				+ Similarity3.Sim(sentence1, sentence2));
//		System.out.println("New : " + tool.Sim(sentence1, sentence2));
//		System.out
//				.println("-------------------------------------------------------------------------------");
//
//		sentence1 = "Copyright 1996, 2011, Oracle and/or its affiliates. All rights reserved. Legal Notices";
//		sentence2 = "Oracle. The J2EE Server instance is down";
//
//		System.out.println("sentence1 : " + sentence1);
//		System.out.println("sentence2 : " + sentence2);
//		System.out.println("Old : "
//				+ Similarity3.Sim(sentence1, sentence2));
//		System.out.println("New : " + tool.Sim(sentence1, sentence2));
//		System.out
//				.println("-------------------------------------------------------------------------------");
//		
//		sentence1 = "test test";
//		sentence2 = "test  test";
//
//		System.out.println("sentence1 : " + sentence1);
//		System.out.println("sentence2 : " + sentence2);
//		System.out.println("Old : "
//				+ Similarity3.Sim(sentence1, sentence2));
//		System.out.println("New : " + tool.Sim(sentence1, sentence2));
//		System.out
//				.println("-------------------------------------------------------------------------------");
//		
//		sentence1 = "test test ";
//		sentence2 = "test test";
//
//		System.out.println("sentence1 : " + sentence1);
//		System.out.println("sentence2 : " + sentence2);
//		System.out.println("Old : "
//				+ Similarity3.Sim(sentence1, sentence2));
//		System.out.println("New : " + tool.Sim(sentence1, sentence2));
//		System.out
//				.println("-------------------------------------------------------------------------------");
//		
//		sentence1 = " test test";
//		sentence2 = "test test";
//
//		System.out.println("sentence1 : " + sentence1);
//		System.out.println("sentence2 : " + sentence2);
//		System.out.println("Old : "
//				+ Similarity3.Sim(sentence1, sentence2));
//		System.out.println("New : " + tool.Sim(sentence1, sentence2));
//		System.out
//				.println("-------------------------------------------------------------------------------");
//		
//		sentence1 = "default value";
//		sentence2 = "default value:";
//
//		System.out.println("sentence1 : " + sentence1);
//		System.out.println("sentence2 : " + sentence2);
//		System.out.println("Old : "
//				+ Similarity3.Sim(sentence1, sentence2));
//		System.out.println("New : " + tool.Sim(sentence1, sentence2));
//		System.out
//				.println("-------------------------------------------------------------------------------");
//		
//		sentence1 = "default value";
//		sentence2 = "default values";
//
//		System.out.println("sentence1 : " + sentence1);
//		System.out.println("sentence2 : " + sentence2);
//		System.out.println("Old : "
//				+ Similarity3.Sim(sentence1, sentence2));
//		System.out.println("New : " + tool.Sim(sentence1, sentence2));
//		System.out
//				.println("-------------------------------------------------------------------------------");
//		
//		sentence1 = "Project data object";
//		sentence2 = "Project Data Object";
//
//		System.out.println("sentence1 : " + sentence1);
//		System.out.println("sentence2 : " + sentence2);
//		System.out.println("Old : "
//				+ Similarity3.Sim(sentence1, sentence2));
//		System.out.println("New : " + tool.Sim(sentence1, sentence2));
//		System.out
//				.println("-------------------------------------------------------------------------------");
//
//		sentence1 = "Do I?";
//		sentence2 = "I do.";
//
//		System.out.println("sentence1 : " + sentence1);
//		System.out.println("sentence2 : " + sentence2);
//		System.out.println("Old : "
//				+ Similarity3.Sim(sentence1, sentence2));
//		System.out.println("New : " + tool.Sim(sentence1, sentence2));
//		System.out
//				.println("-------------------------------------------------------------------------------");
//
//		sentence1 = "Do I?";
//		sentence2 = "I do?";
//
//		System.out.println("sentence1 : " + sentence1);
//		System.out.println("sentence2 : " + sentence2);
//		System.out.println("Old : "
//				+ Similarity3.Sim(sentence1, sentence2));
//		System.out.println("New : " + tool.Sim(sentence1, sentence2));
//		System.out
//				.println("-------------------------------------------------------------------------------");
//		
//		sentence1 = "I?";
//		sentence2 = "I.";
//
//		System.out.println("sentence1 : " + sentence1);
//		System.out.println("sentence2 : " + sentence2);
//		System.out.println("Old : "
//				+ Similarity3.Sim(sentence1, sentence2));
//		System.out.println("New : " + tool.Sim(sentence1, sentence2));
//		System.out
//				.println("-------------------------------------------------------------------------------");
//		
//		sentence1 = "I";
//		sentence2 = "I";
//
//		System.out.println("sentence1 : " + sentence1);
//		System.out.println("sentence2 : " + sentence2);
//		System.out.println("Old : "
//				+ Similarity3.Sim(sentence1, sentence2));
//		System.out.println("New : " + tool.Sim(sentence1, sentence2));
//		System.out
//				.println("-------------------------------------------------------------------------------");
//		
//		sentence1 = "I";
//		sentence2 = "I wanna 123!";
//
//		System.out.println("sentence1 : " + sentence1);
//		System.out.println("sentence2 : " + sentence2);
//		System.out.println("Old : "
//				+ Similarity3.Sim(sentence1, sentence2));
//		System.out.println("New : " + tool.Sim(sentence1, sentence2));
//		System.out
//				.println("-------------------------------------------------------------------------------");
//	
//		
//		sentence1 = "..";
//		sentence2 = "...";
//
//		System.out.println("sentence1 : " + sentence1);
//		System.out.println("sentence2 : " + sentence2);
//		System.out.println("Old : "
//				+ Similarity3.Sim(sentence1, sentence2));
//		System.out.println("New : " + tool.Sim(sentence1, sentence2));
//		System.out
//				.println("-------------------------------------------------------------------------------");
//		
//		sentence1 = ".";
//		sentence2 = ".";
//
//		System.out.println("sentence1 : " + sentence1);
//		System.out.println("sentence2 : " + sentence2);
//		System.out.println("Old : "
//				+ Similarity3.Sim(sentence1, sentence2));
//		System.out.println("New : " + tool.Sim(sentence1, sentence2));
//		System.out
//				.println("-------------------------------------------------------------------------------");
//		
//		sentence1 = "a a";
//		sentence2 = "a  a";
//
//		System.out.println("sentence1 : " + sentence1);
//		System.out.println("sentence2 : " + sentence2);
//		System.out.println("Old : "
//				+ Similarity3.Sim(sentence1, sentence2));
//		System.out.println("New : " + tool.Sim(sentence1, sentence2));
//		System.out
//				.println("-------------------------------------------------------------------------------");
//		
//		sentence1 = ". .";
//		sentence2 = ".  .";
//
//		System.out.println("sentence1 : " + sentence1);
//		System.out.println("sentence2 : " + sentence2);
//		System.out.println("Old : "
//				+ Similarity3.Sim(sentence1, sentence2));
//		System.out.println("New : " + tool.Sim(sentence1, sentence2));
//		System.out
//				.println("-------------------------------------------------------------------------------");
	}

}
