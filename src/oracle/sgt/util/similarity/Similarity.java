package oracle.sgt.util.similarity;


import java.util.ArrayList;
import java.util.List;


public class Similarity {

	final String placeholderRex = "\\{\\d+\\}";
	final String placeholderReplacement = "{0}";
	
	final String numberRex = "\\d+";
	final String numberReplacement = "0";
	
	final double sameIngorePlaceHolderAndNumber = 0.99;
	final double sameIngoreCase = 0.99;
	final double sameIngoreCasePalceHolderAndNumber = 0.98;
	
	boolean debug = false;
	
	double wordsHeavy = 0.8;
	final double fullInterSection = 1;
	final double ingoreCaseInterSection = 0.9;
	final double ingoreSequenceInterSection = 0.7;
	final double ingoreSequenceCaseInterSection = 0.6;
	final double singleWordsSame = 0.5;
	final double singleWordsIngoreCase = 0.4;
	final double singleCharCompare = 0.3;
	
	double s1wordsHeavyPercent = 0;
	double s2wordsHeavyPercent = 0;
	
	//1.1 统一占位符   eg： {0},{1},{2}...{999}...{99999999} 视为相同
	public String ReplacePlaceholder(String s)
	{
		if(debug)
			System.out.println("ReplacePlaceholder(String s):"+s.replaceAll(placeholderRex, placeholderReplacement));
		return s.replaceAll(placeholderRex, placeholderReplacement);
	}
	public String ReplacePlaceholder(String s, String rex, String replacement)
	{
		if(debug)
			System.out.println("ReplacePlaceholder(String s, String rex, String replacement):"+s.replaceAll(rex, replacement));
		return s.replaceAll(rex, replacement);
	}
	
	//1.2 统一数字，所有数字视为相同
	public String ReplaceNumber(String s)
	{
		if(debug)
			System.out.println("ReplaceNumber(String s):"+s.replaceAll(numberRex, numberReplacement));
		return s.replaceAll(numberRex, numberReplacement);
	}
	
	
	//1.3 如果开头结尾都是引号，则去掉
	public String dropQuo(String s)
	{
		if( (s.startsWith("\"")&&s.endsWith("\"")) || (s.startsWith("\'")&&s.endsWith("\'")))
			return s.substring(1,s.length()-1);
		return s;
	}
	
	//2. 字母和标点之间插入空格并去掉多余空格
	public String insertSpaceBetweenWordsAndPunctuation(String s)
	{
		StringBuffer sbWords = new StringBuffer();
		
		for (int i = 0; i < s.length(); i++){
			char ch = s.charAt(i);
			if (Character.isLetter(ch) || Character.isWhitespace(ch))
				sbWords.append(ch);
			else
			{
//				if(i>0 && Character.isLetter(s.charAt(i-1)))
//				{
//					sbWords.append(" ");
//				}
//				sbWords.append(ch);
//				if(i < (s.length()-1) && Character.isLetter(s.charAt(i+1)))
//				{
//					sbWords.append(" ");
//				}
				if(i!=0)
					sbWords.append(" ");
				sbWords.append(ch);
				sbWords.append(" ");
			}
		}
		
		if(debug)
			System.out.println("insertSpaceBetweenWordsAndPunctuation(String s):"+sbWords.toString().trim().replaceAll(" +", " "));
		
		return sbWords.toString().trim().replaceAll(" +", " ");
	}
	
	
//	// 去处多余空格
//	public String trimUnnecessarySpace(String s)
//	{
//		s = s.trim();
//		s = s.replaceAll(" +", " ");
//		return s;
//	}
	
	
	//3. 拆分字符和标点
	public String[] splitWordsAndPunctuation(String s)
	{
		StringBuffer sbWords = new StringBuffer();
		StringBuffer sbPunctuation = new StringBuffer();
		for (int i = 0; i < s.length(); i++){
			char ch = s.charAt(i);
			if(Character.isWhitespace(ch))
			{
				sbWords.append(ch);
				sbPunctuation.append(ch);
			}
			else if(Character.isLetter(ch))
			{
				sbWords.append(ch);
				sbPunctuation.append('a');
			}
			else
			{
//				if(i!=s.length()-1)
//					sbWords.append('.');
				sbPunctuation.append(ch);
			}
		}
		
		
		if(debug)
		{
			System.out.println("splitWordsAndPunctuation(String s):Words:"+sbWords.toString().replaceAll("\\s{1,}"," "));
			System.out.println("splitWordsAndPunctuation(String s):Punctuation:"+sbPunctuation.toString().replaceAll("a+", "a"));
		}
		
		return new String[]{sbWords.toString().replaceAll("\\s{1,}"," "),sbPunctuation.toString().replaceAll("a+", "a")};
		
	}
	
	
	
	//4. 按空格拆分字符串
	public String[] splitStringToArrayBySpace(String s)
	{
		return s.split(" ");
//		String[] ss = s.split(" ");
//		ArrayList<String> al = new ArrayList<String>();
//		for(String sT : ss)
//		{
//			if(!sT.equals(""))
//				al.add(sT);
//		}
//		return (String[]) al.toArray();
	}
	
	//5. 组合成string数组对List
	public ArrayList<String[]> stringArrayPairsToList(String[] ss, int pairsNumber)
	{
		
		//标点符号记为“”
		for(int i = 0; i<ss.length; i++)
			if(ss[i].length()==1 && !Character.isLetter(ss[i].charAt(0)))
				ss[i]="";
		
		
		ArrayList<String[]> allPairs = new ArrayList<String[]>();
		for(int i = 0; i<ss.length-1; i++ )
			allPairs.add(new String[]{ss[i],ss[i+1]});
		if(pairsNumber>ss.length)
			allPairs.add(new String[]{ss[ss.length-1],""});
		
		
		if(debug)
		{
			for(String[] paris : allPairs)
			{
				System.out.println("Pair: "+paris[0]+": "+paris[1]);
			}
		}
		
		return allPairs;
	}
	
	
	//5.1 字符串数组对完全相同
	public boolean absolutelySameCompare(String[] ss1, String[] ss2)
	{
		if(  !(ss1[0].equals("")||ss1[1].equals("")) &&  (ss1[0].equals(ss2[0]) && ss1[1].equals(ss2[1]))      )
			return true;
		
		return false;
	}
	public double absolutelySame(ArrayList<String[]> pairs1, ArrayList<String[]> pairs2)
	{
		double interSection = 0;
		for(int i = 0; i<pairs1.size();i++)
		{
			String[] pair1 = pairs1.get(i);
			for(int j=0;j<pairs2.size();j++)
			{
				String[] pair2 = pairs2.get(j);
				if(absolutelySameCompare(pair1,pair2))
				{
					interSection+=fullInterSection;
					pairs1.set(i, new String[]{"",""});
					//i--;
					pairs2.set(j, new String[]{"",""});
					break;
				}
			}
		}
		
		if(debug)
			System.out.println("absolutelySame:"+interSection);
		
		return interSection;
	}
	
	
	
	
	//5.2 字符串数组对忽略大小写相同
	public boolean ingoreCaseSameCompare(String[] ss1, String[] ss2)
	{
		if(  !(ss1[0].equals("")||ss1[1].equals("")) &&  (ss1[0].equalsIgnoreCase(ss2[0]) && ss1[1].equalsIgnoreCase(ss2[1]))  )
			return true;
		
		return false;
	}
	public double ingoreCaseSame(ArrayList<String[]> pairs1, ArrayList<String[]> pairs2)
	{
		double interSection = 0;
		for(int i = 0; i<pairs1.size();i++)
		{
			String[] pair1 = pairs1.get(i);
			for(int j=0;j<pairs2.size();j++)
			{
				String[] pair2 = pairs2.get(j);
				if(ingoreCaseSameCompare(pair1,pair2))
				{
					interSection+=ingoreCaseInterSection;
					pairs1.set(i, new String[]{"",""});
					//i--;
					pairs2.set(j, new String[]{"",""});
					break;
				}
			}
		}
		
		if(debug)
			System.out.println("ingoreCaseSame:"+interSection);
		
		return interSection;
	}
	
	//5.3 字符串数组顺序相反相同
	public boolean ingoreSequenceSameCompare(String[] ss1, String[] ss2)
	{
		if(  !(ss1[0].equals("")||ss1[1].equals("")) &&  ( (ss1[0].equals(ss2[1]) && ss1[1].equals(ss2[0])) || (ss1[0].equals(ss2[0]) && ss1[1].equals(ss2[1])) )  )
			return true;
		
		return false;
	}
	public double ingoreSequenceSame(ArrayList<String[]> pairs1, ArrayList<String[]> pairs2)
	{
		double interSection = 0;
		for(int i = 0; i<pairs1.size();i++)
		{
			String[] pair1 = pairs1.get(i);
			for(int j=0;j<pairs2.size();j++)
			{
				String[] pair2 = pairs2.get(j);
				if(ingoreSequenceSameCompare(pair1,pair2))
				{
					interSection+=ingoreSequenceInterSection;
					pairs1.set(i, new String[]{"",""});
					//i--;
					pairs2.set(j, new String[]{"",""});
					break;
				}
			}
		}
		
		if(debug)
			System.out.println("ingoreSequenceSame:"+interSection);
		
		return interSection;
	}
	
	//5.4 字符串数组顺序相反忽略大小写相同
	public boolean ingoreSequenceCaseSameCompare(String[] ss1, String[] ss2)
	{
		if(  !(ss1[0].equals("")||ss1[1].equals("")) &&   ( (ss1[0].equalsIgnoreCase(ss2[1]) && ss1[1].equalsIgnoreCase(ss2[0])) || (ss1[0].equalsIgnoreCase(ss2[0]) && ss1[1].equalsIgnoreCase(ss2[1])) )  )
			return true;
		
		return false;
	}
	public double ingoreSequenceCaseSame(ArrayList<String[]> pairs1, ArrayList<String[]> pairs2)
	{
		double interSection = 0;
		for(int i = 0; i<pairs1.size();i++)
		{
			String[] pair1 = pairs1.get(i);
			for(int j=0;j<pairs2.size();j++)
			{
				String[] pair2 = pairs2.get(j);
				if(ingoreSequenceCaseSameCompare(pair1,pair2))
				{
					interSection+=ingoreSequenceCaseInterSection;
					pairs1.set(i, new String[]{"",""});
					//i--;
					pairs2.set(j, new String[]{"",""});
					break;
				}
			}
		}
		
		if(debug)
			System.out.println("ingoreSequenceCaseSame:"+interSection);
		
		return interSection;
	}
	
	//5.5 单个字符串比较
	public double singleWordsSame(ArrayList<String[]> pairs1, ArrayList<String[]> pairs2)
	{
		
//		if(debug)
//			System.out.println("singleWordsSame Begin");
		
		double interSection = 0;
		for(int i = 0; i<pairs1.size();i++)
		{
			String[] pair1 = pairs1.get(i);
			for(int j=0;j<pairs2.size();j++)
			{
				String[] pair2 = pairs2.get(j);
//				if(debug)
//					System.out.println("singleWordsSame compare for loop");
				if(pair1[0]!="")
					if(pair1[0].equals(pair2[0]))
					{
						interSection+=singleWordsSame;
						pairs1.set(i, new String[]{"",pair1[1]});
						pairs2.set(j, new String[]{"",pair2[1]});
						break;
					}
					else if(pair1[0].equals(pair2[1]))
					{
						interSection+=singleWordsSame;
						pairs1.set(i, new String[]{"",pair1[1]});
						pairs2.set(j, new String[]{pair2[0],""});
						break;
					}
				else if(pair1[1]!="")
					if(pair1[1].equals(pair2[0]))
					{
						interSection+=singleWordsSame;
						pairs1.set(i, new String[]{pair1[0],""});
						//i--;
						pairs2.set(j, new String[]{"",pair2[1]});
						break;
					}
					else if(pair1[1].equals(pair2[1]))
					{
						interSection+=singleWordsSame;
						pairs1.set(i, new String[]{pair1[0],""});
						pairs2.set(j, new String[]{pair2[0],""});
						break;
					}
			}
		}
		
		if(debug)
			System.out.println("singleWordsSame:"+interSection);
		
		return interSection;
		
	}
	
	//5.6单个字符串忽略大小比较
	public double singleWordsIngoreCaseSame(ArrayList<String[]> pairs1, ArrayList<String[]> pairs2)
	{
			
//		if(debug)
//			System.out.println("singleWordsSame Begin");
		
		double interSection = 0;
		for(int i = 0; i<pairs1.size();i++)
		{
			String[] pair1 = pairs1.get(i);
			for(int j=0;j<pairs2.size();j++)
			{
				String[] pair2 = pairs2.get(j);
//				if(debug)
//					System.out.println("singleWordsSame compare for loop");
				if(pair1[0]!="")
					if(pair1[0].equalsIgnoreCase(pair2[0]))
					{
						interSection+=singleWordsSame;
						pairs1.set(i, new String[]{"",pair1[1]});
						pairs2.set(j, new String[]{"",pair2[1]});
						break;
					}
					else if(pair1[0].equalsIgnoreCase(pair2[1]))
					{
						interSection+=singleWordsSame;
						pairs1.set(i, new String[]{"",pair1[1]});
						pairs2.set(j, new String[]{pair2[0],""});
						break;
					}
				else if(pair1[1]!="")
					if(pair1[1].equalsIgnoreCase(pair2[0]))
					{
						interSection+=singleWordsSame;
						pairs1.set(i, new String[]{pair1[0],""});
						//i--;
						pairs2.set(j, new String[]{"",pair2[1]});
						break;
					}
					else if(pair1[1].equalsIgnoreCase(pair2[1]))
					{
						interSection+=singleWordsSame;
						pairs1.set(i, new String[]{pair1[0],""});
						pairs2.set(j, new String[]{pair2[0],""});
						break;
					}
			}
		}
		
		if(debug)
			System.out.println("singleWordsIngoreCaseSame:"+interSection);
		
		return interSection;
		
	}
	
	//5.7单个字母近似度比较
	public char[] splitFromArrayToChar(ArrayList<String[]> al)
	{
		StringBuffer sbWords = new StringBuffer();
		for(String[] ss : al)
		{
			if(ss[0]!="")
				sbWords.append(ss[0]);
			if(ss[1]!="")
				sbWords.append(ss[1]);
		}
		if(debug)
			System.out.println("splitFromArrayToChar:"+sbWords.toString());
		
		return sbWords.toString().toCharArray();
	}
	
	public double charSimilarity(char[] x, char[] y) {

		int m = x.length;
		int n = y.length;
		
		int min = m<n?m:n;
		int union = m>n?m:n;
		
		if(min == 0)
			return 0;
		
		int i, j;
		int[][] c = new int[m][n];
		for (i = 1; i < m; i++)
			c[i][0] = 0;
		for (j = 0; j < n; j++)
			c[0][j] = 0;
		for (i = 1; i < m; i++)
			for (j = 1; j < n; j++) {
				if (x[i]==y[j]) {
					c[i][j] = c[i - 1][j - 1] + 1;
				} else if (c[i - 1][j] >= c[i][j - 1]) {
					c[i][j] = c[i - 1][j];
				} else {
					c[i][j] = c[i][j - 1];
				}
			}
		return c[m-1][n-1]*1.0/(union-1);
	}
	
	public double charSimilarityCompare(ArrayList<String[]> pairs1, ArrayList<String[]> pairs2)
	{
		if(debug)
			System.out.println("charSimilarityCompare:"+charSimilarity(splitFromArrayToChar(pairs1),splitFromArrayToChar(pairs2))*singleCharCompare);
		return charSimilarity(splitFromArrayToChar(pairs1),splitFromArrayToChar(pairs2))*singleCharCompare;
	}
	
//	public boolean singleCharCompare(String[] ss1, String[] ss2)
//	{
//		if(  (!ss1[0].equals(""))||(!ss2[0].equals("")) )
//			return true;
//			
//		return false;
//	}
//	
//	public double singleCharSimilarity(String[] ss1, String[] ss2)
//	{
//		int m = ss1.length;
//		int n = ss2.length;
//		int union = m>n?m:n;
//		
//		if(union==1)
//		{
//			if(ss1[0].equals(ss2[0]))
//				return 1;
//		}
//		
//		int i, j;
//		int[][] c = new int[m][n];
//		for (i = 1; i < m; i++)
//			c[i][0] = 0;
//		for (j = 0; j < n; j++)
//			c[0][j] = 0;
//		for (i = 1; i < m; i++)
//			for (j = 1; j < n; j++) {
//				if (ss1[i].equals(ss2[j])) {
//					c[i][j] = c[i - 1][j - 1] + 1;
//				} else if (c[i - 1][j] >= c[i][j - 1]) {
//					c[i][j] = c[i - 1][j];
//				} else {
//					c[i][j] = c[i][j - 1];
//				}
//			}
//		return c[m - 1][n - 1]*1.0/(union-1);
//	}
//	
//	public double singleCharSimilarity(ArrayList<String[]> pairs1, ArrayList<String[]> pairs2)
//	{
//		double interSection = 0;
//		for(int i = 0; i<pairs1.size();i++)
//		{
//			String[] pair1 = pairs1.get(i);
//			for(int j=0;j<pairs2.size();j++)
//			{
//				String[] pair2 = pairs2.get(j);
//				if(singleCharCompare(pair1,pair2))
//				{
//					if(!(pair1[0]==""||pair2[0]==""))
//					{
//						interSection+=singleCharCompare*singleCharSimilarity(pair1,pair2);
//						pairs1.set(i, new String[]{"",pair1[1]});
//						i--;
//						pairs2.set(j, new String[]{"",pair2[1]});
//						break;
//					}
//					else if(!(pair1[0]==""||pair2[1]==""))
//					{
//						interSection+=singleCharCompare*singleCharSimilarity(pair1,pair2);
//						pairs1.set(i, new String[]{"",pair1[1]});
//						i--;
//						pairs2.set(j, new String[]{pair2[0],""});
//						break;
//					}
//					else if(!(pair1[1]==""||pair2[0]==""))
//					{
//						interSection+=singleCharCompare*singleCharSimilarity(pair1,pair2);
//						pairs1.set(i, new String[]{pair1[0]});
//						i--;
//						pairs2.set(j, new String[]{pair2[0],""});
//						break;
//					}
//					
//					interSection+=singleCharCompare*singleCharSimilarity(pair1,pair2);
//					pairs1.set(i, new String[]{"",""});
//					i--;
//					pairs2.set(j, new String[]{"",""});
//					break;
//				}
//			}
//		}
//		
//		if(debug)
//			System.out.println("singleCharSimilarity:"+interSection);
//		
//		return interSection;
//	}
//	
//	//5.8单个字符串忽略大小写近似度比较
//	public double singleCharIngoreSimilarity(String[] ss1, String[] ss2)
//	{
//		int m = ss1.length;
//		int n = ss2.length;
//		int union = m>n?m:n;
//		
//		if(union==1)
//		{
//			if(ss1[0].equals(ss2[0]))
//				return 1;
//		}
//		
//		int i, j;
//		int[][] c = new int[m][n];
//		for (i = 1; i < m; i++)
//			c[i][0] = 0;
//		for (j = 0; j < n; j++)
//			c[0][j] = 0;
//		for (i = 1; i < m; i++)
//			for (j = 1; j < n; j++) {
//				if (ss1[i].equalsIgnoreCase(ss2[j])) {
//					c[i][j] = c[i - 1][j - 1] + 1;
//				} else if (c[i - 1][j] >= c[i][j - 1]) {
//					c[i][j] = c[i - 1][j];
//				} else {
//					c[i][j] = c[i][j - 1];
//				}
//			}
//		return c[m - 1][n - 1]*1.0/(union-1);
//	}
//	
//	public double singleCharIngoreCaseSimilarity(ArrayList<String[]> pairs1, ArrayList<String[]> pairs2)
//	{
//		double interSection = 0;
//		for(int i = 0; i<pairs1.size();i++)
//		{
//			String[] pair1 = pairs1.get(i);
//			for(int j=0;j<pairs2.size();j++)
//			{
//				String[] pair2 = pairs2.get(j);
//				if(singleCharCompare(pair1,pair2))
//				{
//					interSection+=singleCharCompare*singleCharSimilarity(pair1,pair2);
//					pairs1.set(i, new String[]{"",""});
//					i--;
//					pairs2.set(j, new String[]{"",""});
//					break;
//				}
//			}
//		}
//		
//		if(debug)
//			System.out.println("singleCharIngoreCaseSimilarity:"+interSection);
//		
//		return interSection;
//	}
	
	
	
	//主算法 字母算法
	public double wordsCompareSim(String[] ss1, String[] ss2)
	{
		int union = 0;
		ArrayList<String[]> pairs1 = null;
		ArrayList<String[]> pairs2 = null;
		
		//如果只有一个字符串
		if(ss1.length==0 && ss2.length==0)
			return 1;
		else if(ss1.length==0 || ss2.length==0)
			return 0;
		else if(ss1.length==1 && ss2.length==1)
		{
			if(ss1[0].equals(ss2[0]))
				return 1;
			else if(ss1[0].equalsIgnoreCase(ss2[0]))
				return 0.9;
			else
				return 0;
		}
		else if(ss1.length==1)
		{
			pairs1 = new ArrayList<String[]>();
			pairs1.add(new String[]{ss1[0],""});
			pairs2 = stringArrayPairsToList(ss2,ss1.length);
		}
		else if(ss2.length==1)
		{
			pairs1 = stringArrayPairsToList(ss1,ss2.length);
			pairs2 = new ArrayList<String[]>();
			pairs2.add(new String[]{ss2[0],""});
		}
		else
		{
			pairs1 = stringArrayPairsToList(ss1,ss2.length);
			pairs2 = stringArrayPairsToList(ss2,ss1.length);
		}
		
		if(allNullInPairs(pairs1)&&allNullInPairs(pairs2))
		{
			wordsHeavy = 0;
			return 1;
		}
		
		union = pairs1.size()>pairs2.size()?pairs1.size():pairs2.size();
		
		if(debug)
			System.out.println("Words Union:"+union);
		
		double interSection = absolutelySame(pairs1,pairs2) + ingoreCaseSame(pairs1,pairs2) + ingoreSequenceSame(pairs1,pairs2) + ingoreSequenceCaseSame(pairs1,pairs2) + singleWordsSame(pairs1,pairs2) + singleWordsIngoreCaseSame(pairs1,pairs2) + charSimilarityCompare(pairs1,pairs2);	
		
		return interSection/union;
		
	}
	
	//判断ArrayList里是否全为“”值
	public boolean allNullInPairs(ArrayList<String[]> al)
	{
		boolean allNull = true;
		for(String[] ss : al)
			if(ss[0]!=""||ss[1]!="")
				allNull = false;
		return allNull;
	}
	
	//主算法 标点符号算法
	public double punctuationCompareSim(String[] ss1, String[] ss2) {

		int m = ss1.length;
		int n = ss2.length;
		int union = m>n?m:n;
		
		if(debug)
			System.out.println("Union:"+union);
		
		if(union==1)
		{
			if(ss1[0].equals(ss2[0]))
				return 1;
		}
		
		
		int i, j;
		int[][] c = new int[m][n];
		for (i = 1; i < m; i++)
			c[i][0] = 0;
		for (j = 0; j < n; j++)
			c[0][j] = 0;
		for (i = 1; i < m; i++)
			for (j = 1; j < n; j++) {
				if (ss1[i].equals(ss2[j])) {
					c[i][j] = c[i - 1][j - 1] + 1;
				} else if (c[i - 1][j] >= c[i][j - 1]) {
					c[i][j] = c[i - 1][j];
				} else {
					c[i][j] = c[i][j - 1];
				}
			}
		
		if(debug)
			System.out.println("c[m-1][n-1]:"+c[m - 1][n - 1]);
		
		return c[m - 1][n - 1]*1.0/(union-1);
	}
	
	public double Sim(String s1, String s2)
	{
		s1 = s1.trim();
		s2 = s2.trim();
		
		if(s1.equals(s2))
			return 1;
		
		if(s1.equalsIgnoreCase(s2))
			return sameIngoreCase;
		
		//简单处理字符串1
		s1 = dropQuo(s1);
		s1 = ReplacePlaceholder(s1);
		s1 = ReplaceNumber(ReplacePlaceholder(s1));
		
		//简单处理字符串2
		s2 = dropQuo(s2);
		s2 = ReplacePlaceholder(s2);
		s2 = ReplaceNumber(ReplacePlaceholder(s2));
		
		
		if(s1.equals(s2))
			return sameIngorePlaceHolderAndNumber;
		
		if(s1.equalsIgnoreCase(s2))
			return sameIngoreCasePalceHolderAndNumber;
		
		
		//深入处理字符串1
		s1 = insertSpaceBetweenWordsAndPunctuation(s1);
		String[] ss1 = splitWordsAndPunctuation(s1);
		String[] ss1Words = splitStringToArrayBySpace(ss1[0]);
		String[] ss1Punctuation = splitStringToArrayBySpace(ss1[1]);
		
		if(debug)
		{
			System.out.print("SIM s1 punctuation:");
			for(String s : ss1Punctuation)
			{
				System.out.print(s+" ");
			}
			System.out.println();
		}
		
		
		//深入处理字符串2
		s2 = insertSpaceBetweenWordsAndPunctuation(s2);
		String[] ss2 = splitWordsAndPunctuation(s2);
		String[] ss2Words = splitStringToArrayBySpace(ss2[0]);
		String[] ss2Punctuation = splitStringToArrayBySpace(ss2[1]);
		
		if(debug)
		{
			System.out.print("SIM s2 punctuation:");
			for(String s : ss2Punctuation)
			{
				System.out.print(s+" ");
			}
			System.out.println();
		}
		
		
		
		if(debug)
		{
			System.out.println("WordsCompare:"+wordsCompareSim(ss1Words,ss2Words));
			System.out.println("PunctuationCompare:"+punctuationCompareSim(ss1Punctuation,ss2Punctuation));
		}
		
		double lengthPercent = 1;
		
		if(ss1Words.length>ss2Words.length)
			lengthPercent = (double)ss2Words.length/ss1Words.length;
		else
			lengthPercent = (double)ss1Words.length/ss2Words.length;
			
		
		return (double)Math.round((wordsCompareSim(ss1Words,ss2Words)*wordsHeavy + punctuationCompareSim(ss1Punctuation,ss2Punctuation)*(1-wordsHeavy))*100)/100;
		//return (double)Math.round((wordsCompareSim(ss1Words,ss2Words)*wordsHeavy + punctuationCompareSim(ss1Punctuation,ss2Punctuation)*(1-wordsHeavy))*lengthPercent*100)/100;
	}
}
