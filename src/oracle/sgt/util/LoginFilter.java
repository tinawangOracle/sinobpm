package oracle.sgt.util;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet Filter implementation class LoginFilter
 */
public class LoginFilter implements Filter {

	private FilterConfig filterConfig = null;
	private static String realPath = null;

	public static String getRealPath() {
		return realPath;
	}

	public static void setRealPath(String realPath) {
		LoginFilter.realPath = realPath;
	}

	/**
	 * Default constructor.
	 */
	public LoginFilter() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		this.filterConfig = null;
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		Cookie[] cks = null;
		String email = null;
		HttpSession session = ((HttpServletRequest) request).getSession(false);
		HttpServletResponse _response = (HttpServletResponse) response;
		HttpServletRequest _request = (HttpServletRequest) request;
		String _uri = _request.getRequestURI();
		boolean isLogin = _uri.endsWith("login.action")
				|| _uri.endsWith("logout.action")||_uri.endsWith("register.action");
		System.out.println(_uri);
		if (_uri.contains("/search/")){

			chain.doFilter(request, response);
			return;
		}
		if (!isLogin) {
			if (session == null)// timeout
			{
				cks = _request.getCookies();

				if (cks == null) { // still no cookies
					if (_request.getHeader("x-requested-with") != null
							&& _request.getHeader("x-requested-with")
									.equalsIgnoreCase("XMLHttpRequest"))
						_response.setHeader("sessionstatus", "timeout");
					return;
				} else {
					session = _request.getSession();// create new session
					for (int i = 0; i < cks.length; i++) {
						if (cks[i].getName().equals("email")) {
							email = cks[i].getValue();
							session.setAttribute("email", email);
						}
						if (cks[i].getName().equals("admin")) {
							session.setAttribute("admin", cks[i].getValue()
									.equals("true"));
						}
					}
				}
			}
			Object _obj = session.getAttribute("email");
			
			if (_obj == null) {
				if (_request.getHeader("x-requested-with") != null
						&& _request.getHeader("x-requested-with")
								.equalsIgnoreCase("XMLHttpRequest"))
					_response.setHeader("sessionstatus", "timeout");
			}
		}

		// pass the request along the filter chain
		chain.doFilter(request, response);
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		this.filterConfig = fConfig;
		realPath = filterConfig.getServletContext().getRealPath("/");
		if (realPath == null) {
			System.out
					.println("unable to get the current webapp root. Using '/'. ");
			realPath = "/";
		}
		System.out.println(realPath);
	}

}
