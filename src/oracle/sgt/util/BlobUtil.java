package oracle.sgt.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.OutputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Map;

public class BlobUtil {
	//private Connection conn = null;

	private static Connection initConn() {

		String driver = "oracle.jdbc.OracleDriver";
		String url = "jdbc:oracle:thin:@stsdd20.us.oracle.com:1521:orcl";

		try {
			Class.forName(driver);
			return DriverManager.getConnection(url, "trans", "trans");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static void saveOrUpdate(String fileId, ByteArrayOutputStream cur_data) {
		String sql = "select verify_result from trans_verify_result where file_id="
				+ fileId + " for update";
		Connection conn=null;
		Statement stmt = null;
		ResultSet rst = null;
		try {
			conn=initConn();
			if (conn==null) return;
			stmt = conn.createStatement();

			rst = stmt.executeQuery(sql);
			if (rst.next()) {
				Blob _result = rst.getBlob(1);
				long _start = System.nanoTime();

				OutputStream _out = _result
						.setBinaryStream(_result.length() + 1);
				DataOutputStream dis = new DataOutputStream(_out);
				byte[] raw = cur_data.toByteArray();
				//System.out.println("length: "+raw.length);
				dis.writeInt(raw.length);
				dis.write(raw);
				dis.flush();
				dis.close();
				long _end = System.nanoTime();
				// System.out.println(_result.length());
				System.out.println(_end - _start);
			} else {
				String sql1 = "insert into trans_verify_result values("
						+ fileId + ",empty_blob(),sysdate)";
				stmt.executeUpdate(sql1);
				rst = stmt.executeQuery(sql);
				if (rst.next()) {
					Blob _result = rst.getBlob(1);
					long _start = System.nanoTime();

					OutputStream _out = _result.setBinaryStream(_result
							.length() + 1);
					DataOutputStream dis = new DataOutputStream(_out);
					byte[] raw = cur_data.toByteArray();
					//System.out.println("length: "+raw.length);
					dis.writeInt(raw.length);
					dis.write(raw);
					dis.flush();
					dis.close();
					// _out.flush();
					// _out.close();
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (rst != null)
					rst.close();
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	public static void test2() {
		String sql = "select verify_result from trans_verify_result where file_id=787";
		Connection conn=null;
		Statement stmt;
		ResultSet rst = null;
		try {
			conn=initConn();
			if (conn==null) return;
			stmt = conn.createStatement();

			rst = stmt.executeQuery(sql);
			if (rst.next()) {
				Blob _result = rst.getBlob(1);
				DataInputStream dis = new DataInputStream(
						_result.getBinaryStream());
				try {
					while (true) {
						int recLen = dis.readInt();
						if (recLen <= 0)
							break;
						System.out.println(recLen);
						byte[] raw = new byte[recLen];
						dis.read(raw);
						ByteArrayInputStream bis = new ByteArrayInputStream(raw);
						ObjectInputStream is = new ObjectInputStream(bis);
						List<Map<String, Object>> data = (List<Map<String, Object>>) is
								.readObject();
						System.out.println(data);
						is.close();
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				rst.close();
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	public static void main(String[] args) {
		BlobUtil.initConn();
		BlobUtil.test2();
	}

}
