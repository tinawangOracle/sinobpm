package oracle.sgt.util;

public class Similarity {

	private int cnt=0;
	private int[][] lcsLength(char[] x, char[] y) {
		int m = x.length;
		int n = y.length;
		int i, j;
		int[][] c = new int[m][n];
		int[][] b = new int[m][n];
		for (i = 1; i < m; i++)
			c[i][0] = 0;
		for (j = 0; j < n; j++)
			c[0][j] = 0;
		for (i = 1; i < m; i++)
			for (j = 1; j < n; j++) {
				if (x[i] == y[j]) {
					c[i][j] = c[i - 1][j - 1] + 1;
					b[i][j] = 1;
				} else if (c[i - 1][j] >= c[i][j - 1]) {
					c[i][j] = c[i - 1][j];
					b[i][j] = 2;
				} else {
					c[i][j] = c[i][j - 1];
					b[i][j] = 3;
				}
			}
		//System.out.println(c[m-1][n-1]);
		this.cnt=c[m-1][n-1];
		return b;
	}

	private void printLCS(int[][] b, char[] x, int i, int j) {
		if (i == 0 || j == 0)
			return;
		if (b[i][j] == 1) {
			printLCS(b, x, i - 1, j - 1);
			//System.out.print(x[i] + "\t");
		} else if (b[i][j] == 2)
			printLCS(b, x, i - 1, j);
		else
			printLCS(b, x, i, j - 1);
	}
	
	public String trimPunctuation(String org){
		StringBuffer sb=new StringBuffer();
		for (int i = 0; i < org.length(); i++) {
			char ch=org.charAt(i);
			if(Character.isLetter(ch)||Character.isDigit(ch))
				sb.append(ch);
		}
		return sb.toString();
	}
	
	public double sim(String first, String second){
		if (first==null||first.trim().length()==0||second==null||second.trim().length()==0) {
			return 0;
		}
		String f=trimPunctuation(first.trim().toLowerCase());
		String s=trimPunctuation(second.trim().toLowerCase());
		
		if (f.length()==0||s.length()==0) {
			return 0;
		}
		//init to 0
		this.cnt=0;
		
		char[] x = (" "+f).toCharArray();
		char[] y = (" "+s).toCharArray();
		//get the same count
		lcsLength(x, y);
		
		return this.cnt*1.0/Math.max(f.length(), s.length());
	}
	
	public static void main(String[] args) {
		Similarity lcs = new Similarity();
		System.out.println(lcs.sim("abefxtzgutq", "bexztutgq"));
	}
}