package oracle.sgt.util;

import java.io.IOException;
import java.io.StringWriter;
import java.net.URLDecoder;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.commons.lang.StringUtils;

import java.security.Security;

import oracle.sgt.util.auth.DesCrypto;
import oracle.sgt.util.auth.MAuthenticator;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;

public class MailUtil implements Runnable {
	private String from;
	private String passwd;
	private String to;
	private String cc;
	private String bcc;
	private String subject;
	private String text;
	private boolean SSL;
	private String host;
	private String port;

	private String backHost;

	private String backPort;

	public MailUtil(String host, String port, String backHost, String backPort) {
		this.host = host;
		this.port = port;
		this.backHost = backHost;
		this.backPort = backPort;

	}

	public MailUtil(String _h, String _p, String _f, String _pw, String _to,
			String _cc, String _bcc, String _s, String _text, boolean _ssl) {
		host = _h;
		port = _p;
		from = _f;
		passwd = _pw;
		to = _to;
		cc = _cc;
		bcc = _bcc;
		subject = _s;
		text = _text;
		SSL = _ssl;
		Thread t = new Thread(this, "trans");
		t.start();
	}

	public MailUtil(String type, String machine, String member, String purpose,
			String to, String cc, String bcc, String expiredate) {

	}

	public void start() {

	}

	public void initMessageBody(String path, String vmFile,
			Map<String, String> map) {
		try {
			// String udir=System.getProperty("user.dir");
			// System.out.println(udir);
			String pth = URLDecoder.decode(path);
			Velocity.setProperty(Velocity.FILE_RESOURCE_LOADER_PATH, pth);
			Velocity.setProperty(Velocity.INPUT_ENCODING, "UTF-8");
			Velocity.setProperty(Velocity.OUTPUT_ENCODING, "UTF-8");
			Velocity.init();
			/* next, get the Template */
			Template t = Velocity.getTemplate(vmFile);

			t.setEncoding("UTF-8");
			/* create a context and add data */

			VelocityContext context = new VelocityContext(map);
			/* now render the template into a StringWriter */
			StringWriter writer = new StringWriter();
			t.merge(context, writer);
			/* show the World */
			this.text = writer.toString();
			// System.out.println(this.text);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void run() {
		if (SSL)
			sendMailSSL();
		else
			sendPubMail();

	}

	public void sendMailFailover(String from, String to, String cc,
			String subject) {
		this.from = from;
		this.to = to;
		this.cc = cc;
		this.subject = subject;

		if (!sendPubMail())
			sendMailSSL();
	}

	// plain text, not html mode
	public boolean sendPlainMail() {

		Properties props = System.getProperties();
		props.put("mail.smtp.host", host);
		props.put("mail.smtp.port", port);
		Session session = Session.getDefaultInstance(props, null);
		MimeMessage message = new MimeMessage(session);
		try {
			message.setFrom(new InternetAddress(from));

			if (!StringUtils.isEmpty(cc))
				message.addRecipient(javax.mail.Message.RecipientType.CC,
						new InternetAddress(cc));
			if (!StringUtils.isEmpty(to)) {
				String[] _tmp = to.split(",");
				for (String _t : _tmp)
					if (_t.toUpperCase().endsWith("@ORACLE.COM"))
						message.addRecipient(Message.RecipientType.TO,
								new InternetAddress(_t));
					else
						message.addRecipient(Message.RecipientType.TO,
								new InternetAddress(_t + "@oracle.com"));
			}
			message.setSubject(subject);
			message.setText(text);
			Transport.send(message);
		} catch (MessagingException e4) {
			e4.printStackTrace();
			return false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public boolean sendMailSSL() {
		Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
		final String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";
		// Get a Properties object
		Properties props = new Properties();
		props.setProperty("mail.smtp.host", backHost);
		props.setProperty("mail.smtp.socketFactory.class", SSL_FACTORY);
		props.setProperty("mail.smtp.socketFactory.fallback", "false");
		props.setProperty("mail.smtp.port", backPort);
		props.setProperty("mail.smtp.socketFactory.port", backPort);
		props.put("mail.smtp.auth", "true");

		Session session = null;
		session = Session.getInstance(props,
				new MAuthenticator("tina.wang@oracle.com", DesCrypto
						.getInstance().decrypt("J3OlQ2r2iNP98uF0SSki+A==")));
		return send(session, subject, text, from, to, cc);
	}

	private boolean send(Session session, String subject, String text,
			String from, String to, String cc) {
		try {
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(from));
			if (StringUtils.isEmpty(to))
				message.addRecipient(javax.mail.Message.RecipientType.TO,
						new InternetAddress(from));
			else {
				String[] _tos = to.split(",");
				for (String _to : _tos)
					if (_to.toUpperCase().endsWith("@ORACLE.COM"))
						message.addRecipient(
								javax.mail.Message.RecipientType.TO,
								new InternetAddress(_to));
					else
						message.addRecipient(
								javax.mail.Message.RecipientType.TO,
								new InternetAddress(_to + "@oracle.com"));
			}
			if (!StringUtils.isEmpty(cc)) {
				String[] _ccs = cc.split(",");
				for (String _cc : _ccs)
					if (_cc.toUpperCase().endsWith("@ORACLE.COM"))
						message.addRecipient(
								javax.mail.Message.RecipientType.CC,
								new InternetAddress(_cc));
					else
						message.addRecipient(
								javax.mail.Message.RecipientType.CC,
								new InternetAddress(_cc + "@oracle.com"));
			}
			if (!StringUtils.isEmpty(bcc))
				if (bcc.toUpperCase().endsWith("@ORACLE.COM"))
					message.addRecipient(Message.RecipientType.BCC,
							new InternetAddress(bcc));
				else
					message.addRecipient(Message.RecipientType.BCC,
							new InternetAddress(bcc + "@oracle.com"));

			message.setSubject(subject);

			BodyPart messageBodyPart = new MimeBodyPart();
			messageBodyPart.setContent(text, "text/html; charset=utf-8");

			MimeMultipart multipart = new MimeMultipart();
			multipart.addBodyPart(messageBodyPart);

			message.setContent(multipart);
			// message.setText(text);
			Transport.send(message);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public boolean sendMail(String from, String to, String cc, String subject,
			String text) {

		System.out.println(host);
		Properties props = new Properties();
		props.put("mail.smtp.host", host);
		props.put("mail.smtp.port", port);
		props.put("mail.smtp.auth", "false");
		Session session = Session.getDefaultInstance(props, null);
		return send(session, subject, text, from, to, cc);
	}

	public boolean sendPubMail() {

		Properties props = System.getProperties();
		props.put("mail.smtp.host", host);
		props.put("mail.smtp.port", port);
		Session session = Session.getDefaultInstance(props, null);
		return send(session, subject, text, from, to, cc);
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getFrom() {
		return from;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getTo() {
		return to;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getSubject() {
		return subject;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getText() {
		return text;
	}

	public void setCc(String cc) {
		this.cc = cc;
	}

	public String getCc() {
		return cc;
	}

	public void setSSL(boolean SSL) {
		this.SSL = SSL;
	}

	public boolean isSSL() {
		return SSL;
	}

	public String getBackHost() {
		return backHost;
	}

	public void setBackHost(String backHost) {
		this.backHost = backHost;
	}

	public String getBackPort() {
		return backPort;
	}

	public void setBackPort(String backPort) {
		this.backPort = backPort;
	}
}
