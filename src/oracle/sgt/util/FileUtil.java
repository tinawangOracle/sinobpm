package oracle.sgt.util;

import java.net.InetAddress;
import java.net.URL;
import java.net.UnknownHostException;

public class FileUtil {
    public FileUtil() {
        super();
    }

    /**
     *   get   resource   file   path   in   package
     *   @param   packageResource   package   resource   relative   path
     *   @return   String   package   resource   file   path
     *   */
    public static String getPackageResourcePath(String packageResource) {
        if (packageResource == null)
            return null;

        if (packageResource.startsWith("\\") ||
            packageResource.startsWith("/"))
            packageResource = packageResource.substring(1);

        if (packageResource.endsWith("\\") || packageResource.endsWith("/"))
            packageResource =
                    packageResource.substring(0, packageResource.length() - 1);

        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        return loader.getResource(packageResource).getFile().replaceAll("%20",
                                                                        "   ");
    }

    /**
     *   get   resource   file   path   in   package
     *   @param   packageResource   package   resource   relative   path
     *   @return   String   package   resource   file   path
     *   */
    public static URL getPackageResourceUrl(String packageResource) {
        if (packageResource == null)
            return null;

        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        return loader.getResource(packageResource);
    }

    public static  String getHostName() {
        try {
            InetAddress addr = InetAddress.getLocalHost();
            byte[] ipAddr = addr.getAddress();
            
            String hostname = addr.getCanonicalHostName();
            return hostname;
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        return null;
    }
}
