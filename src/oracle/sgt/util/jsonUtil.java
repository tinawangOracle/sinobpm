package oracle.sgt.util;

import java.util.ArrayList;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class jsonUtil {

	public static ArrayList<String> getSortDirForExtjsSorting(String src) {
		StringBuffer _sort, _dir;
		if (src != null) {
			JSONArray _array = JSONArray.fromObject(src);
			int _cnt = _array.size();
			_sort = new StringBuffer();
			_dir = new StringBuffer();
			for (int i = 0; i < _cnt; i++) {
				JSONObject _obj = (JSONObject) _array.get(i);
				_sort.append(_obj.getString("property")).append(",");
				_dir.append(_obj.getString("direction")).append(",");

			}
			ArrayList<String> _list = new ArrayList<String>(2);
			_list.add(_sort.toString());
			_list.add(_dir.toString());
			return _list;
		}
		return null;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
