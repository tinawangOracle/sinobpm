package oracle.sgt.util;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;


import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFPrintSetup;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.CellRangeAddress;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.*;

public class XLSExport {

	// for ansi char
	// private static short XLS_ENCODING = HSSFWorkbook.ENCODING_UTF_16;
	// date format
	private static String DATE_FORMAT = " m/d/yy "; // "m/d/yy h:mm"
	// float format
	private static String NUMBER_FORMAT = " #,##0.00 ";
	private String xlsFileName;
	private String trueName, fileContact;

	public String getFileContact() {
		return fileContact;
	}

	public void setFileContact(String fileContact) {
		this.fileContact = fileContact;
	}

	public String getTrueName() {
		return trueName;
	}

	public void setTrueName(String trueName) {
		this.trueName = trueName;
	}

	private HSSFWorkbook workbook;
	private HSSFSheet sheet, sheet0;
	private HSSFRow row;
	private static short _backColor = -1;

	private HSSFCellStyle redStyle, yellowStyle, blueStyle, normalStyle,
			normalPStyle, headerStyle, whiteStyle, redPStyle, yellowPStyle;
	private HashMap<String, CellStyle> styles = null;
	private HashMap<String, HSSFFont> _fonts = null;
	private static final String[] titles = { "File Name", "Author",
			"Row Count", "Row Count with Similarity==100%",
			"Row Count with Similarity>=90%", "Row Count with Similarity>=80%",
			"Row Count with Similarity>=70%", "Row Count with Similarity<70%",
			"Row Count with Similarity=0%", "Row Count Has T13Y Errors&Warns",
			"Row Count Has T13Y Errors", "Row Count has T13Y Warnings" };
	private static String[] keys = { "key", "arrayFlag", "dupFlag", "svalue","resStatus",
			"similarity", "tvalue", "filepath", "t13y" };
	String[] lbls = { "Key", "Array Flag", "Dup Flag", "Value", "Res Status","Percent",
			"Similar Value", "File Path", "Severity", "Rule Name",
			"Rule Suggestion" };
	private int _t13yb,_t13ye, _t13yw, _rowcnt = 0;
	
	public int get_t13yb() {
		return _t13yb;
	}

	public void set_t13yb(int _t13yb) {
		this._t13yb = _t13yb;
	}

	private int[] _sims = new int[11];

	public int get_t13ye() {
		return _t13ye;
	}

	public void set_t13ye(int _t13ye) {
		this._t13ye = _t13ye;
	}

	public int get_t13yw() {
		return _t13yw;
	}

	public void set_t13yw(int _t13yw) {
		this._t13yw = _t13yw;
	}

	// for output to file
	public XLSExport(String fileName) {
		this.xlsFileName = fileName;
		this.workbook = new HSSFWorkbook();
		styles = createStyles(workbook);
		initStyles();
		createSummerySheet();
		createDetailSheet();
	}

	// for return output stream
	public XLSExport() {
		this.workbook = new HSSFWorkbook();
		styles = createStyles(workbook);
		this.sheet = workbook.createSheet();
		HSSFFont font = workbook.createFont();
		createSummerySheet();
		font.setFontHeight((short) 10);
		// this.sheet.setDisplayGridlines(true);

		initStyles();
		createRow(_rowcnt++);
		for (int i = 0; i < keys.length; i++) {
			setHeaderCell(i, lbls[i]);
		}
	}

	private void createDetailSheet() {
		sheet = workbook.createSheet("Report");
		for (int i = 0; i < lbls.length; i++) {
			sheet.setDefaultColumnStyle(i, whiteStyle);
		}

		// the first row
		createRow(_rowcnt++);
		int _idx = lbls.length - 6;
		setHeaderCell(_idx, null, 3);
		// row.getCell(_idx).getCellStyle()
		// .setBorderRight(HSSFCellStyle.BORDER_NONE);
		setHeaderCell(++_idx, "Similarity Status", 4);
		// row.getCell(_idx).getCellStyle()
		// .setBorderRight(HSSFCellStyle.BORDER_NONE);
		// row.getCell(_idx).getCellStyle()
		// .setBorderLeft(HSSFCellStyle.BORDER_NONE);
		setHeaderCell(++_idx, null, 2);
		// row.getCell(_idx).getCellStyle()
		// .setBorderLeft(HSSFCellStyle.BORDER_NONE);
		setHeaderCell(++_idx, null, 3);
		// row.getCell(_idx).getCellStyle()
		// .setBorderRight(HSSFCellStyle.BORDER_NONE);
		setHeaderCell(++_idx, "T13y Status", 4);
		// row.getCell(_idx).getCellStyle()
		// .setBorderRight(HSSFCellStyle.BORDER_NONE);
		// row.getCell(_idx).getCellStyle()
		// .setBorderLeft(HSSFCellStyle.BORDER_NONE);
		setHeaderCell(++_idx, null, 2);
		// row.getCell(_idx).getCellStyle()
		// .setBorderLeft(HSSFCellStyle.BORDER_NONE);
		for (int i = 0; i < lbls.length - 6; i++) {
			setHeaderCell(i, lbls[i], 0);
			// row.getCell(i).getCellStyle()
			// .setBorderBottom(HSSFCellStyle.BORDER_NONE);
		}

		createRow(_rowcnt++);
		// set background color
		for (int i = 0; i < lbls.length - 6; i++) {
			setHeaderCell(i, null, 1);
			// row.getCell(i).getCellStyle()
			// .setBorderTop(HSSFCellStyle.BORDER_NONE);
		}
		for (int i = lbls.length - 6; i < lbls.length; i++) {
			setHeaderCell(i, lbls[i], -1);
		}

		sheet.createFreezePane(0, 2, 0, 2);
		for (int i = 0; i < 11; i++)
			_sims[i] = 0;
	}

	private void createSummerySheet() {
		sheet0 = workbook.createSheet("Summary");
		PrintSetup printSetup = sheet0.getPrintSetup();
		printSetup.setLandscape(true);
		sheet0.setFitToPage(true);
		sheet0.setHorizontallyCenter(true);

		// title row
		Row titleRow = sheet0.createRow(0);
		titleRow.setHeightInPoints(45);
		Cell titleCell = titleRow.createCell(0);
		titleCell.setCellValue("File Health Report");
		titleCell.setCellStyle(styles.get("title"));
		sheet0.addMergedRegion(CellRangeAddress.valueOf("$A$1:$L$1"));

		for (int i = 0; i < titles.length; i++) {
			Row _row = sheet0.createRow(i + 1);
			Cell headerCell = _row.createCell(0);
			headerCell.setCellValue(titles[i]);
			headerCell.setCellStyle(styles.get("header"));
			_row.setHeightInPoints(20);
		}

		// finally set column widths, the width is measured in units of 1/256th
		// of a character width
		sheet0.setColumnWidth(0, 50 * 256); // 30 characters wide

	}

	public void addSummary(int realCnt) {

		Cell _c = sheet0.getRow(10).createCell(1);
		_c.setCellValue(_t13yb);
		_c = sheet0.getRow(11).createCell(1);
		_c.setCellValue(_t13ye);
		_c = sheet0.getRow(12).createCell(1);
		_c.setCellValue(_t13yw);

		_c = sheet0.getRow(3).createCell(1);
		_c.setCellValue(realCnt);
		_c = sheet0.getRow(1).createCell(1);
		_c.setCellValue(trueName);
		_c = sheet0.getRow(2).createCell(1);
		_c.setCellValue(fileContact);

		for (int i = 4; i < 8; i++) {
			_c = sheet0.getRow(i).createCell(1);
			_c.setCellValue(_sims[4 + (10 - i)]);
		}
		int _less70 = 0;
		for (int i = 1; i < 7; i++)
			_less70 += _sims[i];

		_c = sheet0.getRow(8).createCell(1);
		_c.setCellValue(_less70);
		_c = sheet0.getRow(9).createCell(1);
		_c.setCellValue(_sims[0]);
	}

	public void initStyles() {
		// HSSFDataFormat format = workbook.createDataFormat();

		redStyle = workbook.createCellStyle();
		setBorder(redStyle, 1);
		redStyle.setFillForegroundColor(HSSFColor.RED.index);

		yellowStyle = workbook.createCellStyle();
		setBorder(yellowStyle, 1);
		yellowStyle.setFillForegroundColor(HSSFColor.YELLOW.index);

		blueStyle = workbook.createCellStyle();
		setBorder(blueStyle, 1);
		blueStyle.setFillForegroundColor(HSSFColor.BLUE_GREY.index);

		whiteStyle = workbook.createCellStyle();
		setBorder(whiteStyle, 1);
		whiteStyle.setFillForegroundColor(HSSFColor.WHITE.index);

		normalStyle = workbook.createCellStyle();
		setBorder(normalStyle, 0);
		// normalStyle.setFillForegroundColor(HSSFColor.WHITE.index);

		normalPStyle = workbook.createCellStyle();
		setBorder(normalPStyle, 0);
		normalPStyle.setDataFormat(HSSFDataFormat.getBuiltinFormat("0.00%"));
		// normalPStyle.setFillForegroundColor(HSSFColor.WHITE.index);

		headerStyle = workbook.createCellStyle();
		setBorder(headerStyle, 0);
		HSSFFont font = workbook.createFont();
		font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
		headerStyle.setFont(font);

		redPStyle = workbook.createCellStyle();
		setBorder(redPStyle, 1);
		redPStyle.setFillForegroundColor(HSSFColor.RED.index);
		// redPStyle.setDataFormat(format.getFormat(NUMBER_FORMAT));
		redPStyle.setDataFormat(HSSFDataFormat.getBuiltinFormat("0.00%"));

		yellowPStyle = workbook.createCellStyle();
		setBorder(yellowPStyle, 1);
		yellowPStyle.setFillForegroundColor(HSSFColor.YELLOW.index);
		// yellowPStyle.setDataFormat(format.getFormat(NUMBER_FORMAT));
		yellowPStyle.setDataFormat(HSSFDataFormat.getBuiltinFormat("0.00%"));

		_fonts = new HashMap<String, HSSFFont>(2);

		HSSFFont t13y_red = workbook.createFont();
		t13y_red.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
		t13y_red.setColor(HSSFFont.COLOR_RED);
		_fonts.put("t13yr", t13y_red);
		HSSFFont t13y_blue = workbook.createFont();
		t13y_blue.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
		t13y_blue.setColor(HSSFColor.BLUE_GREY.index);
		_fonts.put("t13yb", t13y_blue);
	}

	public void setWidth(int[] wdt) {
		for (short i = 0; i < wdt.length; i++) {
			this.sheet.setColumnWidth(i, (short) (35.7 * wdt[i]));
		}

		sheet.getRow(2).setHeightInPoints(sheet.getDefaultRowHeightInPoints());
	}

	// output to file
	public void exportXLSToFile() throws FileNotFoundException {
		try {
			if (xlsFileName == null) {
				throw new FileNotFoundException();
			}
			FileOutputStream fOut = new FileOutputStream(xlsFileName);
			workbook.write(fOut);
			fOut.flush();
			fOut.close();
		} catch (IOException e) {
			System.out.println("Error for writing excel!");
		}
	}

	public void exportXLSToStream(OutputStream stream) {
		if (stream == null) {
			return;
		}
		try {
			workbook.write(stream);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void createRow(int index) {
		this.row = this.sheet.createRow(index);
		// row.setHeight((short) (0xFF | 0x8000));

	}

	private void setBorder(HSSFCellStyle cellStyle, int _f, int mode) {
		// 0=bottom, 1=top, 2=left, 3=right,4=all
		if (mode != 0)
			cellStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
		if (mode != 2 && mode != 4)
			cellStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
		if (mode != 3 && mode != 4)
			cellStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
		if (mode != 1)
			cellStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
		if (_f == 1)
			cellStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
		cellStyle.setWrapText(true);
	}

	private void setBorder(HSSFCellStyle cellStyle, int _f) {

		cellStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
		cellStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
		cellStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
		cellStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
		if (_f == 1)
			cellStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
		cellStyle.setWrapText(true);
	}

	public void setPlainCell(int index, String value, int _s, int _e) {
		HSSFCell cell = this.row.createCell((short) index);
		cell.setCellType(HSSFCell.CELL_TYPE_STRING);
		if (_s == -1) {
			HSSFCellStyle cellStyle = whiteStyle;
			// if (index > 0) {
			// if (value.startsWith("ERROR")) {
			// cellStyle = blueStyle;
			// } else {
			// cellStyle = whiteStyle;
			// }
			// }
			cell.setCellStyle(cellStyle);
			// cell.setEncoding(XLS_ENCODING);
			cell.setCellValue(value);
		} else {
			HSSFRichTextString richString = new HSSFRichTextString(value);
			cell.setCellStyle(whiteStyle);
			richString.applyFont(_s, _e, _fonts.get("t13yr"));

			cell.setCellValue(richString);
		}
	}

	public void setCell(int index, String value) {
		HSSFCell cell = this.row.createCell((short) index);
		cell.setCellType(HSSFCell.CELL_TYPE_STRING);
		// HSSFCellStyle cellStyle = workbook.createCellStyle();
		// setBorder(cellStyle);
		HSSFCellStyle cellStyle = whiteStyle;
		if (index > 0) {
			if (_backColor > -1) {
				if (_backColor == HSSFColor.RED.index)
					cellStyle = redStyle;
				else if (_backColor == HSSFColor.YELLOW.index)
					cellStyle = yellowStyle;

			}
		}
		cell.setCellStyle(cellStyle);
		// cell.setEncoding(XLS_ENCODING);
		cell.setCellValue(value);
	}

	public void setHeaderCell(int index, String value, int mode) {
		HSSFCell cell = this.row.createCell((short) index);
		cell.setCellType(HSSFCell.CELL_TYPE_STRING);
		// cell.setCellStyle(headerStyle);
		switch (mode) {
		case 0:
			cell.setCellStyle(styles.get("header1_b"));
			break;
		case 1:
			cell.setCellStyle(styles.get("header1_t"));
			break;
		case 2:
			cell.setCellStyle(styles.get("header1_l"));
			break;
		case 3:
			cell.setCellStyle(styles.get("header1_r"));
			break;
		case 4:
			cell.setCellStyle(styles.get("header1_a"));
			break;
		default:
			cell.setCellStyle(styles.get("header1"));

		}
		// cell.setEncoding(XLS_ENCODING);
		cell.setCellValue(value);
	}

	public void setHeaderCell(int index, String value) {
		HSSFCell cell = this.row.createCell((short) index);
		cell.setCellType(HSSFCell.CELL_TYPE_STRING);
		// cell.setCellStyle(headerStyle);
		cell.setCellStyle(styles.get("header1"));
		// cell.setEncoding(XLS_ENCODING);
		cell.setCellValue(value);
	}

	public void setCell(int index, String value, double similarity) {
		HSSFCell cell = this.row.createCell((short) index);
		cell.setCellType(HSSFCell.CELL_TYPE_STRING);
		cell.setCellValue(value);

		// HSSFCellStyle cellStyle = workbook.createCellStyle();
		// HSSFDataFormat format = workbook.createDataFormat();
		// cellStyle.setDataFormat(format.getFormat(NUMBER_FORMAT));
		// setBorder(cellStyle);
		HSSFCellStyle cellStyle = whiteStyle;
		if (similarity >= 0.8) {
			if (similarity >= 0.9) {
				_backColor = HSSFColor.RED.index;
				cellStyle = redStyle;
				// System.out.println(similarity+"======================");
			} else {
				_backColor = HSSFColor.YELLOW.index;
				cellStyle = yellowStyle;
			}
		}
		cell.setCellStyle(cellStyle);
	}

	public void setCell(int index, Calendar value) {
		HSSFCell cell = this.row.createCell((short) index);
		// cell.setEncoding(XLS_ENCODING);
		cell.setCellValue(value.getTime());
		// for new cell style
		HSSFCellStyle cellStyle = workbook.createCellStyle();
		cellStyle.setDataFormat(HSSFDataFormat.getBuiltinFormat(DATE_FORMAT));
		cell.setCellStyle(cellStyle);
	}

	public void setCell(int index, long value) {
		HSSFCell cell = this.row.createCell((short) index);
		cell.setCellType(HSSFCell.CELL_TYPE_NUMERIC);
		// HSSFCellStyle cellStyle = workbook.createCellStyle();
		// HSSFDataFormat format = workbook.createDataFormat();
		cell.setCellStyle(normalStyle);
		cell.setCellValue(value);
	}

	public void setCell(int index, double value) {
		HSSFCell cell = this.row.createCell((short) index);
		cell.setCellType(HSSFCell.CELL_TYPE_NUMERIC);
		cell.setCellValue(value);
		// HSSFCellStyle cellStyle = workbook.createCellStyle();
		// HSSFDataFormat format = workbook.createDataFormat();
		// setBorder(cellStyle);
		_sims[(int) Math.floor(value * 10)]++;
		HSSFCellStyle cellStyle = normalPStyle;
		if (_backColor > -1) {
			if (_backColor == HSSFColor.RED.index)
				cellStyle = redPStyle;
			else if (_backColor == HSSFColor.YELLOW.index)
				cellStyle = yellowPStyle;

		}
		// cellStyle.setDataFormat(format.getFormat(NUMBER_FORMAT));

		cell.setCellStyle(cellStyle);

	}

	

	public static void main(String[] args) {
		System.out.println(" 开始导出Excel文件 ");
		XLSExport e = new XLSExport("c:/lib/test.xls");
		// e.createRow(0);
		// e.setCell(0, " 编号 ");
		// e.setCell(1, " 名称 ");
		// e.setCell(2, " 日期 ");
		// e.setCell(3, " 金额 ");
		// e.createRow(1);
		// e.setCell(0, 1);
		// e.setCell(1, " 工商银行 ");
		// e.setCell(2, Calendar.getInstance());
		// e.setCell(3, 111123.99);
		// e.createRow(2);
		// e.setCell(0, 2);
		// e.setCell(1, " 招商银行 ");
		// e.setCell(2, Calendar.getInstance());
		// e.setCell(3, 222456.88);
		// e.setCell(4,
		// "Hello, World!dfdsfdsdsfdsfdsfdsfdsfdsfdsfdsfdsfdsfdsfdsfdsf");
		int[] odx = { 40, 40, 40, 70 };
		// e.setWidth(odx);
		try {
			e.exportXLSToFile();
			System.out.println(" 导出Excel文件[成功] ");
		} catch (Exception e1) {
			System.out.println(" 导出Excel文件[失败] ");
			e1.printStackTrace();
		}
	}

	/**
	 * Create a library of cell styles
	 */
	private HashMap<String, CellStyle> createStyles(HSSFWorkbook wb) {
		HashMap<String, CellStyle> styles = new HashMap<String, CellStyle>();
		HSSFCellStyle style;
		Font titleFont = wb.createFont();
		titleFont.setFontHeightInPoints((short) 18);
		titleFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
		style = wb.createCellStyle();
		style.setAlignment(CellStyle.ALIGN_CENTER);
		style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		style.setFont(titleFont);
		styles.put("title", style);

		Font monthFont = wb.createFont();
		monthFont.setFontHeightInPoints((short) 10);
		monthFont.setColor(IndexedColors.WHITE.getIndex());
		// header in first sheet
		style = wb.createCellStyle();
		style.setAlignment(CellStyle.ALIGN_RIGHT);
		style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		style.setFillForegroundColor(IndexedColors.GREY_50_PERCENT.getIndex());
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style.setFont(monthFont);
		style.setWrapText(true);
		styles.put("header", style);

		// header in second sheet
		style = wb.createCellStyle();
		style.setAlignment(CellStyle.ALIGN_CENTER);
		style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		style.setFillForegroundColor(IndexedColors.GREY_50_PERCENT.getIndex());
		setBorder(style, 1);
		style.setFont(monthFont);
		styles.put("header1", style);

		style = wb.createCellStyle();
		style.setAlignment(CellStyle.ALIGN_CENTER);
		style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		style.setFillForegroundColor(IndexedColors.GREY_50_PERCENT.getIndex());
		setBorder(style, 1, 0);
		style.setFont(monthFont);
		styles.put("header1_b", style);

		style = wb.createCellStyle();
		style.setAlignment(CellStyle.ALIGN_CENTER);
		style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		style.setFillForegroundColor(IndexedColors.GREY_50_PERCENT.getIndex());
		setBorder(style, 1, 1);
		style.setFont(monthFont);
		styles.put("header1_t", style);

		style = wb.createCellStyle();
		style.setAlignment(CellStyle.ALIGN_CENTER);
		style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		style.setFillForegroundColor(IndexedColors.GREY_50_PERCENT.getIndex());
		setBorder(style, 1, 2);
		style.setFont(monthFont);
		styles.put("header1_l", style);

		style = wb.createCellStyle();
		style.setAlignment(CellStyle.ALIGN_CENTER);
		style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		style.setFillForegroundColor(IndexedColors.GREY_50_PERCENT.getIndex());
		setBorder(style, 1, 3);
		style.setFont(monthFont);
		styles.put("header1_r", style);

		style = wb.createCellStyle();
		style.setAlignment(CellStyle.ALIGN_CENTER);
		style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		style.setFillForegroundColor(IndexedColors.GREY_50_PERCENT.getIndex());
		setBorder(style, 1, 4);
		style.setFont(monthFont);
		styles.put("header1_a", style);

		style = wb.createCellStyle();
		style.setAlignment(CellStyle.ALIGN_CENTER);
		style.setWrapText(true);
		style.setBorderRight(CellStyle.BORDER_THIN);
		style.setRightBorderColor(IndexedColors.BLACK.getIndex());
		style.setBorderLeft(CellStyle.BORDER_THIN);
		style.setLeftBorderColor(IndexedColors.BLACK.getIndex());
		style.setBorderTop(CellStyle.BORDER_THIN);
		style.setTopBorderColor(IndexedColors.BLACK.getIndex());
		style.setBorderBottom(CellStyle.BORDER_THIN);
		style.setBottomBorderColor(IndexedColors.BLACK.getIndex());
		styles.put("cell", style);

		style = wb.createCellStyle();
		style.setAlignment(CellStyle.ALIGN_CENTER);
		style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		style.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style.setDataFormat(wb.createDataFormat().getFormat("0.00"));
		styles.put("formula", style);

		style = wb.createCellStyle();
		style.setAlignment(CellStyle.ALIGN_CENTER);
		style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		style.setFillForegroundColor(IndexedColors.GREY_40_PERCENT.getIndex());
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style.setDataFormat(wb.createDataFormat().getFormat("0.00"));
		styles.put("formula_2", style);

		return styles;
	}
}