package oracle.sgt.util;

import java.util.*;
import java.io.*;
import javax.mail.*;
import javax.mail.internet.*;
import javax.activation.*;

public class sendFile {

	public static void run(String to, String from, String host, String myBody,
			String filename) {

		String msgText1 = "Hi There: \n  Please check attached excel file \nRegards \n-SGT Group\n";
		String subject = "[TransConsole]]File Health Report(Excel)";

		if (myBody != null && myBody.trim().length() > 0) {
			msgText1 = myBody;
			subject = "[TransConsole]File Health Parser Error";
		}
		// create some properties and get the default Session
		Properties props = System.getProperties();
		props.put("mail.smtp.host", host);

		Session session = Session.getInstance(props, null);

		try {
			System.out.println("Creating Message");
			// create a message
			MimeMessage msg = new MimeMessage(session);
			msg.setFrom(new InternetAddress(from));
			String[] _tos = to.split(",");
			for (String _t : _tos) {
				if (_t.indexOf("@") < 0) {
					msg.setRecipients(Message.RecipientType.TO, _t
							+ "@oracle.com");
				} else
					msg.setRecipients(Message.RecipientType.TO, _t);
			}
			// for (String _t : _tos) {
			// InternetAddress[] address = { new InternetAddress(_t) };
			// msg.setRecipients(Message.RecipientType.TO, address);
			// }
			msg.setSubject(subject);

			// create and fill the first message part
			MimeBodyPart mbp1 = new MimeBodyPart();
			mbp1.setText(msgText1);

			// create the Multipart and add its parts to it
			Multipart mp = new MimeMultipart();
			mp.addBodyPart(mbp1);
			if (filename != null) {
				// create the second message part
				MimeBodyPart mbp2 = new MimeBodyPart();
				// attach the file to the message
				FileDataSource fds = new FileDataSource(filename);
				
				mbp2.setDataHandler(new DataHandler(fds));
				mbp2.setFileName(fds.getName());
				mp.addBodyPart(mbp2);
			}
			// add the Multipart to the message
			msg.setContent(mp);

			// set the Date: header
			msg.setSentDate(new Date());

			// send the message
			Transport.send(msg);
			
			System.out.println("Send Mail");

		} catch (MessagingException mex) {
			mex.printStackTrace();
			Exception ex = null;
			if ((ex = mex.getNextException()) != null) {
				ex.printStackTrace();
			}
		}
		catch (Exception e){
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		//if (args.length != 5) {
		//	System.out
		//			.println("usage: java sendfile <to> <from> <smtp> <file>");
		//	System.exit(1);
		//}
		run("lu.x.guo@oracle.com", "lu.x.guo@oracle.com", "internal-mail-router.oracle.com", "XXXXX", null);
		System.out.println("Send email");

	}
}