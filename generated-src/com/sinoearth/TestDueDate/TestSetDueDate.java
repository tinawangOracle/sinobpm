package com.sinoearth.TestDueDate;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.TaskService;
import org.activiti.engine.task.Task;

public class TestSetDueDate {

	public static void main(String[] args) throws ParseException {
		
		ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
		TaskService taskService = processEngine.getTaskService();
		List<Task> tasks = taskService.createTaskQuery().taskAssignee("stu01").list();
		for(Task task : tasks)
			System.out.println("Task available for Stu01: " + task.getName());
		
		Task task = tasks.get(0);
		
		Date dueDate = new Date();
		SimpleDateFormat bartDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		dueDate = bartDateFormat.parse("2014-07-04");
		
		task.setDueDate(dueDate);
		
		taskService.saveTask(task);
	}

}
