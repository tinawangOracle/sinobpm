package com.sinoearth.activiti;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngineConfiguration;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.TaskService;
import org.activiti.engine.task.Attachment;
import org.activiti.engine.task.Task;

public class UploadScanCopy {

	public static void main(String[] args) throws FileNotFoundException {
		ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
		TaskService taskService = processEngine.getTaskService();
		List<Task> tasks = taskService.createTaskQuery().taskAssignee("stu01").list();
		for(Task task : tasks)
			System.out.println("Task available for Stu01: " + task.getName());
		
		Task task = tasks.get(0);
		
		Attachment pic = taskService.createAttachment("image/jpeg;jpg", task.getId(), task.getProcessInstanceId(), 
				"tianliang02", "" ,  new FileInputStream("C:\\Users\\luluguo\\Desktop\\1.jpg"));
		
		task.setAssignee("stu01");
		
		Map<String, Object> taskVariables = new HashMap<String,Object>();
		taskVariables.put("fileName", "L01");
		taskVariables.put("fileData", "Loughborough University");
		taskService.complete(task.getId(), taskVariables);
		System.out.println("Done!");
	}
}