package com.sinoearth.activiti;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngineConfiguration;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.task.Attachment;
import org.activiti.engine.task.Task;

public class TestGetAttachment {

	public static void main(String[] args) {
//		ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
//		TaskService taskService = processEngine.getTaskService();
//		List<Task> tasks = taskService.createTaskQuery().taskAssignee("peng").list();
//		for(Task task : tasks)
//			System.out.println("All Task available for peng: " + task.getName());
//		
//		Task task = tasks.get(0);
//		
//		System.out.println("Current Task available for peng: " + task.getName() + " : " + task.getId());
//		List<Attachment> attachments = taskService.getTaskAttachments(task.getId());
//		System.out.println(attachments.size());
//		System.out.println(task.getProcessInstanceId());
//		List<Attachment> attachments1 = taskService.getProcessInstanceAttachments(task.getProcessInstanceId());
//		System.out.println(attachments1.size());
//		for(Attachment attachment : attachments1)
//			System.out.println(attachment.getType()+":"+attachment.getName()+":"+attachment.getUserId()+":"+attachment.getUrl());
//		
//		
//		//taskService.
//		
//		System.out.println("Done!");
		ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
		TaskService taskService = processEngine.getTaskService();
		List<Task> tasks = taskService.createTaskQuery().list();
		RuntimeService rs = processEngine.getRuntimeService();
		for(Task task : tasks)
		{
			rs.deleteProcessInstance(task.getProcessInstanceId(), "");
			taskService.deleteTask(task.getId());
		}
		RepositoryService res = processEngine.getRepositoryService();
		List<Deployment> ds = res.createDeploymentQuery().list();
		for(Deployment d : ds)
			res.deleteDeployment(d.getId());
	}
}
