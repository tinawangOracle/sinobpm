package com.sinoearth.activiti;

import java.util.HashMap;
import java.util.Map;

import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.impl.util.ReflectUtil;
import org.activiti.engine.runtime.ProcessInstance;

public class ApplyProcessTemp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
		RepositoryService repositoryService = processEngine.getRepositoryService();
		repositoryService.createDeployment().addInputStream("applyProcessTemp.bpmn20.xml", 
				ReflectUtil.getResourceAsStream("diagrams/ApplyProcessTemp.bpmn"))
				.deploy();
		
		Map<String, Object> variables = new HashMap<String, Object>();
		variables.put("applyUserId","stu01");
		variables.put("stuName", "guolu");
		variables.put("stuEmail", "lu.x.guo@oracle.com");
		RuntimeService runtimeService = processEngine.getRuntimeService();
		ProcessInstance processInstance = runtimeService
		.startProcessInstanceByKey("applyProcessTemp", variables);
		System.out.println("Done!");
	}

}
