package com.sinoearth.activiti;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngineConfiguration;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.TaskService;
import org.activiti.engine.task.Attachment;
import org.activiti.engine.task.Task;

public class ChooseRegUn {

	public static void main(String[] args) {
		ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
		TaskService taskService = processEngine.getTaskService();
		List<Task> tasks = taskService.createTaskQuery().taskAssignee("stu01").list();
		for(Task task : tasks)
			System.out.println("Task available for Stu01: " + task.getName());
		
		Task task = tasks.get(0);
		
		
		
		Map<String, Object> taskVariables = new HashMap<String,Object>();
		taskVariables.put("unID", "L01");
		taskVariables.put("unName", "Loughborough University");
		taskService.complete(task.getId(), taskVariables);
		System.out.println("Done!");
	}
}
