package com.sinoearth.tool;


import java.io.File;
import java.io.IOException;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;


public class HibernateTools {
	private static Configuration configuration;
	private static SessionFactory sessionFactory;
	static {
		// 只会执行一次，在类加载时执行
		
//		File f = new File("/hiberanteExample.xml");
//		try {
//			f.createNewFile();
//			System.out.println("done");
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
		configuration = new Configuration().configure();
		sessionFactory = configuration.buildSessionFactory();
	}

	// 提供Session 会话对象
	public static Session openSession() {
		return sessionFactory.openSession();
	}
	
	public static void closeSession() {
		sessionFactory.openSession().close();
	}
}