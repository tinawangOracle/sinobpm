package com.sinoearth.bpm.domain;

// Generated 2014-7-7 15:18:15 by Hibernate Tools 3.4.0.CR1

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * ActIdInfo generated by hbm2java
 */
@Entity
@Table(name = "act_id_info", uniqueConstraints = @UniqueConstraint(columnNames = {
		"USER_ID_", "KEY_" }))
public class ActIdInfo implements java.io.Serializable {

	private Integer id;
	private ActIdUser actIdUser;
	private Integer rev;
	private String type;
	private String key;
	private String value;
	private byte[] password;
	private String parentId;

	public ActIdInfo() {
	}

	public ActIdInfo(Integer id) {
		this.id = id;
	}

	public ActIdInfo(ActIdUser actIdUser, Integer rev, String type, String key,
			String value, byte[] password, String parentId) {
		this.actIdUser = actIdUser;
		this.rev = rev;
		this.type = type;
		this.key = key;
		this.value = value;
		this.password = password;
		this.parentId = parentId;
	}

	public ActIdInfo(Integer id, ActIdUser actIdUser, Integer rev, String type,
			String key, String value, byte[] password, String parentId) {
		this.id = id;
		this.actIdUser = actIdUser;
		this.rev = rev;
		this.type = type;
		this.key = key;
		this.value = value;
		this.password = password;
		this.parentId = parentId;
	}

	@Id
	@Column(name = "ID_", unique = true, nullable = false, length = 64)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "USER_ID_")
	public ActIdUser getActIdUser() {
		return this.actIdUser;
	}

	public void setActIdUser(ActIdUser actIdUser) {
		this.actIdUser = actIdUser;
	}

	@Column(name = "REV_")
	public Integer getRev() {
		return this.rev;
	}

	public void setRev(Integer rev) {
		this.rev = rev;
	}

	@Column(name = "TYPE_", length = 64)
	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Column(name = "KEY_")
	public String getKey() {
		return this.key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	@Column(name = "VALUE_")
	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Column(name = "PASSWORD_")
	public byte[] getPassword() {
		return this.password;
	}

	public void setPassword(byte[] password) {
		this.password = password;
	}

	@Column(name = "PARENT_ID_")
	public String getParentId() {
		return this.parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

}
