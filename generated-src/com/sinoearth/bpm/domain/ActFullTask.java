package com.sinoearth.bpm.domain;

import java.util.List;
import java.util.Set;

public class ActFullTask {
	private ActRuTask task;
	private Set<ActIdUser> users;
	private ActRuTaskReminder taskReminder;
	
	public ActRuTask getTask() {
		return task;
	}
	public void setTask(ActRuTask task) {
		this.task = task;
	}
	public Set<ActIdUser> getUsers() {
		return users;
	}
	public void setUsers(Set<ActIdUser> users) {
		this.users = users;
	}
	public ActRuTaskReminder getTaskReminder() {
		return taskReminder;
	}
	public void setTaskReminder(ActRuTaskReminder taskReminder) {
		this.taskReminder = taskReminder;
	}
	
	@Override
	public String toString() {
		return "ActFullTask [task=" + task + ", users=" + users
				+ ", taskReminder=" + taskReminder + "]";
	}
	
	
	
}
