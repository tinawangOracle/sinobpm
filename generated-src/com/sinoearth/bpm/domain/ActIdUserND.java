package com.sinoearth.bpm.domain;

// Generated 2014-7-7 15:18:15 by Hibernate Tools 3.4.0.CR1

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * ActIdUser generated by hbm2java
 */
@Entity
@Table(name = "act_id_user")
public class ActIdUserND implements java.io.Serializable {

	private String id;
	private Integer rev;
	private String first;
	private String last;
	private String email;
	private String pwd;
	private String pictureId;

	public ActIdUserND() {
	}

	public ActIdUserND(String id) {
		this.id = id;
	}

	public ActIdUserND(String id, Integer rev, String first, String last,
			String email, String pwd, String pictureId) {
		this.id = id;
		this.rev = rev;
		this.first = first;
		this.last = last;
		this.email = email;
		this.pwd = pwd;
		this.pictureId = pictureId;

	}

	@Id
	@Column(name = "ID_", unique = true, nullable = false, length = 64)
	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Column(name = "REV_")
	public Integer getRev() {
		return this.rev;
	}

	public void setRev(Integer rev) {
		this.rev = rev;
	}

	@Column(name = "FIRST_")
	public String getFirst() {
		return this.first;
	}

	public void setFirst(String first) {
		this.first = first;
	}

	@Column(name = "LAST_")
	public String getLast() {
		return this.last;
	}

	public void setLast(String last) {
		this.last = last;
	}

	@Column(name = "EMAIL_")
	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name = "PWD_")
	public String getPwd() {
		return this.pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	@Column(name = "PICTURE_ID_", length = 64)
	public String getPictureId() {
		return this.pictureId;
	}

	public void setPictureId(String pictureId) {
		this.pictureId = pictureId;
	}

}
