package com.sinoearth.notify;

import java.util.Date;

public class MessageBean {

	//needs task_id, task_name, task_due_date, task_create_date, user_id, user_firstname, user_lastname, user_email
	private String task_id;
	private String task_name;
	private Date task_create_date;
	private Date task_due_date;
	private String user_id;
	private String user_name;
	private String user_email;
	public String getTask_id() {
		return task_id;
	}
	public void setTask_id(String task_id) {
		this.task_id = task_id;
	}
	public String getTask_name() {
		return task_name;
	}
	public void setTask_name(String task_name) {
		this.task_name = task_name;
	}
	public Date getTask_create_date() {
		return task_create_date;
	}
	public void setTask_create_date(Date task_create_date) {
		this.task_create_date = task_create_date;
	}
	public Date getTask_due_date() {
		return task_due_date;
	}
	public void setTask_due_date(Date task_due_date) {
		this.task_due_date = task_due_date;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	public String getUser_email() {
		return user_email;
	}
	public void setUser_email(String user_email) {
		this.user_email = user_email;
	}
	
	@Override
	public String toString() {
		return "MessageBean [task_id=" + task_id + ", task_name=" + task_name
				+ ", task_create_date=" + task_create_date + ", task_due_date="
				+ task_due_date + ", user_id=" + user_id + ", user_name="
				+ user_name + ", user_email=" + user_email + "]";
	}
	
	
}
